T3 is automated unit-testing tool for Java classes. At the top-level T3 consists of
two tools: the generator tool, and the replay tool. The generator is used
to generate a test suite (and in doing so also test the target class).
The generated suite can be saved, and replayed again using the replay tool.

You can also download a separate front-end called T3i featuring  interactive
testing with Groovy, ability to combine and filter test suites, and to query test
suites, e.g. to check if some correctness properties hold.

** To Build T3

T3 requires Java 8. We build with ant. Before running ant, set JAVA_HOME 
to the jdk of Java8. E.g. with bash-shell we would do: 

   export JAVA_HOME=/cygdrive/c/apps/Java/jdk1.8
  
run "ant jar" to build.
  
** To Run

Generator: java -cp T3.jar Sequenic.T3.T3Cmd [option]* targetclass
Replay   : java -co T3.jar Sequenic.T3.ReplayCmd [option]* suitefile

For both, --help will list available options.

** Manual

in /docs