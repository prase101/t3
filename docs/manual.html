<html>
<head>
<link rel="stylesheet" type="text/css" href="docs.css" />
<!--
<script type="text/javascript" src="googleprettifier/run_prettify.js"></script>
-->
<script type="text/javascript" src="./jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="./toc.js"></script>

</head>
<body>


<h1>T3 Manual</h1>

<table>
<tr>
<td>
<img src="logo.jpg">
</td>
<td><i>
T3 is a tool to automatically test a Java class.
<br>Author: Wishnu Prasetya
<br>License: GPL v3</i>
</td>
</tr>
</table>

<div id="toc"></div>

T3 is a light weight testing tool to automatically test Java classes. Given
a target class CUT, T3 randomly generates a set of test-sequences against CUT. Each
sequence starts in principle with the creation of an instance of CUT followed
by calls to the object�s methods, or updates to its fields. T3 can generate a
large amount of such test sequences to trigger faulty behavior.
A convenient way to specify custom values and objects
generators is supported.

<p>T3 is the successor of T2. The main idea is still the same as T2, however
the engine has been completely revamped. Internally, T3 makes a lot of use
of Java 8's closures, to make its generators infrastructure more
customizable (this is a developer's feature though).</p>

<!-- ================================================================= -->
<h2>A simple example.</h2>
<!-- ================================================================= -->

Here is a simple class implementing a sorted list of integers (the example is also
available in T3's source tree, in the package Sequenic.T3.Examples). The list is sorted
in ascending order. It has a method to insert an element, and a method to retrieve
the greatest element from the list.

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
package Examples;

import java.util.LinkedList;

// Sorted list of integers; in ascending order.
public class SimpleIntSortedList {

    private LinkedList<Integer> s;
    private Integer max;

    public SimpleIntSortedList() { s = new LinkedList<Integer>(); }

    public void insert(Integer x) {
    	assert x!=null : "PRE";
        int i = 0;
        for (Integer y : s) {
            if (y > x) break;
            i++;
        }
        s.add(i, x);
        // bug: should be x > max
        if (max == null || x < max) max = x;
    }

    // Retrieve the greatest element from the list, if it is not empty.
    public Integer get() {
    	assert !s.isEmpty() : "PRE";
        Integer x = max;
        s.remove(max);
        if (s.isEmpty()) max = null ;
        else max = s.getLast() ;
        assert s.isEmpty() || x >= s.getLast() : "POST";
        return x;
    }

    // a class invariant
    private boolean classinv__() {
        return s.isEmpty() || s.contains(max);
    }
}
</pre>
</blockquote>

The class already contains some assertions to at least partially express
its correctness. Notice that some assertions are marked
by "PRE", and some by "POST". Those marked by "PRE" are treated as pre-conditions
by T3. A test case that violates a pre-condition of the class is not
considered as an error. Instead, it means that the test is not a valid or
relevant. Assertions not marked by "PRE" are treated as correctness requirement;
so violating them counts as errors.

<p>To run T3 to test this class, we can do it like this:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
java -ea -cp T3.jar Sequenic.T3.T3Cmd --showexc  Examples.SimpleIntSortedList
</pre>
</blockquote>

<ul>
<li>Specify the full name of the target class that we want to test. In the
above example it is: Examples.SimpleIntSortedList. Preceeding it, we can specify
a list of options; above we only have one option, namely: --showexc</li>

<li>The target class should be a concrete class (T3 cannot target an interface
or an abstract class).

<li>The --showexc option will cause T3 to pretty-print the first test-sequence
that causes an exception.</li>

<li>Enable Java assertions with the -ea option.</li>

<li>As said, assertions that are to be interpreted as pre-conditions should be marked
by the string "PRE", as in the above example. Test-sequences that violate
pre-conditions are not valid test-sequences, and are dropped. Thrown
IllegaArgumentException is treated the same as violating a pre-condition.
Violating other assertions counts as an error.</li>

<li>T3 also reports other kind of uncaught exceptions.</li>

<li>A class invariant can be specified in a private method named classinv__()
that returns a boolean, as in the above example. This method is automatically
called by T3 after every test-step in every test-sequence it generates.
If the method returns false, or when it throws an exception, it counts as an
error.</li>

</ul>

The run will produce a summary like the one below:

<blockquote>
<pre  class="prettyprint">
Suite size : 36            -- T3 generated a set of 302 test-sequences
Average length : 8.972222  -- the average length of sequences, in number of steps
Violating  : 10            -- error found
Invalid    : 1             -- number of invalid test-sequences
Failing    : 0             -- number of failing/broken test-sequences
Runtime    : 9 ms
</pre>
</blockquote>

The above statistic says that T3 generates a suite of 36 test-sequences,
of which 1 is invalid and 10 are violating. None of the sequences are
failing/broken. A test-sequence fails if it for some reasons fails
to completely execute. T3 actually runs the generated sequences twice.
The first time is when the sequences are generated, then again to collect
the above statistics. If the SUT is non-deterministic, a previously non-failing
test-sequence can still fails in the second run.

<p>
By default T3 will not show any detail of the found violation.
The --showexc will however pretty-print the first violating
test-sequence. An example of a violation report is shown below,
showing the post-condition of the method get being violated.

<p>Note that the root cause of the error is not necessarily in
the method get itself. In this case, it is caused by a mistake in the method insert, which incorrectly
updates the internal variable max. As a result, the method get may not return
the greatest element, which is detected by the post-condition assertion in the
method. The violating sequence is essentially:

<ol>
<li>Create an empty list.</li>
<li>Call insert(0).</li>
<li>Call insert(8), here max was incorrectly updated.</li>
<li>Call get(), its assertion detects the error.</li>
</ol>

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
** [step 0] CON Sequenic.T3.Examples.SimpleIntSortedList() -- calling constructor
   -- returned value:
    (Sequenic.T3.Examples.SimpleIntSortedList) @ 0
    s (LinkedList)  @  1
    max null
** [step 1] CALL insert on REF 0                           -- calling insert(0)
   with
   (0)
   -- state of the receiver object:                        -- the state of the list is shown here:
    (Sequenic.T3.Examples.SimpleIntSortedList) @ 0
    s (LinkedList)  @  1
         [0] (Integer) : 0
    max (Integer) : 0
** [step 2] CALL insert on REF 0                           -- calling insert(8)
   with
   (8)
   -- state of the receiver object:
    (Sequenic.T3.Examples.SimpleIntSortedList) @ 0
    s (LinkedList)  @  1
         [0] (Integer) : 0
         [1] (Integer) : 8
    max (Integer) : 0                                      -- look, max has an incorrect value
** [step 3] CALL get on REF 0                              -- calling get()
   with
   ()
   >> VIOLATION!                                           -- a violation is reported
   -- state of the receiver object:
    (Sequenic.T3.Examples.SimpleIntSortedList) @ 0
    s (LinkedList)  @  1
         [0] (Integer) : 8
    max (Integer) : 8   -- thrown exception:
java.lang.AssertionError: POST
	at Sequenic.T3.Examples.SimpleIntSortedList.get(SimpleIntSortedList.java:62)
    ...
</pre>
</blockquote>

<!-- ================================================================= -->
<h2>T3's pair-wise algorithm</h2>
<!-- ================================================================= -->

Let CUT be the target class. It should be a concrete class (not an
interface nor an abstract class). When testing CUT, T3 pretends
to be a fictious client class T from the same package as CUT.
CUT's <i>testing scope</i> consists of all members of CUT which are visible
from T; so:

<ol>
<li>All non-private members declared by CUT is in the scope.</li>
<li>Inherited, non-private members are also in the scope. In the case
when a member is overridden or shadowed, only the lowest one, towards CUT,
will be in the scope.</li>
<li>Fields inherited from Object are always excluded.</li>
<li>Static fields are excluded from the scope.</li>
</ol>

Only members from the scope will be invoked (or updated, if it is a field)
during the test by T3.

<h3>ADT and non-ADT suite</h3>

T3 can generate so-called ADT and non-ADT test suites (the default is both,
else use the -adtOption).

<p>
Each test-sequence in an ADT-suite starts with the creation of
an instance of CUT, and every step along the sequence affects or
inspects this <i>target instance/object</i>. Obviously this requires
that CUT can be instantiated. This can be done by calling a constructor
of CUT, if there is one. T3 also looks for <i>creator methods</i>,
which are static methods of CUT, with CUT as return type, and does not have
a parameter x of type CUT.</p>

<p>Sequences in a non-ADT-suite focus on testing static methods
and fields of CUT.
</p>

<h4>ADT-suite.</h4>

<p>An ADT-suite consists of sequences of these forms:

    <ol>
	<li>c++&tau;</li>
	<li>c++&sigma;++[nm]</li>
	<li>c++&sigma;++[m1,m2]++&tau;</li>
    </ol>

c is a call to a constructor or a creator method of CUT.
The instance o of CUT it creates is called <i>target object</i>. Every
subsequent step along a test-sequence always operate on its target object, either by
taking it as the receiver, or as a parameter. So, either as o.step(x),
or as p.step(o).

<p>nm is a non-mutator, and m1 and m2 are
mutators from CUT's testing scope; &sigma; and &tau; are sequences
of steps. Every sequence has a primary goal, or simply <i>goal</i>,
to test. These are respectively c, [nm], and [m1,m2] in the
first, second, and third forms.

<p>A <i>mutator</i> is a method that may cause side effect on the target object,
or an update to a field. T3 does not currently implement a precise algorithm
to identify which methods are mutators. All methods whose names do not
start with "get" or "is" are assumed to be mutators.

<p>In the second and third forms, &sigma; is a sequence of calls to mutators from
CUT's scope. Its role is to bring the sequence's target object to a particular
state, before the sequence tests its goal.

<p>The suffix &tau; consists of calls to non-mutators from CUT's testing
scope. These are methods that are side effect-free. Such a method is
typically used to observe into some part of the target object's state.
The idea of &tau; is to trigger various observations on the possible side
effect of the sequence's goal.

<p>Before generating a test suite, T3 first calculates all possible test
goals (of the form c, [nm], or [m1,m2] as above) induced by CUT's testing
scope. For every goal, T3 estimates its 'complexity', which is the
estimated number of control flow paths through the goal. Suppose this number
is N; T3 then generates ms*N number of random test-sequences (of the
above form) containing the goal. The default value of ms is 4.0.

<p>Note that by quantifying over pairs of mutators [m1,m2] T3 trigger
to for interactions between mutators. Then, by
generating multiple sequences per goal, we quantify the goal
over different states of the target object, as well as quantifying over
different observations on the effect of the goal.

<p>Currently there is a limit of 1000 [m1,m2] pairs. If this number would be exceeded,
then T3 only generate sequences for these kinds of pairs:

    <blockquote>
	c++&sigma;++[m1]++&tau;
    </blockquote>



<h4>Non-ADT-suite</h4>

A non-ADT suite consists of sequences of the form &sigma;++[m1,m2]++&tau;.
In particular, it has no creation step. All steps calls to the static methods
in CUT's testing scope. For the rest, it is the same idea as in the ADT testing.

<h2>T3 options.</h2>

General usage:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
java -ea -cp &lt;T3jar&gt;&lt;:other path&gt;*  Sequenic.T3.T3Cmd  &lt;options&gt;* &lt;targetclass&gt;
</pre>
</blockquote>

Options:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
 -adt,--adtOption &lt;boolean&gt;         true to create only ADT suite, false for only non-ADT. Both if
                                    left unset (default).
 -co,--collection &lt;int&gt;             Maximum size of collections and arrays that T3 generates.
                                    Default 3.
 -core,--core &lt;int&gt;                 To specify the number of cores.
 -cvg,--customvalgen &lt;class-name&gt;   A class specifying a custom values generator.
 -d,--savedir &lt;dir&gt;                 Save the generated suite in this directory.
 -dd,--dropDuplicates               If set, will drop duplicate sequences from the generated suite.
 -dg,--dropgood                     If set, non-violating traces will be dropped.
 -fup,--fieldupdate &lt;float&gt;         The probability to do a field update. Default is 0.1.
 -help,--help                       Print this message.
 -lf,--logfile &lt;file&gt;               To echo messages to a log-file.
 -ms,--msamples &lt;float&gt;             Multiplier on number of samples to collect per goal. Default
                                    4.0.
 -norc,--nooracle                   If set, T3 will not inject oracles in when ran in the regression
                                    mode.
 -obn,--objectsNesting &lt;int&gt;        Maximum object-depth that T3 generates. Default 4.
 -pl,--prefixlength &lt;int&gt;           Maximum prefix length of each sequence. Default 8.
 -reg,--regressionmode              If set, T3 will generate a suite for regression test; the target
                                    class is assumed correct.
 -rs,--reportStream &lt;file&gt;          To send report to a file.
 -rv,--reportVerbosity &lt;int&gt;        Verbosity level of the report.
 -sd,--scandir &lt;dir&gt;                Directory to scan for classes.
 -sex,--showexc                     When set, will show the first exception throwing execution.
 -sl,--suffixlength &lt;int&gt;           Maximum suffix length of each sequence. Default 3.
 -sqr,--seqretry &lt;int&gt;              Maximum number that each sequence is retried. Default 5.
 -ss,--splitsuite &lt;int&gt;             Split the generated suite to this number of smaller suites.
                                    Default 1.
 -str,--stepretry &lt;int&gt;             Maximum number that each step is retried. Default 30.
 -tfop,--tfop                       When set, we assume to test from a different package.
 -to,--timeout &lt;int&gt;                When set, specifies time out in ms.
 -tps,--tracePrintStream &lt;file&gt;     To send sequence-prints to a file.
 -vp,--variprefix                   If set, T3 will generate variable legth prefixes, up to the
                                    specified maximum.
</pre>
</blockquote>



<!-- ================================================================= -->
<h2>Writing custom value generators.</h2>
<!-- ================================================================= -->

Consider the following class representing persons (this example is also available in
T3's source code tree, in the package Sequenic.T3.Examples.CustomInputGenerator). The constructor
requires two strings representing a person's name and her email, and
integer representing her age, and another integer representing some
code. Say, that valid codes are -1,0,1. If we just let T3 use its
default value generator to randomly generate them, most of the time
we will get uninteresting values, or even invalid values.

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
package Examples;

public class Person {

  private String name ;
  private String email ;
  private int age;
  private int code ;

  public Person(String name, String email, int age, int code) {
    this.name = name ;
    this.email  = email ;
    this.age = age ;
    this.code = code ;
  }
  ...
}
</pre>
</blockquote>

<p>We can write a custom value generator, here is an example:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
package Examples;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.*;
import static Sequenic.T3.Generator.GenCombinators.* ;
import static Sequenic.T3.Generator.Value.ValueMetaGeneratorsCombinators.* ;

public class CustomInputGenerator {

  public static Generator<PARAM,STEP> myvalgen =
    FirstOf(Integer(OneOf(17,18,60)).If(hasParamName("age")),
            Integer(OneOf(-1,0,1)).If(hasParamName("code")),
            String(OneOf("anna","bob")).If(hasParamName("name")),
            String(OneOf("anna@gmail.com","k1@ppp")).If(hasParamName("email"))
    ) ;
}
</pre>
</blockquote>

For any parameter (of any constructor or method) of type int or Integer, whose
name is "age", the generator will randomly select from the list (17,18,60).
If the parameter is named "code", it will randomly select from (-1,0,1).
For other integer-types parameter, T3 will use its default generator.

<p>For the above to work, you should <b>compile the target class with with the
-parameters option enabled.</b></p>

<p>You also need to inform T3 where it can find the custom generator, using the
--customvalgen option. T3 assumed the testgenerator is bound to a static field
named myvalgen, as in the above example.</p>

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
java -ea -cp T3.jar Sequenic.T3.T3Cmd --customvalgen Examples.CustomInputGenerator Sequenic.T3.Examples.Friends.Person
</pre>
</blockquote>

<h3>Controlling the distribution of the generated values.</h3>

The function OneOf(x1,x2,...) is exported by some class in T3.
It is a selector that randomly chooses one of
its parameter. This function is overloaded to range over various primitive-typed and
string parameters. It used Java's built-in random generator, which has
uniform distribution. There are two ways to influence the distrubution.
The easiest way is by duplicating values, as in:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
OneOf(-1,0,0,1)
</pre>
</blockquote>

which would have twice more chance to select a 0 over -1 and 1.

<p>We can also define our own random selector; it needs to be a method that returns
an instance of Supplier&lt;T&gt;, where T is the type of value that we want
to produce. A Supplier (java.util.function.Supplier) is a functional interface
(a Java 8 new concept)
representing functions that takes no argument () and returns a value of type T.
For example, to select from a list of integers, based on Gaussian distribution
on the indices of the choices, we can define this:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
import java.util.function.Supplier;
...

static Random rnd = new java.util.Random(seed) ;

static Supplier&lt;Integer&gt; GaussianOneOf(int ...s) {
   return () -> {
      int k = rnd.nextGaussian() * s.length ;
      return s[k] ;
   } ;
}
</pre>
</blockquote>

Then we can use it, e.g. as in:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
FirstOf(Integer(OneOf(17,18,60)).If(hasParamName("age")),
        Integer(GaussianOneOf(-1,0,1)).If(hasParamName("code")),
        ...
       )
</pre>
</blockquote>


<h3>Object generator.</h3>

<p>We can also write a generator that produces objects. Suppose the class
Person above has another constructor, as shown below:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
public Person(String name, Email email, int age, int code) { ...  }
</pre>
</blockquote>

Notice that it now requires an instance of Email as a parameter. When
this parameter is required, T3 will randomly generate one.
However, sometimes we need to have more control on the kind of
instances we want to pass as parameters. In other words, we want to
have a custom object generator to generate instances of Email.
A custom object generator must be a static
method of this signature: O generator(int k),
where O is the class whose instances we want to generate.
Here is an example, of a custom Email generator:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
static Email genEmail(int k) {
  switch (k) {
   case 0 : return new Email("root") ;
   case 1 : return new Email("annaz") ;
   default : return new Email("guest") ;
  }
}
</pre>
</blockquote>

Now we need to adjust our custom value generator, to also include the above generator:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
public static Generator<PARAM,STEP> myvalgen =
  FirstOf(Integer(OneOf(17,18,60)).If(hasParamName("age")),
          Integer(OneOf(-1,0,1)).If(hasParamName("code")),
          String(OneOf("anna","bob")).If(hasParamName("name")),
          String(OneOf("anna@gmail.com","k1@ppp")).If(hasParamName("email")),
          Object(CustomInputGenerator.class,"genEmail",OneOf(0,1,2)).If(hasClass(Email.class).and(hasParamName("email")))
    ) ;
}
</pre>
</blockquote>

The last line says, to use the genEmail generator, and passing to it a
randomly chosen integer, taken from the list (0,1,2). The "If"-condition
specifies that generator is only applied on parameters of type
Email, and whose paratemers' name is "email".

<p>To run T3 using the above custom generator, we call it e.g. as below (assuming the generator
is put in the class Examples.CustomInputGenerator). Notice the --customvalgen option.

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
java -ea -cp T3.jar Sequenic.T3.T3Cmd --customvalgen Examples.CustomInputGenerator Examples.Person
</pre>
</blockquote>

<!-- ================================================================= -->
<h2>Saving and replaying test suites.</h2>
<!-- ================================================================= -->

By default T3 only generates the test suite internally. It does not save it.
However, with the --savedir dir option we can save the suite, and later we can
replay it. The suite(s) will be saved in the specified directory. ADT and non-ADT
suites are named, respectively:

<blockquote>
ADT_Cname_timestamp.tr <br>
nonADT_Cname_timestamp.tr
</blockquote>

where Cname is the fully qualified name.


<p>Saved test suites can be replayed. For example, suppose T3 has saved a suite
in a file named Examples.SimpleIntSortedList__1393751293489.tr. To replay it we do:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
java -cp T3.jar -sx Sequenic.T3.ReplayCmd Examples.SimpleIntSortedList__1393751293489.tr
</pre>
</blockquote>

This causes the test suite to be replayed. The replay will stop if a test sequence
throws an exception. The -sx option will cause this sequence to be printed. Alternatively,
we can use the --runall option to run the whole suite. At the end, statistics will be
printed, e.g.:

<blockquote>
<pre  class="prettyprint">
** CUT               : Sequenic.T3.Examples.SimpleIntSortedList
** Suite size        : 36        -- the number of test-sequences in this suite
** Avrg. seq. length : 9.027778  -- the average length of sequences, in number of steps
** Executed   : 6                -- the number of sequences executed before the replay stops
** Violating  : 1                -- number of violating sequences that were replayed
** Invalid    : 0                -- number of invalid sequences that were replayed
** Failing    : 0                -- number of sequences that were replayed and failed
** Runtime    : 11 ms
</pre>
</blockquote>

The general syntax to replay is:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
java -cp &lt;T3.jar&gt;&lt;:otherpath&gt;*  Sequenic.T3.ReplayCmd  [options]* &lt;file or dir&gt
</pre>
</blockquote>

Options:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
 -b,--bulk &lt;string&gt;               If specified the target is a directory; all suite-files there whose name match with the regex represented by this string are replayed.**
 -help,--help                     Print this message.
 -lf,--logfile &lt;file&gt;             To echo messages to a log-file.
 -ra,--runall                     If set, all sequences will be replayed; exception will not be rethrown.
 -rm,--regressionmode             If set, and runall is false, only thrown OracleError will be rethrown.
 -rs,--reportStream &lt;file&gt;        To send report to a file.
 -sd,--showdepth &lt;int&gt;            If set, when on object is printed, it is printed up to this depth. Defailt=3.
 -sl,--showlength &lt;int&gt;           If set will print each step in the suffix of each replayed sequence, up to this length. Default=10.
 -sx,--showXexec                  If set, then execution that throws an exception will be printed.
 -tps,--tracePrintStream &lt;file&gt;   To send sequence-prints to a file.
</pre>
</blockquote>

** Note: for the syntax of the Regex in the -b option, check the Javadoc of <tt>java.util.regex.Pattern</tt>.

<h3>Using T3 to generate regression test-suite.</h3>

A test suite generated by T3 can always be used to do regression testing.
However, the suite that we get by default will lack <i>negative tests</i>.
When given invalid inputs, a program should ideally reject those inputs,
rather than just consuming them and potentially producing harmful behavior.
A negative test is aimed at testing this ability. To make T3 to also generate
negative tests, invoke it with --regressionmode option enabled.
A test-sequence that violations to a pre-conditon ("PRE"-decorated assertion)
or throws an IllegalArgumentException will no longer be considered as invalid.

<!-- ================================================================= -->
<h2>Calling T3 from APIs.</h2>
<!-- ================================================================= -->

T3 can also be called from APIs, so that e.g. you can call it from a
JUnit test class. The simplest way is to simply call its main-method, and
passing options to it as a string. For example:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
Sequenic.T3.T3Cmd.main("-d . Examples.SimpleIntSortedList") ;
</pre>
</blockquote>

This will generate test sequences for Examples.SimpleIntSortedList, and save them
as suites in the current directory. Typically two suites will be generated:
ADT_Examples.SimpleIntSortedList_timestap.tr and
nonADT_Examples.SimpleIntSortedList_timestap.tr

<p>To replay them we can call (using a regular expression to match the file-names):

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
Sequenic.T3.ReplayCmd.main("-sx -b .*SortedList.* . ") ;
</pre>
</blockquote>

<p>Note that T3Cmd is used to generate test suites. If it finds a violation, it does not
re-throw it. It saves the violating sequence in a suite, and proceeds with the generation
of the next sequence. ReplayCmd can be used to replay a saved suite, and re-throw the
first violation it finds. So, if you want to call T3 from a Junit test method, you
probably want to arrange it like this:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
@Test
public void testC() {
   if(test-cases for C exists)  {
      Sequenic.T3.ReplayCmd.main("-sx -b ADT_C.* . ") ;
      Sequenic.T3.ReplayCmd.main("-sx -b nonADT_C.* . ") ;
   }
   else Sequenic.T3.T3Cmd.main("-d . C") ;
}
</pre>
</blockquote>


<h3>Sequenic.T3.T3SuiteGenAPI</h3>

This class provides the logical API underneath T3Cmd. It allows you to directly control T3's
configuration and to collect the generated test suites. Here is an example:


<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
useT3() throws Exception {
  Config config = new Config() ;
  // setting T3 parameters:
  config.CUT = Person.class ;  // target class to test
  config.setDirsToClasses("./bin") ;
  T3SuiteGenAPI tester = new T3SuiteGenAPI(null,config) ;  // null means we use T3 custom value generator
  // generate suites:
  List<SUITE> SS = tester.suites(true,".") ;
  // now, what do you want to do with SS? As an example:
  tester.reportCoverage(SS.get(0));
}
</pre>
</blockquote>



<h3>Custom sequence generator.</h3>

With the -customvalgen option we can specify a custom value generator to be used by T3.
However, it still uses its default sequence generator. If it is needed to specify
a custom sequence generator, we can use the class Sequenic.T3.CustomSuiteGenAPI.

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint">
customSuiteGen() throws Exception {
   	 Config config = new Config() ;
   	 config.CUT = Person.class ;
     config.setDirsToClasses("./bin") ;
     // a flexible generator:
     CustomSuiteGenAPI sg = new CustomSuiteGenAPI(null,config) ;  // null means we use T3 custom value generator
     sg.scope.configureForADTtesting();

	 // now generate a suite of 500 sequences, using the specified sequence generator:
     SUITE S = sg.suite(SequenceWhile(
        		r -> ! r.isFail(),
        		sg.create(),
        		sg.mutatorsSegment(10),
        		sg.instrument(),
        		sg.method("friend"),
        		sg.method("unfriend"),
        		sg.nonMutatorsSegment(10)
        		),
        		500
        		) ;

      sg.reportCoverage(S) ;

}
</pre>
</blockquote>

The generator above will generate a suite targetting the class Person. This
suite will consist of maximum 500 sequences. Each sequence is generated
with the above specified sequence-generator. It says that each sequence is to be like this:

    <ol>
	<li>The sequence starts with the creation of a target object (an instance of Person),
	<li>followed by a segment of up to 10 calls to mutator-methods or field updates,
	<li>followed by a call to friend then unfriend,
	<li>followed by a segment of up to 10 calls to non-mutator-methods.
	</ol>

<script type="text/javascript">
  $('#toc').toc({
    'selectors': 'h2,h3'}) ;
</script>
</body>
</html>
