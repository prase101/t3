---------------------------------------------------------------------
T3 LICENSE
---------------------------------------------------------------------

This copyright and license statements apply to the entire T3 software.

Copyright 2013 Wishnu Prasetya.

T3 is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) version 3, as
published by the Free Software Foundation.

T3 is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

A copy of the GNU General Public License (GPL) can be found in the
file gpl_license.txt, which is included in this software. If it is
missing, see http://www.gnu.org/licenses.

---------------------------------------------------------------------

Third Party Content

T3 includes the following third party binary items.

* Commons CLI Package: copyright (c) 2001-2009 The Apache Software Foundation.

    This item is subject to the Apache License version 2.0, January 2004. 
    See http://www.apache.org/licenses/

* Jacoco: copyright (c) 2009, 2015 Mountainminds GmbH & Co. KG and Contributors.

    This item is subject to the Eclipse Public License Version 1.0 ("EPL"). See
    http://www.eclipse.org/legal/epl-v10.html.

* ASM: copyright (c) 2000-2011 INRIA, France Telecom. All rights reserved.
    
    This item is subject to the following license:

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
    1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holders nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.
 
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
    THE POSSIBILITY OF SUCH DAMAGE.
    
    
T3 support tools include the following third party binary items.

* Daikon: copyright (C) 1998-2008 Massachusetts Institute of Technology; 
copyright (C) 2008-2014 University of Washington. This item is subject to the 
license: https://plse.cs.washington.edu/daikon/download/doc/daikon.html#License

* Reflections, this item is subject to the WTFPL license.
* Javassist, this item is subject to the MOZILLA PUBLIC LICENSE Version 1.1.
* Guava, this item is subject to the Apache License 2.0.
* JDT, this item is subject to the Eclipse Public License 1.0.
