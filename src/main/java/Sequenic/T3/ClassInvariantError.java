/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

/**
 * To represent a violation to a class invariant.
 */
public class ClassInvariantError extends AssertionError {

	private static final long serialVersionUID = 1L;

	public ClassInvariantError(String info) {
        super(info) ;
    }
	
	public ClassInvariantError(String info, Throwable cause) {
        super(info,cause) ;
    }
	
    public ClassInvariantError(Throwable cause) {
        super(cause) ;
    }
}
