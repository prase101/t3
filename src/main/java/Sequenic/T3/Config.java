/*
 * Copyright 2014 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.logging.Logger;

/**
 * Used to hold various configuration parameters for T3. The use of this Config
 * class is shared by multiple T3's API classes, but keep in mind that such an
 * API class may not utilize all configuration parameters available in this
 * Config class.
 */
public class Config {
		
    public Class CUT ;
    public boolean assumeClientInTheSamePackage = true  ;
    public boolean includeInheritedMembers = true ;
    public boolean includePrivateAndDefaultMembers = false ;
    public int maxLevelOfObjectsNesting = 4 ;
    public int maxCollectionSize = 3 ;
    public int maxNumberOfStepRetry = 30 ;
    public int maxNumberOfSeqRetry  = 5 ;
    public int maxPrefixLength = 8 ;
    public int maxSuffixLength = 3 ;
    
    /**
     * If true will try to maximize the prefix to the specified maximum length.
     * Default it true.     
     */
    public boolean maximizePrefix = true ;
    public double suiteSizeMultiplierPerGoal = 4.0 ;
    public double fieldUpdateProbability = 0.1 ;
    
    /**
     * Specify the max. number of pairs, in T3's pairwise goal calculation. This is to prevent 
     * exploding the size of the suite:
     */
    public  int maxNumberOfPairs = 1000 ;
    
    /**
     * If true will supress the pair-wise generation of test goals by T3. The default is false.
     */
    public boolean surpressPairwiseTesting = false ;
    
    /**
     * If true we will only keep sequences that violates assertions, or throws an exception,
     * one that is caused by a violation to a pre-condition. Other sequences are dropped.
     * Default is false.
     */
    public boolean keepOnlyRedTraces = false ;
    
    /**
     * In non-regression mode, if true this will drop traces that violate some assumption,
     * e.g. throwing IllegalArgumentException. In the regression-mode this has no effect.
     * The default is true.
     */
    public boolean dropInvalidTraces = true ;
    
    
    public boolean dropDuplicates = false ;
    
    /**
     * The resulting suite will be split into this number of sub-suites.
     */
    public int splitSuite = 1 ;
    
    /**
     * An output stream to print out sequence-executions (if requested).
     */
    public OutputStream sequencePrintOut = System.out ;

    /**
     * An output stream to print out report and statistics.
     */    
    public OutputStream reportOut = System.out ;

    public int reportVerbosity = 1 ;
    
    
    /**
     *  If true, the CUT is assumed to be correct, and we want to generate
     *  a suite to be used for regression testing. Oralces will be injected
     *  into the sequences; e.g. stating that methods should return exactly
     *  the same values as they do now. If exceptions are thrown, then they
     *  are converted into oracles, stating that the same exceptions should 
     *  be thrown by replay (negative tests).
     *  
     *  The default is false.
     */
    public boolean regressionMode = false ;
    
    /**
     * If regressionMode and this flag are true, this will automatically
     * inject oracles into the generated test-sequence. Default is true.
     */
    public boolean injectOracles = true ;

    /**
     * The number of processor cores. The default is 1.
     */
    public int numberOfCores = 1 ;
    
    
    /**
     * A list of paths to roots of the class files that are to be scanned for building
     * an interface map.
     */
    public String[] dirsToClasses = new String[0] ;
    
    public void setDirsToClasses(String... dirs) {
    	dirsToClasses = dirs ;
    }
    
    
    /**
	 * If true, when replaying a suite, all sequences will be run; else the execution will 
	 * stop at the first step, in the first violation. 
	 * This exception will be wrapped in a Violation, and then (re-)thrown.
     */
	public boolean replayRunAll = false ;
     
	/**
     * If true, then replays that throw exception (or Oracle
     * Error in the regression-mode) will be shown/reported into the given output stream.
	 */
    public boolean replayShowExcExecution = true ;
     
    public int replayShowLength = 10 ;
    public int replayShowDepth = 3 ;
    
    
    
    /**
     * Create a config, with default setting.
     */
    public Config() { ; }
    
    
    protected void reportWrite(String s){
    	try {  
    		reportOut.write(s.getBytes());
    	}
    	catch(Exception e) {
    		Logger.getLogger(CONSTANTS.T3loggerName).warning("Fail to write to a report-output-stream.");
    	}
    }
    
    protected void reportWriteln(String s){
    	reportWrite(s + "\n") ;
    }    
    
    public void print()  {
    	printGeneral() ;
    	printGeneratorRelated() ;
    	printReplayRelated() ;
    }
    
    public void printGeneral()  {
        reportWriteln("-----") ;
    	reportWriteln("Configuration:") ;
    	reportWriteln("** General parameters:") ;
    	reportWriteln("   #cores : " + numberOfCores) ; 	
    }
    
    public void printGeneratorRelated()  {
    	reportWriteln("** Generator related parameters: ") ;
    	reportWrite  ("   Dirs to class-files to be scanned : ") ;
    	if (dirsToClasses.length==0) reportWriteln("-") ;
    	else {
    		for (int k=0; k<dirsToClasses.length; k++) {
    			if (k>0) reportWrite(", ") ;
    			reportWrite(dirsToClasses[k]) ;
    		}
    		reportWriteln("") ;
    	}
    	reportWriteln("   CUT : " + CUT) ;
    	if (assumeClientInTheSamePackage) reportWriteln("   Testing CUT from the same package's perspective") ;
    	else reportWriteln("   Testing CUT from a different package's perspective") ;   
    	if (includeInheritedMembers)  reportWriteln("   Inherited members are included.") ;
    	else reportWriteln("   Inherited members are excluded.") ;
    	reportWriteln("   Max. number of step-retries : " + maxNumberOfStepRetry) ;
    	reportWriteln("   Max. number of sequence-retries : " + maxNumberOfSeqRetry) ;
    	reportWriteln("** T3-default-generator's specific parameters:") ;
    	reportWriteln("   Regression-mode : " + regressionMode) ;
    	reportWriteln("   Injecting oracles : " + injectOracles) ;
    	reportWriteln("   Max. collection size : " + maxCollectionSize) ;
    	reportWriteln("   Max. level of object nesting : " + maxLevelOfObjectsNesting) ;
    	reportWriteln("   Max. prefix length : " + maxPrefixLength) ;
    	reportWriteln("   Max. suffix length : " + maxSuffixLength) ;   
    	reportWriteln("   Variable length prefix : " + !maximizePrefix) ;   
    	reportWriteln("   Field-update probablity as a step : " + fieldUpdateProbability) ;
    	reportWriteln("   Suite size multiplier for each goal : " + suiteSizeMultiplierPerGoal) ;
    	reportWriteln("   Keeping only exception-throwing sequences : " + keepOnlyRedTraces) ;
    	reportWriteln("   Dropping invalid sequences : " + dropInvalidTraces) ;
    	reportWriteln("   Dropping duplicates : " + dropDuplicates) ;  
    	reportWriteln("   Suite is to be split in : " + splitSuite + " subsuites") ;
    	
    }

    public void printReplayRelated()  {
    	reportWriteln("** Replay related parameters: ") ;
    	reportWriteln("   Replaying all sequences: "+ replayRunAll) ;
    	reportWriteln("   Regression-mode : " + regressionMode) ;
    	reportWriteln("   Show exception-throwning execution: "+ replayShowExcExecution) ;
    	reportWriteln("   Max. length of shown suffix: " + replayShowLength) ;
    	reportWriteln("   Max. depth of objects shown: " + replayShowDepth) ;			
    }
}
