/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

import static Sequenic.T3.Generator.GenCombinators.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.function.Function;

import Sequenic.T3.Pool;
import Sequenic.T3.TestingScope;
import Sequenic.T3.Examples.Friends.Person;
import Sequenic.T3.Examples.InterfaceAndAbstract.Vehicle;
import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Generator.SeqAndSuite.MethodG;
import Sequenic.T3.Generator.SeqAndSuite.ObjectUnderTestG;
import Sequenic.T3.Generator.SeqAndSuite.SegmentG;
import Sequenic.T3.Generator.SeqAndSuite.SuiteG;
import Sequenic.T3.Generator.Step.*;
import Sequenic.T3.Generator.Value.ValueMG;
import Sequenic.T3.Info.FunctionalCoverage;
import Sequenic.T3.Info.FieldPairsCoverage;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.Maybe;
import Sequenic.T3.utils.Unit;

/**
 * A class to facilitate the definition of a custom suite generator. Usage is
 * for example:
 * 
 *    CustomSuiteGenerator G = new CustomSuiteGenerator(...) ;
 *    G.seqGenerator = sequence(G.create(),
 *                              G.segment(2),
 *                              G.method("foo"),
 *                              G.method("fii") );
 *    G.maxSamplesToCollect = 20 ;
 *    SUITE S = G.suite() ;
 *                              
 * For now, the defined generator will only produce non-asm-viol sequences. The sequences
 * are generated sequentially.
 *
 */
public class CustomSuiteGenAPI extends SuiteAPI2 {
	
    protected Pool pool ;

	protected ObjectUnderTestG creationG ;
	protected MethodG methodG ;
	protected SegmentG segmentG ;
	
	// overriding the name
	protected Generator<PARAM,STEP> valueMetaGenerator ;
	
   
    /**
     * Construct a custom generator base, with a custom value generator supplied.
     */
    public CustomSuiteGenAPI(
    		Function<ImplementationMap,Function<Pool,Generator<PARAM,STEP>>> fValueMetaGenerator,
    		Config config, 
    		Class...classesTobeScanned)
    {
    	
    	super(config) ;
    	
    	// setting up implementation-map and testing scope:
    	impMap = new ImplementationMap(config.dirsToClasses,classesTobeScanned) ;
    	scope = new TestingScope(impMap,config.CUT) ;
    	scope.testingFromTheSamePackagePespective = config.assumeClientInTheSamePackage ;
    	scope.includePrivateAndDefaultMembers = config.includePrivateAndDefaultMembers ;
    	
    	config.print() ; 
    	
    	config.reportWriteln("-----");
    	config.reportWriteln("** Scanned : " + impMap.knownClasses.size() + " classes, "
    			+ impMap.numberOfInstantiable()
    			+ " are instantiable."
    			) ;
    	
    	// setting up generators:
    	pool = new Pool() ;
    	if (fValueMetaGenerator == null) {
    		// use T3's default value generator:
    		getT3logger().info("Using T3's default value generator.");
    		ValueMG valueMG = new ValueMG(config.maxLevelOfObjectsNesting,config.maxCollectionSize,impMap) ;
    		this.valueMetaGenerator = valueMG.gen1closed(pool) ;
    	}
    	else {
    		getT3logger().info("Using a custom value generator.");
    		this.valueMetaGenerator = fValueMetaGenerator.apply(impMap).apply(pool) ;
    	}
    	
    	creationG = new ObjectUnderTestG(scope,pool,config.maxNumberOfStepRetry,valueMetaGenerator) ; 
    	methodG   = new MethodG(scope,pool,config.maxNumberOfStepRetry,valueMetaGenerator) ;
    	segmentG  = new SegmentG (scope,pool,config.maxNumberOfStepRetry,valueMetaGenerator) ;
    }
	
    
    public SUITE suite(
    		Generator<SEQ_RT_info,SEQ_RT_info> sequenceGenerator,
    		int maxSamplesToCollect
    		) 
    		throws Exception 
    { 		
    	long t0 = System.currentTimeMillis() ;
        
    	SuiteG suiteG = new SuiteG(scope) ;
		pool.reset();
		config.reportWriteln("-----") ;
		config.reportWriteln(scope.toString()) ;
		SUITE S = suiteG.run(pool, sequenceGenerator, maxSamplesToCollect) ;
		if (config.keepOnlyRedTraces) dropNonViolatingSequences(S) ;
		dropBrokenSequences(S) ;
        
        S.suitename = "" + S.CUTname ;
        S.timeStamp = "" + System.currentTimeMillis()  ;

        config.reportWriteln("-----") ;
        config.reportWriteln("** Suite generated.") ;
        config.reportWriteln(S.showSuiteStatistics()) ;
        
        config.reportWriteln("** Runtime = " + (System.currentTimeMillis() - t0)) ;
        flushReportStream() ;
		
        return S ;
	}
    
    
    /**
     * Create an instance of the CUT. It should not throw any exception.
     */
	public Generator<SEQ_RT_info,SEQ_RT_info> create() { 
		return creationG.nonnull_and_no_exc() ;
	}
	
	/**
	 * Generate a call to a constructor with the specified type. It should not throw
	 * any assumption violation.
	 */
	public Generator<SEQ_RT_info,SEQ_RT_info> constructor(Class... paramtypes) { 
		return creationG.no_asmviol(paramtypes) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> grey_constructor(Class... paramtypes) { 
		return  creationG.grey(paramtypes) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> creatorMethod(String creatorMethodName, Class... paramtypes) { 
		return  creationG.no_asmviol(creatorMethodName, paramtypes) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> grey_creatorMethod(String creatorMethodName, Class... paramtypes) { 
		return  creationG.grey(creatorMethodName, paramtypes) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> method(String methodName, Class... paramtypes) { 
		return methodG.no_asmviol(methodName, paramtypes) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> grey_method(String methodName, Class... paramtypes) { 
		return methodG.grey(methodName, paramtypes) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> segment(int k) { 
		return segment(config.fieldUpdateProbability, k) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> grey_segment(int k) { 
		return grey_segment(config.fieldUpdateProbability, k) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> no_exc_segment(int k) { 
		return no_exc_segment(config.fieldUpdateProbability, k) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> segment(double fieldUpdateProbability, int k) { 
		return info -> {
			int k_ = k ;
			if (! config.maximizePrefix) k_ = T3Random.getRnd().nextInt(k + 1) ;
			return segmentG.no_asmviol(fieldUpdateProbability, k_).generate(info) ;
		} ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> no_exc_segment(double fieldUpdateProbability, int k) { 
		return info -> {
			int k_ = k ;
			if (! config.maximizePrefix) k_ = T3Random.getRnd().nextInt(k + 1) ;
			return segmentG.no_exc(fieldUpdateProbability, k_).generate(info) ;
		} ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> grey_segment(double fieldUpdateProbability, int k) { 
		return info -> {
			int k_ = k ;
			if (! config.maximizePrefix) k_ = T3Random.getRnd().nextInt(k + 1) ;
			return segmentG.grey(fieldUpdateProbability, k_).generate(info) ;
		} ;
	}

	public Generator<SEQ_RT_info,SEQ_RT_info> mutatorsSegment(int k) { 
		return mutatorsSegment(config.fieldUpdateProbability, k) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> grey_mutatorsSegment(int k) { 
		return grey_mutatorsSegment(config.fieldUpdateProbability, k) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> no_exc_mutatorsSegment(int k) { 
		return no_exc_mutatorsSegment(config.fieldUpdateProbability, k) ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> mutatorsSegment(double fieldUpdateProbability,int k) { 
		return info -> {
			int k_ = k ;
			if (! config.maximizePrefix) k_ = T3Random.getRnd().nextInt(k + 1) ;
			return segmentG.no_asmviol_mutators(fieldUpdateProbability, k_).generate(info) ;
		} ;
	}

	public Generator<SEQ_RT_info,SEQ_RT_info> grey_mutatorsSegment(double fieldUpdateProbability, int k) { 
		return info -> {
			int k_ = k ;
			if (! config.maximizePrefix) k_ = T3Random.getRnd().nextInt(k + 1) ;
			return segmentG.grey_mutators(fieldUpdateProbability, k_).generate(info) ;
		} ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> no_exc_mutatorsSegment(double fieldUpdateProbability, int k) { 
		return info -> {
			int k_ = k ;
			if (! config.maximizePrefix) k_ = T3Random.getRnd().nextInt(k + 1) ;
			return segmentG.no_exc_mutators(fieldUpdateProbability, k_).generate(info) ;
		} ;
	}
	
	public Generator<SEQ_RT_info,SEQ_RT_info> nonMutatorsSegment(int k) { 
		return info -> {
			int k_ = k ;
			if (! config.maximizePrefix) k_ = T3Random.getRnd().nextInt(k + 1) ;
			return segmentG.no_asmviol_nonmutators(k_).generate(info) ;
		} ;
	}

	public Generator<SEQ_RT_info,SEQ_RT_info> grey_nonMutatorsSegment(int k) { 
		return info -> {
			int k_ = k ;
			if (! config.maximizePrefix) k_ = T3Random.getRnd().nextInt(k + 1) ;
			return segmentG.grey_nonmutators(k_).generate(info) ;
		} ;
	}
	

	
    /**
     * To insert an INSTRUMENT step.
     */
    public Generator<SEQ_RT_info,SEQ_RT_info> instrument() {
    	return info -> {
    		info.seq.steps.addLast(new INSTRUMENT()) ;
    		return new Maybe(info) ;
    	} ;
    }
    
	
	

	
    public static void main(String[] args) throws Exception {
        test1() ;
    }
	
	private static void test1() throws Exception {
		
   	 Config config = new Config() ;
   	 //config.CUT = Person.class ;
     //config.CUT = Vehicle.class ;
     config.CUT = LinkedList.class ;
     config.setDirsToClasses("./bin") ;
        
     CustomSuiteGenAPI sg = new CustomSuiteGenAPI(null,config) ;
     sg.scope.configureForADTtesting();
     
     SUITE S = sg.suite(SequenceWhile(
        		r -> ! r.isFail(),
        		sg.create(),
        		sg.mutatorsSegment(10),
        		sg.instrument(),
        		sg.method("friend"),
        		sg.method("unfriend"),
        		//sg.grey_method("add"),
        		//sg.grey_method("remove"),
        		sg.nonMutatorsSegment(10)
        		),
        		500
        		) ;

      sg.reportCoverage(ClassLoader.getSystemClassLoader(),S) ; 
	
    }

}
