package Sequenic.T3.DerivativeSuiteGens.Gen2;

import static Sequenic.T3.Generator.GenCombinators.OneOfVal;

import java.io.Serializable;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.JavaType.JTypeUtils;
import Sequenic.T3.Sequence.Datatype.PARAM;

public class CustomGenDSL {
	
	static boolean ofOfTheseTypes(PARAM P, Class... cls) {
		Class C = JTypeUtils.getTopClass(P.ty) ;
		for (Class D : cls) {
			if(C == D) return true ;
		}
		return false ;
	}
	
	public static Generator<PARAM,Serializable> OneOfVal_(Float... f) {
		Serializable[] f_ = f ;
		Generator<PARAM,Serializable> gen = OneOfVal(f_) ;
		return gen.If(P -> ofOfTheseTypes(P,Float.class, Float.TYPE)) ;
	}
	
	public static Generator<PARAM,Serializable> OneOfVal_(Integer... f) {
		Serializable[] f_ = f ;
		Generator<PARAM,Serializable> gen = OneOfVal(f_) ;
		return gen.If(P -> ofOfTheseTypes(P,Integer.class, Integer.TYPE)) ;
	}

	public static Generator<PARAM,Serializable> OneOfVal_(String... f) {
		Serializable[] f_ = f ;
		Generator<PARAM,Serializable> gen = OneOfVal(f_) ;
		return gen.If(P -> ofOfTheseTypes(P,String.class)) ;
	}
	
}
