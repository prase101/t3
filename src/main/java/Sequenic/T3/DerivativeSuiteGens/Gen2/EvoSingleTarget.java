package Sequenic.T3.DerivativeSuiteGens.Gen2;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import Sequenic.T3.Sequence.Datatype.Mutation;
import Sequenic.T3.Sequence.Datatype.SUITE;

/**
 * A version of SingleTarget with some evolutionary heuristic.
 */
public class EvoSingleTarget extends SingleTarget{
	
	private String mode = "random" ;
	private SUITE evolvedPopulation = null ;
	
	public EvoSingleTarget(G2Config config, G2SuiteGen g2sg, Method target) {
		super(config,g2sg,target) ;
	}
	
	public EvoSingleTarget(G2Config config, G2SuiteGen g2sg, Constructor target) {
		super(config,g2sg,target) ;
	}

	private boolean isInRandomMode() {
		return mode.equals("random") ;
	}
	
	private void flipMode() {
		if (isInRandomMode()) {
			mode = "evo" ;
			t3log.info("** SingleTarget switches the evo-mode.");
		}
		else {
			t3log.info("** SingleTarget switches back the random-mode.");
			mode = "random" ;
		}
	}
	
	/**
	 * Return the last-k improvement
	 */
	private double getImprovement(int k) { 
		int N = targetcov.size() ;
		if (N <= 1) return 0 ;
		k = Math.min(k,N-1) ;
		double improvemet = 0 ;
		while(0<k) {
			double td = targetcov.get(N-k) - targetcov.get(N-k-1) ;
			double cd = classcov.get(N-k) - classcov.get(N-k-1) ;
			improvemet += td+cd ;
			k-- ;
		}
		return improvemet ;
	}
	
	protected SUITE generateFreshSuite() {
		if (numberOfIterations()>=10 && getImprovement(4) <= 0.0) { 
			// we get too little improvementin the last n rounds, flip the mode
			flipMode() ;
			// if the mode is switch back to random, throw away the evolved population
			if (isInRandomMode()) evolvedPopulation = null ;
		}
		if (suite==null || isInRandomMode()) return super.generateFreshSuite() ;
		
		//System.out.println(">>>>>>>>") ;
		// evo-mode
		if (evolvedPopulation==null) {
			evolvedPopulation = new SUITE(suite.CUTname) ;
			evolvedPopulation.suite.addAll(suite.suite) ;
		}
		SUITE crossed = Mutation.crossover(g2sg.scope.CUT, evolvedPopulation,15) ;
		
		if (crossed.suite.size()<=0) {
			t3log.info("** SingleTarget: cross over does not yield any new sequences. Using random mode.");
			return super.generateFreshSuite() ;
		}
		
		if (config.injectOracles) {
			crossed.injectOracles(g2sg.scope.CUT, true, g2sg.maxSuffixLength+1);
		}
		evolvedPopulation.suite.addAll(crossed.suite) ;
		return crossed ;	
	}
	
}
