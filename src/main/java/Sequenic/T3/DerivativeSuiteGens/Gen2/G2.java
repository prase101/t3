/*
 * Copyright 2016 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.DerivativeSuiteGens.Gen2;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.logging.Logger;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.ImplementationMap;
import Sequenic.T3.Pool;
import Sequenic.T3.SimpleClassLoader;
import Sequenic.T3.DerivativeSuiteGens.Gen2.Worklist.TargetQuality;
import Sequenic.T3.Info.Analytics.Analytics;
import Sequenic.T3.Info.Analytics.DataTable;
import Sequenic.T3.Sequence.Datatype.*;

/**
 * This provides top-level APIs to invoke G2 generator. Use the static method:
 * 
 *    generateSuites(CUTname,config,timebudget) ... the budget is in ms.
 *    
 */

public class G2 {

	/**
	 * The worker generator.
	 */
	public G2SuiteGen g2sg ;
	public G2Config config ;
	
	private TimeBudget timebudgetTracker = new TimeBudget() ;
	
	private Worklist worklist ;
	
	static private Logger t3log = Logger.getLogger(CONSTANTS.T3loggerName) ;
	
	static public class TimeBudget {
		private long starttime = 0 ;
		// elapsed time of the timer
		private long time = 0 ;
		// the given budget
		private long timeBudget = 60000 ;
		private boolean started = false ;
		
		public TimeBudget() {
			time = System.currentTimeMillis() ;
			starttime = time ;
		}
		
		synchronized public void start(long budget) {
			if (started) return ;
			started = true ;
			time = 0 ;
			starttime = System.currentTimeMillis() ;
			timeBudget = budget ;
			//System.out.println("=== timebudget started: " + budget + ", " + starttime);
		}
		
		public void printElapsedTime() {
			System.err.println(">> Time elapses: " + (System.currentTimeMillis() - starttime)) ;
		}
		
		synchronized long getBudget() { return timeBudget ; }

		synchronized public void stop() { started = false ; }
		
		synchronized public long check() throws TimeBudgetException, InterruptedException {
			// first check if the current thread has been interrupted
			if (Thread.interrupted()) throw new InterruptedException("The current thread has been interrupted; perhaps higher level time out?") ;
			
			if (!started) return timeBudget ;
			long tnow = System.currentTimeMillis() ;
			time = (tnow - starttime) ;
			long remaining = timeBudget - time ;
			t3log.info("=== remaining: " + remaining + "/" + timeBudget + ", " + tnow);
			if (remaining <= 0) {
				t3log.info("Ran out of time budget!");
				throw new TimeBudgetException() ;
			}
			return remaining ;
		}
	}
		
	static public class TimeBudgetException extends Exception { }
	
	/**
	 * Create an instance of G2 test suites generator. Typically, the CUT is a concrete
	 * class (it is not an interface or an abstract class). If an interface or an abstract
	 * class is given as the CUT, it may still contain concrete static methods, which we
	 * can test; so in these cases we will target them.
	 */
	protected G2(String CUTname, G2Config config) throws Exception {
		this.config = config ;
		g2sg = new G2SuiteGen(CUTname,config.CUTrootDir, config.useCoverageGuidance,config.useStaticInfo,config.dirOfStaticInfo,config.customPrimitivesGenerator) ;
		g2sg.regressionMode = config.regressionMode ;
		g2sg.injectOracles = config.injectOracles ;
		g2sg.maxSuffixLength = config.maxSuffixLength ;
		// configuring testing scope:
		g2sg.scope.testingFromTheSamePackagePespective = config.assumeClientInTheSamePackage ;
		g2sg.scope.includePrivateAndDefaultMembers = config.includePrivateAndDefaultMembers ;		
		
		// initialize the worklist
		if (config.worklistType.equals("random")) {
			worklist = new WorklistRandom() ;
		}
		else if (config.worklistType.equals("lowcovfirst")) {
			worklist = new WorklistLowcovFirst() ;
		}
		else worklist = new Worklist() ;
		
		g2sg.scope.configureForNonADTTesting();
		for (Method m : g2sg.scope.methods) {
			SingleTarget t ;
			if (config.refinementHeuristic.equals("evo")) t = new EvoSingleTarget(config,g2sg,m) ; 
			else t = new SingleTarget(config,g2sg,m) ; 
			worklist.addTarget(t);
		}
		
		if (!g2sg.scope.CUT.isInterface() && ! Modifier.isAbstract(g2sg.scope.CUT.getModifiers())) {
			// set targets for ADT testing; only when the CUT is concrete:
			g2sg.scope.configureForADTtesting();
			for (Constructor co : g2sg.scope.constructors) {
				SingleTarget t ;
				if (config.refinementHeuristic.equals("evo")) t = new EvoSingleTarget(config,g2sg,co) ; 
				else t = new SingleTarget(config,g2sg,co) ; 
				worklist.addTarget(t);
			}
			for (Method m : g2sg.scope.methods) {
				if (Modifier.isStatic(m.getModifiers())) continue ;
				SingleTarget t ; 
				if (config.refinementHeuristic.equals("evo")) t = new EvoSingleTarget(config,g2sg,m) ; 
				else t = new SingleTarget(config,g2sg,m) ; 
				worklist.addTarget(t);
			}
		}
		
		t3log.info("Creating " + printTargets()) ;
	}
	
	private String printTargets() {
		StringBuilder s = new StringBuilder() ;
		s.append("G2 generator targeting " + g2sg.scope.CUT.getName()) ;
		s.append(", #worklist=" + worklist.waiting.size() + ", targets: ") ;
		for (SingleTarget target : worklist.waiting) { 
		  target.maxNumberOfRefinement = config.maxNumberOfRefinements_ofEachTarget ;
		  target.minimumCovTobeHappy   = config.minimumCovTobeHappy_ofEachTarget ;
		  s.append("\n  " + target.getName() + "(" + target.getNumOfParams() + ")") ;
		}
		return s.toString() ;
	}
	
	public void startTimeBudget(long timebudget) {
		timebudgetTracker.start(timebudget);
	}
		    
    /**
     * Generate test suites for each method/constructor (target) of the CUT. One suite 
     * is generated for each target, and will be immediately saved. 
     * The process stop if we run out of time budget. After a test suite for a target
     * is generated, the target can be put back into an internal work-list, if the
     * generated coverage is deemed to be not enough. If may be
     * picked out and its saved suite refined.
     * The whole process will keep going, until it runs out time budget.
     * @throws IOException 
     */
	protected void generateAndRefine() throws TimeBudgetException, InterruptedException, IOException {
		
		if (worklist.isEmpty()) {
			t3log.info("" + g2sg.scope.CUT.getName() + " induces no target, so NO TEST SUITE is generated.") ;
			return ;
		}
		// DON't do shut down here!! --> JacocoInstrumenter.shutDownJacocoLogger();
		double budget = Math.max(1,timebudgetTracker.getBudget()) ;
		double remaining = timebudgetTracker.check() ;
		t3log.info("Start generating suites for: " + g2sg.scope.CUT.getName());
		
		// Initial idea is to generate the suites for multiple targets in parallel.
		// Unfortunately this won't work, because this would mean that generators
		// have to concurrently update and read from coverage information and the
		// special pool used by G2SuiteGen. This would require quite intricate 
		// programming.
		// DROPPED!!!
		// int numberOfCPUcores = config.numberOfCPUcores ;
		
		int prefixRefinementCount = 0 ;	
		int prefixStagnationCount = 0 ;
		int stagnationLimit = 3 ;
		int topIterationCount = 0 ; // counting target-refinement
		record_and_report_WorkListStatistics(topIterationCount) ; // this will initialize analystics
		record_and_report_prefixStatistics(prefixRefinementCount,0) ; // initialize too
		boolean generateMorePrefix = worklist.hasADTtarget() ;
		while (!worklist.isEmpty()) {
			if (generateMorePrefix && prefixStagnationCount < stagnationLimit) {
				prefixRefinementCount++ ;
				Integer K = config.numberOfPrefixes ;
				if (K==null) K = 50 ;
				if (g2sg.getPrefixes() != null) K = Math.min(K,10) ;
				Integer numOfAddedPrefixes = g2sg.incrementallyGeneratePrefixes(K,config.maxPrefixLength,config.maxObjectDepth) ;
				// reset this flag to false:
				generateMorePrefix = false ;
				if (numOfAddedPrefixes==null) {
					// g2sg signaled that it cannot generate prefixes because CUT has no constructor/factory, 
					// we should now remove all ADT targets because we can't solve them anyway
					worklist.removeADTtargets();
					t3log.info("Since " + g2sg.scope.CUT.getName() + " can't be instantiated, we now DROP all ADT targets.") ;
					// go back to the loop-head again:
					continue ;
				}
					
				if (numOfAddedPrefixes == 0) {
					prefixStagnationCount++ ;	
					if (prefixStagnationCount >= stagnationLimit) {
						t3log.warning("Prefix generation for " + g2sg.scope.CUT.getName() + " reaches stagnation. NO further prefixes will be generated."); 		        		
					}
				}
				
				if (g2sg.currentPrefixes != null) {	
					record_and_report_prefixStatistics(prefixRefinementCount,numOfAddedPrefixes) ;
				}
			}
			// pick a target and refine:
			SingleTarget target = worklist.getNext(remaining/budget) ;
			int sizeOld = 0 ;
			if (target.suite != null && target.suite.suite != null) sizeOld = target.suite.suite.size() ;
			target.refine() ;
			int sizeNew = sizeOld ;
			if (target.suite != null && target.suite.suite != null) sizeNew = target.suite.suite.size() ;
			
			TargetQuality quality = worklist.evaluateAndPutBack(target) ;
			if (quality == TargetQuality.TOBE_REFINED && target.isADT && target.target instanceof Method) {
				// a method-ADT target cannot be solved. Signal need to generate more prefixes.
				// Note that a constructor-target does not need prefix.
				generateMorePrefix = true ;	
			}
				
			// only save if the new suite is larger:
			if (sizeNew > sizeOld) target.saveSuite() ;
			// save a minimized version of the test suite; however this seem to drop
			// mutation kill rates in some situation; so I will just comment this out.
			// Will later make it configurable.
			//target.saveMinimizedSuite();
			if (quality == TargetQuality.STAGNATE) 
				t3log.info("Refinement for " + target.getName() + " has STAGNATED; it will not be further refined.");
			
			topIterationCount++ ;	
			record_and_report_WorkListStatistics(topIterationCount) ;
			remaining = timebudgetTracker.check() ;
		}
		Analytics.save(config.dirToSaveSuites,null,null);
		int Ntargets = 0 ;
		int NconstrTargets = 0 ;
		int NmethodADT = 0 ;
		int NmethodNonADT = 0 ;
		for (SingleTarget ST : worklist.allTargets) {
			Ntargets++ ;
			if (ST.target instanceof Constructor) NconstrTargets++ ;
			else if (ST.target instanceof Method && ST.isADT) NmethodADT++ ;
			else NmethodNonADT++ ;
		}
		t3log.info("#target constructors:" + NconstrTargets
				+ ", #target methdods:" + NmethodADT
				+ ", #target static methods:" + NmethodNonADT
				+ ", #tot. targets:" + Ntargets) ;
	}
	
	
	private String worklistStats_tablename() {
		return "" + "wlist_" + g2sg.scope.CUT.getName() ;
	}
	private String worklistTotCovStats_tablename() {
		return "" + "wlistTotcov_" + g2sg.scope.CUT.getName() ;
	}
	private String prefixesDiversityStats_tablename() {
		return "" + "prefixesDiversity_" + g2sg.scope.CUT.getName() ;
	}
	private String prefixCovStats_tablename() {
		return "" + "prefixesCov_" + g2sg.scope.CUT.getName() ;
	}
	
	void record_and_report_WorkListStatistics(int topIterationCount) {
		if(topIterationCount <= 0) {
			// when called first time, create the tables
			Analytics.registerDataTable(worklistStats_tablename(),
					"iteration",
					"target",
					"generation",
					"triedseqs",
					"suitesize",
					"cov");
			Analytics.registerDataTable(worklistTotCovStats_tablename(), 
					"iteration",
					"totTriedseqs",
					"totSuitesize",
					"totcov");
			return ;
		}

		String s = "Worklist iteration " + topIterationCount + "\n";
		int totTriedSeqs = 0 ;
		int totSuiteSize = 0 ;
		double totTargetCov = 0d ;
		for(SingleTarget ST : worklist.allTargets) {
			double tcov = ST.getLastTargetCov()  ;
			totTargetCov += tcov ;
			totTriedSeqs += ST.totNumberOfTriedSequences  ;
			int size = 0 ;
			if (ST.suite != null) size = ST.suite.suite.size() ;
			totSuiteSize += size ;
			Analytics.addRow(worklistStats_tablename(), 
				topIterationCount,
				ST.getName(),
				ST.numberOfIterations(), 
				ST.totNumberOfTriedSequences,
				size,
				tcov);
			
			s +=  "   " + ST.getName() + "_" + ST.getNumOfParams() 
			      + ", generation:" + ST.numberOfIterations() 
			      + ", seqs-tried:" + ST.totNumberOfTriedSequences 
			      + ", size:" + size
			      + ", cov:" + tcov + "\n" ;	
		}
		totTargetCov = totTargetCov / (double) worklist.allTargets.size();
		
		Analytics.addRow(worklistTotCovStats_tablename(), 
				topIterationCount,
				totTriedSeqs,
				totSuiteSize,
				totTargetCov) ;
		
		s += "Tot. tried sequences:" + totTriedSeqs + "\n";
		s += "Tot. suite size:" + totSuiteSize + "\n";
		s += "Tot. target coverage:" + totTargetCov ;
		t3log.info(s);
	}

	void record_and_report_prefixStatistics(int prefixRefinementCount, int added) {
		if(prefixRefinementCount <= 0) {
			Analytics.registerDataTable(prefixCovStats_tablename(),  
					"iteration",
					"added",
					"totSize",
					"classcov");
			return ;
		}
		
		int size = g2sg.currentPrefixes.size() ;
		double cov = g2sg.currentPrefixes.coverage ;
		//int Ntobjs = g2sg.currentPrefixes.tobjs.size() ;

		Analytics.addRow(prefixCovStats_tablename(), 
				prefixRefinementCount,
				added,
				size,
				cov);
		
		if (added>0) {
			String tablename = prefixesDiversityStats_tablename() + "_iter_" + prefixRefinementCount ;
			Analytics.registerDataTable(tablename, 
					"depth",
					"diversitySum",
					"avrgBranchDegr");
			for(ObjStructure o : g2sg.currentPrefixes.tobjs) {
				if (o == null) Analytics.addRow(tablename,0,0d) ;
				else Analytics.addRow(tablename,o.getDepth(),o.getDiversitySum(),o.getAvrgBranchingDegree());
				//System.out.println(">>> objstructure " + o.toString()) ;
			}
		}
		
		t3log.info("=== Generating/refining prefixes of " 
			      + g2sg.scope.CUT.getName()
			      + ". Generation: " + prefixRefinementCount
			      + ", adding: " + added
			      + ", tot-size: " + size
			      + ", cov: " + cov
			      //+ ", #tobjs: " + Ntobjs		
		);
		
	}

	
	/**
	 * Use this factory method to construct instances of G2 to generate test
	 * suites for a given CUT.
	 * There are three special cases:
	 * 
	 * (1) CUT is an interface. It may have static methods, which are then concrete.
	 * G2 will then target these methods.
	 * 
	 * (2) CUT is an abstract class. Like Interface, it may gace static methods, which
	 * must be concrete. G2 will in any case target these methods.
	 * 
	 * An abstract class may also have some concrete constructors and instance methods.
	 * These methods cannot be tested directly since Java prevents direct creation of
	 * instances of an abstract class. We will have to test the CUT indirectly through
	 * a concrete subclass. Ideally, the tester should provide a proxy subclass that 
	 * will simply pass calls to implemented methods of the CUT, but for now G2 will just
	 * search for a random implementation of the CUT and targets this implementation instead.
	 * 
	 * This will create a separate instance of G2, targeting the implementation.
	 * 
	 * TODO: a tool to generate such a proxy class.
	 * 
	 * (3) If the CUT has inner classes, and if G2 is configured to chase them, it will 
	 * target these inner classes as well. Fresh instances of G2 will be created for each
	 * inner class.
	 */
	 static public List<G2> mkG2(String CUTname, G2Config config) {
		ImplementationMap imap = new ImplementationMap(new String[0]) ;
		try { 
			// read from imap.txt
			ClassLoader classloader = new SimpleClassLoader(config.CUTrootDir) ;
			imap.addFromImapFile(classloader,config.dirOfStaticInfo); 
			return mkG2worker(CUTname,config,imap,classloader) ;
		}
		catch(Exception e) {
			//e.printStackTrace();
			t3log.warning("Fail to read imap.txt in " + config.dirOfStaticInfo);
			return mkG2worker(CUTname,config,imap,ClassLoader.getSystemClassLoader()) ;
		}
		
	 }
	 
	 static private boolean hasInternalClass(Class CUT) {
		 Class[] Ds = CUT.getDeclaredClasses() ;
		 return (Ds!=null && Ds.length>0) ;
	 }
	 
	 static private List<G2> mkG2worker(
			 String CUTname, 
			 G2Config config, 
			 ImplementationMap imap,
			 ClassLoader loader) 
	 {
	   List<G2> g2s = new LinkedList<G2>() ;
	   try { 
		  // First create G2 for the CUT itself. Even if it is non-concrete it may contains concrete
		  // static classes that we should test
		  g2s.add(new G2(CUTname,config)) ;  
		  
		
		  // If CUT is abstract, it may have concrete methods. To test these we need to create
		  // instances of the CUT. However, an abstract class cannot be instantiated, so we need
		  // to find a concrete implementation, and test the CUT indirectly through this concrete
		  // implementation:  
		  Class CUT = loader.loadClass(CUTname) ;
		  if (!CUT.isInterface() && Modifier.isAbstract(CUT.getModifiers())) {
        	  // if CUT is abstract, find an implementation:
        	  Class imp = imap.getRndImp(CUT) ;
        	  if (imp!=null)  {
            	t3log.info(CUT.getName() + " is abstract; we will test indirectly through a concrete implementation, namely:" + imp.getName());
        	    G2 g2indirect = new G2(imp.getName(),config) ;
        	    g2s.add(g2indirect) ;
        	  }
        	  else t3log.warning(CUT.getName() + " is abstract; but failed to find an implementation to target!");
            }
		    
           
          
          // if configured to do so, we additionally constructs instances of G2 for every static inner 
          // class of the CUT  
		  if (config.targetStaticInnerClassesToo) {
		     // in some rare case CUT.getDeclaredClasses() may throw java.lang.IllegalAccessError, cannot access superclass... 
	         // can't figure out why; probably classloader issue
	         for (Class innerC : CUT.getDeclaredClasses()) {
	            // non static inner classes and inherited inner classes are NOT targeted:
	            // NOTE: well, include them nonetheless...
	        	// if (! Modifier.isStatic(innerC.getModifiers())) continue ;
	        	// if (innerC.getDeclaringClass() != CUT) continue ;
	        	// t3log.info("Found an inner static class " + innerC.getName());
	        	g2s.addAll(mkG2worker(innerC.getName(),config,imap,loader)) ;
	          }
		    }
		  }
          catch (Throwable t) { 
        	  t3log.warning("G2.mkG2worker throws an exception: " + t.getStackTrace()) ;
        	  //System.out.println("### G2.mkG2worker throws:") ;
        	  //t.printStackTrace();
          }     
          return g2s ;
     }
	 
	 /**
	  * This will generate test suites for the given CUT. If CUT is abstract or
	  * an interface, we will target a randomly chosen implementation instead.
	  * If CUT has static inner classes, and if G2 is configured to go after them,
	  * these will be recursively targeted as well.
	  */
	 static public void generateSuites(String CUTname, G2Config config, long timebudget) throws TimeBudgetException, InterruptedException
	 {
		 TimeBudget budget = new TimeBudget() ;
		 budget.start(timebudget) ;
		 
		 t3log.info("CUT = " + CUTname + ", overall timebudget=" + timebudget) ;
		 
		 G2[] g2s = mkG2(CUTname,config).toArray(new G2[0]) ;
		 if (g2s==null || g2s.length==0) {
			 t3log.warning("Cannot instantiate G2-generator for " + CUTname);
			 return ;
		 }
		 if (g2s.length>1) {
			 String s = "G2 will target these classes: " ;
			 for (int k=0; k<g2s.length; k++) {
				 if (k>0) s += ", " ;
				 s += g2s[k].g2sg.scope.CUT.getName() ;
			 }
			 t3log.info(s);
		 }
		 
		 // calculate the weight of each target class, to estimate the budget it gets
		 double[] weights = new double[g2s.length] ;
		 //System.out.println(">>> #g2s=" + g2s.length);
		 for (int i=0; i<g2s.length; i++) {
			 weights[i] = g2s[i].g2sg.scope.CUT.getDeclaredMethods().length
					     + g2s[i].g2sg.scope.CUT.getDeclaredConstructors().length ;	 
		 }
		 Function<Integer,Double> totalRemainngWeight = k0 -> {
			 double total = 0 ;
			 for (int i=k0; i<weights.length; i++) total += weights[i] ;
			 return total ;
		 } ;

		 // let's just go through the generators in the order as given:
		 for (int i=0; i<g2s.length; i++) {
			   // calculate the budget.. proportional wrt to remaing target classes, and remaining budget
        	   double remainingBudget = budget.check() ;
        	   double remainingW = totalRemainngWeight.apply(i) ;
        	   double mybudget ;
        	   if (remainingW == 0) mybudget = remainingBudget ;
        	   else mybudget = (weights[i]/remainingW) * remainingBudget ;
        	   // assign budget, then generate:
        	   g2s[i].startTimeBudget(Math.round(Math.floor(mybudget))) ;
        	   try {
        	     g2s[i].generateAndRefine() ;
        	   }
        	   catch (TimeBudgetException te) { }
        	   catch (IOException e) { }
		 }
	 }
}