package Sequenic.T3.DerivativeSuiteGens.Gen2;

import java.io.Serializable;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;

public class G2Config {

	public String CUTrootDir ;
	public String dirOfStaticInfo ;
	
	// some variables to define scope (which members of the CUT are to be tested)
    public boolean assumeClientInTheSamePackage = true  ;
    public boolean includePrivateAndDefaultMembers = false ;

    public boolean targetStaticInnerClassesToo = false ;
    
	public String worklistType = "standard" ;
	
	public boolean regressionMode = false;
	public boolean injectOracles = true ;
	public String refinementHeuristic = "random" ;
	
	/**
	 * If true (default) that will in principle only add new test sequences to the current suite if they improve
	 * coverage. If false, will refine suites with randomly generated new sequences, disregarding coverage.
	 */
	public boolean useCoverageGuidance = true ;
	
	/**
	 * If true (default) will read a file containing lists of constants exctracted from the CUT. 
	 * If false will ignore such a file.
	 */
	public boolean useStaticInfo = true ;
	
	/**
	 * If not null, will determine the number of prefixes that will be generated for
	 * ADT testing. If it is left null, it will be calculated. The default is null.
	 */
	public Integer numberOfPrefixes = null ;
	
	public int maxPrefixLength = 4 ;
	public int maxObjectDepth = 5 ;
	public int maxSuffixLength = 5 ;
	
	/**
	 * If not null will determine the number of test sequences generated when 
	 * targeting a single method (e.g. a constructor or a static method) that
	 * does not require a prefix. If null then this will be calculated. The default
	 * is null.
	 */
	public Integer singleMethodTestSuiteSize = null ;
	
	public String dirToSaveSuites = null ;
	public String dirToSaveJunit = null ;
	public boolean generateJunitForEachSuite = true ;	
	
	public int maxNumberOfRefinements_ofEachTarget = 8 ;
	public double minimumCovTobeHappy_ofEachTarget = 0.95 ;
	
	/**
	 * Multi-core is disabled. It's not going to work because generating
	 * suites for multiple targets (of the same CUT) in parallel implies the
	 * generators need to concurrentlly update and access common shared coverage
	 * information. This will require quite intricate concurrent programming.
	 * Dropped.
	 */
	// public int numberOfCPUcores = 1 ;
	
	// controlling how violating sequences are shown:
	public boolean showExcExecution = true ;
	public int showLength = 4 ;
	public int showDepth = 5 ;   
	
	public Generator<PARAM,Serializable> customPrimitivesGenerator = null ;
	
	public G2Config usePrimitiveGenerator(Generator<PARAM,Serializable> gen) {
		customPrimitivesGenerator = gen ;
		return this ;
	}
	
	
}
