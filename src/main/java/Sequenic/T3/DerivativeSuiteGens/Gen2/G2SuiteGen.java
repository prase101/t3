/*
 * Copyright 2016 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.DerivativeSuiteGens.Gen2;

import static Sequenic.T3.Generator.GenCombinators.SequenceWhile;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.* ;

import Sequenic.T3.*;
import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Generator.SeqAndSuite.*;
import Sequenic.T3.Info.CodeCoverage;
import Sequenic.T3.Info.JacocoInstrumenter;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.Maybe;

/**
 * Provides basic generators to generate suite for a given method or constructor of a
 * CUT. Suppose the method is m. A suite for m consists of test sequences,
 * each consist of a prefix, m, then suffix. If m is static, or a constructor,
 * no prefix is needed. The generator for m does not generate the prefixes, but
 * require them to be given.
 * 
 * This class also provides methods to incrementally generating prefixes. They generate
 * a base set of prefixes, which are then incrementally refined. Structural coverage
 * and code coverage are used to determine refinement.
 * 
 */
public class G2SuiteGen {

	public TestingScope scope ;
	public ImplementationMap imap ;
	public int maxSuffixLength = 5 ;
	public int maxNumberOfStepRetry = 10 ;
	
	// notice that regression mode is set by default to false
	public boolean regressionMode = false ;
	public boolean injectOracles = true ;
	
	// multi-core is currently not supported for this generator!
	private final boolean isSingleCore = true ;
	
	// If true (default) that will in principle only add new test sequences to the current suite if they improve
	// coverage. If false, will refine suites with randomly generated new sequences, disregarding coverage.
	public boolean useCoverageGuidance = true ;
	
	// If true (default) will read a file containing lists of constants exctracted from the CUT. 
	// If false will ignore such a file.
	public boolean useStaticInfo = true ;
	
	public Pool pool ;
	private G2ValueMG gen2vmg   ;
	private Generator<PARAM,STEP> valueMG ;
	private Generator<PARAM,STEP> valueMG_with_tendency_to_reuse_objs ;
	
    public StaticInfo staticInfo = null ;
    public CodeCoverage codeCoverage = null ;

	static public Logger t3log = Logger.getLogger(CONSTANTS.T3loggerName) ;
	
	public float fieldUpdateProbability = 0.3f ;
	
	
	//private G2SuiteGen() {}
	
	public G2SuiteGen(String CUTname, 
			String CUTrootDir) 
			throws Exception
	{
		this(CUTname,CUTrootDir,true,true,null,null) ;
	}
	
	/**
	 * Create an instance of this G2-generator. Specify paths to the roots of
	 * class files, and the directory where to look for the file of staticinfo.txt
	 * of the CUT.
	 */
	public G2SuiteGen(String CUTname,
			String CUTrootDir,
			boolean useCoverageGuidance,
			boolean useStaticInfo,
			String staticInfoDir,
			Generator<PARAM,Serializable> customPrimValGenerator 
			) 
	throws Exception
	{
		//t3log.info("** CUT = " + CUT.getName());
		Class CUT ;
		this.useCoverageGuidance = useCoverageGuidance ;
		this.useStaticInfo = useStaticInfo ;
		if (CUTrootDir != null && useCoverageGuidance) {
			try {
				JacocoInstrumenter ji = new JacocoInstrumenter(CUTrootDir,CUTname) ;
				codeCoverage = new CodeCoverage(ji) ;
				CUT = ji.getInstrumentedCUT() ;
			}
			catch(Exception e) {
				codeCoverage = null ;
				t3log.warning("Jacoco instrumentation of " + CUTname + " is problematical; dropping it.");
				try {
				   CUT = Class.forName(CUTname) ;
				   t3log.warning("Using JVM's standard classloader to load " + CUTname + ".");
				}
				catch(Exception f) {
					t3log.warning("Loading " + CUTname + " with JVM's standard class loader also fails!");
					throw f ;
				}
			}	
		}
		else {
			try {
				   CUT = Class.forName(CUTname) ;
				   t3log.info("Using JVM's standard classloader to load " + CUT.getName() + ".");
				}
				catch(Exception f) {
					t3log.warning("Loading " + CUTname + " with JVM's standard class loader fails!");
					throw f ;
				}
		}
		
		configure(CUT,staticInfoDir,customPrimValGenerator) ;	
	}
	
	// configuring other stuffs ; called from the constructor.
	private void configure(Class CUT, String staticInfoDir, Generator<PARAM,Serializable> customPrimValGenerator) {
		// Be careful with the order of setting up!
		
		// setting up implementation-map and testing scope:
		imap = new ImplementationMap(new String[0]) ;
		scope = new TestingScope(imap,CUT) ;
    	 	
		// read various static information provided in file(s):
		if (staticInfoDir != null) {
			try {
				if (codeCoverage != null)
					imap.addFromImapFile(codeCoverage.instrumenter,staticInfoDir);
				else 
					imap.addFromImapFile(ClassLoader.getSystemClassLoader(), staticInfoDir);
			}
			catch(Exception e) {
				t3log.warning("Fail to read imap.txt in " + staticInfoDir);
			}
			if (useStaticInfo) {
				try {
					staticInfo = new StaticInfo(scope,imap,staticInfoDir) ;
					t3log.info("Staticinfo.txt read, #constants=" 
							   + staticInfo.allConstants.length
							   + ", #instanceOfs="
							   + staticInfo.allInstanceOfClasses.length);
				}
				catch (IOException e) {
					t3log.warning("Fail to read staticinfo.txt in " + staticInfoDir);
				}			
			}
			else t3log.warning("G2 is configured to ignore staticinfo.txt, so it does.");
		
		}	
				
		// setup value generator, and custom pool:
		gen2vmg = new G2ValueMG(imap,customPrimValGenerator) ;
		resetSeededConstants() ;
		pool = gen2vmg.pool ;
		valueMG = gen2vmg.valueMG() ;	
		valueMG_with_tendency_to_reuse_objs = gen2vmg.valueMG_leaningTowardsReuseOldObjects() ;
	}
		
	private String[] seededSPrimitives;
	
	/** 
	 * Reset the list of seeded S-primitives to the list of constants specified
	 * below, and the list of instanceOf constants is cleared.
	 */
	public void resetSeededConstants() {
		String[] seeded = { "0", "1", "-1", "2", "7", 
				"10", "0.0001", "0.05", "-0.05" , "2018" , "<X>", "<joe>", "<127.0.0.1>", 
				"<abc@com>","www.w3.org>","<2000-12-12>","<24:00:00>"
	    } ;	
		seededSPrimitives = seeded ;
		gen2vmg.configureSeededSPrimitives(seededSPrimitives);
		gen2vmg.instanceOfConstants.clear(); 
	}
	
	/**
	 * Override the list of seeded primitives with the given constants.
	 * String and character constants should be explicitly double-quoted.
	 */
	public void configureSeededSPrimitives(String... x) {
		seededSPrimitives = x ;
		gen2vmg.configureSeededSPrimitives(x);
	}
	
	/**
	 * Extend the list of seeded primitives with the specified constants.
	 */
	public void addSeededSPrimitives(String... x) {
		if (x==null) return ;
		String[] seeded = new String[seededSPrimitives.length + x.length] ;
		for (int k=0; k<seededSPrimitives.length; k++) seeded[k] = seededSPrimitives[k] ;
		int N = seededSPrimitives.length ;
		for (int k=0; k<x.length; k++) seeded[N+k] = x[k] ;
		seededSPrimitives = seeded ;
		gen2vmg.configureSeededSPrimitives(seededSPrimitives);
	}
	
	public void addInstanceOfConstants(Class ... cs) {
		if (cs == null) return ;
		for (Class C : cs) gen2vmg.instanceOfConstants.add(C) ;
	}
	
	static private String tySignature(Class[] paramtys) {
		StringBuilder s = new StringBuilder() ;
		for (Class C : paramtys) {
			s.append("_") ;
			s.append(C.getSimpleName().replaceAll("\\[\\]","Array")) ;
		}
		return s.toString() ;
	}
	
	static private String getLongName(Method m) {
		StringBuilder s = new StringBuilder() ;
		s.append(m.getName()) ;
		s.append(tySignature(m.getParameterTypes())) ;
		return s.toString() ;
	}
	
	static private String getLongName(Constructor c) {
		StringBuilder s = new StringBuilder() ;
		//System.out.println(">>> " + c.getName());
		
		// This throws an exception when CUT is an inner class... bug in Jacoco/Java??
		// s.append(c.getDeclaringClass().getSimpleName()) ;
		// so we will do it the long way like this:
		
		String name = c.getName() ;
		int k = name.lastIndexOf('$') ;
		if (k<0) k = name.lastIndexOf('.') ;
		if (k>=0) name = name.substring(k+1, name.length()) ;
		//System.out.println(">>> " + name);
		s.append(name) ;
		try {
			String sig = tySignature(c.getParameterTypes()) ;
			s.append(sig) ;
		}
		catch (Throwable t) { s.append("_xxx") ; }
		return s.toString() ;
	}
	
	static private String fileNameAcceptable_className(String cname) {
		String name = cname.replace("$","_i_").replace(".","_") ;
		//System.out.println(">>>>"  + cname + " replaced by " + name);
		return name ;
	}
	
	static private String fileNameAcceptable_className(Class C) {
		return fileNameAcceptable_className(C.getName()) ;
	}
	
	
	/**
	 * Generate a test suite to test a single static method, in a non-ADT mode.
	 */
	public SUITE generateSuiteForASingleStaticMethod(Method m, int N) {	
		scope.configureForNonADTTesting();
		return generateSuiteForASingleStaticMethod_worker(m,N) ;
	}
	
	private SUITE generateSuiteForASingleStaticMethod_worker(Method m, int N) {	
		//reset() ;
		
		if (Modifier.isAbstract(m.getModifiers()) || ! Modifier.isStatic(m.getModifiers())) return null ;
		
		if (staticInfo != null) {
			resetSeededConstants() ;
			addSeededSPrimitives(staticInfo.getConstantsOfPrivateMethods()) ;
			addSeededSPrimitives(staticInfo.getConstants(m)) ;
			addInstanceOfConstants(staticInfo.getInstanceOfClassesOfPrivateMethods()) ;
			addInstanceOfConstants(staticInfo.getInstanceOfClasses(m)) ;
		}
		
		T3SeqG seqG = new T3SeqG(pool,scope,maxNumberOfStepRetry,valueMG) ;
		Generator<SEQ_RT_info,SEQ_RT_info> seqgen ;
		if (regressionMode) seqgen = seqG.grey_nonadt(0, maxSuffixLength, false, 0, m) ;
		else seqgen = seqG.no_asmviol_nonadt(0, maxSuffixLength, false, 0, m) ;
		SuiteG suitegen = new SuiteG(scope) ;
		SUITE S = suitegen.run(pool,seqgen,N) ;
		if (injectOracles) S.injectOracles(scope.CUT, isSingleCore, maxSuffixLength+1);
		S.suitename = fileNameAcceptable_className(scope.CUT.getName()) + "__" + getLongName(m) ;
		if (S.suite.size() == 0) t3log.warning("Cannot generate any test sequence for " + getLongName(m));
		return S ;
	}
	
	/**
	 * Generate a test suite targeting all static methods whose names contain
	 * the given string.
	 */
	public SUITE generateSuiteForASingleStaticMethod(String mname, int N) {
		scope.configureForNonADTTesting();
		SUITE All = new SUITE() ;
		for (Method m : scope.methods) {
			if (m.getName().contains(mname)) {
				SUITE S = generateSuiteForASingleStaticMethod_worker(m,N) ;
				All = SUITE.union(All,S) ;
			}
		}
		All.CUTname = scope.CUT.getName() ;
		All.suitename = fileNameAcceptable_className(All.CUTname) + "__" + mname ;
		return All ;
	}
	
	/**
	 * Generate a test suite for a specific constructor c of the CUT.
	 */
	public SUITE generateSuiteForConstructor(Constructor c, int N) {
		scope.configureForADTtesting();
		return generateSuiteForConstructor_worker(c,N) ;
	}
	
	/**
	 * Generate a test suite of all constructors of the CUT. For each, N test sequences
	 * will be generated. All sequences will be put in a single test suite.
	 */
	public SUITE generateSuiteForConstructor(int N) {
		scope.configureForADTtesting() ;
		SUITE S = new SUITE() ;
		for (Constructor c : scope.constructors) {
			S = SUITE.union(S, generateSuiteForConstructor_worker(c,N)) ;
		}
		S.CUTname = scope.CUT.getName() ;
		S.suitename = fileNameAcceptable_className(S.CUTname) + "__" + scope.CUT.getSimpleName() ; 
		return S ;
	}
	
	private SUITE generateSuiteForConstructor_worker(Constructor c, int N) {	
		//reset() ;
		
		if (Modifier.isAbstract(c.getModifiers())) return null ;

		if (staticInfo != null) {
			resetSeededConstants() ;
			addSeededSPrimitives(staticInfo.getConstantsOfPrivateMethods()) ;
			addSeededSPrimitives(staticInfo.getConstants(c)) ;
			addInstanceOfConstants(staticInfo.getInstanceOfClassesOfPrivateMethods()) ;
			addInstanceOfConstants(staticInfo.getInstanceOfClasses(c)) ;
		}
		
		T3SeqG seqG = new T3SeqG(pool,scope,maxNumberOfStepRetry,valueMG) ;
		Generator<SEQ_RT_info,SEQ_RT_info> seqgen ;
		if (regressionMode) seqgen = seqG.grey_adt(maxSuffixLength, c) ;
		else seqgen = seqG.no_asmviol_adt(maxSuffixLength, c) ;
		SuiteG suitegen = new SuiteG(scope) ;
		SUITE S = suitegen.run(pool,seqgen,N) ;
		if (injectOracles) S.injectOracles(scope.CUT, isSingleCore, maxSuffixLength+1);
		S.suitename = fileNameAcceptable_className(scope.CUT.getName()) + "__" + getLongName(c) ;
		if (S.suite.size() == 0) t3log.warning("Cannot generate any test sequence for " + getLongName(c));
		return S ;
	}

	
	/**
	 * Hold the prefixes generated so far, for ADT testing.
	 */
	protected Prefixes currentPrefixes = null ;
	public SUITE getPrefixes() {
		if (currentPrefixes==null) return null ;
		return currentPrefixes.prefixes ;
	}
	
	/**
	 * To hold test sequences that are to be used as prefixes in ADT testing.
	 */
	protected class Prefixes {
		
		SUITE prefixes = new SUITE() ;
		// the list of normalized target-object structures that the prefixes produce
		protected List<ObjStructure> tobjs = new LinkedList<ObjStructure>() ;
		// code-coverage reached by the current prefixes
		protected double coverage = 0 ;
		protected int ObjStructureMaxDepth ;
		
		// will start by generating prefix of length 1, and will increase the length
		// progressively
		protected int currentLengthToGenerate = 1 ;
		protected int crlAccepted = 0 ;
		protected int crlRejected = 0 ;
		protected int maxlength ;
		
		Prefixes(int maxlength, int ObjStructureMaxDepth) {
			prefixes.CUTname = scope.CUT.getName() ;
			prefixes.suitename = prefixes.CUTname + "_adtPrefixes" ;
			this.maxlength = maxlength ;
			this.ObjStructureMaxDepth = ObjStructureMaxDepth ;			
		}
		
		int size() { return prefixes.suite.size() ; }
		
		private double getCoverage() {
			try { 
				codeCoverage.collectRTdata();
				coverage = codeCoverage.analyze().getClassCov(scope.CUT.getName(),"ratio") ;
			}
			catch(Throwable e) {
				Logger.getLogger(CONSTANTS.T3loggerName).warning("Code Coverage throws an exception when trying to analyze coverage.");
			}
			return coverage ;
		}
		
		private boolean enoughDiversity() {
			return crlAccepted >= 10 ;
		}
		
		private boolean progressHasStalled() {
			return crlRejected >= 5 ;
		}
		
		/**
		 * Adjust the target prefix-length. If the current length has enough diversity,
		 * or stagnates, increase the target length. Reset to length 1 if this would
		 * exceed max-length.
		 */
		private void adjustTargetLengthAsNeeded() {
			boolean enoughDiversity = enoughDiversity() ;
			boolean stagnated = progressHasStalled()  ;
			int oldlength = currentLengthToGenerate ;
			if (enoughDiversity || stagnated) {
				currentLengthToGenerate++ ;
				if (currentLengthToGenerate > maxlength) currentLengthToGenerate = 1 ;
				crlAccepted = 0 ;
				crlRejected = 0 ;
				
				if (enoughDiversity) 
					System.err.println(">>> Prefix generation at length " + oldlength 
							   + " has enough diversity. Adjusting target length to "
							   + currentLengthToGenerate) ;
				if (stagnated)
					System.err.println(">>> Prefix generation at length " + oldlength 
							   + " stagnated. Adjusting target length to "
							   + currentLengthToGenerate) ;
				
			}
		}
		
		/**
		 * Refine the current set of prefixes by generating N new prefixes. Only
		 * prefixes that produce "different" tobj or adds in code coverage will
		 * be added to the collection; so it is possible that less that N prefixes
		 * are added; or none at all.
		 * 
		 * The algorithm starts by generating prefixes of length 1 and will progressively
		 * generate longer prefixes if either it thinks it has produce enough variation
		 * for the current length, or if the generation has stagnated. 
		 * 
		 * If maximum length is reached and we reach stagnation, we reset the legth 
		 * to 1.
		 * 
		 * If the testing scope does not contain any constructor/factory to create an instance of
		 * the SUT the method will however returns null.
		 */
		private Integer refine(int N) {
			// prepare the scope
			scope.configureForADTtesting();
			
			// If we have no constructor nor creation method, forcefully add declared
			// non-visible constructors:	
			if(scope.constructors.isEmpty() && scope.creatorMethods().isEmpty()) {
				boolean added = false ;
		        for (Constructor co : scope.CUT.getDeclaredConstructors()) {
		    	    int mod = co.getModifiers() ;
		    	    if (! Modifier.isAbstract(mod)) {
		    	        added = true ; scope.constructors.add(co) ;
		    	    }                                          
		    	}
		        if(added) {
		        	Logger.getLogger(CONSTANTS.T3loggerName).info("Forcefully adding non-visible constructors into the scope of " + scope.CUT.getName()) ;
		        }
		        else {
		        	// if forcing fails, then we simply have no means to create an instance of
		        	// the CUT, hence also no means to produce prefixes ;
		        	Logger.getLogger(CONSTANTS.T3loggerName).info("Cannot find a constructor/factory to instantiate "
		        			+ scope.CUT.getName()
		        			+ " to create prefixes. No prefixes will be generated.") ;
		        	return null ;
		        }
		    }
			
			
			if (staticInfo != null) {
				resetSeededConstants() ;
				addSeededSPrimitives(staticInfo.getAllConstansts()) ;
				addInstanceOfConstants(staticInfo.getAllInstanceOfClasses()) ;
			}
			// do not include negative mutators like remove, delete, clear
			List<Method> tobeRemoved = new LinkedList<Method>() ;
			for (Method m : scope.mutators) {
				String name = m.getName().toLowerCase() ;
				if (name.contains("remove") || name.contains("delete") 
						|| name.contains("clear") 
						|| name.contains("reset"))
					tobeRemoved.add(m) ;
			}
			scope.mutators.removeAll(tobeRemoved) ;

			T3SeqG seqG = new T3SeqG(pool,scope,maxNumberOfStepRetry,valueMG) ;
			SuiteG suitegen = new SuiteG(scope) ;
			Function<Integer,Generator<SEQ_RT_info,SEQ_RT_info>> seqgen ;
			seqgen = length -> SequenceWhile(
		        		    r -> ! r.isFail(),
		        		    seqG.creationGen.nonnull_and_no_exc(),
		        		    seqG.no_exc_mutatorsPrefix(fieldUpdateProbability, length, true),
		        		    seqG.instrument()
		                  ) ;
			
			Pool cheappool = new Pool() ;
			// first reset coverage tracking and rerun the prefixes to restore their
			// coverage info
			if (codeCoverage!=null) codeCoverage.clearRawRTdata();
			for (SEQ seq : prefixes.suite) {
				try { seq.exec(scope.CUT,cheappool) ; }
				catch (Throwable e) { }
			}

			int numAdded = 0 ;
			int kmax = 3*N ;
			for (int k=0; k<kmax && numAdded<N ; k++) {
				SUITE T = suitegen.run(pool,seqgen.apply(currentLengthToGenerate-1),1) ;
				if (T == null || T.suite.isEmpty()) {
					crlRejected++ ;	
				}
				else {
					// check if the prefix generate something new; add if it does:
					SEQ seq = T.suite.get(0) ;
					double coverage0 = coverage ;
					ObjStructure t = ObjStructure.toObjsStruture(seq, scope.CUT, cheappool, ObjStructureMaxDepth) ;
					getCoverage() ;
					if (coverage > coverage0 || ! tobjs.contains(t)) {
						//System.out.println("Adding. Cov: " + coverage + ", old cov: " + coverage0) ;
						tobjs.add(t) ;
						prefixes.suite.add(seq) ;
						crlAccepted++ ;
						numAdded++ ;
					}
					else {
						//System.out.println("Reject. Cov: " + coverage + ", old cov: " + coverage0) ;
						crlRejected++ ;
					}
				}
				adjustTargetLengthAsNeeded() ;			
			}
			return numAdded ;
		}
	}
	
	
	/**
	 * This will generate up to N prefixes iteratively. It returns the number
	 * of prefixes it manages to generate.
	 * 
	 * If the testing scope does not contain any constructor/factory to create an instance of
     * the SUT the method will however returns null.
	 */
	public Integer incrementallyGeneratePrefixes(int N, int maxlength, int maxdepth) {
		// t3log.info(">>> invoking incrementallyGeneratePrefixes...") ;
		if (currentPrefixes == null) {
			currentPrefixes = new Prefixes(maxlength,maxdepth) ;
		}
		else {
			currentPrefixes.maxlength = maxlength ;
			currentPrefixes.ObjStructureMaxDepth = maxdepth ;
		}
		Integer added = currentPrefixes.refine(N) ;
		if(added == null) {
			return null ;
		}
			
		if(added == 0) {
			t3log.info("FAIL to add any new prefix.") ;
		}
		else {
			t3log.info("Adding " + added + "; #prefixes now =" + currentPrefixes.size()) ;
		}
		return added ;
	}
	
	public SUITE generateSuiteForASingleMethod(SUITE prefixes, String mname, int K) {
		scope.configureForADTtesting() ;
		SUITE S = new SUITE() ;
		for (Method m : scope.methods) {
			if (m.getName().contains(mname)) {
				S = SUITE.union(S, generateSuiteForASingleMethod_worker(prefixes,m,K)) ;
			}
		}
		S.CUTname = scope.CUT.getName() ;
		S.suitename = fileNameAcceptable_className(S.CUTname) + "__" + mname ;
		return S ;
	}
	
	public SUITE generateSuiteForASingleMethod(SUITE prefixes, Method m, int K) {
		scope.configureForADTtesting() ;
		return generateSuiteForASingleMethod_worker(prefixes,m,K) ;
	}
	
	private SUITE generateSuiteForASingleMethod_worker(SUITE prefixes, Method m, int K) {
		// scope.configureForADTtesting() ;
		
		if (Modifier.isAbstract(m.getModifiers())) return null ;
		
		if (m.getName().equals("equals")) {
			addInstanceOfConstants(scope.CUT) ;
		}
		
		if (staticInfo != null) {
			resetSeededConstants() ;
			addSeededSPrimitives(staticInfo.getConstantsOfPrivateMethods()) ;
			addSeededSPrimitives(staticInfo.getConstants(m)) ;
			addInstanceOfConstants(staticInfo.getInstanceOfClassesOfPrivateMethods()) ;
			addInstanceOfConstants(staticInfo.getInstanceOfClasses(m)) ;
		}
		
		//T3SeqG seqG = new T3SeqG(pool,scope,maxNumberOfStepRetry,valueMG) ;
		T3SeqG seqG = new T3SeqG(pool,scope,maxNumberOfStepRetry,valueMG_with_tendency_to_reuse_objs) ;
		Generator<SEQ_RT_info,SEQ_RT_info> suffixgen ;
	    if (regressionMode)  {
	    	if (scope.mutators.contains(m)) {
	    		//System.out.println("==> mutator: " + m.getName());
	    		suffixgen = SequenceWhile(
		        		r -> ! r.isFail(),
		        		seqG.methodGen.grey(m),
		        		seqG.segmentGen.grey_nonmutators(maxSuffixLength)) ;
	    	}
	    	else suffixgen = seqG.methodGen.grey(m) ;
	    }	
	    else {
	    	if (scope.mutators.contains(m)) {
	    		//System.out.println("==> mutator: " + m.getName());
	    		suffixgen = SequenceWhile(
		        		r -> ! r.isFail(),
		        		seqG.methodGen.no_asmviol(m),
		        		seqG.segmentGen.no_asmviol_nonmutators(maxSuffixLength)) ;
	    	}
	    	else suffixgen = seqG.methodGen.no_asmviol(m) ;
	    }
	    		
		SuiteG suitegen = new SuiteG(scope) ;
		if (m.getParameterCount() == 0) K = 1 ;
		SUITE S = suitegen.extend(scope.CUT, prefixes, pool, suffixgen, K) ;
		
		//System.err.println(">>> about to inject oracles " + injectOracles);
		if (injectOracles) {
			// should inject along maxSuffixLength+1 to include the tested method
			S.injectOracles(scope.CUT, isSingleCore, maxSuffixLength+1);
		}
		//System.err.println(">>> done injecting oracles...");
		S.suitename = fileNameAcceptable_className(S.CUTname) + "__" + getLongName(m) ;
		if (S.suite.size() == 0) t3log.warning("Cannot generate any test sequence for " + getLongName(m));

		return S ;
	}
	
	
	public static void main(String[] args) throws Exception{
		String CUTname = "SomeExamples.Triangle1" ;
		String CUTdir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3_externalExamples/bin" ;
		G2SuiteGen g2 = new G2SuiteGen(CUTname,CUTdir) ;
		Class CUT = g2.scope.CUT ;
		//g2.configureSeededPrimitives("0","3","test");
		g2.maxSuffixLength = 0 ;
		g2.regressionMode = true ;
		//SUITE S = g2.generatePrefixes(200,3) ;
		g2.incrementallyGeneratePrefixes(40,4,5) ;
		//g2.incrementallyGeneratePrefixes(10,4,5) ;
		SUITE S = g2.getPrefixes() ;
		System.out.println(S.showSuiteStatistics());
		System.out.println("** Variations: " + ObjStructure.countDifferentTargetObjs(S,CUT,5)) ;
		g2.codeCoverage.analyze().print();
		
		g2.codeCoverage.clear();
		SUITE T = g2.generateSuiteForASingleMethod(S, "isIsoleces", 2) ;
		System.out.println(T.showSuiteStatistics());
		System.out.println("** Variations: " + ObjStructure.countDifferentTargetObjs(T,CUT,5)) ;
		g2.codeCoverage.collectRTdata();
		g2.codeCoverage.analyze().print();
		//S.save("/Users/iswbprasetya/tmp", "testsuite_3", false) ;
		
		Integer[][] a = new Integer[0][0] ;
		Class[] tys = { a.getClass() } ;
		System.out.println(tySignature(tys));
	}
}
