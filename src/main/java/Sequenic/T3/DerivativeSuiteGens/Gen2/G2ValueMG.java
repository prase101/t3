/*
 * Copyright 2016 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.DerivativeSuiteGens.Gen2;

import static Sequenic.T3.Generator.GenCombinators.FirstOf;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.*;
import java.util.Map.Entry;

import Sequenic.T3.ImplementationMap;
import Sequenic.T3.Pool;
import Sequenic.T3.T3Random;
import Sequenic.T3.Generator.GenCombinators;
import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Generator.Value.*;
import Sequenic.T3.JavaType.JTfun;
import Sequenic.T3.JavaType.JType;
import Sequenic.T3.JavaType.JTypeUtils;
import Sequenic.T3.Reflection.Reflection;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.FUN;
import Sequenic.T3.utils.Maybe;
import Sequenic.T3.utils.SomeObject;
/**
 * 
 * Terminology: an SPrimitive value is either a Java primitive value or a string.
 */
public class G2ValueMG {
	
	public G2Pool pool ;
	
	/**
	 * Two ways to provide custom control on generating primitive values. One is by supplying
	 * an explicit generator (below). And the other  is by providing a list of constants.
	 */
	public Generator<PARAM,Serializable> customPrimValGenerator = null ;
	
	/**
	 *  primitive values and strings that are seeded (more likely to be needed):
	 */
	public Map<Class,Serializable[]> seededSPrimitives = new HashMap<Class,Serializable[]>() ;

	/**
	 * A list of class names mentioned in (... instanceof C) expressions.
	 */
	public List<Class> instanceOfConstants = new LinkedList<Class>() ;
	/**
	 * characters to used when generating random char or string
	 */
	private char[] charsSet ;
	
	public Random rnd = T3Random.getRnd() ; 
	
	public ImplementationMap ImpsMap ;
	public int maxLevelOfObjectsNesting = 3 ;
	public int collectionSize = 3 ;
	public float epsilon = 0.05f ; // max. distance to neighbor in float and double
	public float chanceToPickPreviousPrimitive = 0.5f ;
	public float chanceToPickSeededPrimitive = 0.35f ;
	public float chanceToPickNeighbor = 0.1f ; // chance to choose a neighbor of a seeded value
	
    private EnumMG enumgens = new EnumMG();
    private NullMG nullgens = new NullMG();   
    private ObjectMG constructorgens ;
    private ClazzMG clazzgens = new ClazzMG() ;
    private LamdaMG lambdagens ;
    private CollectionLikeMG 	collectiongens ;
    private REFMG refgens ;
    
    /**
     * Use a special Pool for generating values/sequences, to also keep a history of used 
     * strings and primitive values in the pool.
     * 
     * This special pool is only needed for generating sequences. For replay we can use 
     * standard Pool.
     */
    public static class G2Pool extends Pool {
    	
    	/**
    	 *  For maintaining a history of primitive values and strings that are used so far.
    	 */
    	private Map<Class,List<Serializable>> usedSPrimitives ;
    	
    	public G2Pool() { 
    	   super() ; 
    	   usedSPrimitives = new HashMap<Class,List<Serializable>>() ;
		   usedSPrimitives.put(Boolean.class, new LinkedList<Serializable>()) ;
		   usedSPrimitives.put(Byte.class, new LinkedList<Serializable>()) ;
		   usedSPrimitives.put(Short.class, new LinkedList<Serializable>()) ;
		   usedSPrimitives.put(Integer.class, new LinkedList<Serializable>()) ;
		   usedSPrimitives.put(Long.class, new LinkedList<Serializable>()) ;
		   usedSPrimitives.put(Float.class, new LinkedList<Serializable>()) ;
		   usedSPrimitives.put(Double.class, new LinkedList<Serializable>()) ;
		   usedSPrimitives.put(Character.class, new LinkedList<Serializable>()) ;
		   usedSPrimitives.put(String.class, new LinkedList<Serializable>()) ;	
    	}
    	
    	@Override
    	public void reset() { 
    		super.reset();
    		for (List<Serializable> s : usedSPrimitives.values()) s.clear();
    	}
    	
    	@Override
    	public int putSPrimitive(JType ty, Serializable u){
    		if (u == null) return -1 ;
    		Class C = ((JTfun) ty).fun ;
    		List<Serializable> s = usedSPrimitives.get(normalizeSPrimitiveTy(C)) ;
      	    if (!s.contains(u)) {
      	    	// System.out.println(">> registering a primitive in the pool: " + u);
      	    	s.add(u) ;
      	    }
      	    // well... we don't really index the primitives, so just return -1 ...
      	    return -1 ;
    	}
    }
	
	public G2ValueMG(ImplementationMap ImpsMap, Generator<PARAM,Serializable> customPrimValGenerator) {
		String chars = "abcdefghijklmnopqrstuvwxyz" ;
		chars = chars 
				+ chars.toUpperCase() 
		        + "!@#$%^&*()_+-=<>,.:;\"\'/\\" 
		        + "1234567890" ;
		charsSet = chars.toCharArray() ;
		
		// creating the Pool and instantiating some support generators:
		pool = new G2Pool() ;
		this.ImpsMap = ImpsMap ;
		constructorgens = new ObjectMG(maxLevelOfObjectsNesting,ImpsMap) ;
        collectiongens = new CollectionLikeMG(collectionSize,ImpsMap) ;
        lambdagens = new LamdaMG(ImpsMap) ;
        refgens = new REFMG(pool) ;
        this.customPrimValGenerator = customPrimValGenerator ;
	}
	
	/**
	 * To configure the list of seeded primitive values and seeded strings. String constants
	 * should be explicitly quoted as in "this is a string". A single character can be passed
	 * as as string, so for example "a".
	 */
	public void configureSeededSPrimitives(String... xs) {
		List<Boolean> booleans = new LinkedList<Boolean>() ;
		List<Byte> bytes = new LinkedList<Byte>() ;
		List<Short> shorts = new LinkedList<Short>() ;
		List<Integer> ints = new LinkedList<Integer>() ;
		List<Long> longs = new LinkedList<Long>() ;
		List<Float> floats = new LinkedList<Float> () ;
		List<Double> doubles = new LinkedList<Double>() ;
		List<Character> chars = new LinkedList<Character>() ;
		List<String> strings = new LinkedList<String>() ;
		for (String x : xs) {
			if (x.toLowerCase() == "true") {
				booleans.add(true) ;
				continue ;
			}
			if (x.toLowerCase() == "false") {
				booleans.add(false) ;
				continue ;
			}
			boolean numeric = false ;
			try { bytes.add(Byte.parseByte(x)) ; numeric = true ; }
			catch(Exception e){}
			try { shorts.add(Short.parseShort(x)) ; numeric = true ;}
			catch(Exception e){}
			try { ints.add(Integer.parseInt(x)) ; numeric = true ;}
			catch(Exception e){}
			try { longs.add(Long.parseLong(x)) ; numeric = true ;}
			catch(Exception e){}
			try { floats.add(Float.parseFloat(x)) ; numeric = true ;}
			catch(Exception e){}
			try { doubles.add(Double.parseDouble(x)) ; numeric = true ;}
			catch(Exception e){}
			if (numeric) continue ;
			// then it is a string; strip off the enclosing quotes first:
			x = x.substring(1,x.length()-1) ;
			if (x.length() == 1) chars.add(x.charAt(0)) ;
			strings.add(x) ;		
		}	
		Boolean[] booleans_ = new Boolean[booleans.size()] ;
		for (int i=0; i<booleans_.length; i++) booleans_[i] = booleans.get(i) ;
		Byte[] bytes_ = new Byte[bytes.size()] ;
		for (int i=0; i<bytes_.length; i++) bytes_[i] = bytes.get(i) ;
		Short[] shorts_ = new Short[shorts.size()] ;
		for (int i=0; i<shorts_.length; i++) shorts_[i] = shorts.get(i) ;
		Integer[] ints_ = new Integer[ints.size()] ;
		for (int i=0; i<ints_.length; i++) ints_[i] = ints.get(i) ;
		Long[] longs_ = new Long[longs.size()] ;
		for (int i=0; i<longs_.length; i++) longs_[i] = longs.get(i) ;
		Float[] floats_ = new Float[floats.size()] ;
		for (int i=0; i<floats_.length; i++) floats_[i] = floats.get(i) ;
		Double[] doubles_ = new Double[doubles.size()] ;
		for (int i=0; i<doubles_.length; i++) doubles_[i] = doubles.get(i) ;
		Character[] chars_ = new Character[chars.size()] ;
		for (int i=0; i<chars_.length; i++) chars_[i] = chars.get(i) ;
		String[] strings_ = new String[strings.size()] ;
		for (int i=0; i<strings_.length; i++) strings_[i] = strings.get(i) ;
		
		seededSPrimitives.put(Boolean.class, (Serializable[]) booleans_) ;
		seededSPrimitives.put(Byte.class, (Serializable[]) bytes_) ;
		seededSPrimitives.put(Short.class, (Serializable[]) shorts_) ;
		seededSPrimitives.put(Integer.class, (Serializable[]) ints_) ;
		seededSPrimitives.put(Long.class, (Serializable[]) longs_) ;
		seededSPrimitives.put(Float.class, (Serializable[]) floats_) ;
		seededSPrimitives.put(Double.class, (Serializable[]) doubles_) ;
		seededSPrimitives.put(Character.class, (Serializable[]) chars_) ;
		seededSPrimitives.put(String.class, (Serializable[]) strings_) ;
	}
	
	public void configureInstanceOfConstants(Class ... Cs) {
		instanceOfConstants.clear(); 
		if (Cs==null) return ;
		for (Class C : Cs) instanceOfConstants.add(C) ;
	}
	
	static private Class normalizeSPrimitiveTy(Class ty) {
		if (ty == Boolean.TYPE) return Boolean.class ;
		if (ty == Byte.TYPE) return Byte.class ;
		if (ty == Short.TYPE) return Short.class ;
		if (ty == Integer.TYPE) return Integer.class ;
		if (ty == Long.TYPE) return Long.class ;
		if (ty == Float.TYPE) return Float.class ;
		if (ty == Double.TYPE) return Double.class ;
		if (ty == Character.TYPE) return Character.class ;
		return ty ;
	}
	
	
	private boolean toss(Float chance) {
		return rnd.nextFloat() <= chance ;
	}
	
	private boolean toss() {
		return rnd.nextBoolean() ;
	}
	
	private Serializable getRandomSeededSPrimitive(Class ty) {
		ty = normalizeSPrimitiveTy(ty) ;
		Serializable[] os = seededSPrimitives.get(ty) ;
		//Serializable[] ss = seededSPrimitives.get(String.class) ;
		//for (Serializable s : ss) System.out.println(">>> constant: " + s) ;
		if (os == null || os.length == 0) return null ;
		if (os.length == 1) return os[0] ;
		return os[rnd.nextInt(os.length)] ;
	}
	
	private Serializable getRandomNeighbor(Class ty) {
		Serializable o = getRandomSeededSPrimitive(ty) ;
		if (o==null) return null ;
		return neighbor(o) ;
	}
	
	
	private Serializable neighbor(Serializable x) {
		if (x instanceof Boolean) return x ;
		if (x instanceof Byte) {
			Byte x_ = (Byte) x ;
			if (toss() && x_ < Byte.MAX_VALUE) { x_ ++ ; return x_ ; }
			x_ -- ;
			return x_ ;
		}
		if (x instanceof Short) {
			Short x_ = (Short) x ;
			if (toss() && x_ < Short.MAX_VALUE) { x_++ ; return x_ ; }
			x_ -- ; return x_ ;
		}
		if (x instanceof Integer) {
			Integer x_ = (Integer) x ;
			if (toss() && x_ < Integer.MAX_VALUE) { x_++ ; return x_ ; }
			x_ -- ; return x_ ;
		}
		if (x instanceof Long) {
			Long x_ = (Long) x ;
			if (toss() && x_ < Long.MAX_VALUE) { x_ ++ ; return x_ ; }
			x_ -- ; return x_ ;
		}
		Float delta = rnd.nextFloat() * 2.0f * epsilon - epsilon ;
		if (x instanceof Float) {
			Float x_ = (Float) x ;
			try { x_ += delta ; return x_ ; }
			catch(Exception e){ x_ -= delta ; return x_ ;} 
		}
		if (x instanceof Double){
			Double x_ = (Double) x ;
			try { x_ += delta ; return x_ ; }
			catch(Exception e){ x_ -= delta ; return x_ ; } 
		}
		if (x instanceof Character) {
			int code = Character.getNumericValue((char) x) ; 
			if (toss()) {
				code++ ;
				try { Character c = Character.valueOf((char) code) ; return c ; }
				catch(Exception e) { }
			}
			code-- ;
			try { Character c = Character.valueOf((char) code) ; return c ; }
			catch(Exception e) { return x ; }
		}
		// last case.. x must be a string
		String s = (String) x ;
		if (s.length() == 0) return "" ;
		if (toss()) return s.substring(1, s.length()) ;
		return s.substring(0,s.length()-1) ;
	}
	
	
	private Serializable getRandomPreviouslyGeneratedSPrimitive(Class ty) {
		//System.out.println(">> trying to use previous primitive: " + ty);
		ty = normalizeSPrimitiveTy(ty) ;
		List<Serializable> os = pool.usedSPrimitives.get(ty) ;
		if (os == null) return null ;
		int N = os.size() ;
		if (N==0) return null ;
		int selected = 0 ;
		if (N > 1) selected = rnd.nextInt(N) ;
		Serializable o = os.get(selected) ;
		//System.out.println(">> using previous primitive: " + o);
		return o ;
	}

	
	private Serializable randomSPrimitive(Class ty){
		ty = normalizeSPrimitiveTy(ty) ;
		if (ty == Boolean.class) { Boolean o = rnd.nextBoolean() ; return o ; }
		if (ty == Byte.class) {
			byte[] os  = new byte[1] ;
			rnd.nextBytes(os);
			Byte o = os[0] ;
			return o ;
		}
		if (ty == Short.class) {
			int x = rnd.nextInt(Short.MAX_VALUE - Short.MIN_VALUE + 1) + Short.MIN_VALUE ;
			Short o = (short) x ;
			return o ;
		}
		if (ty == Integer.class) {
			int i = rnd.nextInt(2301) - 300 ;
			return (Integer) i ;
		}
		if (ty == Long.class) {
			Long o = rnd.nextLong() ; return o ; 
		}
		if (ty == Float.class) {
			Float o = rnd.nextFloat() ; return o ;
		}
		if (ty == Double.class) {
			Double o = rnd.nextDouble() ; return o ; 
		}
		if (ty == Character.class) {
			Character o = charsSet[rnd.nextInt(charsSet.length)] ;
			return o ;
		}
		// last case is when ty is string; only generate random string up to length ...
		int n = rnd.nextInt(12) ;
		if (n==0) return "" ;
		StringBuilder s = new StringBuilder() ;
		while (n>0) {
			n-- ;
			s.append(charsSet[rnd.nextInt(charsSet.length)]) ;
		}
		return s.toString() ;
	}
		

	/**
	 * Meta Generator for primitive values and strings. This takes seeded primitives and previously
	 * generated primitives into account.
	 */
    private Generator<PARAM,STEP> sprimitiveMG() {
    	return P -> {
    	  Class ty = ((JTfun) P.ty).fun ;
    	  if (!Reflection.isPrimitiveLike(ty) && ty != String.class) return null ;
    	  Serializable o = null ;
    	  float toss = rnd.nextFloat() ;
    	  if (toss < chanceToPickPreviousPrimitive) {
    	  	  //System.out.println("===> chanceToPickPreviousPrimitive " ) ;
    		  o = getRandomPreviouslyGeneratedSPrimitive(ty) ;
    		  //System.out.println("===> REUSING " + o) ;
    	  }
    	  else {
    		  toss = toss - chanceToPickPreviousPrimitive ;
    		  if (toss < chanceToPickSeededPrimitive) {
    			  o = null ;
    			  if (customPrimValGenerator != null) {
        			  if (rnd.nextFloat() <= 0.5) {
        				 // 50% chance to pick using the custom generator
        				 Maybe<Serializable> o_ = customPrimValGenerator.generate(P) ;
        				 if(o_ != null) o = o_.val ;
        			  }
        		  }
    			  if(o==null) o = getRandomSeededSPrimitive(ty) ;
    		  }
    		  else {
    			  toss = toss - chanceToPickSeededPrimitive ;
    			  if (toss < chanceToPickNeighbor) o = getRandomNeighbor(ty) ;
    		  }
    	  }
    	  if (o==null) o = randomSPrimitive(ty) ;
    	  if (o==null) return null ;
    	  // add o to the cache of used primitive  --> now done implicitly by G2Pool
    	  //List<Serializable> s = usedSPrimitives.get(normalizeSPrimitiveTy(ty)) ;
    	  //if (!s.contains(o)) s.add(o) ;
    	  // then finally, construct the CONST representation:
    	  //System.out.println(">> generating " + o + " : " + P.ty);
    	  return new Maybe(new CONST(o,P.ty)) ;
    	}
    	;
    }
    

    /**
     * To transform a PARAM that comes into a meta-value generator (below), so that the type
     * requirement is first matched with the list of InstanceOf constants, and replaced by
     * one randomly, if there are matches found.
     */
    private PARAM remapPARAM_with_InstanceOfConstants(PARAM P) {
    	if (P == null) return null ;
    	if (P.ty instanceof JTfun) {
    		JTfun ty_ = (JTfun) P.ty ;
    		if (ty_.args.length == 0) {
    			Class C = ty_.fun ;
    			List<Class> candidates = new LinkedList<Class>() ;
    			for (Class D : instanceOfConstants) {
    				if (C.isAssignableFrom(D)) candidates.add(D) ;
    			}
    			int N = candidates.size() ;
    			if (N>0) {			
    				// the is at least one instance-of constant that can be used instead of P;
    				// choose one randomly, then later continue with standard obj generator:
    				C = candidates.get(rnd.nextInt(N)) ;
    				PARAM Q = new PARAM(P.name,new JTfun(C)) ;
    				//System.out.println("***** " + Q.ty);
    				return Q ;
    			}
    		}
    	} 
    	return P ;
   }
    
    
    static Class[] serializableTypes = {
    		String.class,
    		Integer.class,
    		Double.class,
    		SomeObject.class
    } ;
    
    static Class[] ObjectSubclasses = {
    		String.class,
    		Integer.class,
    		Double.class,
    		SomeObject.class
    } ;
    
    
   private Generator<PARAM,STEP> serializableMG(FUN<Generator<PARAM,STEP>> recGen) { 
	   return P -> {
		  if (!(P.ty instanceof JTfun)) return null ;
		  JTfun ty_ = (JTfun) P.ty ;
		  Class F = ty_.fun ;
		  if (F == Serializable.class) {
			  Class R = serializableTypes[T3Random.getRnd().nextInt(serializableTypes.length)] ;
			  PARAM Q = new PARAM(P.name,JTypeUtils.convert(R)) ;
			  return recGen.fun.generate(Q) ;
		  }
		  return null ; 
	   } ;
   }
   
   // When asked to generate literally instance of Object
   private Generator<PARAM,STEP> ObjectInstance_MG(FUN<Generator<PARAM,STEP>> recGen) { 
	   return P -> {
		  if (!(P.ty instanceof JTfun)) return null ;
		  JTfun ty_ = (JTfun) P.ty ;
		  Class F = ty_.fun ;
		  if (F == Object.class) {
			  Class R = ObjectSubclasses[T3Random.getRnd().nextInt(ObjectSubclasses.length)] ;
			  PARAM Q = new PARAM(P.name,JTypeUtils.convert(R)) ;
			  return recGen.fun.generate(Q) ;
		  }
		  return null ; 
	   } ;
   }
   
    /**
     * This will construct the meta-generator to generate values.
     */
    public Generator<PARAM,STEP> valueMG() {

    	FUN<Generator<PARAM,STEP>> recGenerator = new FUN<Generator<PARAM,STEP>>() ;
    	Generator<PARAM,STEP> g =
    			FirstOf(
                        nullgens.constant().WithChance(0.04),
                        sprimitiveMG(),
                        enumgens.random(),
                        clazzgens.classMG(recGenerator),
                        lambdagens.constlambdaMG(recGenerator),
                        serializableMG(recGenerator),
                        ObjectInstance_MG(recGenerator).WithChance(0.3),
                        refgens.objectUnderTest().WithChance(0.4),
                        refgens.random().WithChance(0.4),
                        refgens.objectUnderTestIfSubclass().WithChance(0.1),
                        collectiongens.collectionlike(recGenerator),
                        constructorgens.useConstructor(recGenerator).transformReq(P -> remapPARAM_with_InstanceOfConstants(P)).WithChance(0.4),
                        constructorgens.useConstructor(recGenerator),
                        constructorgens.useCreationyMethod(recGenerator).transformReq(P -> remapPARAM_with_InstanceOfConstants(P)).WithChance(0.4),
                        constructorgens.useCreationyMethod(recGenerator),
                        // if all the above fails.. then generate null
                        nullgens.defaulting()
                ) ;
    	recGenerator.fun = g ;
    	return g ;
    }
    
    /**
     * This will construct the meta-generator to generate values; the generator will have
     * higher probability to reuse objects from the pool.
     */
    public Generator<PARAM,STEP> valueMG_leaningTowardsReuseOldObjects() {
    	FUN<Generator<PARAM,STEP>> recGenerator = new FUN<Generator<PARAM,STEP>>() ;
    	Generator<PARAM,STEP> g =
    			FirstOf(
                        refgens.objectUnderTest().WithChance(0.35),
                        refgens.objectUnderTestIfSubclass().WithChance(0.35),
                        refgens.random().WithChance(0.35),
                        nullgens.constant().WithChance(0.05),
                        sprimitiveMG(),
                        enumgens.random(),
                        collectiongens.collectionlike(recGenerator),
                        clazzgens.classMG(recGenerator),
                        lambdagens.constlambdaMG(recGenerator),
                        serializableMG(recGenerator),
                        ObjectInstance_MG(recGenerator).WithChance(0.3),
                        constructorgens.useConstructor(recGenerator).transformReq(P -> remapPARAM_with_InstanceOfConstants(P)).WithChance(0.4),
                        constructorgens.useConstructor(recGenerator),
                        constructorgens.useCreationyMethod(recGenerator).transformReq(P -> remapPARAM_with_InstanceOfConstants(P)).WithChance(0.4),
                        constructorgens.useCreationyMethod(recGenerator),
                        // if all the above fails.. then generate null
                        nullgens.defaulting()
                ) ;
    	recGenerator.fun = g ;
    	return g ;
    }
    
    public static void main(String[] args) {
    	System.out.println(String.class.isPrimitive()) ;
    	
    }
}
