package Sequenic.T3.DerivativeSuiteGens.Gen2;

import java.io.File;
import java.nio.file.Paths;
import java.util.logging.Logger;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.ReplayCmd;
import Sequenic.T3.DerivativeSuiteGens.Gen2.G2.TimeBudgetException;

public class G2_forSBST {
	
	static class TimeOut extends Thread {
    	 long TIMEOUT ; 
    	 TimeOut(long timeout) { this.TIMEOUT = timeout ; }
    	 
        public void run() {
           try {
               sleep(TIMEOUT);
               Logger.getLogger(CONSTANTS.T3loggerName).warning("** G2 has timeout! (" + TIMEOUT
            		       + "). Performing system exit...");
               System.exit(-1);
           }
           catch (Exception e) { /* the sleep was interrupted... ok. */ }
        }
   }
	
	static void createDir(String path) {
		File d = new File(path) ;
		if (!d.exists()) d.mkdirs() ;
	}
	
	static void generate(
			String CUTname, 
			String CUTrootDir, // the dir of the binaries
			String traceDir, 
			String junitDir, 
			long timebudget,
			String worklisttype,
			String refinementHeuristic,
			int maxNumberOfTargetRefinements,
			boolean turnOnCoverageGuidance,
			boolean turnOnUseOfStaticInfo
			
		) throws TimeBudgetException, InterruptedException {
		TimeOut hardtimeOut = new TimeOut(timebudget+10000) ;
		hardtimeOut.start(); 
		
		G2Config config = new G2Config() ;
		config.CUTrootDir = CUTrootDir ;
		createDir(traceDir) ;
		config.dirOfStaticInfo = traceDir ;
		config.dirToSaveSuites = traceDir ;
		config.dirToSaveJunit = junitDir ;
		config.injectOracles = true ;
		config.regressionMode = true ;
		config.worklistType = worklisttype ;
		config.refinementHeuristic = refinementHeuristic ;
		config.generateJunitForEachSuite = true ;
		config.maxNumberOfRefinements_ofEachTarget = maxNumberOfTargetRefinements ;
		//config.numberOfCPUcores = numberOfcores ;
		config.useCoverageGuidance = turnOnCoverageGuidance ;
		config.useStaticInfo = turnOnUseOfStaticInfo ;
		config.maxPrefixLength = 6 ;
		config.maxSuffixLength = 1 ;
		config.numberOfPrefixes = 10 ; // number of prefixes to generate whenever we want to increase them
		config.includePrivateAndDefaultMembers = true ; // force private members to be tested as well...
		G2.generateSuites(CUTname, config, timebudget) ;
		System.exit(-1);
	}
	
	/**
	 * Not used, but maybe useful for debugging the deployment.
	 */
	static void replayAll(String CUTname, String traceDir) throws Throwable {
		ReplayCmd.main("--bulk=trace(.*)" + CUTname + "(.*)tr --regressionmode " + traceDir) ;
	}
	
	
	/**
	 * arg0 : generate or replayall
	 * 
	 * If arg0 is generate, then:
	 *    arg1 CUTname
	 *    arg2 CUTroot-directory (binary)
	 *    arg3 tracefile-directory
	 *    arg4 junitDir
	 *    arg5 time-budget in ms
	 *    arg6 worklist-type:  standard/random/lowcovfirst
	 *    arg7 trace-refinement-heuristic: random/evo
	 *    arg8 the maximum number of times each test-target will be refined
	 *    arg9 whether or not to use code-coverage guidance
	 *    arg10 whether or not to use staticinfo.txt
	 *    
	 *    
	 *if arg0 is replayall:
	 *   arg1 CUTname
	 *   arg2 tracefile-directory
	 */
	static public void main(String[] args) throws Throwable {
		if (args[0].equals("generate")) {
			if (args.length != 11) throw new IllegalArgumentException() ;
			long timebudget = Integer.parseInt(args[5]) ;
			int maxNumberOfTargetRefinements = Integer.parseInt(args[8]) ;
			boolean useCoverageGuide = Boolean.parseBoolean(args[9]) ;
			boolean useStaticInfo = Boolean.parseBoolean(args[10]) ;
			generate(args[1],args[2],args[3],args[4],timebudget, args[6], args[7], 
					maxNumberOfTargetRefinements, 
					useCoverageGuide,
					useStaticInfo) ;
			return ;
		}
		if (args[0].equals("replayall")) {
			replayAll(args[1],args[2]) ;
			return ;
		}
		throw new IllegalArgumentException("Invalid selector!") ;
	}

}
