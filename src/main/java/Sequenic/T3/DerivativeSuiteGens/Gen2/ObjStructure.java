/*
 * Copyright 2016 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.DerivativeSuiteGens.Gen2;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import Sequenic.T3.Reflection.Reflection;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.Show;
import Sequenic.T3.Pool;
import Sequenic.T3.Examples.SimpleIntSortedList;
import Sequenic.T3.Examples.Friends.*;


/**
 * To represent an object structure. For now, it will be a tree.
 * Circles will be cutoff, with the back pointer replaced by
 * a pointer to a class name. Two isomorphic objects will have
 * equal tree-representation, but not necessarily the other way
 * around.
 */
public class ObjStructure {

    Node root = new Node() ;
    // for statistics:
    private Integer depth = null ;
    public Integer getDepth() {
    	if (depth == null) {
    		depth = root.getDepth() ;
    	}
    	return depth ;
    }
    private Double  diversitySum = null ;
    public Double getDiversitySum() {
    	if (diversitySum == null) {
    		diversitySum = root.diversityMetric() ;
    	} 
    	return diversitySum ;
    }
    private Double avrgBranchingDegree = null ;
    public Double getAvrgBranchingDegree() {
    	if(avrgBranchingDegree == null) {
    		avrgBranchingDegree = root.getBranchingDegrees().stream().collect(Collectors.averagingDouble(i -> (double) i)) ;
    	}
    	return avrgBranchingDegree ;
    }
    
	static public class Node {
		List<Arrow> next = new LinkedList<Arrow>() ;
	    Node() { }
	    void linkto(Node n, String fname) {
	    	Arrow a = new Arrow() ;
	    	a.fieldname = fname ;
	    	a.target = n ;
	    	next.add(a) ;
	    }
	    
	    Node getChild(String fname) {
	    	for (Arrow a : next) {
	    		if (a.fieldname.equals(fname)) return a.target ;
	    	}
	    	return null ;
	    }
	    
	    public boolean equals(Object o) {
	    	if (o==null) return false ;
	    	if (! (o instanceof Node)) return false ;
	    	Node p = (Node) o ;
	    	if (next.size() != p.next.size()) return false ;
	    	for (Arrow a : next) {
	    		Node x = a.target ;
	    		Node y = p.getChild(a.fieldname) ;
	    		if (y==null) return false ;
	    		if (! x.equals(y)) return false ;
	    	}
	    	return true ;
	    }
	    
	    void normalize(List<String> seenValues) {
	    	for (Arrow a : next) a.target.normalize(seenValues) ;
	    }
	    
	    int getDepth() {
	    	if (next.isEmpty()) return 1 ;
	    	return 1 + next.stream().map(arrow -> arrow.target.getDepth()).max(Comparator.naturalOrder()).get() ;
	    }
	    
	    double diversityMetric() {
	    	if (next.isEmpty()) return 0d ;
	    	return next.stream().collect(Collectors.summingDouble(arrow -> arrow.target.diversityMetric())) ;
	    }
	    
	    List<Integer> getBranchingDegrees() {
	    	List<Integer> result = new LinkedList<Integer>() ;
	    	result.add(next.size()) ;
	    	for(Arrow A : next) {
	    		result.addAll(A.target.getBranchingDegrees()) ;
	    	}
	    	/*
	    	System.out.print(">>>> branch degrees: ") ;
	    	for (Integer k : result) {
	    		System.out.print(", " + k) ;
	    	}
	    	System.out.println("") ; */
	    	return result ;
	    }
	    
	    void show(StringBuilder s, int indent) {
			s.append(" -->") ;
			int k = 0 ;
			for (Arrow a : next) {
				a.show(s,indent+2) ;
			}
		}
	}
	
	static public class Terminal extends Node{
		String value = null ;
		Terminal(){ }
		Terminal(String v) { value = v ; }
		public boolean equals(Object o) {
			if (o == null) return false ;
			if (o instanceof Terminal) {
				Terminal p = (Terminal) o ;
				if (value == null) return p.value == null ;
				else return value.equals(p.value) ;
			}
			return false ;
		}
		 List<Integer> getBranchingDegrees() {
			 List<Integer> result = new LinkedList<Integer>() ;
			 //result.add(0) ;
			 return result ;
		 }
		 
		void show(StringBuilder s, int indent) {
			s.append(" : " + value) ;
		}
		
		// return log2(x), rounded down.
		private int log2(Long x) {
			// x is assumed to be non-negative
			return 63 - x.numberOfLeadingZeros(x) ;
		}
		
		int getDepth() { return 0 ; }
		double diversityMetric() { return strDiversity(value) ; }
		
		void normalize(List<String> seenValues) {
			if (value == null) return ;
			if (value.equals("") || value.equals("0") || value.equals("1")
					|| value.equals("-1") || value.equals("2")
					|| value.equals("true") || value.equals("false")
					|| value.equals("0.05") || value.equals("0.01")
					) return ; 
			
			String absval = null ;
			try {
				Long x = Long.parseLong(value) ;
				if (0<x) absval = "+Z" + log2(x) ;
				else absval = "-Z" + log2(x) ;
			}
			catch(NumberFormatException e) {
				try {
					Long x = Math.round(Double.parseDouble(value)) ;
					if (0<x) absval = "+R" + log2(x) ;
					else absval = "-LR" + log2(x) ;
				}
				catch(NumberFormatException f) {}
			}
			if (absval==null) absval = "S" + value.length() ;
			int k = 0 ;
			for (String v : seenValues) {
				if (v.equals(value)) {
					value = "@" + k + ":" + absval ; return ;
				}
				k++ ;
			}
			seenValues.add("" + value) ;
			value = "@" + (seenValues.size() - 1) + ":" + absval ;
			
		}
	}
	
	static private void indent(StringBuilder s, int k) {
		for (int i=0; i<k; i++) s.append(" ") ;
	}
	
	static public class Arrow {
		String fieldname ;
		Node target ;
		public void show(StringBuilder s, int indent) {
			s.append("\n") ;
			indent(s,indent) ;
			s.append(fieldname) ;
			target.show(s,indent);
		}
	}
	
	private ObjStructure() {}
	
	public ObjStructure(Object o, int maxdepth) {
		super() ;
		List visited = new LinkedList() ;
		constructWorker(maxdepth,o,root,"root",visited) ;
		//depth = root.getDepth() ;
		//diversitySum = root.diversityMetric() ;
	}
	
	private static void constructWorker(int depth, Object o, Node parent, String fname, List visited) {
		if (o == null) {
			parent.linkto(new Terminal(), fname);
			return ;
		}
		Class C = o.getClass() ;
		if (Reflection.isPrimitiveLike(C) || C == String.class || C.isEnum()) {
			Terminal t = new Terminal(o.toString()) ;
			parent.linkto(t, fname);
			return ;
		}
		if (visited.contains(o) || depth==0) {
			Terminal t = new Terminal(C.getName()) ;
			parent.linkto(t, fname);
			return ;
		}
		visited.add(o) ;
		depth-- ;
		Node n = new Node() ;
		parent.linkto(n, fname);
		if (C.isArray()) {
			int N = Array.getLength(o) ;
			for (int k=0; k<N; k++) {
				constructWorker(depth,Array.get(o,k),n,"elem_"+k, visited) ; 
			}
			return ;
		}
		if (Collection.class.isAssignableFrom(C)) {
			Collection a = (Collection) o ;
			int k = 0 ;
			for (Object y : a) {
				constructWorker(depth,y,n,"elem_"+k, visited) ; 
				k++ ;
			}
			return ;
		}
		if (Map.class.isAssignableFrom(C)) {
			Map m = (Map) o ;
			int k = 0 ;
			for (Object e_ : m.entrySet()) {
				Entry e = (Entry) e_ ;
				constructWorker(depth,e.getKey(),n,"key_"+k, visited) ; 
				constructWorker(depth,e.getValue(),n,"val_"+k, visited) ; 
				k++ ;
			}
			return ;
		}
		List<Field> fields = Show.getAllNonStaticFields(o.getClass()) ;
		for (Field f : fields) {
			f.setAccessible(true);
			try { 
				Object p = f.get(o) ;
				constructWorker(depth,p,n,f.getName(),visited) ;
			}
			catch(Exception e) {
				constructWorker(0,"???",n,f.getName(),visited) ;
			}
		}
		return ;
	}
	
	public boolean equals(Object o) {
		if (o==null) return false ;
		if (!(o instanceof ObjStructure)) return false ;
		ObjStructure p = (ObjStructure) o ;
		return root.equals(p.root) ;
	}
	
	public String toString() { 
		StringBuilder s = new StringBuilder() ;
		root.show(s,2);
		return s.toString() ;
	}
	
	/**
	 * Normalize this tree, in such a way so that we abstract individual 
	 * values to some logarithmic-scaled ranges; and we still want to
	 * identify duplicated values.
	 */
	public void normalize() {
		root.normalize(new LinkedList<String>());
	}
	
	public static List<ObjStructure> nub(List<ObjStructure> z) {
		List<ObjStructure> result = new LinkedList<ObjStructure>() ;
		for (ObjStructure o : z) {
			if (result.contains(o)) continue ;
			result.add(o) ;
		}
		return result ;
	}
	
	/**
	 * Count the number of unique normalized ObjStructure representations of the final 
	 * states of the target objects produced by the given test suite S. 
	 */
	static public int countDifferentTargetObjs(SUITE S, Class CUT, int maxdepth) {
		return getTargetObjsStrutures(S,CUT,maxdepth).size() ;
	}
	
	/**
	 * Return the list of unique normalized ObjStructure representations of the final states 
	 * of the target objects produced by the given test suite S. 
	 * Duplicates ObjStructures are removed.
	 */
	static public List<ObjStructure> getTargetObjsStrutures(SUITE S, Class CUT, int maxdepth) {
		Pool pool = new Pool() ;
		List<ObjStructure> tobjs = new LinkedList<ObjStructure>() ;
		for (SEQ seq : S.suite) {
			ObjStructure t = toObjsStruture(seq,CUT,pool,maxdepth) ;
			if (t!=null) tobjs.add(t) ;
		}
		return nub(tobjs) ;
	}
	
	
	/**
	 * Get the normalized ObjStructure representation of the final state of the
	 * target object produced by the given test sequence.
	 */
	static public ObjStructure toObjsStruture(SEQ seq, Class CUT, Pool pool, int maxdepth) {
		try {
			SEQ_RT_info info = seq.exec(CUT, pool) ;
			Object tobj = info.lastInfo.objectUnderTest ;
		    if (tobj != null) {
		    	ObjStructure t = new ObjStructure(tobj,maxdepth) ;
		    	t.normalize();
		    	return t ;
		    }
		    return null ;
		}
		catch(Exception e) { return null ; }
	}
	
	
	/**
	 * Get a metric of a string. It is 1 + the sum of the diversity of its elements.
	 */
	static Double strDiversity(String s) {
		if (s==null) return 0d ;
		char[] s_ = s.toCharArray() ;
		int N = s_.length ;
		if(N == 0) return 0d ;
		int minchar = (int) s_[0] ;
		int maxchar = (int) s_[0] ;
		for (int k = 1 ; k<N ; k++) {
			int c = (int) s_[k] ;
			if (c<minchar) minchar = c ;
			if (c>maxchar) maxchar = c ;
		}
		int delta = maxchar - minchar ;
		if (delta==0) return 1d ;
		int r = 0 ;
		for (int k =  0 ; k<N ; k++) {
			int c = (int) s_[k] ;
			try { r += c - minchar ; }
			catch(Throwable e) { }
		}
		return 1d + ((double) r) / ((double) delta) ;
	}
	
	static public void main(String[] args) {
		ObjStructure t ;
		ObjStructure u ;
		int[] a = {1,2,3} ;
		int[] b = {1,2,3} ;
		t = new ObjStructure(a,4) ;
		u = new ObjStructure(b,4) ;
		
		System.out.println(t);
		System.out.println(u);
		System.out.println(t.equals(u));
		
		Person p = new Person("Foo", new Email("Foo@blob.org")) ;
		p.emails.add(new Email("Foo@blob.org")) ;
		t = new ObjStructure(p,3) ;
		System.out.println(t.toString());
		t.normalize();
		System.out.println(t.toString());
		
		SimpleIntSortedList z = new SimpleIntSortedList() ;
		z.insert(1);
		z.insert(100);
		z.insert(2);
		ObjStructure zo = new ObjStructure(z,4) ;
		System.out.println(zo.depth) ;
		System.out.println(zo.diversitySum) ;
		System.out.println(zo.toString());
		

	}
	
}
