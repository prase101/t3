/*
 * Copyright 2016 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.DerivativeSuiteGens.Gen2;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Pool;
import Sequenic.T3.T3Random;
import Sequenic.T3.Sequence.Datatype.SEQ;
import Sequenic.T3.Sequence.Datatype.SUITE;

/**
 * Provide a more powerful functionality (than G2SuiteGen) to generate a suite for 
 * a single target, which is either a method or constructor in the CUT. It offers 
 * for example ability to refine the target's current suite.
 */
public class SingleTarget {

	public G2Config config;
	protected G2SuiteGen g2sg;
	public Object target; // a Method or a Constructor ;
	public boolean isADT; // when true, target is a target for ADT testing
	public SUITE suite = null; // best suite we have so far for the target
	
	public int ID = 0 ;

	// maximum number this suite can be refined
	protected int maxNumberOfRefinement ;
	// minimum coverage to strive
	protected double minimumCovTobeHappy ;

	// history of achieved coverage over iterations:
	public List<Double> targetcov;
	public List<Double> classcov;
	
	// for additional statistics:
	public int totNumberOfTriedSequences = 0 ;
	
	static protected Logger t3log = Logger.getLogger(CONSTANTS.T3loggerName) ;
			
	protected SingleTarget(){}
	
	private SingleTarget(G2Config config, G2SuiteGen g2sg) {
		targetcov = new LinkedList<Double>() ; targetcov.add(0.0) ; 
		classcov  = new LinkedList<Double>() ; classcov.add(0.0) ; 
		this.config = config ;
		this.g2sg = g2sg ;	
		maxNumberOfRefinement = config.maxNumberOfRefinements_ofEachTarget ;
		minimumCovTobeHappy = config.minimumCovTobeHappy_ofEachTarget ;
	}
		
	public SingleTarget(G2Config config, G2SuiteGen g2sg, Method target) {
		this(config,g2sg) ;
		this.target = target ;
		if (Modifier.isStatic(target.getModifiers())) isADT = false ;
		else isADT = true ;
	}
	
	public SingleTarget(G2Config config, G2SuiteGen g2sg, Constructor target) {
		this(config,g2sg) ;
		this.target = target ;
		this.isADT = true ;
	}
		
	public int numberOfIterations() { return targetcov.size() - 1 ; }
	
	/**
	 * Return the sum of the difference between the last target and class
	 * coverage, and the previous one.
	 */
	public double getImprovement() { 
		int N = targetcov.size() ;
		if (N <= 1) return 0 ;
		double td = targetcov.get(N-1) - targetcov.get(N-2) ;
		double cd = classcov.get(N-1) - classcov.get(N-2) ;
		return 	td + cd ;
	}
	
	public double getLastTargetCov() { return targetcov.get(targetcov.size() - 1) ; }
	public double getLastClassCov()  { return classcov.get(classcov.size() - 1) ; }
	
		
		/**
		 * Return the number of parameters of the target method/constructor.
		 */
	public int getNumOfParams() {
		if (target instanceof Constructor) {
			return ((Constructor) target).getParameterCount() ;
		}
		return ((Method) target).getParameterCount() ;
	}
		
	public String getName() {
		if (target instanceof Method) return ((Method) target).getName() ;
		return ((Constructor) target).getName() ;
	}
	
	public String getUniqueName() {
		return "" + getName() + "_" + getNumOfParams() 
		       + "_id" + ID ; 
	}
		
	private static int power(int e, int k) {
		int r = 1 ;
		while (k>0) { r = r*e ; k-- ; }
		return r ;
	}
		
	/**
	 * This is used to generate a fresh suite for this target. 
	 */
	protected SUITE generateFreshSuite() {
		if (target instanceof Method) {
			Method m = (Method) target ;
			if (isADT) {
				// g2sg.scope.configureForADTtesting(); this will be done in the generate below
				SUITE prefixes = g2sg.getPrefixes() ;
				
				// take a selection of these prefixes:
				SUITE selected = prefixes ;
				int selectedMaxSize = 50 ;
				if (prefixes != null && prefixes.suite.size() > selectedMaxSize) {
					selected = new SUITE() ;
					selected.suitename = prefixes.suitename ;
					selected.CUTname = prefixes.CUTname ;
					int K = selectedMaxSize/3 ;
					SEQ[] array_ = prefixes.suite.toArray(new SEQ[0]) ;
					int N = array_.length ;
					// always include the last selectedMaxSize/3, since these are more recently added
					for (int k=0; k<K; k++) {
						selected.suite.add(array_[N-k-1]) ;
					}
					// randomly select the rest:
					for (int k=K; k<N; k++) {
						selected.suite.add(array_[T3Random.getRnd().nextInt(N-K)]) ; 
					}
				}
					
				int multiplier = 2 ;
				if (selected != null) totNumberOfTriedSequences += multiplier*selected.suite.size() ;
				return g2sg.generateSuiteForASingleMethod(selected,m,multiplier) ;
			}
			else {
				// g2sg.scope.configureForNonADTTesting(); this will be done in the generate below
				Integer N = config.singleMethodTestSuiteSize ;
				if (N == null) {
					N = Math.max(50,power(4,m.getParameterCount())) ;
				}
				totNumberOfTriedSequences += N ;
				return g2sg.generateSuiteForASingleStaticMethod(m,N) ;
			}
		}
		// else target is a constructor
		// g2sg.scope.configureForADTtesting(); ; this will be done in the generate below
		Constructor co = (Constructor) target ;
		Integer N = config.singleMethodTestSuiteSize ;
		if (N == null) {
			N = Math.max(50,power(4,co.getParameterCount())) ;
		}
		totNumberOfTriedSequences += N ;
		return g2sg.generateSuiteForConstructor(co,N) ;
	}
		
	private double getFreshTargetCoverage() {
		if (g2sg.codeCoverage == null) return 0 ;
		String CUTname = g2sg.scope.CUT.getName() ;
		if (target instanceof Method) {
			Method m = (Method) target ;
			return g2sg.codeCoverage.getCoverageInfo().getCov(CUTname, m, "ratio") ;
		}
		Constructor co = (Constructor) target ;
		return g2sg.codeCoverage.getCoverageInfo().getCov(CUTname, co, "ratio") ;
	}
		
	private double getFreshClassCoverage() {
		if (g2sg.codeCoverage == null) return 0 ;
		return g2sg.codeCoverage.getCoverageInfo().getClassCov(g2sg.scope.CUT.getName(),"ratio") ;
	}
		
	/**
		 * Refine this target's suite. If it has none, a fresh suite will be generated.
		 * If S is the current suite, to refine it a fresh suite T will be generated.
		 * However, only those sequences in T that actually improve the target or class
		 * coverage of S will be added to S.
=		 */
	void refine()  {
		if (suite == null) {
			// suite was null, generate it for the first time:
			t3log.info("Generating 1st suite for " + getName());
			suite = generateFreshSuite() ; 
			if (suite == null) {
				t3log.warning("Fail to generate initial suite for " + getName()) ;
				//System.out.println(">>> " + numberOfIterations());
				classcov.add(0.0) ;
				targetcov.add(0.0) ;
				return ;
			}
			if (g2sg.codeCoverage == null) {
				classcov.add(0.0) ;
				targetcov.add(0.0) ;
				return ;
			}
			try {
			   Pool pool = new Pool() ;  
			   g2sg.codeCoverage.clear();
			   suite.exec(pool,g2sg.codeCoverage.instrumenter, null,0,0,false,true,true,null) ;
			   g2sg.codeCoverage.collectRTdata();
			   g2sg.codeCoverage.analyze() ;
			   double cc = getFreshClassCoverage() ;
			   double tc = getFreshTargetCoverage() ;
			   classcov.add(cc) ; 
			   targetcov.add(tc) ;
		    }
			catch(Exception e) { 
				//e.printStackTrace();
				t3log.warning("Fail to obtain code coverage information for " + getName()) ; 
				classcov.add(0.0) ;
				targetcov.add(0.0) ;
			}
			return ;
		}
		
		// Suite was not null, (so this is not the first iteration) so refine it
		SUITE T = generateFreshSuite() ;
		if (T == null) {
			classcov.add(getLastClassCov() ) ;
			targetcov.add(getLastTargetCov()) ;
			return ;
		}
		t3log.info("Refining suite for " + getName() + ", iteration " + numberOfIterations());
		if (g2sg.codeCoverage == null) {
			// no Jacoco!, then add the whole T
			suite.suite.addAll(T.suite) ;
			classcov.add(getLastClassCov()) ;
			targetcov.add(getLastTargetCov()) ;
			return ;
		}
		
		double overallCov0 = getLastClassCov() ;
		double targetCov0  = getLastTargetCov() ;
		Pool pool = new Pool() ;  
		try {
		   g2sg.codeCoverage.clear();
		   suite.exec(pool,g2sg.codeCoverage.instrumenter,null,0,0,false,true,true,null) ;
		   g2sg.codeCoverage.collectRTdata();
		   g2sg.codeCoverage.analyze() ;
		   double cc = getFreshClassCoverage() ;
		   double tc = getFreshTargetCoverage() ;
		   overallCov0 = Math.max(overallCov0, cc) ;
		   targetCov0  = Math.max(targetCov0, tc) ;
		}
		catch(Exception e) { }
		for (SEQ seq : T.suite) {
			try {
			  seq.exec(g2sg.scope.CUT,pool) ;
			  g2sg.codeCoverage.collectRTdata();
			  g2sg.codeCoverage.analyze() ;
			  double cc = getFreshClassCoverage() ;
			  double tc = getFreshTargetCoverage() ;
			  if (cc > overallCov0 || tc > targetCov0) {
				 suite.suite.add(seq) ;
				 overallCov0 = Math.max(overallCov0, cc) ;
				 targetCov0 = Math.max(targetCov0, tc) ;
			  }
			}
			catch (Exception e) {
			   //e.printStackTrace(); 
			   t3log.warning("Failing to execute a sequence, or fail to get coverage info from a sequence.");	
			   //throw new Error() ;
			}
		}	
		classcov.add(overallCov0) ;
		targetcov.add(targetCov0) ;
		return ;
	}
	
	/**
	 * Return a minimized version of the current test suite.
	 */
	SUITE minimize()  {
		if (suite==null) {
			t3log.warning("Test suite minimalization is asked to minimize a null SUITE");
			return null ;
		}
		if (g2sg.codeCoverage == null) {
			// cannot minimize without code coverage guidance
			t3log.warning("Test suite minimalization is ignored because Jacoco instrumentation fails or is not present.");
			return suite ;
		}
		SUITE S = new SUITE(suite.CUTname) ;
		S.suitename = suite.suitename ;
		
		List<SEQ> dropped = new LinkedList<SEQ>() ;
		
		try { double overallCov = 0.0 ;
		      double targetCov  = 0.0 ;
		      Pool pool = new Pool() ;  
			  g2sg.codeCoverage.clear();
			  for (SEQ seq : suite.suite) {
			     try { seq.exec(g2sg.scope.CUT,pool) ;
				 	   g2sg.codeCoverage.collectRTdata();
					   g2sg.codeCoverage.analyze() ;
					   double cc = getFreshClassCoverage() ;
					   double tc = getFreshTargetCoverage() ;
					   if (cc > overallCov || tc > targetCov) {
						  S.suite.add(seq) ;
						  overallCov = cc ;
						  targetCov = tc ;
					   }
					   else dropped.add(seq) ;
				 }
				 catch (Exception e) { /* the test sequence crashes ... don't add it */ }
			  }
		}
		catch(Exception e) { /* coverage instrumentation fails */ 
			t3log.warning("Test suite minimalization is ignored because Jacoco instrumentation fails.");
            return suite ; 
        }
		
		int ND = dropped.size() ;
		// put back about 10% of dropped suite:
		int putback = ND/10 ;
		for (int k=0; k<putback; k++) {
			int i = T3Random.getRnd().nextInt(dropped.size()) ;
			S.suite.add(dropped.get(i)) ;
			dropped.remove(i) ;
		}
		t3log.info("Constructing a minimized SUITE: from #" + suite.suite.size() + " to #" + S.suite.size());
		return S ;
	}
				
	/**
	 * Save the suite S to a trace file. Generate a JUnit class wrapper if requested.
	 * Swallow exception!
	 */
	public void saveSuite() { saveSuiteWorker(suite) ; }
	
	/**
	 * First minimize the suite before saving it.
	 */
	public void saveMinimizedSuite() { saveSuiteWorker(minimize()) ; }
	
	private void saveSuiteWorker(SUITE S) {	
		if (config.dirToSaveSuites == null) return ;
		if (S == null) return ;
		try {
		  String trname = "trace_" + suite.suitename ;
		  // save the suite:
	      String traceFilePath = S.save(config.dirToSaveSuites, trname, false);
	      
	      if (config.generateJunitForEachSuite) {
	    	  String testClassName = "Test_" + suite.suitename ;
	    	  SUITE.mkJunitTestFile(config.dirToSaveJunit, 
	    			                testClassName, 
	    			                traceFilePath, 
	    			                config.showLength, config.showDepth, config.showExcExecution, 
	    			                config.regressionMode) ;
	      }
	      t3log.info("=== Saving suite for " + getName() + "(" + getNumOfParams() + ")"
					+ ", iteration:" + numberOfIterations()
					+ ", #size:" + S.suite.size()
					+ ", tcov:"  + getLastTargetCov()
					+ ", ccov:"  + getLastClassCov()
					);
	      
		}
		catch(Exception e) {
			e.printStackTrace();
			t3log.warning("Problem in saving test suite " + S.suitename + ", or its JUnit testclass.");
		}
	}

	
}
