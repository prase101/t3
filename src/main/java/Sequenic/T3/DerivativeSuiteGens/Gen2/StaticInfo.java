package Sequenic.T3.DerivativeSuiteGens.Gen2;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.nio.file.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Logger;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.ImplementationMap;
import Sequenic.T3.TestingScope;
import Sequenic.T3.utils.Pair;

/**
 * Provide some extended static information about a CUT. This class does not do the
 * static analysis itself, but will read the info from some info file provided
 * by the actual static analyzer.
 */
public class StaticInfo {

	Class CUT ;
	TestingScope scope ;
	ImplementationMap imap ;
	private List<String> allConstants_ = new LinkedList<String>() ;
	String[] allConstants = {} ;
	private List<String> constantsOfPrivateMethods_ = new LinkedList<String>() ;
	String[] constantsOfPrivateMethods = {} ;
	private Map<Object,List<String>> constants_ = new HashMap<Object,List<String>>() ;
	// mapping methods and constructors to constants
	Map<Object,String[]> constants = new HashMap<Object,String[]>() ;
	private List<Class> allInstanceOfClasses_ = new LinkedList<Class>() ;
	Class[] allInstanceOfClasses = {} ;
	private List<Class> instanceOfClassesOfPrivateMethods_ = new LinkedList<Class>() ;
	Class[] instanceOfClassesOfPrivateMethods = {} ;
	private Map<Object,List<Class>> instanceOfClasses_ = new HashMap<Object,List<Class>>() ;
	// mapping methods and constructors to instanceof-classes
	Map<Object,Class[]> instanceOfClasses = new HashMap<Object,Class[]>() ;
	
	/**
	 * Will search for the file staticinfo.txt in the given directory. This file is assumed
	 * to list all numeric and string constants/literals in the CUT, as well as class names
	 * mentioned in the instanceof expressions. These information will be read and put
	 * into the new instance of this class.
	 */
	public StaticInfo(TestingScope scope, ImplementationMap imap, String dir) throws IOException {
		this.CUT = scope.CUT ;
		this.scope = scope ;
		this.imap = imap ;
		readConstants(dir) ;
	}
	
	//public StaticInfo(TestingScope scope, ImplementationMap imap) throws IOException {
	//	this.CUT = scope.CUT ;
	//	this.scope = scope ;
	//	this.imap = imap ;
	//}
	
	/**
	 * Read the constants info from an info-file. 
	 * NOTE: the algorithm assumes the constants are arranged in lines; thus assuming
	 * each constant not to contain new-line characters. This is fragile, and should
	 * be replaced in the future. TODO.
	 */
	private void readConstants(String dir) throws IOException {
		// read staticinfo.txt into lines:
		Path path =FileSystems.getDefault().getPath(dir,"staticinfo.txt") ;
		String[] lines  = Files.readAllLines(path).toArray(new String[0]) ;
		//System.out.println(">>> " + lines.length);
		
		// now parse the lines:
		// skip the first line, it says the class name; we assume this is staticinfo of the right class
		// skip the second line, it says numeric constants title
		int head = 2 ;
		// parse numeric constants
		List<Pair<String,List<String>>> rows = new LinkedList<Pair<String,List<String>>>() ;
		head = parseConstantsLines(head,lines,rows) ;
		for (Pair<String,List<String>> constants : rows) registerConstants(constants,"numeric") ;
		// skip the title line
		head++ ;
		rows.clear(); ;
		head = parseConstantsLines(head,lines,rows) ;
		for (Pair<String,List<String>> constants : rows) registerConstants(constants,"string") ;
		// skip the title line
		head++ ;
		rows.clear() ; 
		head = parseConstantsLines(head,lines,rows) ;
		for (Pair<String,List<String>> constants : rows) registerConstants(constants,"instanceof") ;
		
		allConstants = allConstants_.toArray(new String[0]) ;
		constantsOfPrivateMethods = constantsOfPrivateMethods_.toArray(new String[0]) ;
		
		for (Entry<Object,List<String>> e : constants_.entrySet()) {
			String[] cs = e.getValue().toArray(new String[0]) ;
			constants.put(e.getKey(), cs) ;
		}
		
		allInstanceOfClasses = allInstanceOfClasses_.toArray(new Class[0]) ;
		instanceOfClassesOfPrivateMethods = instanceOfClassesOfPrivateMethods_.toArray(new Class[0]) ;
		
		for (Entry<Object,List<Class>> e : instanceOfClasses_.entrySet()) {
			Class[] cs = e.getValue().toArray(new Class[0]) ;
			instanceOfClasses.put(e.getKey(), cs) ;
		}
	}
	
	// parse a group of constant-lines
	private int parseConstantsLines(int headPtr, String[] lines, List<Pair<String,List<String>>> result) {
		// move header ahead until the first "->"
		String s = null ;
		while (headPtr < lines.length) {
			if (lines[headPtr].startsWith("-> ")) {
				s = lines[headPtr] ;
				break ;
			}
			if (lines[headPtr].startsWith("*")) return headPtr ;
			headPtr++ ;
		}
		if (s == null) return headPtr ;	
		Pair<String,List<String>> row = parseConstantsLine(s) ;
		if (row != null) result.add(row) ;
		return parseConstantsLines(headPtr+1,lines,result) ;
	}
	
	// parse a single constants-line from staticinfo.txt
	private Pair<String,List<String>> parseConstantsLine(String s) {
		List<String> constants = new LinkedList<String>() ;
		String methodName = null ;
		try {
		  int split =  s.indexOf(':') ; 
		  methodName = s.substring(3, split-1) ; 
		  s = s.substring(split+1) ;
		
  		  while(s.indexOf(';')>=0) {
			 split = s.indexOf(';') ;
			 String c = s.substring(1,split) ;
			 constants.add(c) ;
			 s = s.substring(split+1) ;
		  } 
		}
		catch (Throwable t) { 
			Logger.getLogger(CONSTANTS.T3loggerName).warning("Problem in parsing staticinfo.txt");
		}
		if (methodName!=null)return new Pair(methodName,constants) ;
		return null ;
	}
	
	private void registerConstants(Pair<String,List<String>> constants, String type) {
		//System.out.println(">> " + constants.snd);
		for (String c : constants.snd) {
			registerConstant(constants.fst,c,type) ;
			// for integer constant, also register c-1 and c+1 border values:
			if (type == "numeric") {
			  try {
				int i = Integer.parseInt(c.substring(1,c.length()-1)) ;
				int iplus = i+1 ;
				int imin = i-1 ;
				registerConstant(constants.fst, "<" + iplus + ">" , type) ;
				registerConstant(constants.fst, "<" + imin + ">"  , type) ;
			  }
			  catch(Exception e) { }
			}
		}
	}
	
	private void registerConstant(String mname, String constant, String type) {		
		//System.out.println(">>>" + constant + " : " + type);
		if (constant.length() < 2) return ;
		constant = constant.substring(1,constant.length()-1) ;
		if (type.equals("string")) {
			// ignore if the string constant if it is too looooong:
			if (constant.length() > 16) return ;
			
			constant = "\"" + constant + "\"" ;
		}
		if (! type.equals("instanceof")) {
			// for numeric and string constants:
			allConstants_.add(constant) ;
			boolean added = false ;
			for (Method m : scope.getAccessibleMethods()) {
				//System.out.println(">> matching " + m.getName() + " vs " + mname);
				if (m.getName().equals(mname)) {
					List<String> cs = constants_.get(m) ;
					if (cs == null) {
						cs = new LinkedList<String>() ;
						constants_.put(m,cs) ;
					}
					cs.add(constant) ;
					added = true ;
				}
			}
			if (!added) {
				for (Constructor constr : scope.getAccessibleConstructors()) {
					// this can sometime cause problem in combination with Jacoco:
					//String cname = constr.getDeclaringClass().getSimpleName() ;
					String cname = constr.getName() ;
					int k = cname.lastIndexOf('.') ;
					if (k>=0) cname = cname.substring(k+1) ;
					k = cname.lastIndexOf('$') ;
					if (k>=0) cname = cname.substring(k+1) ;
					if (cname.equals(mname)) {
						List<String> cs = constants_.get(constr) ;
						if (cs == null) {
							cs = new LinkedList<String>() ;
							constants_.put(constr,cs) ;
						}
						cs.add(constant) ;
						added = true ;
					}
				}
			}
			if (! added) constantsOfPrivateMethods_.add(constant) ;
			return ;
		}
		// else then it is an "e instanceOf C" constant
		// since the constant only specifies C's simple name, we will look it up in
		// the implementation map to try to figure out the actual C, then add it into
		// the list of constants:
		boolean found = false ;
		for (Entry<Class,ImplementationMap.Instantiation> e : imap.knownClasses.entrySet()) {
			Class D = e.getKey() ;
			//System.out.println(">> matching " + D.getName() + " vs " + constant);
			boolean match = false ;
			try {
				match = D.getSimpleName().equals(constant) ; // hackish... this may throw exception!!
			}
			catch(Throwable t) { }
		    if (match) {
		    	allInstanceOfClasses_.add(D) ;	
		    	found = true ;
		    	boolean added = false ;
				for (Method m : scope.getAccessibleMethods()) {
					if (m.getName().equals(mname)) {
						List<Class> cs = instanceOfClasses_.get(m) ;
						if (cs == null) {
							cs = new LinkedList<Class>() ;
							instanceOfClasses_.put(m, cs) ;
						}
						cs.add(D) ;
						added = true ;
					}
				}
				if (!added) {
					for (Constructor constr : scope.getAccessibleConstructors()) {
						if (constr.getDeclaringClass().getSimpleName().equals(mname)) {
							List<Class> cs = instanceOfClasses_.get(constr) ;
							if (cs == null) {
								cs = new LinkedList<Class>() ;
								instanceOfClasses_.put(constr,cs) ;
							}
							cs.add(D) ;
							added = true ;
						}
					}
				}
				if (! added) instanceOfClassesOfPrivateMethods_.add(D) ;
			}
		}
		if (!found) Logger.getLogger(CONSTANTS.T3loggerName).warning("Cannot load class " + constant);
	}
	
	public void toString(StringBuilder s) {
		s.append("* All constants: ") ;
		for (String c : allConstants) s.append(c + "; ") ;
		s.append("\n* All constants in non-visible methods: ") ;
		for (String c : constantsOfPrivateMethods) s.append(c + "; ") ;
		s.append("\n* Constants per method in CUT: ") ;
		for (Entry<Object,String[]> e : constants.entrySet()) {
			if (e.getKey() instanceof Method)
			    s.append("\n   " + ((Method) e.getKey()).getName() + ": ") ;
			else
				s.append("\n   " + ((Constructor) e.getKey()).getName() + ": ") ;
			for (String c : e.getValue()) s.append(c + "; ") ;
		}
		s.append("\n* All instanceOfs: ") ;
		for (Class C : allInstanceOfClasses) s.append(C.getName() + "; ") ;
		s.append("\n* All instanceOfs in non-visible methods: ") ;
		for (Class C : instanceOfClassesOfPrivateMethods) s.append(C.getName() + "; ") ;
		s.append("\n* Instanceofs per method in CUT: ") ;
		for (Entry<Object,Class[]> e : instanceOfClasses.entrySet()) {
			if (e.getKey() instanceof Method)
			   s.append("\n   " + ((Method) e.getKey()).getName() + ": ") ;
			else
			   s.append("\n   " + ((Constructor) e.getKey()).getName() + ": ") ;
			for (Class C : e.getValue()) s.append(C.getName() + "; ") ;
		}
	}
	
	@Override 
	public String toString() {
		StringBuilder s = new StringBuilder() ;
		toString(s) ; return s.toString() ;
	}
	
	public String[] getConstantsOfPrivateMethods() { return constantsOfPrivateMethods ; }
	public Class[] getInstanceOfClassesOfPrivateMethods(){ return instanceOfClassesOfPrivateMethods ; }
	public String[] getAllConstansts() { return allConstants ; }
	public Class[] getAllInstanceOfClasses() { return allInstanceOfClasses ; }
	
	public String[] getConstants(Method M) { return constants.get(M) ; }
	public Class[] getInstanceOfClasses(Method M){ return instanceOfClasses.get(M) ; }
	public String[] getConstants(Constructor c) { return constants.get(c) ; }
	public Class[] getInstanceOfClasses(Constructor c){ return instanceOfClasses.get(c) ; }
	
	// test
	static public void main(String[] args) throws Exception {
		String CUTname = "SomeExamples.Triangle1" ;
		String CUTdir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3_externalExamples/bin" ;
		G2SuiteGen g2 = new G2SuiteGen(CUTname,CUTdir) ;
		String StaticInfoDir = "/Users/iswbprasetya/tmp/t3" ;
		StaticInfo info = new StaticInfo(g2.scope,g2.imap,StaticInfoDir) ;
		System.out.println(info.toString());
	}
}
