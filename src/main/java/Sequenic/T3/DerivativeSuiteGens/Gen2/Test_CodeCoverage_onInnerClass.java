package Sequenic.T3.DerivativeSuiteGens.Gen2;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import Sequenic.T3.Info.CodeCoverage;
import Sequenic.T3.Info.JacocoInstrumenter;


public class Test_CodeCoverage_onInnerClass {
	
	static void test1() throws Exception {
		// Using CUT in a separated root-dir:
		//String rootDir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3_externalExamples/bin" ;
		//JacocoInstrumenter ji = new JacocoInstrumenter(rootDir,"SomeExamples.InnerClass.Logic") ;

		// Using CUT in a shared root-dir; CAREFUL! Might be because something in T3-lib refer to it
		String rootDir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		JacocoInstrumenter ji = new JacocoInstrumenter(rootDir,"Sequenic.T3.Examples.InnerClass.Logic") ;
		CodeCoverage CC = new CodeCoverage(ji) ;
		Class CUT = ji.getInstrumentedCUT() ;
		System.out.println(CUT.getName());
		
		CC.clear() ;
		CC.collectRTdata();
		CC.analyze() ;
		CC.getCoverageInfo().print();
		
		Constructor LogicCo = CUT.getDeclaredConstructor(Integer.TYPE) ;
		Method test = CUT.getDeclaredMethod("test", Boolean.TYPE) ;
		Object logic = LogicCo.newInstance(10) ;
		test.invoke(logic,false) ;
		
		CC.collectRTdata();
		CC.analyze() ;
		CC.getCoverageInfo().print();

		//JacocoInstrumenter.shutDownJacocoLogger();
		
		// Using CUT in a separated root-dir:
		// rootDir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3_externalExamples/bin" ;		
		// ji = new JacocoInstrumenter(rootDir,"SomeExamples.InnerClass.Logic$AND") ;
		
		// Using CUT in a shared root-dir; CAREFUL!
		rootDir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
	    ji = new JacocoInstrumenter(rootDir,"Sequenic.T3.Examples.InnerClass.Logic$AND") ;
				
		CUT = ji.getInstrumentedCUT() ;
		System.out.println(CUT.getName());
		CC = new CodeCoverage(ji) ;
		
		CC.clear();
		CC.collectRTdata();
		CC.analyze() ;
		CC.getCoverageInfo().print();
		
		Constructor AndCo = CUT.getDeclaredConstructor() ;
		Method valid = CUT.getDeclaredMethod("valid") ;
		Object and = AndCo.newInstance() ;
		
		
		CC.collectRTdata();
		CC.analyze() ;
		CC.getCoverageInfo().print();
		
		valid.invoke(and) ;
		
		CC.collectRTdata();
		CC.analyze() ;
		CC.getCoverageInfo().print();
	}
	
	public static void main(String[] args) throws Exception {
		test1() ; 
	}

}
