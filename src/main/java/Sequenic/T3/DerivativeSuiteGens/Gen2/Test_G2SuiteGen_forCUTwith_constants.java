package Sequenic.T3.DerivativeSuiteGens.Gen2;

import Sequenic.T3.ReplayCmd;
import Sequenic.T3.T3Cmd;
import Sequenic.T3.Sequence.Datatype.SUITE;

public class Test_G2SuiteGen_forCUTwith_constants {

	static void genWithG2() throws Exception{
		String staticInfoDir = "/Users/iswbprasetya/tmp/t3" ;
		Class CUT = Sequenic.T3.Examples.IncomeTax.class ;
		G2SuiteGen g2 = new G2SuiteGen(CUT.getName(),null,true,true,staticInfoDir,null) ;
		g2.maxSuffixLength = 0 ;
		g2.regressionMode = true ;
		int N0 = Math.min(500, Math.max(10, g2.staticInfo.allConstants.length * 20)) ;
		System.out.println(">> N0 = " + N0);
		g2.incrementallyGeneratePrefixes(N0,1,5) ;
		SUITE S = g2.getPrefixes() ;
		System.out.println(S.showSuiteStatistics());
		System.out.println("** Variations: " + ObjStructure.countDifferentTargetObjs(S,CUT,5)) ;
		
		//SUITE T = g2.generateSuiteForASingleMethod(S, "getTax", 2) ;
		SUITE T = g2.generateSuiteForASingleMethod(S, "equals", 2) ;
		System.out.println(T.showSuiteStatistics());
		System.out.println("** Variations: " + ObjStructure.countDifferentTargetObjs(T,CUT,5)) ;
		
		T.save("/Users/iswbprasetya/tmp/t3", "testsuite_IncomeTax", false) ;
	}
	
	static void genWithT3() throws Throwable {
		T3Cmd.main("--regressionmode --adtOption=true --savedir=/Users/iswbprasetya/tmp/t3 --msamples=10 "
				   + "Sequenic.T3.Examples.IncomeTax");
	}
	
	public static void replayG2() throws Throwable {
		ReplayCmd.main("--runall /Users/iswbprasetya/tmp/t3/testsuite_IncomeTax.tr") ;
	}
	
	static public void main(String[] args) throws Throwable {
		//genWithT3() ;
		//genWithG2() ;
		replayG2() ;
	}
}
