package Sequenic.T3.DerivativeSuiteGens.Gen2;

import Sequenic.T3.ReplayCmd;
import Sequenic.T3.Examples.Friends.Person;
import Sequenic.T3.Sequence.Datatype.SUITE;

public class Test_G2SuiteGen_forConstructor {

	public static void generate() throws Exception{
		// using shared root-dir.. hope this works:
		String CUTname = "Sequenic.T3.Examples.Friends.Person" ;
		String CUTrootdir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		G2SuiteGen g2 = new G2SuiteGen(CUTname,CUTrootdir) ;
		Class CUT = g2.scope.CUT ;
		
		//g2.configureSeededPrimitives("0","3","\"test\"");
		
		g2.maxSuffixLength = 0 ;
		SUITE S = g2.generateSuiteForConstructor(10) ;
		System.out.println(S.showSuiteStatistics());
		System.out.println("** Variations: " + ObjStructure.countDifferentTargetObjs(S,CUT,5)) ;
		S.save("/Users/iswbprasetya/tmp/t3", "testsuite_2", false) ;
	}

	public static void replay() throws Throwable {
		ReplayCmd.main("--runall /Users/iswbprasetya/tmp/t3/testsuite_2.tr") ;
	}

	
	public static void main(String[] args) throws Throwable {
		//generate() ;
		replay() ;
	}
}
