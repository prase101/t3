package Sequenic.T3.DerivativeSuiteGens.Gen2;

import Sequenic.T3.ReplayCmd;
import Sequenic.T3.T3Cmd;
import Sequenic.T3.Sequence.Datatype.SUITE;

public class Test_G2SuiteGen_forSingleMethod {
	
	static void genWithG2() throws Exception{
		// using shared root-dir.. careful
		String CUTname = "Sequenic.T3.Examples.Triangle1" ;
		String CUTrootdir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		G2SuiteGen g2 = new G2SuiteGen(CUTname,CUTrootdir) ;
		Class CUT = g2.scope.CUT ;
		//g2.configureSeededPrimitives("0","3","\"test\"");
		g2.maxSuffixLength = 0 ;
		g2.regressionMode = true ;
		//SUITE S = g2.generatePrefixes(200,3) ;
		g2.incrementallyGeneratePrefixes(10,3,5) ;
		SUITE S = g2.getPrefixes() ;
		System.out.println(S.showSuiteStatistics());
		System.out.println("** Variations: " + ObjStructure.countDifferentTargetObjs(S,CUT,5)) ;
		
		SUITE T = g2.generateSuiteForASingleMethod(S, "isIsoleces", 2) ;
		System.out.println(T.showSuiteStatistics());
		System.out.println("** Variations: " + ObjStructure.countDifferentTargetObjs(T,CUT,5)) ;
		
		T.save("/Users/iswbprasetya/tmp/t3", "testsuite_3", false) ;
	}
	
	static void replayG2() throws Throwable {
		ReplayCmd.main("--runall /Users/iswbprasetya/tmp/t3/testsuite_3.tr") ;
	}
	
	static void genWithT3() throws Throwable {
		T3Cmd.main("--regressionmode --adtOption=true --savedir=/Users/iswbprasetya/tmp --msamples=10 "
				   + "Sequenic.T3.Examples.Triangle1");
	}
	
	public static void main(String[] args) throws Throwable {
		//genWithT3() ;
		//replayT3() ;
		genWithG2() ; 
		//replayG2() ;
	}

}
