package Sequenic.T3.DerivativeSuiteGens.Gen2;

import Sequenic.T3.ReplayCmd;
import Sequenic.T3.T3Cmd;
import Sequenic.T3.Examples.Staticx.StringUtils;
import Sequenic.T3.Sequence.Datatype.SUITE;

public class Test_G2SuiteGen_forSingleStaticMethod {

    static void genWithG2() throws Exception{
		// using shared root-dir.. careful
		String CUTname = "Sequenic.T3.Examples.Staticx.StringUtils" ;
		String CUTrootdir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		G2SuiteGen g2 = new G2SuiteGen(CUTname,CUTrootdir) ;
		g2.configureSeededSPrimitives("0","3","test");
		g2.maxSuffixLength = 0 ;
		g2.regressionMode = true ;
		SUITE S = g2.generateSuiteForASingleStaticMethod("drop", 30) ;
		System.out.println(S.showSuiteStatistics());
		S.save("/Users/iswbprasetya/tmp/t3", "testsuite", false) ;
	}

	static void genWithT3() throws Throwable {
		T3Cmd.main("--regressionmode --adtOption=false --savedir=/Users/iswbprasetya/tmp/t3 "
				   + "Sequenic.T3.Examples.Staticx.StringUtils");
	}

	static void replayG2() throws Throwable {
		ReplayCmd.main("--runall /Users/iswbprasetya/tmp/t3/testsuite.tr") ;
	}

	static void replayT3() throws Throwable {
		ReplayCmd.main("--runall /Users/iswbprasetya/tmp/t3/nonADT_Sequenic.T3.Examples.Staticx.StringUtils__1451311223238.tr") ;
	}

	
	public static void main(String[] args) throws Throwable {
		//replayT3() ;
	    //genWithG2() ; 
		replayG2() ;
	}
}
