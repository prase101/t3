package Sequenic.T3.DerivativeSuiteGens.Gen2;

import Sequenic.T3.Sequence.Datatype.SUITE;

public class Test_G2SuiteGen_prefixes {

	static void genPrefixes() throws Exception{
		// using shared root-dir.. careful
		String CUTname = "Sequenic.T3.Examples.Item" ;
		String CUTrootdir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		G2SuiteGen g2 = new G2SuiteGen(CUTname,CUTrootdir) ;
		Class CUT = g2.scope.CUT ;
		//g2.configureSeededPrimitives("0","3","test");
		g2.maxSuffixLength = 0 ;
		g2.regressionMode = true ;
		//SUITE S = g2.generatePrefixes(200,3) ;
		g2.incrementallyGeneratePrefixes(10,3,5) ;
		SUITE S = g2.getPrefixes() ;
		System.out.println(S.showSuiteStatistics());
		System.out.println("** Variations: " + ObjStructure.countDifferentTargetObjs(S,CUT,5)) ;
		
		SUITE T = g2.generateSuiteForASingleMethod(S, "setCode", 2) ;
		System.out.println(T.showSuiteStatistics());
		System.out.println("** Variations: " + ObjStructure.countDifferentTargetObjs(T,CUT,5)) ;
	}
	
	public static void main(String[] args) throws Exception{
		genPrefixes() ;
	}
}
