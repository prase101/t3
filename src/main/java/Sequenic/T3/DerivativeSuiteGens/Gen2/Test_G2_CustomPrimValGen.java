package Sequenic.T3.DerivativeSuiteGens.Gen2;

import static Sequenic.T3.DerivativeSuiteGens.Gen2.CustomGenDSL.* ;

public class Test_G2_CustomPrimValGen {
	
	static G2Config config() {
		G2Config config = new G2Config() ;
		config.regressionMode = true ;
		config.CUTrootDir =  "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		config.dirToSaveSuites = "/Users/iswbprasetya/tmp/t3" ;
		config.dirOfStaticInfo = "/Users/iswbprasetya/tmp/t3" ;
		config.generateJunitForEachSuite = false ;
		config.maxPrefixLength = 6 ;
		config.maxSuffixLength = 1 ;
		config.numberOfPrefixes = 40 ;
		// config.refinementHeuristic="evo" ;
		return config ;
	}
	
	
	
	static void genWithG2() throws Exception {
		G2Config config = config() ;
		config.usePrimitiveGenerator(OneOfVal_(777f,707f)) ;
		G2.generateSuites("Sequenic.T3.Examples.Triangle1",config,60000) ;
	}
	
	
	static public void main(String[] args) throws Throwable {
		genWithG2() ;
	}

}
