package Sequenic.T3.DerivativeSuiteGens.Gen2;

import Sequenic.T3.ReplayCmd;

public class Test_G2_SimpleIntSortedList {
	
	static G2Config config() {
		G2Config config = new G2Config() ;
		config.regressionMode = true ;
		config.CUTrootDir =  "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		config.dirToSaveSuites = "/Users/iswbprasetya/tmp/t3" ;
		config.dirOfStaticInfo = "/Users/iswbprasetya/tmp/t3" ;
		config.generateJunitForEachSuite = false ;
		config.maxPrefixLength = 6 ;
		config.maxSuffixLength = 1 ;
		config.numberOfPrefixes = 10 ;
		// config.refinementHeuristic="evo" ;
		return config ;
	}
	
	static void genWithG2() throws Exception {
		G2.generateSuites("Sequenic.T3.Examples.SimpleIntSortedList",config(),120000) ;
	}

	public static void replayG2Regression() throws Throwable {
		ReplayCmd.main("--bulk=trace(.*)SimpleIntSortedList(.*)tr --regressionmode /Users/iswbprasetya/tmp/t3") ;
	}
	
	public static void main(String[] args) throws Throwable {
		genWithG2() ;
		//replayG2Regression() ;
	}

}
