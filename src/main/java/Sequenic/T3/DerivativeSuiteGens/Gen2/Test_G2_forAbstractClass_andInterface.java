package Sequenic.T3.DerivativeSuiteGens.Gen2;

import Sequenic.T3.ReplayCmd;

public class Test_G2_forAbstractClass_andInterface {

	static G2Config config() {
		G2Config config = new G2Config() ;
		config.regressionMode = true ;
		config.CUTrootDir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		config.dirToSaveSuites = "/Users/iswbprasetya/tmp/t3" ;
		config.dirToSaveJunit = "/Users/iswbprasetya/tmp/t3" ;
		config.dirOfStaticInfo = "/Users/iswbprasetya/tmp/t3" ;
		return config ;
	}
	static void genWithG2_1() throws Exception {	
		G2.generateSuites("Sequenic.T3.Examples.InterfaceAndAbstract.Vehicle",config(),60000) ;
	}
	
	static void genWithG2_2() throws Exception {
		G2.generateSuites("Sequenic.T3.Examples.InterfaceAndAbstract.IStudent", config(),60000) ;
	}
	
	public static void replayG2() throws Throwable {
		ReplayCmd.main("--bulk=trace(.*)Interface(.*)tr --runall /Users/iswbprasetya/tmp/t3") ;
	}
	
	static public void main(String[] args) throws Throwable {
	    //genWithG2_1() ;
		genWithG2_2() ;
		
	    //replayG2() ;
	}
	
}
