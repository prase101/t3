package Sequenic.T3.DerivativeSuiteGens.Gen2;

import java.lang.reflect.Constructor;

import Sequenic.T3.Pool;
import Sequenic.T3.ReplayCmd;
import Sequenic.T3.T3Cmd;
import Sequenic.T3.Examples.InnerClass.Logic;
import Sequenic.T3.JavaType.JType;
import Sequenic.T3.JavaType.JTypeUtils;
import Sequenic.T3.Sequence.Datatype.CONSTRUCTOR;
import Sequenic.T3.Sequence.Datatype.STEP;

public class Test_G2_forClass_withInnerClasses {

	
	// NOTE need to generate staticinfo.txt first!
	static G2Config config() {
		G2Config config = new G2Config() ;
		config.regressionMode = true ;
		config.CUTrootDir =  "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3_externalExamples/bin" ;
		config.dirToSaveSuites = "/Users/iswbprasetya/tmp/t3" ;
		config.dirOfStaticInfo = "/Users/iswbprasetya/tmp/t3" ;
		return config ;
	}
		
	static void genWithG2() throws Exception {
		G2.generateSuites("SomeExamples.InnerClass.Logic",config(),60000) ;
	}
	
	static void test_one_bareG2() throws Exception {
		G2 g2 = new G2("SomeExamples.InnerClass.Logic",config()) ;
		g2.generateAndRefine();
		System.out.println(g2.g2sg.getPrefixes().showSuiteStatistics());
	}
	
	static void test_all_bareG2() throws Exception {
		G2 g2 = new G2("SomeExamples.InnerClass.Logic",config()) ;
		g2.generateAndRefine();
		System.out.println(g2.g2sg.getPrefixes().showSuiteStatistics());
		
		g2 = new G2("SomeExamples.InnerClass.Logic$AND",config()) ;
		g2.generateAndRefine();
		System.out.println(g2.g2sg.getPrefixes().showSuiteStatistics());
		
		g2 = new G2("SomeExamples.InnerClass.Logic$FF",config()) ;
		g2.generateAndRefine();
		System.out.println(g2.g2sg.getPrefixes().showSuiteStatistics());
		
		g2 = new G2("SomeExamples.InnerClass.Logic$TT",config()) ;
		g2.generateAndRefine();
		System.out.println(g2.g2sg.getPrefixes().showSuiteStatistics());
		
		g2 = new G2("SomeExamples.InnerClass.Logic$AND$X",config()) ;
		g2.generateAndRefine();
		System.out.println(g2.g2sg.getPrefixes().showSuiteStatistics());
		
		g2 = new G2("SomeExamples.InnerClass.Logic$XX",config()) ;
		g2.generateAndRefine();
		System.out.println(g2.g2sg.getPrefixes().showSuiteStatistics());
	}
	
	static void genWithT3() throws Throwable {
		T3Cmd.main("--regressionmode --adtOption=true --savedir=/Users/iswbprasetya/tmp/t3 "
				   + "Sequenic.T3.Examples.InnerClass.Logic");
	}
	
	public static void replayG2() throws Throwable {
		ReplayCmd.main("--bulk=trace(.*)Inner(.*)tr --runall /Users/iswbprasetya/tmp/t3") ;
	}
	
	static public void main(String[] args) throws Throwable {
		genWithG2() ;
		//test_one_bareG2() ;
		//test_all_bareG2() ;
		//replayG2() ;
		//genWithT3() ;

	}
}
