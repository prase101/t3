package Sequenic.T3.DerivativeSuiteGens.Gen2;

import Sequenic.T3.ReplayCmd;

public class Test_G2_forTriangle {
	
	// NOTE need to generate staticinfo.txt first!
	static G2Config config() {
		G2Config config = new G2Config() ;
		config.regressionMode = true ;
		config.CUTrootDir =  "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		config.dirToSaveSuites = "/Users/iswbprasetya/tmp/t3" ;
		config.dirOfStaticInfo = "/Users/iswbprasetya/tmp/t3" ;
		config.generateJunitForEachSuite = false ;
		config.maxPrefixLength = 6 ;
		config.maxSuffixLength = 1 ;
		config.numberOfPrefixes = 10 ;
		// config.refinementHeuristic="evo" ;
		return config ;
	}
	
	static void test_one_bareG2() throws Exception {
		G2 g2 = new G2("Sequenic.T3.Examples.Triangle1",config()) ;
		//g2.g2sg.incrementallyGeneratePrefixes(100, 10, 1000, 3, 5);
		//g2.refinePrefixes();
		g2.generateAndRefine();
		System.out.println(g2.g2sg.getPrefixes().showSuiteStatistics());
	}
	
	static void test_two_bareG2() throws Exception {
		G2 g2 = new G2("Sequenic.T3.Examples.Triangle1",config()) ;
		g2.generateAndRefine();
		System.out.println(g2.g2sg.getPrefixes().showSuiteStatistics());
		
		g2 = new G2("Sequenic.T3.Examples.Triangle1",config()) ;
		g2.generateAndRefine();
		System.out.println(g2.g2sg.getPrefixes().showSuiteStatistics());
	}
	
	static void genWithG2() throws Exception {
		G2.generateSuites("Sequenic.T3.Examples.Triangle1",config(),60000) ;
	}
	
	public static void replayG2() throws Throwable {
		ReplayCmd.main("--bulk=trace(.*)Triangle(.*)tr --runall /Users/iswbprasetya/tmp/t3") ;
	}
	
	public static void replayG2Regression() throws Throwable {
		ReplayCmd.main("--bulk=trace(.*)Triangle(.*)tr --regressionmode /Users/iswbprasetya/tmp/t3") ;
	}
	
	static public void main(String[] args) throws Throwable {
		//test_one_bareG2() ;
		//test_two_bareG2() ;
	    genWithG2() ;
		//replayG2Regression() ;
	}
}
