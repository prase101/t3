package Sequenic.T3.DerivativeSuiteGens.Gen2;

// Tetsing that G2 generate prefixes where each prefix tries not to call
// the same method more than twice.
public class Test_G2_generating_prefix_with_minimal_duplicate {
	
	
	static G2Config config() {
		G2Config config = new G2Config() ;
		config.regressionMode = true ;
		config.CUTrootDir =  "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		config.dirToSaveSuites = "/Users/iswbprasetya/tmp/t3" ;
		config.dirOfStaticInfo = "/Users/iswbprasetya/tmp/t3" ;
		config.generateJunitForEachSuite = false ;
		config.refinementHeuristic="evo" ;
		config.maxPrefixLength = 7 ;
		config.maxSuffixLength = 0 ;
		config.numberOfPrefixes = 10 ;
		return config ;
	}
	
	static void genWithG2() throws Exception {
		G2.generateSuites("Sequenic.T3.Examples.Item",config(),5000) ;
	}

	
	static public void main(String[] args) throws Throwable {
		genWithG2() ;
	}
}
