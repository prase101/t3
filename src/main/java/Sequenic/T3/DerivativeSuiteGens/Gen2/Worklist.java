/*
 * Copyright 2016 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.DerivativeSuiteGens.Gen2;

import java.lang.reflect.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * A worklist maintains a list of "targets" for G2 generator. For each target, a test
 * suite needs to be generated, and possibly refined. Each target maintains the suite
 * generated for it so far. A worklist does not generate suites; it used to control
 * the order in which targets are processed.
 */
public class Worklist {
	
	// all the targets, including those that have been processed. We keep
	// the for e.g. collecting statistics.
	protected List<SingleTarget> allTargets = new LinkedList<SingleTarget>() ;
	
	protected List<SingleTarget> activeTargets = new LinkedList<SingleTarget>() ;
	protected List<SingleTarget> waiting = new LinkedList<SingleTarget>() ;
	protected Random rnd = new Random() ;
	
	public Worklist() { }
	
	/**
	 * This decides the order in which targets are sorted. Override this to impose
	 * a different ordering. True if t1 is favored over t2.
	 */
	protected boolean isFavoredOver(SingleTarget t1, SingleTarget t2) {
		if (t1.isADT  && !t2.isADT) return false ;
		if (!t1.isADT && t2.isADT)  return true ;
		// both ADT or both not ADT
		if (t1.getNumOfParams() == t2.getNumOfParams()) {
			return t1.getLastTargetCov() < t2.getLastTargetCov()  ;
		}
		return t1.getNumOfParams() < t2.getNumOfParams() ;
	}
	
	public static enum TargetQuality { GOOD_ENOUGH, STAGNATE, TOBE_REFINED }
	
	/**
	 * Determine when a target's suite is considered to be good enough.
	 * If a suite is already good enough, the worklist will not put it
	 * back in the list. Override this method to define a different concept
	 * of "good enough".
	 */
	protected TargetQuality checkQuality(SingleTarget target) {
		// progressively lower the minimum-coverage requirement:
		double progressiveMinimumCoverage = target.minimumCovTobeHappy ;
		int Nmax = target.maxNumberOfRefinement ;
		if (Nmax > 1) {
			double loweringCorrection = Math.max(0,1.0 - progressiveMinimumCoverage)/(double)(Nmax - 1) ;
			progressiveMinimumCoverage = 1d - (double) target.numberOfIterations()*loweringCorrection ;
		}		
		if (target.getLastTargetCov() >= progressiveMinimumCoverage) 
			return TargetQuality.GOOD_ENOUGH ;
		if (target.numberOfIterations()  >= Nmax)
			return TargetQuality.STAGNATE ;
		return TargetQuality.TOBE_REFINED ;
	}
	
	/**
	 * To initially add targets to the worklist, before it is being used.
	 * (it should be added to the waiting list, not to the target list immediately)
	 */
	
	public void addTarget(SingleTarget target) { 
		target.ID = allTargets.size() ;
		allTargets.add(target) ;
		waiting.add(target) ; 
	}	
	
	/**
	 * Check if the target is already good enough. If so, return true. If not,
	 * put it back in the waiting list, and return false.
	 */
	public TargetQuality evaluateAndPutBack(SingleTarget target)   { 
		TargetQuality quality = checkQuality(target) ;
		if (quality == TargetQuality.TOBE_REFINED) waiting.add(target) ; 
		//System.out.println(">>> putting back " + target.getName());
		return quality ;
	}
	
	public boolean isEmpty() { return activeTargets.isEmpty() && waiting.isEmpty() ; }
	
	public boolean hasADTtarget() {
		return activeTargets.stream().anyMatch(T -> T.isADT) || waiting.stream().anyMatch(T -> T.isADT) ;
	}
	
	/**
	 * Remove all ADT targets (e.g. because we can't generate prefixes).
	 */
	public void removeADTtargets() {
		activeTargets.removeIf(T -> T.isADT) ;
		waiting.removeIf(T -> T.isADT) ;
	}
	
	/**
	 * To innitialize the worklist, before using it.
	 */
	public void initialize() { sort() ; }
	
	/**
	 * Retrieve the next target from the work list. Pass the remaining time budget (in
	 * terms of the ratio of what is left of it, 0..1). Null is returned if there is
	 * no more target in the worklist. This implementation return a random target if
	 * the budget is still at least 0.5, and otherwise it gives the first one in the
	 * list. Override this if a different way of selecting target is wanted.
	 */
	public SingleTarget getNext(double remainingTimeBudget) {
		if (isEmpty()) return null ;
		if (activeTargets.isEmpty()) {
			activeTargets.addAll(waiting) ;
			waiting.clear();
			sort() ;
		}
		if (remainingTimeBudget >= 0.5) {
			int k = rnd.nextInt(activeTargets.size()) ;
			SingleTarget T = activeTargets.remove(k) ;
			return T ;
		}
		SingleTarget T = activeTargets.remove(0) ;
		return T ;
	}
		
	protected void sort() {
		SingleTarget[] ts = activeTargets.toArray(new SingleTarget[0]) ;
		activeTargets.clear() ; 
		int N = ts.length ;
		for (int i=0; i<N ; i++) {
			 int best = i ;
			 for (int j=i ; j<N ; j++) {
				 if (isFavoredOver(ts[j],ts[best])) best = j ;
			 }
			 activeTargets.add(ts[best]) ;
			 SingleTarget tmp = ts[i] ;
			 ts[i] = ts[best] ; ts[best] = tmp ;
		}
	}	
}