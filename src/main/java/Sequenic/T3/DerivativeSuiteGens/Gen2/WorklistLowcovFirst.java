/*
 * Copyright 2016 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.DerivativeSuiteGens.Gen2;

public class WorklistLowcovFirst extends Worklist {
	
	public WorklistLowcovFirst() { super() ; }
	
	@Override
	protected boolean isFavoredOver(SingleTarget t1, SingleTarget t2) {
		return t1.getLastTargetCov() < t2.getLastTargetCov() ;
	}
	
	@Override
	public SingleTarget getNext(double remainingTimeBudget) {
		if (isEmpty()) return null ;
		if (activeTargets.isEmpty()) {
			activeTargets.addAll(waiting) ;
			waiting.clear();
			sort() ;
		}
		return activeTargets.remove(0) ;
	}

}
