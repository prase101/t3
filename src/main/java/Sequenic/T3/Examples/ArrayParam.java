package Sequenic.T3.Examples;

public class ArrayParam {
	
    public static int[] f1(double[] s) { 
       System.out.print(">> f1(double[] x) with x = ") ;
       if (s==null) System.out.println("null") ;
       else{
    	   for(int i=0 ; i<s.length; i++) System.out.print(" " + s[i]) ;
    	   System.out.println("") ;
       }
       return null ; 
    }

    public static Object[] f2(Object[] objs) { 
    	System.out.print(">> f2(Object[] x) with x = ") ;
        if (objs==null) System.out.println("null") ;
        else{
     	   for(int i=0 ; i<objs.length; i++) {
     		   if (objs[i] == null) System.out.print("null") ;
     		   else System.out.print(" " + objs[i] + " ," + objs[i].getClass().getName()) ;
     	   }
     	   System.out.println("") ;
        }
    	return null ;
   }

   public static int f3(String[] s) {  
	   System.out.print(">> f3(String[] x) with x = ") ;
       if (s==null) System.out.println("null") ;
       else{
    	   for(int i=0 ; i<s.length; i++) System.out.print(" " + s[i]) ;
    	   System.out.println("") ;
       }
	   return 0 ; 
   }
   
   public static <T> int f4(T[] s) {  
	   System.out.print(">> <T> f4(T[] x) with x = ") ;
       if (s==null) System.out.println("null") ;
       else{
    	   for(int i=0 ; i<s.length; i++) {
    		   if (s[i] == null) System.out.print("null") ;
    		   else System.out.print(" " + s[i] + " ," + s[i].getClass().getName()) ;
    	   }
    	   System.out.println("") ;
       }
	   return 0 ; 
   }
}
