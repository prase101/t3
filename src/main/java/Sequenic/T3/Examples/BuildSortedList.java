package Sequenic.T3.Examples;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class BuildSortedList implements Serializable {

    List<Integer> a ;

    public BuildSortedList() {
        a = new LinkedList<Integer>() ;
    }

    public void add(int x) {
        for (Integer y : a) {
            if (y > x) throw new IllegalArgumentException() ;
        }
        a.add(x) ;
    }


}
