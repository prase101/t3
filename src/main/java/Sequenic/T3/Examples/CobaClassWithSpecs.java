package Sequenic.T3.Examples;

public class CobaClassWithSpecs {
	
	private boolean classinv__() { 
		System.out.println(">>>> classinv()") ; 
		return x==0 ; 
	}
	
	public CobaClassWithSpecs() {}
	
	int x = 0 ;
	
	public int m(){ 
		System.out.println(">>>> m()") ;
		x++ ;
		return 999 ; 
	}
	
	private boolean pre() { 
		System.out.println(">>>> checking pre-cond") ; return true ;
	}
	
	private boolean post() { 
		System.out.println(">>>> checking post-cond") ; return true ;
	}
	
	public int m_spec() {
		System.out.println(">>>> invoking m_spec()") ;
		assert pre() : "PRE" ;
		int v = m() ;
		assert post() ;
		System.out.println(">>>> leaving m_spec()") ;
		return v ;
	}
	
	
	

}
