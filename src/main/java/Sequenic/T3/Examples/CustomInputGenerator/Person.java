package Sequenic.T3.Examples.CustomInputGenerator;

public class Person {

	private String name ;
	private String email ;
	private int age;
	private int code ;
	
	public Person(String name, String email, int age, int code) {
		System.out.println(">>"
				+ "Person(" + name
				+ "," + email
				+ "," + age
				+ "," + code
				+ ")"
				) ;
		this.name = name ;
		this.email  = email ;
		this.age = age ;
		this.code = code ;
	}
	

	public Person(String name, Email email, int age, int code) {
		this(name,email.show(),age,code) ;
		System.out.println("--") ;
	}
	
	
	public static class Email {
		String username ;
		
		public Email(String username) {
			this.username = username ;
		}
		
		public String show() {
			return "" + username + "@T3.com" ;
		}
		
	}
	
	
}
