package Sequenic.T3.Examples.Cycles;

import Sequenic.T3.Examples.Cycles.Cycle;
import Sequenic.T3.Examples.Friends.Person;

public class PersonCycle <T extends Person> extends Cycle<T> {

    public PersonCycle(T x) {
        item = x ;
        next = this ;
        System.out.println(">> PersonCycle(...)") ;
    }

}
