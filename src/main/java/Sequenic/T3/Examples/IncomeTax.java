package Sequenic.T3.Examples;

import java.io.Serializable;
import java.util.List;

public class IncomeTax implements Serializable {
	
	private int salary ;
	private String location ;
	
	public IncomeTax() {
		salary = 0 ;
		location = "OUTSIDE" ;
	}

	
	public IncomeTax(int salary, String location) {
		if (! (location.equals("INSIDE") || location.equals("OUTSIDE"))) throw new IllegalArgumentException() ;
		if (salary<0) throw new IllegalArgumentException() ;
		this.salary = salary ;
		this.location = location ;
	}
	
	public int getTax() {
		//System.out.println(">>getTax") ;
		if (location.equals("OUTSIDE")) return 0 ;
		double salary_ = (double) salary ;
		double tax = 0 ;
		if (salary <= 10000) {
			tax = 0.33 * salary_ ;
			return (int) Math.floor(tax) ;
		}
		tax += 0.33 * 10000 ;
		if (salary <= 30000) {
			tax += 0.43 * (salary_ - 10000) ;
			return (int) Math.floor(tax) ;
		}
		tax += 0.43 * (30000 - 10000) ;
		if (salary <= 40000) {
			tax += 0.47 * (salary_ - 30000) ;
			return (int) Math.floor(tax) ;
		}
		tax += 0.47 * (40000 - 30000) ;
		tax += 0.55 * (salary_ - 40000) ;
		return (int) Math.floor(tax) ;
	}

	@Override
	public boolean equals(Object o) {
		//System.out.println(">>>>" + o.getClass().getName());
		if (o instanceof Item) 
			return false ;
		if (! (o instanceof IncomeTax)) return false ;
		IncomeTax t = (IncomeTax) o ;
		if (salary == t.salary && location.equals(t.location)) return true ;
		return false ;
	}
	/*
	boolean equals() {
		if (salary >0) return true ; 
		return false ;
	}
	*/
}
