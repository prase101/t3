package Sequenic.T3.Examples.InnerClass;

public class Logic {
	
	public Logic(int k){  System.out.println("creating logic");}

	public static abstract class Formula {
		public abstract boolean valid() ;
	}
	
	public static class TT extends Formula {
		public boolean valid() { return true ; }
	}
	
	public static class FF extends Formula {
		public boolean valid() { return false ; }
	}
	
	public static class AND extends Formula {
		Formula p ;
		Formula q ;
		Logic lg = null ;
		public AND() {} ;
		public AND(Logic lg) { this.lg = lg ; } ;
		public AND(Formula p, Formula q) {
			this.p = p ; this.q = q ;
		}
		public boolean valid() { 
			if (p==null || q==null) return false ;
			return p.valid() && q.valid() ; 
		}
		
		public static class X{
			public static int test() { int x = 10 ; return x ; }
		}
	}
	private static class XX extends Formula {
		Formula p = new TT() ;
		public XX() {}
		public XX(Formula p) { this.p = p ; }
		public boolean valid() { return p.valid() ; }
	}
	
	// this on-the fly class creation, when assigned to the initializtion of a static
	// variable seems to be problematic for Jacoco; it causes crash, probably to confusion
	// on which class to refer to
	//
	static Formula gg = new XX() { @Override public boolean valid() { return false ; }} ;
	
	public boolean test(boolean b) {
		Formula f = new AND(this) ;
		if (b || f.valid() || gg.valid()) return true ;
		//if (b || f.valid()) return true ;
		return false ;
	}
}
