package Sequenic.T3.Examples.InterfaceAndAbstract;

public class BachelorStudent implements IStudent {

	@Override
	public String nr() {
		return "123" ;
	}

	@Override
	public int tuitionFee() {
		return 100 ;
	}

}
