package Sequenic.T3.Examples.InterfaceAndAbstract;

public class Car extends Vehicle {

    public Car(int x) {
        super(0) ;
        if (x<0) throw new IllegalArgumentException() ;
        this.x = x ;
    }

    public int slowdown() {
        if (speed>0) { speed-- ; return move(1) ; }
        else return x ;
    }

}
