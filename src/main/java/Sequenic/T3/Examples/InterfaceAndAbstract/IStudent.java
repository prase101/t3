package Sequenic.T3.Examples.InterfaceAndAbstract;

public interface IStudent {

	public String nr() ;
	public default String fullNr() {
		System.out.println("=====> fullNr");
		return "xxx" + nr() ;
	}
	public int tuitionFee() ;
	
	public static int foo(int x) { return x+1 ; }
	
}
