package Sequenic.T3.Examples.InterfaceAndAbstract;

public class PaperVehicle extends Vehicle {

    public PaperVehicle() {
        super(0) ;
    }

    public int slowdown() {
    	speed = 0 ;
        return x ;
    }

    public int dummy() { return 10 ; }
    /*
    public static void main(String[] args) {
        PaperVehicle v = new PaperVehicle() ;
        System.out.println("v.x =" + v.x) ;
    }
    */
}
