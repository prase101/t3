package Sequenic.T3.Examples.InterfaceAndAbstract;

import java.io.IOException;
import java.util.List;

public abstract class Vehicle {

    protected int x = 90 ;
    protected int time = 0 ;
    protected int speed = 0 ;
    
    public Vehicle(int speed) { this.speed = speed ; }

    public int move(int t) {
    	if (t<=0) throw new IllegalArgumentException() ;
    	x += speed ;
    	return x ;
    	
    }

    public int accelerate()  {
        move(1) ;
        speed++ ;
        return x ;
    }

    abstract public int slowdown() ;
    
    public static int foo(int x) { return x+1 ; }
    
    /*
    protected List<Integer> m100(Vehicle[] vs, List<Vehicle> xs) throws IOException {
    	return null ;
    }
    */

}
