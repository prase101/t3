package Sequenic.T3.Examples;

import java.io.Serializable;

public class Item implements Serializable {
    String name  ;
    int    price ;
    private String code  ;
    
    public Item() {
    	System.out.println(">>> Item ") ;
    }
    
    public Item(String name) {
    	this.name = name ;
    	this.price = 100 ;
    	code = null ;
    	System.out.println(">>> Item " + name) ;
    }
    
    public void incPrice(int x) { 
    	System.out.println(">>> incPrice " + x) ;
    	price += x ; 
    }
    
    public static class Code {
    	String code ;
    	public Code(String c) { code = c ; }
    	public String toString() { return "Code " + code ; }
    }
    
    public String setCode(Code c) { 
    	System.out.println(">>> setCode " + c.code) ;
    	this.code = c.code; return this.code ; 
    }
    
    public boolean checkzSameCode(Code c) { 
    	System.out.println(">>> checkzSameCode " + c.code) ;
    	return code.equals(c.code) ;
    } 
    
    public String getCode() { 
    	System.out.println(">>> getCode ") ;
    	return code ; 
    }
    public int getPrice() { 
    	System.out.println(">>> getPrice ") ;
    	return price ; 
    }
    public String resetCode() { 
    	System.out.println(">>> resetCode ") ;
    	code = null ; return code ; 
    }
    
}
