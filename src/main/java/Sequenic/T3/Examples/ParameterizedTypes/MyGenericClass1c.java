package Sequenic.T3.Examples.ParameterizedTypes;

public class MyGenericClass1c <
    H2 extends H3,
    I extends J,
    H1 extends H2,
    K1 extends I,
    J extends H1,
    K2 extends H3,
    H8 extends H10,
    H3 extends H4,
    H4 extends H5,
    K4 extends H4,
    H7 extends H8,
    H6 extends H7,
    H10 extends Integer,
    H5 extends H6
    >

{

}
