package Sequenic.T3.Examples.ParameterizedTypes;

import java.lang.reflect.* ;
import java.util.Collection;

public class MyMap <I extends Comparable, O extends I>{
	
	Object[] domain ;
	Object[] range ;
	int size ;
	
	public MyMap() {
		domain = new Object[10] ;
		range = new Object[10] ;
		size = 0 ;
	}
	
	
	public boolean add(I i, O x) {
		if (size < 10) { 
			domain[size] = i ; range[size] = i ; size++ ;
		    return true ;
		}
		return false ;
	}
	
	public <K extends Integer & Collection> boolean find(K key) {
		for (int i=0; i<size; i++) {
			if (domain[i] == key) return true ;
		}
		return false ;
	}
	
	public <I extends Integer> void test1(I i) { }

	public <K extends I> void test2(K i) { }

	public <K extends I, I extends Integer> void test3(K i) { }
	
	public void test4(Collection<? extends Integer > x) { }
	public void test5(Collection<? super String> x) { }
	// multiple bounds for wildcard does not seem to be syntactically allowed

	
	private static void write(String s) {
		System.out.print(s) ;
	}
	
	private static void writeln(String s) {
		System.out.println(s) ;
	}
	
	private static void printSignature(Method m) {
		write("** ") ;
		printTypeVarParams(m.getTypeParameters()) ;
		write(" " + m.getName() + "(") ;
		int N = m.getGenericParameterTypes().length ;
		int k = 0 ;
		for (Type D : m.getGenericParameterTypes()) {
			printType(D) ;
			if(k<N-1) write(", ") ;
		    k++ ;
		}
		write(")  :  ") ;
		printType(m.getGenericReturnType()) ;
		writeln("") ;
	}
	
	private static void printType(Type ty) {	
		if (ty instanceof TypeVariable) {
			printTypeVriable((TypeVariable) ty) ;
			return ;
		}
		if (ty instanceof WildcardType) {
			printWildCard((WildcardType) ty) ;
			return ;
		}
		write(ty.getTypeName()) ;
	}
	
	private static void printTypeVriable(TypeVariable a) {
		write(a.getName()) ;
		Type[] upperbounds = a.getBounds() ;
		if (upperbounds.length > 0) {
			write(" extends ") ;
			int k=0 ;
			int N = upperbounds.length ;
			//write("" + N) ;
			for (Type bound : upperbounds) {
				printType(bound) ;
				if (k<N-1) write(", ") ;
				k++ ;
			}
		}
	}

	private static void printWildCard(WildcardType w) {
		write("?") ;
		Type[] upperbounds = w.getUpperBounds() ;
		Type[] lowerbounds = w.getLowerBounds() ;
		if (upperbounds.length > 0) {
			write(" extends ") ;
			int k=0 ;
			int N = upperbounds.length ;
			//write("" + N) ;
			for (Type bound : upperbounds) {
				printType(bound) ;
				if (k<N-1) write(", ") ;
				k++ ;
			}
		}
		if (lowerbounds.length > 0) {
			write(" super ") ;
			int k=0 ;
			int N = lowerbounds.length ;
			//write("" + N) ;
			for (Type bound : upperbounds) {
				printType(bound) ;
				if (k<N-1) write(", ") ;
				k++ ;
			}
		}
	}
	
	private static void printTypeVarParams(TypeVariable[] args) {
		int k = 0 ;
		int N = args.length ;
		if (N>0) {
		   write("" + N) ;
		   write("<") ;	 
		   for (TypeVariable a : args) {
			  printTypeVriable(a) ;
			  if (k<N-1) write(", ") ;
			  k++ ;
		   }
		   write(">") ;
	    }
	}
	
	public static void main(String[] args) {
		
		write("Class type-params: ") ;
		printTypeVarParams(MyMap.class.getTypeParameters()) ;
		writeln("") ;
		
		Method[] ms = MyMap.class.getDeclaredMethods() ;
		for (Method m : ms) printSignature(m) ;
	}

}
