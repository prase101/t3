package Sequenic.T3.Examples;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Sorted list of integers; in ascending order.
 */
public class SimpleIntSortedList implements Serializable {
	
	private LinkedList<Integer> s;
    private Integer max = null ;

    /**
     * This constructs an empty list.
     */
    public SimpleIntSortedList() {
        s = new LinkedList<Integer>();
    }

    /**
     * This class invariant only specifies that the max-pointer points
     * to some element in the list, if it is not empty. 
     */
    private boolean classinv__() {
    	//System.out.println("-- Checking classinv...") ;
        return s.isEmpty() || s.contains(max);
    }

    /**
     * This inserts a new element into the list.
     */
    public void insert(Integer x) {
    	//if (x == null) {
    	//	System.out.println("### insert(null)") ;
    	//	assert x!=null : "PRE";
    	//	throw new IllegalArgumentException() ;
    	//}
    	assert x!=null : "PRE";
    	
    	int i = 0;
        for (Integer y : s) {
            if (y > x) {
                break;
            }
            i++;
        }
        s.add(i, x);
        
        //System.out.println("     ### insert " + x + ", max=" + max) ;

        // deliberate error: should be x > max
        if (max == null || x < max) {
            max = x;
        }
    }

    /**
     * Return and remove the greatest element from the list, if it is
     * not empty.
     */
    public Integer get() {
    	//assert !s.isEmpty() : "PRE";
        Integer x = max;
        s.remove(max);
        if (s.isEmpty()) {
            max = null;
        } else {
            max = s.getLast();
        }
        assert s.isEmpty() || x >= s.getLast() : "POST";
        return x;
    }

}
