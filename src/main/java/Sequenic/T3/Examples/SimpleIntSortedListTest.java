package Sequenic.T3.Examples;

import static org.junit.Assert.fail;

import org.junit.Test;

import Sequenic.T3.OracleError;
import Sequenic.T3.ReplayCmd;

public class SimpleIntSortedListTest {
	
	public static void replayG2() throws Throwable {
		try {
		ReplayCmd.main("--bulk=trace(.*)Sorted(.*)tr --regressionmode /Users/iswbprasetya/tmp/t3") ;
		}
		catch(OracleError e) {
			System.out.println(">>>>> an oracle is violated!") ;
			fail("CUT violted an oracle" + e.getMessage()) ;
		}
	}
	
	//@Test
	public void test1() throws Throwable {
		replayG2() ;
	}

}
