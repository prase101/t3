package Sequenic.T3.Examples;

import java.io.Serializable;

public class Triangle1 implements Serializable {
	
	private float x = 0 ;
	private float y = 0 ;
	private float z = 0 ;

	
	
	public Triangle1(){ 
		//System.out.println("*** Triangle()") ;
	}

	
	public void setX(float x) {
		//System.out.println("* setX " + x) ;
		if (x<=0) throw new IllegalArgumentException() ;
		this.x = x ;
	}
	public void setY(float y) {
		//System.out.println("* setY " + y) ;
		if (y<=0) throw new IllegalArgumentException() ;
		this.y = y ;
	}
	public void setZ(float z) {
		//System.out.println("* setZ " + z) ;
		if (z<=0) throw new IllegalArgumentException() ;
		this.z = z ;
	}
	public boolean isTriangle() {
		//System.out.println("* isTriangle") ;
		if (x >= y+z) return false ;
		if (y >= x+z) return false ;
		return z < x+y ;
	}
	public boolean isIsoleces() {
		//System.out.println("* isIsoleces") ;
		if (x==y) return true ;
		if (y==z) return true ;
		if (x==z) return true ;
		return false ;
	}
	public boolean isEquilateral() {
		//System.out.println("* isEquilateral") ;
		return (x==y) && (y==z) ;
	}
	public boolean isScalene() {
		//System.out.println("* isEquilateral") ;
		return ! isIsoleces() ;
	}
	
	@Override
	public boolean equals(Object o) {
		//System.out.println("* equals") ;
		if (o instanceof Triangle1) {
			//if (o != this) System.out.println(">>> o is NOT this!") ;
			//else System.out.println(">>> o is this!") ;
			Triangle1 t = (Triangle1) o ;
			if (x == t.x && y == t.y && z == t.z) 
				 return true ;
			else return false ;
		}
		return false ;
	}
	
	public String toString() {
		//System.out.println("* toString") ;
		return "Triangle: " + x + "/" + y + "/" + z ;
	}


}
