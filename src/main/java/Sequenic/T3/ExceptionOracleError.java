package Sequenic.T3;

public class ExceptionOracleError extends OracleError {

	public ExceptionOracleError(String info) {
		super(info);
	}

	public ExceptionOracleError(String info, Throwable cause) {
        super(info,cause) ;
    }
	
    public ExceptionOracleError(Throwable cause) {
        super(cause) ;
    }
}
