/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator;

import Sequenic.T3.JavaType.JTypeUtils;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.Sequence.Datatype.STEP;
import Sequenic.T3.utils.*;
import Sequenic.T3.Generator.Value.* ;
import Sequenic.T3.* ;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class GenCombinators {

    public static <R,T> Generator<R,T> Fail() {
        return R -> null ;
    }

    /**
     * Execute the first generator that does not fail. If all generators
     * fail, the composition fails.
     */
    public static <R,T> Generator<R,T> FirstOf (Generator<R,T> ... gens) {
        return R1 -> {
            for (Generator<R,T> g : gens) {
                Maybe<T> result = g.generate(R1) ;
                if (result != null) return result ;
            }   ;
            return null ;
        } ;
    }
    
    /**
     * Randomly chooses one of the generators.
     */
    public static <R,T> Generator<R,T> OneOf (Generator<R,T> ... gens) {
        return R1 -> {
        	int k = T3Random.getRnd().nextInt(gens.length) ;
        	return gens[k].generate(R1) ;
        } ;
    }
    
    /**
     * Randomly chooses one of the values. The condition is ignored.
     */
    public static <R,T> Generator<R,T> OneOfVal(T ... values) {
        return R1 -> {
        	int k = T3Random.getRnd().nextInt(values.length) ;
        	return new Maybe(values[k]) ;
        } ;
    }
    
    /**
     * Just another name for OneOf
     */
    public static <R,T> Generator<R,T> SumGens(Generator<R,T> ... gens) {
    	return OneOf(gens) ;
    }
    
    
    /**
     * Execute a sequence of generators. If one fails, the execution will be stopped at the failing
     * generator. The result would the result of the last non-failing generator.
     */
    public static <T> Generator <T,T> Sequence(Generator<T,T> ... gens) {
        return t -> {
            T res = t ;
            for (Generator<T,T> G : gens) {
                Maybe<T> u_ = G.generate(res) ;
                if (u_ == null) break ;
                else res = u_.val ;
            }
            return new Maybe(res) ;
        } ;
    }

    /**
     * Execute a sequence of generators, while a guard is true. If one generator in the sequence fails,
     * the execution will be stopped at the failing generator. The result would the result of the
     * last non-failing generator.
     *
     * If a generator gives a result that violates a guard, the execution is stopped as well. The returned
     * result is this violating result.
     */
    public static <T> Generator <T,T> SequenceWhile(Predicate<T> guard, Generator<T,T> ... gens) {
        return t -> {
            T res = t ;
            for (Generator<T,T> G : gens) {
                Maybe<T> u_ = G.generate(res) ;
                if (u_ == null) break ;
                if (! guard.test(u_.val)) return u_ ;
                else res = u_.val ;
            }
            return new Maybe(res) ;
        } ;
    }

    /**
     * Iterate a generator, while it does not fail, and the result still satisfies a 
     * given predicate.
     * If an iteration fails (it returns a null), the iterator will stop. The result is 
     * the result of the last iteration that does not fail.
     * If an iteration produces a result where the guard evaluates to false, the iterator also stops.
     * The result would be the result of the iteration whose result violates the guard.
     */
    public static <T> Generator <T,T> IterateWhile(Predicate<T> guard, Generator<T,T> gen) {
        return t -> {
            T res = t ;
            while (guard.test(res)) {
                Maybe<T> u_ = gen.generate(res) ;
                if (u_ == null) break ;
                else res = u_.val ;
            }
            return new Maybe(res) ;
        } ;
    }

    public static <R,T>  Generator<List<R>,T>  mapReduceWith(Generator<R,T> g,
                                                       T zero,
                                                       BinaryOperator<T> operator,
                                                       boolean isSingleCore) {
        return inputs -> {
            Stream<R> S ;
            if (isSingleCore)
                S = inputs.stream() ;
                else S = inputs.parallelStream() ;
            Function<R,T> g_ = R -> {
                Maybe<T> y = g.generate(R) ;
                if (y == null) return zero ;
                else return y.val ;
            } ;
            return new Maybe(S.map(g_).reduce(zero,operator)) ;
        }   ;
    }



    public static void main(String[] args) {

        Generator<PARAM,STEP> g =
         FirstOf(new NullMG().constant(), //.WithChance(0.2) ,
                 //new IntGen().uniform(Integer.MAX_VALUE-3,Integer.MAX_VALUE).WithChance(0.2) ,
                 //new IntGen().uniform(7).WithChance(0.2) ,
                 new IntMG().uniform(999)
                 )
                 //.WithPreRequirement( P -> P.cty.isConcrete())
                 ;

        System.out.println("** " + g.generate(new PARAM("", JTypeUtils.convert(Integer.TYPE))).val) ;
        System.out.println("** " + g.generate(new PARAM("", JTypeUtils.convert(Integer.class))).val) ;
        System.out.println("** " + g.generate(new PARAM("", JTypeUtils.convert(Maybe.class)))) ;

    }

}
