/*
 * Copyright 2014 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.SeqAndSuite;

import java.lang.reflect.*;
import java.util.*;
import java.util.function.Predicate;

import Sequenic.T3.Pool;
import Sequenic.T3.TestingScope;
import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Generator.Step.*;
import Sequenic.T3.Sequence.Datatype.*;


/**
 * To generate a sequence containing a single call to a method.
 *
 */
public class MethodG extends AbstractSeqGenerator {
	
	METHODMG methodMetaGen ;
	
    public MethodG(TestingScope scope,
            Pool pool,
            int maxNumberOfStepRetry,
            Generator<PARAM,STEP> valueGenerator
            )
    {
       super(scope,pool,maxNumberOfStepRetry,valueGenerator) ;
       methodMetaGen = new METHODMG(this.pool, this.scope, this.valueGenerator) ;
    }
    
    /**
     * A generic worker to construct the generator; customized by the predicate.
     */
    private Generator<SEQ_RT_info,SEQ_RT_info> call(Predicate<STEP_RT_info> requirement, String mname, Class...paramtypes) {
    	List<Method> methods = scope.methods(mname, paramtypes) ;
    	return STEPexec.until(
    			requirement,
    			maxNumberOfStepRetry,
    			methodMetaGen.oneOf(methods),
    			scope.CUT, 
    			pool) ;
    }
    
    private Generator<SEQ_RT_info,SEQ_RT_info> call(Predicate<STEP_RT_info> requirement, Method m) {
    	return STEPexec.until(
    			requirement,
    			maxNumberOfStepRetry,
    			methodMetaGen.select(m),
    			scope.CUT, 
    			pool) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> grey(Method m) {
    	//System.out.println(">> " + m.getName()) ;
    	return call(r -> true, m) ;
    }

    public Generator<SEQ_RT_info,SEQ_RT_info> grey(String mname, Class...paramtypes) {
    	return call(r -> true, mname, paramtypes) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol(Method m) {
    	return call(r -> !r.inputWasIncorrect(), m) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol(String mname, Class...paramtypes) {
    	return call(r -> !r.inputWasIncorrect(), mname, paramtypes) ;
    }
    
    
    /**
     * Avoid throwing any exception.
     */
    public Generator<SEQ_RT_info,SEQ_RT_info> no_exc(Method m) {
    	return call(r -> r.exc == null, m) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_exc(String mname, Class...paramtypes) {
    	return call(r -> r.exc == null, mname, paramtypes) ;
    }
    
    
}
