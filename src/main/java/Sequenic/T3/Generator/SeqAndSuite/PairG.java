/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.SeqAndSuite;

import Sequenic.T3.Generator.*;
import Sequenic.T3.*;
import Sequenic.T3.Generator.Step.METHODMG;
import Sequenic.T3.Generator.Step.STEPexec;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.Maybe;

import java.lang.reflect.Method;
import java.util.function.Predicate;

import static Sequenic.T3.Generator.GenCombinators.*;

/**
 * To create a sequence consisting of calls to two methods.
 */
public class PairG extends AbstractSeqGenerator {

    protected METHODMG methodMG ;

    public PairG (TestingScope scope,
    		             Pool pool,
                         int maxNumberOfStepRetry,
                         Generator<PARAM,STEP> valueGenerator
    )
    {
    	super(scope,pool,maxNumberOfStepRetry,valueGenerator) ;
    	methodMG = new METHODMG(this.pool, this.scope, this.valueGenerator) ;	
    }

    private Generator<SEQ_RT_info,SEQ_RT_info> pair(Predicate<STEP_RT_info> requirement, Method m1, Method m2) {
   	    assert methodMG.testingscope.methods.contains(m1) ;
        assert methodMG.testingscope.methods.contains(m2) ;
        Generator<SEQ_RT_info,SEQ_RT_info> m1_ ;
        Generator<SEQ_RT_info,SEQ_RT_info> m2_  ;
        //System.out.println("*** scope.cut = " + scope.CUT) ;
        //System.out.println("*** " + methodMetaGenerators.select(m1)) ;
        m1_ = STEPexec.until(requirement, maxNumberOfStepRetry, methodMG.select(m1), scope.CUT, pool) ;
        m2_ = STEPexec.until(requirement, maxNumberOfStepRetry, methodMG.select(m2), scope.CUT, pool) ;
        return SequenceWhile(r -> ! r.isFail(), m1_ , m2_) ;  	
   }

    public Generator<SEQ_RT_info,SEQ_RT_info> grey(Method m1, Method m2) {
    	return pair(r -> true, m1,m2) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol(Method m1, Method m2) {
    	return pair(r -> !r.inputWasIncorrect(), m1,m2) ;
    }
            
}
