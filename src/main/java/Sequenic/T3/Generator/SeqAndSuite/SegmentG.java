/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.SeqAndSuite;

import java.util.function.Predicate;
import java.lang.reflect.*;
import java.util.*;
import Sequenic.T3.Generator.*;
import Sequenic.T3.Generator.Step.*;
import Sequenic.T3.Generator.Value.*;
import Sequenic.T3.*;
import Sequenic.T3.Sequence.Datatype.* ;
import Sequenic.T3.TestingScope;
import Sequenic.T3.utils.*;
import static Sequenic.T3.Generator.GenCombinators.* ;
import Sequenic.T3.Examples.Cycles.Cycle;
import Sequenic.T3.Examples.Friends.* ;

/**
 * For generating segments, we provide three sorts: 
 *    (1) the standard one consists of method calls and field updates
 *    (2) segments of calls to mutators and field updates
 *    (3) segments of calls to non-mutators
 *    
 *  Each sort has grey, no_asmviol, and no_exc variants.  
 */
public class SegmentG extends AbstractSeqGenerator {

    public SegmentG (TestingScope scope,
                   Pool pool,
                   int maxNumberOfStepRetry,
                   Generator<PARAM,STEP> valueGenerator)
    {
        super(scope,pool,maxNumberOfStepRetry,valueGenerator) ;  
    }

    private Generator<SEQ_RT_info,SEQ_RT_info> segment(
    		List<Method> methods,
    		Predicate<STEP_RT_info> requirement,
    		double fieldUpdateProbability,
    		int maxLength
    		) 
    {
    	Generator<SEQ_RT_info,STEP> fieldMG = new UPDATE_FIELDMG(this.scope,this.valueGenerator).random() ;
    	Generator<SEQ_RT_info,STEP> methodMG = new METHODMG(this.pool,this.scope,this.valueGenerator).oneOf(methods) ;
    	Generator<SEQ_RT_info,STEP> stepMG = FirstOf(fieldMG.WithChance(fieldUpdateProbability),methodMG) ;
    	Generator<SEQ_RT_info,SEQ_RT_info> stepGenerator = STEPexec.until(requirement, maxNumberOfStepRetry,stepMG,this.scope.CUT,pool) ;
        return info -> {
        	int N = info.executionCounter + maxLength ;
        	//System.out.println("**>> current legth = " + info.executionCounter) ;
        	return IterateWhile(info_ -> info_.executionCounter < N && !info_.isFail(),
                       stepGenerator
                       )
                       .generate(info) ;
        } ;
    }
    
    
    /**
     * Generate a segment where the same methods are invoked at most twice.
     */
    private Generator<SEQ_RT_info,SEQ_RT_info> segmentWithMinimizedDuplicates(
    		List<Method> methods,
    		Predicate<STEP_RT_info> requirement,
    		double fieldUpdateProbability,
    		int maxLength
    		) 
    {
    	Generator<SEQ_RT_info,STEP> fieldMG = new UPDATE_FIELDMG(this.scope,this.valueGenerator).random() ;
    	
    	// make a custom methodMG that will not invoke the same method more than twice
    	METHODMG methodMGbase = new METHODMG(this.pool,this.scope,this.valueGenerator) ;
    	List<Method> methods_ = new LinkedList<Method>() ;
    	methods_.addAll(methods) ;
    	Map<Method,Integer> counts = new HashMap<Method,Integer>() ;
    	Generator<SEQ_RT_info,STEP> methodMGxx = info -> {
    		int N = methods_.size() ;
            if (N==0) return null ;
            Method M = methods_.get(methodMGbase.rnd.nextInt(N))  ;
            String mname = M.getName().toLowerCase() ;
            if (mname.startsWith("set") || mname.startsWith("clear")
            		|| mname.startsWith("reset")
            		) 
            	methods_.remove(M) ;
            else {
            	Integer M_cnt = counts.get(M) ;
            	if (M_cnt == null) M_cnt = 1 ; else M_cnt ++ ;
            	if (M_cnt >= 2) methods_.remove(M) ;  
            	counts.put(M, M_cnt) ;
            }
            return methodMGbase.select(M).generate(info) ;
    	} ;
    			
    	Generator<SEQ_RT_info,STEP> stepMG = FirstOf(fieldMG.WithChance(fieldUpdateProbability),methodMGxx) ;
    	Generator<SEQ_RT_info,SEQ_RT_info> stepGenerator = STEPexec.until(requirement, maxNumberOfStepRetry,stepMG,this.scope.CUT,pool) ;
        return info -> {
        	int N = info.executionCounter + maxLength ;
        	//System.out.println("**>> current legth = " + info.executionCounter) ;
        	return IterateWhile(info_ -> info_.executionCounter < N && !info_.isFail(),
                       stepGenerator
                       )
                       .generate(info) ;
        } ;	
    }
    
    
    /**
     * Extend the current sequence with a segment, up to the specified length.
     */
    public Generator<SEQ_RT_info,SEQ_RT_info> grey(double fieldUpdateProbability,int maxLength) {
    	return segment(scope.methods,r -> true, fieldUpdateProbability, maxLength) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol(double fieldUpdateProbability, int maxLength) {
    	return segment(scope.methods,r -> !r.inputWasIncorrect(), fieldUpdateProbability, maxLength) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_exc(double fieldUpdateProbability, int maxLength) {
    	return segment(scope.methods,r -> r.exc == null, fieldUpdateProbability, maxLength) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> grey_mutators(double fieldUpdateProbability,int maxLength) {
    	return segment(scope.mutators,r -> true, fieldUpdateProbability, maxLength) ;
    }
       
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol_mutators(double fieldUpdateProbability, int maxLength) {
    	return segment(scope.mutators,r -> !r.inputWasIncorrect(), fieldUpdateProbability, maxLength) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_exc_mutators(double fieldUpdateProbability, int maxLength) {
    	//return segment(scope.mutators,r -> r.exc == null, fieldUpdateProbability, maxLength) ;
    	return segmentWithMinimizedDuplicates(scope.mutators,r -> r.exc == null, fieldUpdateProbability, maxLength) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> grey_nonmutators(int maxLength) {
    	return segment(scope.nonMutators,r -> true,0.0 , maxLength) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol_nonmutators(int maxLength) {
    	return segment(scope.nonMutators,r -> !r.inputWasIncorrect(),0.0, maxLength) ;
    }
    

    
    
    public static void main(String[] args) throws Exception {

        Pool pool = new Pool() ;
        String[] dirsToScan =  {"./bin"} ;
        ImplementationMap impsmap = new ImplementationMap(dirsToScan) ;
        
        //Class CUT = Person.class ;
        Class CUT = Sequenic.T3.Examples.InterfaceAndAbstract.Vehicle.class ;
        TestingScope scope =  new TestingScope(impsmap,CUT)  ;
        scope.configureForADTtesting();
        
        ValueMG valueg = new ValueMG(3,2,impsmap) ;
        
        ObjectUnderTestG outg = new ObjectUnderTestG(scope,pool,30,valueg.gen1closed(pool)) ;
        SegmentG  segmentg = new SegmentG(scope,pool,30,valueg.gen1closed(pool)) ;

        SEQ_RT_info info = new SEQ_RT_info(new SEQ()) ;

        System.out.println(scope.toString());

        Maybe<SEQ_RT_info> info2 = Sequence(outg.nonnull_and_no_exc(),segmentg.no_exc(0,2),segmentg.grey(.5,2))
        		                   .
        		                   generate(info) ;

        info2.val.seq.exec(CUT,pool,0,System.out) ;

    }
}
