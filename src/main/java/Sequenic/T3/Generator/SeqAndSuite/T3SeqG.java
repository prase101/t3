/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.SeqAndSuite;

import Sequenic.T3.Generator.*;
import Sequenic.T3.Generator.Step.*;
import Sequenic.T3.Generator.Value.*;
import Sequenic.T3.*;
import Sequenic.T3.Sequence.Datatype.* ;
import Sequenic.T3.TestingScope;
import Sequenic.T3.utils.*;
import static Sequenic.T3.Generator.GenCombinators.* ;

import java.lang.reflect.*;

import Sequenic.T3.Examples.Cycles.Cycle;
import Sequenic.T3.Examples.Friends.* ;

/**
 * T3's standard generators for generating full test sequences. Two types of sequences:
 * 
 * (1) Generated sequences consist of three part: s ++ [m1,m2] ++ u. The first part s is always 
 * a non-failing sequence, used to set up the state. It consist of mutators and field updates.
 * The middle part is aimed to test a pair-combination of m1 and m2. 
 * The suffix u, which can be empty, takes the role of validating the effect of 
 * the pair. The suffix contains no field updates.
 * 
 * They come in two variants: ADT sequences start with the creation of 
 * a target object; non-ADT sequences don't. 
 * 
 * (2) A variation of (1) where the sequences have the form s ++ [m1] ++ s2; will be implemented
 * as type-1, where m2 is set to null.
 * 
 * (2) For testing constructors, we have sequences of the form [c] ++ u, where c is
 * a constructor or creator method. 
 *   
 */

public class T3SeqG extends AbstractSeqGenerator {
	
    public ObjectUnderTestG creationGen ;
    public SegmentG segmentGen ;
    public PairG pairGen ;
    public MethodG methodGen ;

    public T3SeqG (Pool pool,
                   TestingScope scope,
                   int maxNumberOfStepRetry,
                   Generator<PARAM,STEP> valueGenerator
                   )
    {
      super(scope,pool,maxNumberOfStepRetry,valueGenerator) ;
      creationGen = new ObjectUnderTestG(scope,pool,maxNumberOfStepRetry,valueGenerator) ;
      segmentGen  = new SegmentG(scope,pool,maxNumberOfStepRetry,valueGenerator) ;
      pairGen     = new PairG(scope,pool,maxNumberOfStepRetry,valueGenerator) ;
      methodGen   = new MethodG(scope,pool,maxNumberOfStepRetry,valueGenerator) ;
    }


    public Generator<SEQ_RT_info,SEQ_RT_info> no_exc_mutatorsPrefix(double fieldUpdateProbability,
    		int length, 
    		boolean maximizePrefix 
    		) 
    {
    	return r -> {
    		int k = length ;
    		if (!maximizePrefix) k = T3Random.getRnd().nextInt(length+1) ;
    		return segmentGen.no_exc_mutators(fieldUpdateProbability,k).generate(r) ;
    	} ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> grey_nonadt(int maxPrefixLength, int maxSuffixLength, 
    		boolean maximizePrefix,
    		double fieldUpdateProbability, Method m1, Method m2) 
    {
    	//System.out.println(">> grey_nonadt_pseq " + m1 + ", "  + m2) ;
    	Generator<SEQ_RT_info,SEQ_RT_info> middlePartgen ;
    	if (m2==null) middlePartgen = methodGen.grey(m1) ;
    	else middlePartgen = pairGen.grey(m1, m2) ;
    	return SequenceWhile(
        		r -> ! r.isFail(),
        		no_exc_mutatorsPrefix(fieldUpdateProbability,maxPrefixLength,maximizePrefix),
        		instrument(),
        		middlePartgen,
                segmentGen.grey(0,maxSuffixLength)
        ) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> grey_nonadt(int maxPrefixLength, int maxSuffixLength, 
    		boolean maximizePrefix,
    		double fieldUpdateProbability, Method m1) 
    {
    	return grey_nonadt(maxPrefixLength,maxSuffixLength,maximizePrefix,fieldUpdateProbability,m1,null) ;
    }
        
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol_nonadt(int maxPrefixLength, int maxSuffixLength, 
    		boolean maximizePrefix,
    		double fieldUpdateProbability, Method m1, Method m2) 
    {
    	Generator<SEQ_RT_info,SEQ_RT_info> middlePartgen ;
    	if (m2==null) middlePartgen = methodGen.no_asmviol(m1) ;
    	else middlePartgen = pairGen.no_asmviol(m1, m2) ;
    	return SequenceWhile(
        		r -> ! r.isFail(),
        		no_exc_mutatorsPrefix(fieldUpdateProbability,maxPrefixLength,maximizePrefix),
        		instrument(),
        		middlePartgen,
                segmentGen.no_asmviol(0,maxSuffixLength)
        ) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol_nonadt(int maxPrefixLength, int maxSuffixLength, 
    		boolean maximizePrefix,
    		double fieldUpdateProbability, Method m1) 
    {
    	return no_asmviol_nonadt(maxPrefixLength,maxSuffixLength,maximizePrefix,fieldUpdateProbability,m1,null) ;
    }

    public Generator<SEQ_RT_info,SEQ_RT_info> grey_adt(int maxPrefixLength, int maxSuffixLength, 
    		boolean maximizePrefix,
    		double fieldUpdateProbability, Method m1, Method m2) 
    {
    	Generator<SEQ_RT_info,SEQ_RT_info> middlePartgen ;
    	if (m2==null) middlePartgen = methodGen.grey(m1) ;
    	else middlePartgen = pairGen.grey(m1, m2) ;
    	return SequenceWhile(
        		r -> ! r.isFail(),
        		creationGen.nonnull_and_no_exc(),
        		no_exc_mutatorsPrefix(fieldUpdateProbability,maxPrefixLength-1,maximizePrefix),
        		instrument(),
        		middlePartgen,
                segmentGen.grey(0,maxSuffixLength)
        ) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> grey_adt(int maxPrefixLength, int maxSuffixLength, 
    		boolean maximizePrefix,
    		double fieldUpdateProbability, Method m1) 
    {
    	return grey_adt(maxPrefixLength,maxSuffixLength,maximizePrefix,fieldUpdateProbability,m1,null) ;
    }

    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol_adt(int maxPrefixLength, int maxSuffixLength, 
    		boolean maximizePrefix,
    		double fieldUpdateProbability, Method m1, Method m2) 
    {
    	Generator<SEQ_RT_info,SEQ_RT_info> middlePartgen ;
    	if (m2==null) middlePartgen = methodGen.no_asmviol(m1) ;
    	else middlePartgen = pairGen.no_asmviol(m1, m2) ;
    	return SequenceWhile(
        		r -> ! r.isFail(),
        		creationGen.nonnull_and_no_exc(),
        		no_exc_mutatorsPrefix(fieldUpdateProbability,maxPrefixLength,maximizePrefix),
        		instrument(),
        		middlePartgen,
                segmentGen.no_asmviol(0,maxSuffixLength)
        ) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol_adt(int maxPrefixLength, int maxSuffixLength, 
    		boolean maximizePrefix,
    		double fieldUpdateProbability, Method m1) 
    {
    	return no_asmviol_adt(maxPrefixLength,maxSuffixLength,maximizePrefix,fieldUpdateProbability,m1,null) ;
    }
    
    /**
     * For testing a given constructor.
     */
    public Generator<SEQ_RT_info,SEQ_RT_info> grey_adt(int maxSuffixLength, Constructor co) {
        return SequenceWhile(
        		//r -> ! r.isFail() && r.lastInfo != null && r.lastInfo.returnedObj !=null ,
        		r -> ! r.isFail() && r.lastInfo != null ,
        		creationGen.grey(co),
        		instrument(),
        		segmentGen.grey(0,maxSuffixLength)
        ) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol_adt(int maxSuffixLength, Constructor co) {
        return SequenceWhile(
        		//r -> ! r.isFail() && r.lastInfo != null && r.lastInfo.returnedObj !=null ,
        		r -> ! r.isFail() && r.lastInfo != null ,
        		creationGen.no_asmviol(co),
        		instrument(),
        		segmentGen.no_asmviol(0,maxSuffixLength)
        ) ;
    }
    
    /**
     * For testing a given creator method.
     */
    public Generator<SEQ_RT_info,SEQ_RT_info> grey_adt(int maxSuffixLength, Method cm) {
        return SequenceWhile(
        		//r -> ! r.isFail() && r.lastInfo != null && r.lastInfo.returnedObj !=null ,
        		r -> ! r.isFail() && r.lastInfo != null ,
        		creationGen.grey(cm),
        		instrument(),
        		segmentGen.grey(0,maxSuffixLength)
        ) ;
    }
    
    public Generator<SEQ_RT_info,SEQ_RT_info> no_asmviol_adt(int maxSuffixLength, Method cm) {
        return SequenceWhile(
        		//r -> ! r.isFail() && r.lastInfo != null && r.lastInfo.returnedObj !=null ,
        		r -> ! r.isFail() && r.lastInfo != null ,
        		creationGen.no_asmviol(cm),
        		instrument(),
        		segmentGen.no_asmviol(0,maxSuffixLength)
        ) ;
    }
    
    public static void main(String[] args) throws Exception {
        test1() ;
        //test2() ;
    }
    
    // testing against the class Person; ADT
    private static void test1() throws Exception {
       	Class CUT = Person.class ;
        Pool pool = new Pool() ;
        String[] dirsToScan =  {"./bin"} ;
        ImplementationMap impsmap = new ImplementationMap(dirsToScan) ;
        TestingScope scope =  new TestingScope(impsmap,CUT)  ;
        scope.configureForADTtesting();

        ValueMG valuegens = new ValueMG(3,2,impsmap) ;     
        T3SeqG sgens = new T3SeqG(pool,scope,30,valuegens.gen1closed(pool)) ;

        SEQ_RT_info info = new SEQ_RT_info(new SEQ()) ;

        System.out.println(scope.toString());

        Method m1 = scope.methods("friend").get(0) ;
        Method m2 = scope.methods("unfriend").get(0) ;
        
        System.out.println(">>> m1 = " + m1.getName());
        System.out.println(">>> m2 = " + m2.getName());

        //Maybe<SEQ_RT_info> info2 = sgens.grey_adt_pseq(1,0,m1,m2).generate(info) ;
        //Maybe<SEQ_RT_info> info2 = sgens.no_asmviol_adt_pseq(1,1,m1,m2).generate(info) ;
        
        Maybe<SEQ_RT_info> info2 = sgens.grey_adt(2,1,true,0.5,m1,m2).generate(info) ;
        // Maybe<SEQ_RT_info> info2 = sgens.no_asmviol_cseq(1).generate(info) ;

        info2.val.seq.exec(CUT,pool,0,System.out) ;  	
    }
    
    // testing against the class StringUtils; that contain statics
    private static void test2() throws Exception {
       	Class CUT = Sequenic.T3.Examples.Staticx.StringUtils.class ;
        Pool pool = new Pool() ;
        String[] dirsToScan =  {"./bin"} ;
        ImplementationMap impsmap = new ImplementationMap(dirsToScan) ;
        TestingScope scope =  new TestingScope(impsmap,CUT)  ;
        scope.configureForNonADTTesting();

        ValueMG valuegens = new ValueMG(3,2,impsmap) ;
        T3SeqG sgens = new T3SeqG(pool,scope,30,valuegens.gen1closed(pool)) ;

        SEQ_RT_info info = new SEQ_RT_info(new SEQ()) ;

        System.out.println(scope.toString());

        Method m1 = scope.methods("drop").get(0) ;
        Method m2 = scope.methods("take").get(0) ;
        
        System.out.println(">>> m1 = " + m1.getName());
        System.out.println(">>> m2 = " + m2.getName());

        Maybe<SEQ_RT_info> info2 = sgens.grey_nonadt(1,0,true,0,m1,m2).generate(info) ;
        //Maybe<SEQ_RT_info> info2 = sgens.no_asmviol_nonadt_pseq(3,2,m1,m2).generate(info) ;
        
        // Maybe<SEQ_RT_info> info2 = sgens.grey_cseq(1).generate(info) ;
        // Maybe<SEQ_RT_info> info2 = sgens.no_asmviol_cseq(1).generate(info) ;

        info2.val.seq.exec(CUT,pool,0,System.out) ;  	
    }
}
