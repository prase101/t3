/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.SeqAndSuite;

import Sequenic.T3.Generator.*;
import Sequenic.T3.*;
import Sequenic.T3.Generator.Step.*;
import Sequenic.T3.Generator.Value.*;
import Sequenic.T3.Info.FunctionalCoverage;
import Sequenic.T3.Info.FieldPairsCoverage;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.*;
import static Sequenic.T3.Generator.GenCombinators.* ;

import java.lang.reflect.* ;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import Sequenic.T3.Examples.SimpleSortedList;
import Sequenic.T3.Examples.Friends.Person;
import Sequenic.T3.Examples.InterfaceAndAbstract.Vehicle;
import Sequenic.T3.Examples.Staticx.* ;

/**
 * Provide a bunch of T3's standard generators for test-suites. 
 * A suite can be ADT or non-ADT, and grey or no-asm-viol.
 */
public class T3SuiteG {

	static public int minSuiteSizePerGoal = 10 ;
	static public int maxSuiteSizePerGoal = 500 ;
	
    protected TestingScope scope ;
    
    public boolean includeInheritedMembers = true ;
    public  int maxNumberOfStepRetry = 30 ;
    public  int maxNumberOfSeqRetry  = 5  ;
    // capping the max. number of pairs to this, to prevent exploding the size of the suite:
    public  int maxNumberOfPairs = 1000 ;

    public int maxPrefixLength = 7 ;
    public int maxSuffixLength = 2 ;
    public boolean maximizePrefix = true ;
    public double fieldUpdateProbability = 0.1 ;

    public double multiplier = 4.0 ;
    
    // we need the generator to be a function from pool, to let each sub-suite to be
    // generated from its own pool, and hence allowing parallel generation of sub-suites
    protected Function<Pool,Generator<PARAM,STEP>> valueMetaGenerator   ;
    
    public T3SuiteG(TestingScope ts,
            Function<Pool,Generator<PARAM,STEP>> valueMetaGenerator
           )
    {
       this.scope = ts ; 
       this.valueMetaGenerator = valueMetaGenerator ;
    }
    
    /**
     * A class to represent a goal, from which we generate a sub-suite.
     */
    abstract class Goal {
    	Constructor c = null ;
    	Method cm = null ; // creation method
    	Method m1 = null ;
    	Method m2 = null ;
    	int suffixlength = maxSuffixLength ;
    	int suiteSize ;
    	protected Goal (Constructor c, int suiteSize) { this.c = c ; this.suiteSize = suiteSize ; }
    	protected Goal (Method cm, int suiteSize) { this.cm = cm ; this.suiteSize = suiteSize ; }
    	protected Goal (Method m1, Method m2, int suiteSize) { this.m1 = m1; this.m2 = m2 ; this.suiteSize = suiteSize ;}
    	protected Goal (Method m1, int suiteSize, int suffixlength) {
    		this.m1 = m1 ; this.suffixlength = suffixlength ;
    		this.suiteSize = suiteSize ;
    	}
    	
    	abstract Generator<SEQ_RT_info,SEQ_RT_info> getGenerator(T3SeqG sequenceG) ;
    }
    
    class ADT_grey_Goal extends Goal {
    	protected ADT_grey_Goal (Constructor c, int suiteSize) { super(c,suiteSize); }
    	protected ADT_grey_Goal (Method cm, int suiteSize) { super(cm,suiteSize) ; }
    	protected ADT_grey_Goal (Method m1, Method m2, int suiteSize) { super(m1,m2,suiteSize) ;}
    	protected ADT_grey_Goal (Method m1, int suiteSize, int suffixlength) { super(m1,suiteSize,suffixlength) ; }
    	Generator<SEQ_RT_info,SEQ_RT_info> getGenerator(T3SeqG sequenceG) {
    		if (c!=null)  return sequenceG.grey_adt(suffixlength,c) ;
    		if (cm!=null) return sequenceG.grey_adt(suffixlength, cm) ;
    		if (m2==null) return sequenceG.grey_adt(maxPrefixLength, suffixlength, maximizePrefix, fieldUpdateProbability, m1) ;
    		// System.out.println("##>>") ;
    		return sequenceG.grey_adt(maxPrefixLength, suffixlength, maximizePrefix, fieldUpdateProbability, m1, m2) ;
    	}
    }
    
    class NonADT_grey_Goal extends Goal {
    	protected NonADT_grey_Goal (Constructor c, int suiteSize) { super(c,suiteSize); }
    	protected NonADT_grey_Goal (Method cm, int suiteSize) { super(cm,suiteSize) ; }
    	protected NonADT_grey_Goal (Method m1, Method m2, int suiteSize) { super(m1,m2,suiteSize) ;}
    	protected NonADT_grey_Goal (Method m1, int suiteSize, int suffixlength) { super(m1,suiteSize,suffixlength) ; }
    	Generator<SEQ_RT_info,SEQ_RT_info> getGenerator(T3SeqG sequenceG) {
    		if (m2==null) return sequenceG.grey_nonadt(maxPrefixLength, suffixlength, maximizePrefix, fieldUpdateProbability, m1) ;
    		return sequenceG.grey_nonadt(maxPrefixLength, suffixlength, maximizePrefix, fieldUpdateProbability, m1, m2) ;
    	}
    }
    
    class ADT_no_asmviol_Goal extends Goal {
    	protected ADT_no_asmviol_Goal (Constructor c, int suiteSize) { super(c,suiteSize); }
    	protected ADT_no_asmviol_Goal (Method cm, int suiteSize) { super(cm,suiteSize) ; }
    	protected ADT_no_asmviol_Goal (Method m1, Method m2, int suiteSize) { super(m1,m2,suiteSize) ;}
    	protected ADT_no_asmviol_Goal (Method m1, int suiteSize, int suffixlength) { super(m1,suiteSize,suffixlength) ; }
    	Generator<SEQ_RT_info,SEQ_RT_info> getGenerator(T3SeqG sequenceG) {
    		if (c!=null)  return sequenceG.no_asmviol_adt(suffixlength,c) ;
    		if (cm!=null) return sequenceG.no_asmviol_adt(suffixlength, cm) ;
    		if (m2==null) return sequenceG.no_asmviol_adt(maxPrefixLength, suffixlength, maximizePrefix, fieldUpdateProbability, m1) ;
    		return sequenceG.grey_adt(maxPrefixLength, suffixlength, maximizePrefix, fieldUpdateProbability, m1, m2) ;
    	}
    }
    
    class NonADT_no_asmviol_Goal extends Goal {
    	protected NonADT_no_asmviol_Goal (Constructor c, int suiteSize) { super(c,suiteSize); }
    	protected NonADT_no_asmviol_Goal (Method cm, int suiteSize) { super(cm,suiteSize) ; }
    	protected NonADT_no_asmviol_Goal (Method m1, Method m2, int suiteSize) { super(m1,m2,suiteSize) ;}
    	protected NonADT_no_asmviol_Goal (Method m1, int suiteSize, int suffixlength) { super(m1,suiteSize,suffixlength) ; }
    	Generator<SEQ_RT_info,SEQ_RT_info> getGenerator(T3SeqG sequenceG) {
    		if (m2==null) return sequenceG.no_asmviol_nonadt(maxPrefixLength, suffixlength, maximizePrefix, fieldUpdateProbability, m1) ;
    		return sequenceG.no_asmviol_nonadt(maxPrefixLength, suffixlength, maximizePrefix, fieldUpdateProbability, m1, m2) ;
    	}
    }
    
    /**
     * Given a list of goals, execute them all, and collect the results in a single
     * suite. They will be executed in a single thread.
     */
    private SUITE generateSubSuite(List<Goal> goals) {
    	Pool pool = new Pool() ;
    	T3SeqG T3sequenceG = new T3SeqG(pool,scope,maxNumberOfStepRetry,valueMetaGenerator.apply(pool)) ;
    	
    	Function<Goal,SUITE> runGoal = goal -> {
    		
    		/*
    		System.out.println("**>> T3SuiteG, goal = " + goal 
    				+ "/c = " + goal.c
    				+ "/cm =  " + goal.cm
    				+ "/m1 = " + goal.m1
    				+ "/m2 = " + goal.m2
    				+ "/N = " + goal.suiteSize) ; 
    		*/
    		
    		SuiteG suiteGenerator = new SuiteG(scope) ;
    		suiteGenerator.maxNumberOfSeqRetry = maxNumberOfSeqRetry ;
    		SUITE T = suiteGenerator.run(pool,goal.getGenerator(T3sequenceG),goal.suiteSize) ;
    		//System.out.println("**>> T3SuiteG, # = " + T.suite.size()) ;       	
    		return T ;
    	} ;
    	
    	BinaryOperator<SUITE> union = (S1,S2) -> SUITE.union(S1, S2) ;
    	
    	SUITE zero = new SUITE(scope.CUT.getName()) ;
    	
    	SUITE S =  goals.stream().map(runGoal).reduce(zero, union) ;
    	
    	// System.out.println("**>> T3SuiteG, generating subsuite # = " + S.suite.size()) ;
    	return S ;
    }
    
    /**
     * Given a set of set of goals, we run them all and collect the results in a single suite.
     * The execution is at the top level parallel.
     */
    private SUITE generateSuite(List<List<Goal>> goals) {
    	
        BinaryOperator<SUITE> union = (S1,S2) -> SUITE.union((SUITE) S1, (SUITE) S2) ;
    	
    	SUITE zero = new SUITE(scope.CUT.getName()) ;
    	Stream<List<Goal>> stream ;
    	if (goals.size() == 1) stream = goals.stream() ;
    	else {
    		//System.out.println("### number of worklist " + goals.size()) ;
    		//System.out.println("### using parallel stream to generate suites...") ;
    		stream = goals.parallelStream() ;
    	}
    	SUITE S = stream
    	          . map(subgoals -> { 
    	        	  //System.out.println("### generating suite") ; 
    	        	  return generateSubSuite(subgoals) ; })    	
    	          . reduce(zero, union) ;
    	
    	Logger.getLogger(CONSTANTS.T3loggerName).info("Suite for "
    			+ S.CUTname + " is generated, size="
			    + S.suite.size()
				);
    	
    	return S ;
    }
    
    /**
     * Construct an ADT grey-suite for the given CUT.
     */
    public SUITE adt_grey(int numberOfCores) {
    	List<List<Goal>> goals    = calculateGoals(true,true,numberOfCores) ;
    	return generateSuite(goals) ;
    }
    
    public SUITE adt_no_asmviol(int numberOfCores) {
    	List<List<Goal>> goals    = calculateGoals(true,false,numberOfCores) ;
    	return generateSuite(goals) ;
    }
    
    public SUITE nonadt_grey(int numberOfCores) {
    	List<List<Goal>> goals    = calculateGoals(false,true,numberOfCores) ;
    	return generateSuite(goals) ;
    }
    
    public SUITE nonadt_no_asmviol(int numberOfCores) {
    	List<List<Goal>> goals    = calculateGoals(false,false,numberOfCores) ;
    	return generateSuite(goals) ;
    }

    
    private List<List<Goal>> calculateGoals(boolean adt, boolean grey, int numberOfCores) {
    	List<Goal> CMgoals = new LinkedList<Goal>() ;
    	List<Goal> MutGoals = new LinkedList<Goal>() ;
    	List<Goal> NonMutGoals = new LinkedList<Goal>() ;
    	if (adt) scope.configureForADTtesting() ;
    	else scope.configureForNonADTTesting() ;
    	
    	if (! includeInheritedMembers) {
    		scope.fields = scope.fields.stream()
    		     .filter(field -> field.getDeclaringClass() == scope.CUT)
    		     .collect(Collectors.toList()) ;
    		scope.mutators =scope.mutators.stream()
     		     .filter(m -> m.getDeclaringClass() == scope.CUT)
     		     .collect(Collectors.toList()) ;  	
    		scope.nonMutators =scope.nonMutators.stream()
        		     .filter(m -> m.getDeclaringClass() == scope.CUT)
        		     .collect(Collectors.toList()) ;  	
    	}
    	
    	//System.out.println("**>> #constructors    =" + scope.constructors.size()) ; 
    	//System.out.println("**>> #methods         =" + scope.methods.size()) ; 
    	//System.out.println("**>> #creator methods =" + scope.creatorMethods.size()) ; 
    	
    	if (adt) {
    	   // note that map is lazy!
    	   scope.constructors.stream().map(
    			co -> {
    				//System.out.println("***>>>") ;
    				int k = (int) (multiplier * FunctionalCoverage.estimatedNumberOfPaths(co)) ;
    				int suiteSize = Math.max(minSuiteSizePerGoal,Math.min(maxSuiteSizePerGoal,k)) ;
    				if (grey) CMgoals.add(new ADT_grey_Goal(co,suiteSize)) ;
    				else CMgoals.add(new ADT_no_asmviol_Goal(co,suiteSize)) ;
    				return null ;
    			}
    			)
    			.toArray() ;
    	} ;
    	
    	int numOfMutators = scope.mutators.size() ;
    	if (numOfMutators*numOfMutators <= maxNumberOfPairs) {
    		for (Method m1 : scope.mutators) 
    			for (Method m2 : scope.mutators) {
    				int k = (int) 
    						(multiplier 
    						  * FunctionalCoverage.estimatedNumberOfPaths(m1) 
    						  * FunctionalCoverage.estimatedNumberOfPaths(m2)) ;
    				int suiteSize = Math.max(minSuiteSizePerGoal,Math.min(maxSuiteSizePerGoal,k)) ;
    				if (adt) {
    					if (grey) MutGoals.add(new ADT_grey_Goal(m1,m2,suiteSize)) ;
    					else MutGoals.add(new ADT_no_asmviol_Goal(m1,m2,suiteSize)) ; }
    				else {
    					if (grey) MutGoals.add(new NonADT_grey_Goal(m1,m2,suiteSize)) ;
    					else MutGoals.add(new NonADT_no_asmviol_Goal(m1,m2,suiteSize)) ; }
    			}
    	}
    	else {
    		scope.mutators.stream().map(
    				m -> {
    				   int k = (int) (multiplier * FunctionalCoverage.estimatedNumberOfPaths(m)) ;
    				   int suiteSize = Math.max(minSuiteSizePerGoal,Math.min(maxSuiteSizePerGoal,k)) ;
    				   if (adt) {
    					   if (grey) MutGoals.add(new ADT_grey_Goal(m,suiteSize,maxSuffixLength)) ;
    					   else MutGoals.add(new ADT_no_asmviol_Goal(m,suiteSize,maxSuffixLength)) ; }
    				   else {
    					   if (grey) MutGoals.add(new NonADT_grey_Goal(m,suiteSize,maxSuffixLength)) ;
    					   else MutGoals.add(new NonADT_no_asmviol_Goal(m,suiteSize,maxSuffixLength)) ; }	
    				   return null ;
    				}
    				) 
    				.toArray() ;
    	}
    	
    	scope.nonMutators.stream().map(
    			m -> {
    			   int k = (int) (multiplier * FunctionalCoverage.estimatedNumberOfPaths(m)) ;
 				   int suiteSize = Math.max(minSuiteSizePerGoal,Math.min(maxSuiteSizePerGoal,k)) ;
    			   if (adt) {
    				   if (grey) NonMutGoals.add(new ADT_grey_Goal(m,suiteSize,0)) ;
    				   else NonMutGoals.add(new ADT_no_asmviol_Goal(m,suiteSize,0)) ; }
    			   else {
    				   if (grey) NonMutGoals.add(new NonADT_grey_Goal(m,suiteSize,0)) ;
    				   else NonMutGoals.add(new NonADT_no_asmviol_Goal(m,suiteSize,0)) ; }
    			   return null ;
    			}
    			) 
    			.toArray() ;
    	
    	
    	String msg = "Constructing ADT goals of " ;
    	if (!adt) msg = "Constructing Non-ADT goals of " ;
    	Logger.getLogger(CONSTANTS.T3loggerName).info(msg
    			    + scope.CUT + ": "
    				+ CMgoals.size() + " CM<t>, "
    				+ MutGoals.size() + " <s>Mut[Mut]<t>, "
    				+ NonMutGoals.size() + " <s>NonMut."
    				);
        

    	// now distributing the goals to work lists:
    	int N = Math.max(1,numberOfCores) ;
    	int count = 0 ;
    	List<List<Goal>> worklist = new LinkedList<List<Goal>>() ;
    	for (int k=0; k<N; k++) worklist.add(new LinkedList<Goal>()) ;
    	distribute(CMgoals,worklist) ;
    	distribute(MutGoals,worklist) ;
    	distribute(NonMutGoals,worklist) ;
    	for (int k=0; k<N; k++) count += worklist.get(k).size() ;
    	//System.out.println("**>> T3SuiteG: total goals = " + count) ;
    	return worklist ;
    }
    
    
    private void distribute(List<Goal> goals, List<List<Goal>> worklist) {
    	if (worklist.size()==1) {
    		worklist.get(0).addAll(goals) ;
    		return ;
    	}
    	int N = worklist.size() ;
    	int k = 0 ;
    	for(Goal g : goals) {
    		worklist.get(k % N).add(g) ;
    		k++ ;
    	}
    }
    
    
   /*
    private void cleanupSuite(SUITE S, boolean isSingleCore) {
        Stream<SEQ> suite_ ;
        if (isSingleCore) suite_ = S.suite.stream() ;
            else suite_ = S.suite.parallelStream() ;
         S.suite = suite_
                        . filter(seq -> seq != null && seq.steps != null && seq.steps.size()>0)
                        . collect(Collectors.toList()) ;
    }
    */
    
    public static void main(String[] args) throws Exception {

    	//Class CUT = Person.class ;
    	Class CUT = LinkedList.class ;
    	//Class CUT = SimpleSortedList.class ;
    	//Class CUT = StringUtils.class ;
    	//Class CUT = Vehicle.class ;
        String[] dirsToScan =  {"./bin"} ;
        ImplementationMap impsmap = new ImplementationMap(dirsToScan) ;
        TestingScope scope =  new TestingScope(impsmap,CUT)  ;
        
        //scope.configureForADTtesting() ;
        //System.out.println("**>> #methods = " + scope.methods.size()) ;

        ValueMG valueMG = new ValueMG(3,2,impsmap) ;
        
        Function<Pool,Generator<PARAM,STEP>> valuemetagen = pool -> {
        	return valueMG.gen1closed(pool) ;
        }   ;

        T3SuiteG suitegen = new T3SuiteG(scope,valuemetagen) ;

        // sequential :
        long time = System.currentTimeMillis() ;
        SUITE S = suitegen.adt_grey(8) ;
        time = System.currentTimeMillis() - time ;
        System.out.println(">>> suite's size = " + S.suite.size());
        System.out.println(">>> Time = " + time); ;
        
        /*
        SUITE_RT_info suiteinfo = S.exec(new Pool(), new ObjectCoverage(CUT), 0, 0, false, true, false, System.out) ;

        suiteinfo.printReport(System.out);    
        FunctionalCoverage fc = new FunctionalCoverage(scope,System.out) ;
        fc.calculate(S);
        fc.printReport(1); 
        */

        // parallel :
        
        /*
        time = System.currentTimeMillis() ;
        S = suitegen.adt_grey(8) ;
        time = System.currentTimeMillis() - time ;
        System.out.println(">>> suite's size = " + S.suite.size());
        System.out.println(">>> Time = " + time); ;
        */
        
       
        
    }

}
