package Sequenic.T3.Generator;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import Sequenic.T3.T3Random;

/**
 * Providing some standard generators; not very sophisticated, but can be borrowed.
 *
 */
public class SomeObjGenerators {
	
	static public <A,B> Function<A,B> genConstantFunction(A ignored, B y) {
		return o -> y ;
	}
	
	static public <A> Predicate<A> genConstantPredicate(A ignored, boolean y) {
		return o -> y ;
	}

	static public <A,B,C> BiFunction<A,B,C> genConstantBiFunction(A ignored, B alsoignored, C y) {
		return (o,p) -> y ;
	}
	
	public static enum SomeEnum_ { X1, X2 } ;
	
	static Class[] classes_ = {
			String.class,
			Integer.class,
			Integer.TYPE,
			SomeEnum_.class, 
			Object.class
	        } ;

	static public Class genClass(int k) {
		if (k<0) k = -k ;
		return classes_[k % classes_.length] ;
	}
	

}
