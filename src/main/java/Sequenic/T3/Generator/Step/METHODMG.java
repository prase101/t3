/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Step;

import Sequenic.T3.Generator.*;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Pool;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.T3Random;
import Sequenic.T3.TestingScope;
import Sequenic.T3.utils.*;

import java.lang.reflect.*;
import java.util.*;


public class METHODMG {

    public Pool pool ;
    public Random rnd = T3Random.getRnd() ;
    public Generator<PARAM,STEP> valueGenerator ;
    public TestingScope testingscope ;

    public METHODMG(Pool pool, TestingScope ts, Generator<PARAM, STEP> valueGenerator) {
        this.valueGenerator = valueGenerator ;
        this.pool = pool ;
        testingscope = ts ;
    }

    public Generator<SEQ_RT_info,STEP> oneOf(List<Method> methods) {
        return info -> {
        	int N = methods.size() ;
            if (N==0) return null ;
            if (N==1) return select(methods.get(0)).generate(info) ;
            // randomly select a method :
            Method M = methods.get(rnd.nextInt(N))  ;
            return select(M).generate(info) ;
        } ;
    }

    /**
     * Execute as a test-step, a call to a given method M. If M is abstract, we check if
     * the object under test has a concrete implementation for M, and use it instead.
     */
    public Generator<SEQ_RT_info,STEP> select(Method M) {
        return info -> {
            if (! testingscope.methods.contains(M)) return null ;
            
            // System.err.println("***>>> METHODMG: " + M) ;

            Object objectUnderTest = pool.getObjectUnderTest() ;
            //System.out.println("***>>> obj under test: " + objectUnderTest) ;
            
            Method M_ = M ;
            // the object-under-test can actually be of a subclass of the CUT, e.g. if
            // the CUT is abstract, or has no constructor nor creation-method. In this case,
            // M can actually be abstract, or overriden by the object-under-test. The code
            // below will get the concrete M:
            
            int modifier = M.getModifiers() ;
            
            if (objectUnderTest==null && Modifier.isAbstract(modifier)) return null ; 

            if (objectUnderTest!=null 
            		&& objectUnderTest.getClass()!=testingscope.CUT
            		&& Modifier.isAbstract(modifier)) 
            {
            	try {
            	   M_ = objectUnderTest.getClass().getMethod(M.getName(), M.getParameterTypes()) ;
            	}
            	catch(Exception e) { return null ; }
            	assert ! Modifier.isAbstract(M_.getModifiers()) ;
            }
            //System.err.println("***>>> METHODMG: " + M_) ;
            Parameter[] params = M_.getParameters() ;
            STEP[] argSteps = new STEP[params.length] ;
            Type[] paramsTypes = M_.getGenericParameterTypes() ;
            
            // determine where we can pass object-under-the test, if there is one
            int passObjectUnderTestHere = -1 ; // -1 is a trick, don't change to 0 --> not correct
            STEP passingObjectUnderTestStep = null ;
            if (objectUnderTest != null) {
                List<Integer> candidates = new LinkedList<Integer>() ;
                if (Modifier.isStatic(M.getModifiers())) {
                    // if the method is static, choose where to pass the object-under-test:
                    int i = 0 ;
                    for (Class D : M.getParameterTypes()) {
                        if (D == testingscope.CUT) candidates.add(new Integer(i)) ;
                        i++ ;
                    }
                    // choose one:
                    if (!candidates.isEmpty())
                        passObjectUnderTestHere = rnd.nextInt(candidates.size()) ;
                }  
                passingObjectUnderTestStep = new REF(pool.getIndexOfObjectUnderTest()) ;
            }

            for (int i=0; i<argSteps.length; i++) {
                if (passObjectUnderTestHere==i) {
                    // check if we are to pass object-under-test here:
                    argSteps[i] = passingObjectUnderTestStep ;
                    continue ;
                }
                JType argty = Solver.solve(testingscope.impsMap, info.tySubsts, JTypeUtils.convert(paramsTypes[i])) ;
                if (argty==null) {
                	//System.err.println("** FAIL " + M.getName() + "(.." + paramsTypes[i] + "..)") ;
                	return null ;
                }
                PARAM p = new PARAM(params[i].getName(),argty) ;
                Maybe<STEP> step_ = valueGenerator.generate(p) ;
                // valueGenerator should actually not return null
                if (step_ == null) return null;
                argSteps[i] = step_.val ;
            }
            //System.err.println("***>>> METHODMG: DONE") ;
            return new Maybe(new METHOD(M_,passingObjectUnderTestStep,argSteps))   ;
        }   ;
    }
}
