/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Step;

import Sequenic.T3.Generator.*;
import static Sequenic.T3.Generator.GenCombinators.*;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.*;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.*;

import java.lang.reflect.*;
import java.util.*;


public class ObjectUnderTestMG {

    public Random rnd = T3Random.getRnd() ;
    public Generator<PARAM,STEP> valueGenerator ;
    public TestingScope testingScope ;

    public ObjectUnderTestMG(TestingScope ts, Generator<PARAM,STEP> valueGenerator) {
        testingScope = ts ;
        this.valueGenerator = valueGenerator ;
    }

    public Generator<SEQ_RT_info,STEP> useConstructor(Constructor co) {
        return info -> {
           Parameter[] params = co.getParameters() ;
           STEP[] argSteps = new STEP[params.length] ;
           Type[] paramsTypes = co.getGenericParameterTypes() ;
           JTfun concreteType = new JTfun(testingScope.CUT) ;
           concreteType.args = new JType[params.length] ;
           
           // this happens on some CUT:
           if (params.length != paramsTypes.length) {
        	   // this should not happen! Perhaps the runtime system somehow uses two different version of the
        	   // CUT. 
        	   // Below is a a HACK to fix this:
        	   System.err.println(">>>>>>>>>>>>>>>>>>>>>> mismtach in number of params and paramtypes!!") ;
        	   System.err.println(">> " + co + " " + params.length) ;
        	   for (Type t : paramsTypes) System.out.println("  >> ty: " + t) ;
        	   System.err.println(">> hack: using plain-types instead...") ;
        	   paramsTypes = co.getParameterTypes() ;
           }

           for (int i=0; i<argSteps.length; i++) {
        	  // solve the type of i-th the argument: 
        	  JType argty = Solver.solve(testingScope.impsMap, info.tySubsts, JTypeUtils.convert(paramsTypes[i])) ;
     	      // if the type of arg-i cannot be solved; fail:
     	      if (argty==null) return null ;
        	  concreteType.args[i] = argty ;
              PARAM p = new PARAM(params[i].getName(),argty) ;
              Maybe<STEP> step_ = valueGenerator.generate(p) ;
              // valueGenerator should not actually return null
              if (step_ ==null) return null ;
              argSteps[i] = step_.val ;
           }
           CONSTRUCTOR step = new CONSTRUCTOR(concreteType,co,argSteps) ;
           step.isObjectUnderTestCreationStep = true ;
           return new Maybe(step)  ;
        } ;
    }

    public Generator<SEQ_RT_info,STEP> useConstructor(Class... paramtypes) {
		List<Constructor> constructors = testingScope.constructors(paramtypes) ;
    	int N = constructors.size() ;
    	//System.out.println("==> N=" +  N);
    	   
        return info -> {
           if (N==0) return null ;
           // randomly select a constructor from the candidates-list:
           Constructor co ;
           if (N==1) co = constructors.get(0) ;
           else co = constructors.get(rnd.nextInt(N)) ;
           //System.out.println("==> co=" +  co.getName());
           Maybe<STEP> step = useConstructor(co).generate(info) ;
           //System.out.println("==> step=" +  step);
           return step ;
        }   ;
    }


    public Generator<SEQ_RT_info,STEP> useCreationMethod(Method cm) {
    	return info -> {
            Parameter[] params = cm.getParameters() ;
            STEP[] argSteps = new STEP[params.length] ;
            Type[] paramsTypes = cm.getGenericParameterTypes() ;
            JTfun concreteType = new JTfun(testingScope.CUT) ;
            concreteType.args = new JType[params.length] ;

            for (int i=0; i<argSteps.length; i++) {
            	// solve the type of i-th the argument: 
          	    JType argty = Solver.solve(testingScope.impsMap, info.tySubsts, JTypeUtils.convert(paramsTypes[i])) ;
          	    // if the type of arg-i cannot be solved; fail:
          	    if (argty==null) return null ;
          	    concreteType.args[i] = argty ;
          	    PARAM p = new PARAM(params[i].getName(),argty) ;
                Maybe<STEP> step_ = valueGenerator.generate(p) ;
                // valueGenerator should not actually return null
                if (step_ ==null) return null ;
                argSteps[i] = step_.val ;
            }
            METHOD step = new METHOD(cm,null,argSteps) ;
            step.isObjectUnderTestCreationStep = true ;
            return new Maybe(step)  ;
    	} ;	
    }
    
    
    public Generator<SEQ_RT_info,STEP> useCreationMethod(String name, Class... paramtypes) {
    	List<Method> creatormethods = testingScope.creatorMethods(name, paramtypes) ;
    	int N = creatormethods.size() ;
    	//System.out.println("*** " + testingScope.CUT.getName() + ", #creatormethdo = " + N) ;
        return info -> {
        	if (N==0) return null ;
            // randomly select a creation method from the list of candidates:
            Method cm ;
            if (N==1) cm = creatormethods.get(0) ;
            else  cm = creatormethods.get(rnd.nextInt(N)) ;    
            //System.out.println(">>> " + cm.getName()) ;
            return useCreationMethod(cm).generate(info) ;
        }   ;
    }

    // WP NOTE: don't think this will generate a constructor from a subclass...
    // the call to the Solver below will only instantiate CUT's type variables,
    // and won't look for for its subclass.
    public Generator<SEQ_RT_info,STEP> useSubclassConstructor() {
        return info -> {
        	//System.out.println("**>> info:" + info) ;
        	Map<String,JType> myTySubsts = JTypeUtils.cloneTypeSubsts(info.tySubsts) ;
        	JType CUT = JTypeUtils.convert(testingScope.CUT).subst(myTySubsts) ;
        	JType solution = Solver.solve(testingScope.impsMap, myTySubsts,CUT)  ;
        	//System.out.println("**>> solution:" + solution) ;
        	if (solution==null) return null ;
        	// if the solution is just the same as CUT, let it fail:
        	if (((JTfun) solution).fun == testingScope.CUT) return null ;
        	// WP NOTE: wonder if it will ever pass to this point....
        	PARAM p = new PARAM(null,solution) ;
        	Maybe<STEP> step = valueGenerator.generate(p) ;    
        	if (step != null) {
        		if (step.val instanceof CONSTRUCTOR) {
        			((CONSTRUCTOR) step.val) .isObjectUnderTestCreationStep = true ;
        		}
        		else if (step.val instanceof METHOD) {
        			((METHOD) step.val).isObjectUnderTestCreationStep = true ;
        		}
        		else return null ;
        	}
        	return step ;
        } ;
    }
    
    //public Generator<SEQ_RT_info,STEP> useConstructorElseCreator() {
    //	return FirstOf(useConstructor(),useCreationMethod()) ;
    //}

    public Generator<SEQ_RT_info,STEP> createObjectUnderTest(String name, Class... paramtypes) {
    	if (name!=null) return useCreationMethod(name,paramtypes) ;
    	if (paramtypes.length==0) {
    		return FirstOf(
    				useCreationMethod(null,paramtypes).WithChance(0.2),
    				useConstructor(paramtypes),
    				useCreationMethod(null,paramtypes),
        			useSubclassConstructor()) ;
    	}
    	else return FirstOf(useConstructor(paramtypes)) ;
    }
    


}
