/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Step;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Generator.*;
import Sequenic.T3.JavaType.JType;
import Sequenic.T3.Pool;
import Sequenic.T3.Sequence.Datatype.* ;
import Sequenic.T3.TestingScope;
import Sequenic.T3.utils.*;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.Map;
import java.util.function.Predicate;
import java.util.logging.*;

/**
 * Provides utilities to lift STEP generator to ExecInfo generator. Npte that STEP is a meta
 * representation of a test-step. Generating it does not trigger its execution. On the other
 * hand, and ExecInfo is an info about the execution of such a step. So, the lifting effectively
 * (tries to) execute the step.
 */
public class STEPexec {

    /**
     * Re-execute the sequence to clear out the side effect of a new step we tried, 
     * e.g. on the pool.
     */
    private static SEQ_RT_info reExecutePrefix(SEQ sequence, Class CUT, Pool pool) {
    	// HACK!
    	// try to re-execute 2x ... for some reason if first re-execution fails,
    	// re-executing the 2nd time seems to succeed. Unclear why!??
    	try { return sequence.exec(CUT,pool) ; }
    	catch(Exception e0) {		
    		Logger.getLogger(CONSTANTS.T3loggerName).log(Level.WARNING,"1st attemp re-executing a test-sequence failed");  
    	    try {  return sequence.exec(CUT,pool) ; }
            catch (Exception e) {
               Logger.getLogger(CONSTANTS.T3loggerName).log(Level.WARNING,"2nd attemp re-executing a test-sequence failed" + e); 
               // Debug
               /*
               System.out.println("**>> Pool " + pool) ;
               int k=0;
               for (STEP step : sequence.steps)  { System.out.println("**>> " + k + " .. " + step) ; k++ ; }
               int N = sequence.steps.size() ;
               SEQ z = new SEQ() ;
               z.steps = new LinkedList<STEP>(sequence.steps) ;
               while (N>0) {
            	  N-- ;
            	  try {
            		z.exec(CUT,pool) ;	
             	  }
            	  catch(Exception f) { z.steps.removeLast() ; continue ; }
            	  System.out.println("**>> Failed at step no " + N  + " (counting from 0)") ;
            	  break ;
               }
               */
               return null ;
            }
    	}
    }
    
    /**
     * Try to generate a step that when executed does not fail, and furthermore 
     * satisfies the given requirement. This will be tried the given number of times.
     * If no such step can be found, null is returned.
     */
    public static Generator<SEQ_RT_info,SEQ_RT_info> until(
            Predicate<STEP_RT_info> requirement,
            int maxNumberOfRetry,
            Generator<SEQ_RT_info,STEP> stepgen,
            Class CUT,
            Pool pool)
    {
        return  info -> {

        	SEQ sequence = info.seq ;
        	
            for (int numberOfTry=0 ; numberOfTry< maxNumberOfRetry ; numberOfTry++)  {

                // System.out.println(">> re-trying a step... " + numberOfTry);
            	
            	// IMPORTANT: this part is error prone. As we try a new step, it will
            	// typically have side effects on the pool. So, when that step is
            	// dropped, we have to re-execute the step's prefix to clear out its
            	// side effects. In theory, the first step attempted, won't require
            	// its prefix to be re-executed; but we also need to make sure that
            	// the last attemped step, whether it succeeds or fail must be followed
            	// by re-execution of the prefix. 
            	// Below, we will just re-execute the prefix before trying a step. This
            	// is not the most efficient way, but it is less error prone:
            	info = reExecutePrefix(sequence,CUT, pool) ;
            	
            	if (info==null) continue ;

            	// generate a candidate step, and execute it:
            	Maybe<STEP> candidate = stepgen.generate(info) ;
            	if (candidate == null) {
            		// no need to re-execute the prefix here
            		continue ;
            	}
            	STEP_RT_info result = null ;
            	try {
            		result = candidate.val.exec(CUT,pool,null) ;
            		//System.err.println("*** step: " + candidate) ;
            		//System.out.println("*** result exc: " + result.exc) ;
            		/*
            		if (candidate.val instanceof METHOD) {
            		   System.err.println(">>> STEP " + candidate.val.toString()) ;
            		   if (result.exc != null) {
            			   System.err.println(">>> FAILING") ;
            			result.exc.printStackTrace(System.err);
            		   }
            		   else System.err.println(">>> SUCCESS") ;
            		}
            		*/
            		
            	}
            	catch(Throwable t) { 
            		//System.out.println("*** " + t) ;
            		//System.out.println("*** " + candidate) ;
            		//e.printStackTrace(System.out);
            		//System.err.println(">>> PROBLEMATIC") ;
            		continue ; 
            	}
            	
            	if (result == null) continue ; 
            	
                //if (result != null && result.val.lastInfo.exc !=null ) {
                //    System.err.println(">>> STEP " + result.val.lastInfo.step + " throws " + result.val.lastInfo.exc) ;
                //}

                if (requirement.test(result))  {
                    // a matching step is found, return it:
                    //System.err.println(">>> STEP-exec succeeds: " + ((STEP) result.step) + ", exception=" + result.exc) ;
                    info.lastInfo = result ;
                    info.seq.steps.add(candidate.val) ;
                    info.executionCounter ++ ;
                    if (candidate.val.isCreationStep() && result.returnedObj!=null) {
                    	//System.out.println("**>> STEPexec: " + candidate.val + ", result: " + result.returnedObj) ;
                    	pool.markAsObjectUnderTest(result.returnedObj) ;
                    	// System.out.println("**>> index of_OUT = " + pool.getIndexOfObjectUnderTest()) ;
                    }
                    return new Maybe(info) ;
                }

            }
            // max. number of tries exceeded:
            // System.err.println(">>> STEP-exec-until FAIL.. exceeding max. number of attemp") ;
            return null;
        }  ;
    }

    
    
    /**
     * Generate and execute a step; retries until the step throws an exception.
     */
    /*
    public static Generator<SEQ_RT_info,SEQ_RT_info> untilException(
            int maxNumberOfRetry,
            Generator<SEQ_RT_info,STEP> stepgen,
            Class CUT,
            Pool pool
    )
    {
        return until(r -> r.exc != null,
                maxNumberOfRetry,
                stepgen,
                CUT,
                pool
        ) ;
    }
*/
    /**
     * Generate and execute a step; retries until the step throws a post-condition-like
     * violation.
     */
    /*
    public static Generator<SEQ_RT_info,SEQ_RT_info> untilViolation(
            int maxNumberOfRetry,
            Generator<SEQ_RT_info,STEP> stepgen,
            Class CUT,
            Pool pool
    )
    {
        return until(r -> r.raisesViolation(),
                maxNumberOfRetry,
                stepgen,
                CUT,
                pool
        ) ;
    }
    */
    
    /**
     * Generate and execute a step; retries until the step does not throw an assumption-like
     * exception. It may throw a post-condition-like exception.
     */
    /*
    public static Generator<SEQ_RT_info,SEQ_RT_info> untilNoAsmViolation(
            int maxNumberOfRetry,
            Generator<SEQ_RT_info,STEP> stepgen,
            Class CUT,
            Pool pool
    )
    {
        return until(r -> !r.inputWasIncorrect(),
                maxNumberOfRetry,
                stepgen,
                CUT,
                pool
        ) ;
    }
    */
    
    /**
     * Generate and execute a step; retries until the step does not throw any exception.
     */
    /*
    public static Generator<SEQ_RT_info,SEQ_RT_info> untilNoException(
            int maxNumberOfRetry,
            Generator<SEQ_RT_info,STEP> stepgen,
            Class CUT,
            Pool pool
    )
    {
        return until(r -> r.exc == null,
                maxNumberOfRetry,
                stepgen,
                CUT,
                pool
        ) ;
    }
    */

}
