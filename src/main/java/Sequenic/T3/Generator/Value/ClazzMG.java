package Sequenic.T3.Generator.Value;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.function.Predicate;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Generator.SomeObjGenerators;
import Sequenic.T3.JavaType.JTfun;
import Sequenic.T3.JavaType.JType;
import Sequenic.T3.JavaType.Solver;
import Sequenic.T3.Sequence.Datatype.METHOD;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.Sequence.Datatype.STEP;
import Sequenic.T3.utils.FUN;
import Sequenic.T3.utils.Maybe;

/**
 * Providing meta-generators for generating instances of "Class".
 */
public class ClazzMG {
	
	Method genclass_ = null ;

    public ClazzMG() {
    	try {
    	   genclass_ = SomeObjGenerators.class.getMethod("genClass", Integer.TYPE) ;
    	}
    	catch(Exception e) { }
    }


	
	public  Generator<PARAM,STEP> classMG(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
		return P -> {
			if (genclass_ == null) return null ;
			if (!(P.ty instanceof JTfun)) return null ;
			JTfun ty_ = (JTfun) P.ty ;
			Class F = ty_.fun ;
			//Map<String, JType> myTySubsts = Solver.solveClassTyvars(ImpsMap, ty_) ;
			
			if (Class.class.isAssignableFrom(F)) {
				JType Kty = new JTfun(Integer.class) ;
				Maybe<STEP> K = recvalueMetaGenerator.fun.generate(new PARAM("k",Kty)) ;
				if (K==null) return null ;
				STEP[] args = {K.val} ;
				return new Maybe(new METHOD(genclass_, null , args))   ;	
			} ;
			return null ;
		} ;
	}
	
	static public void main(String[] args) throws NoSuchMethodException, SecurityException {
		ClazzMG cmg = new ClazzMG() ;
	}

}
