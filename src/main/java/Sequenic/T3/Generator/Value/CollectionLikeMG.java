/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Value;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.Sequence.Datatype.STEP;
import Sequenic.T3.ImplementationMap;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.T3Random;
import Sequenic.T3.utils.*;
import static Sequenic.T3.Generator.GenCombinators.* ;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.stream.Collectors;

public class CollectionLikeMG {

    public Random rnd = T3Random.getRnd() ;
    public ImplementationMap ImpsMap ;

    /**
     * The size of arrays/collections/maps to generate.
     */
    public int size = 3 ;


    public CollectionLikeMG(int size,
                             ImplementationMap ImpsMap) {
        this.size = size ;
        this.ImpsMap = ImpsMap ;
    }

    private STEP[] mkArgs(Generator<PARAM,STEP> valueMetaGenerator, int N, JType argsType) {
        List<STEP> args = new LinkedList<STEP>() ;
        for (int n=0; n<N; n++) {
            Maybe<STEP> r = valueMetaGenerator.generate(new PARAM(null,argsType)) ;
            if (r==null) {
                //System.out.println(">> fail! ");
                // failing to generate an element; too bad, then simply don't include it
                continue ;
            }
            args.add(r.val) ;
        }
        /*  should not do this in parallel...
        List<STEP> args =  argsTypes_
                . stream()
                . parallel()
                . map(D -> { Maybe<STEP> r = recGenerator.fun.generate(new PARAM(D)) ;
                             // recGenerator should not return null !
                             assert r!=null ;
                             return r.val ;
                           }
                )
                . collect(Collectors.toList())    ;
        */
        STEP[] r = new STEP[args.size()] ;
        int i= 0 ;
        for (STEP S : args) {
            r[i] = S ;
            i++ ;
        }
        return r ;
    }
    
    public Generator<PARAM,STEP> array(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
         return P -> {
        	 // P.ty is assumed to have been solved by the caller:
        	 int dimension = JTypeUtils.getArrayDimension(P.ty) ;
             if (dimension <= 0) return null ;
             //System.out.println("**>> MG array: " + P.ty + ", dimension=" + dimension) ;
        	 
             Maybe<STEP> step ;
             if (dimension == 1) step = new Maybe(mkOneDimensionalArray(recvalueMetaGenerator, (JTfun) P.ty)) ;
             // arrays of higher dimension will be generated through 2D array generator:
             else {
            	 step = new Maybe(mkTwoDimensionalArray(recvalueMetaGenerator, (JTfun) P.ty)) ;
            	 //System.out.println("**>> producing " + step) ;
             }
             return step ;
         }  ;
    }
    
    
    
    STEP mkOneDimensionalArray(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator, JTfun arrayTy) {
    	JType elemType = arrayTy.args[0] ;
    	// choose the size to be generated:
        int actualSize = rnd.nextInt(size+1) ;
        STEP[] elems = mkArgs(recvalueMetaGenerator.fun,actualSize,elemType) ;
        return new CONSTRUCT_COLLECTION(arrayTy,elems) ;
    }
    
    // generating 2D array. It has a reasonable chance to produce a well aligned 2D array, and
    // otherwise it produces a jagged array.
    STEP mkTwoDimensionalArray(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator, JTfun arrayTy) {
    	JType subArrayType = arrayTy.args[0] ;
    	JType elemType = ((JTfun) subArrayType).args[0] ;
    	int dim1 = rnd.nextInt(size+1) ;
    	List<STEP> elemsDim1 = new LinkedList<STEP>() ;
    	// chose first whether to produce equal rows 2D array or a jagged 2D array:
    	float equalRowsProbability = 0.8f ;
    	if (rnd.nextFloat() <= equalRowsProbability) {
    		int dim2 = rnd.nextInt(size+1) ;
    		for (int k=0; k<dim1; k++) {
    			STEP[] elems = mkArgs(recvalueMetaGenerator.fun,dim2,elemType) ;
    			elemsDim1.add(new CONSTRUCT_COLLECTION(subArrayType,elems)) ;
    		}
    	}
    	else {
    		for (int k=0; k<dim1; k++) {
    			int dim2 = rnd.nextInt(size+1) ;
    			STEP[] elems = mkArgs(recvalueMetaGenerator.fun,dim2,elemType) ;
    			elemsDim1.add(new CONSTRUCT_COLLECTION(subArrayType,elems)) ;
    		}
    	}
    	STEP[] dummy = new STEP[0] ;
    	return new CONSTRUCT_COLLECTION(arrayTy,elemsDim1.toArray(dummy)) ;
    }
    

    public Generator<PARAM,STEP> collection(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
        return P -> {
        	// P.ty is assumed to have been solved by the caller:
            if (! P.ty.isConcreteCollection()) return null ;
            
            Class implClass = ImpsMap.getRndImp(JTypeUtils.getTopClass(P.ty)) ;
            if (implClass == null) return null ;
            JTfun ty_ = (JTfun) P.ty ;
            JTfun ty__ =  new JTfun(implClass,ty_.args) ;

            //System.out.println("**>> MG collection: " + ty__) ;
            
            JType elemType = null ;
            if (ty__.args.length == 0) elemType = new JTfun(SomeObject.class) ;
            else elemType = ty_.args[0] ;

            // choose the size to be generated:
            int actualSize = rnd.nextInt(size+1) ;
            STEP[] args = mkArgs(recvalueMetaGenerator.fun,actualSize,elemType) ;
            //for (int n=0; n<args.length; n++) System.out.println("**>> Arg" + n + " " + args[n]) ;
            return new Maybe(new CONSTRUCT_COLLECTION(ty__,args)) ;
        }  ;
    }    

    public Generator<PARAM,STEP> map(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
        return P -> {
        	// P.ty is assumed to have been solved by the caller:
            if (! P.ty.isConcreteMap()) return null ;
            
            Class implClass = ImpsMap.getRndImp(JTypeUtils.getTopClass(P.ty)) ;
            if (implClass == null) return null ;
            JTfun ty_ = (JTfun) P.ty ;
            JTfun ty__ =  new JTfun(implClass,ty_.args) ;
            
            JType elemType = null ;
            JType keyType  = null ;
            if (ty__.args.length < 2) {
                keyType  = new JTfun(Integer.class) ;
                elemType = new JTfun(SomeObject.class) ;
            }
            else {
                keyType  = ty__.args[0] ;
                elemType = ty__.args[1] ;
            }

            // choose the size to be generated:
            int actualSize = rnd.nextInt(size+1) ;

            STEP[] keys = mkArgs(recvalueMetaGenerator.fun,actualSize,keyType) ;
            STEP[] args = mkArgs(recvalueMetaGenerator.fun,actualSize,elemType) ;

            return new Maybe(new CONSTRUCT_COLLECTION(ty__,keys,args)) ;
        }  ;
    }

    public Generator<PARAM,STEP> collectionlike(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
        //return R -> {
        //		System.out.println("***>>> ty = " + R.ty) ;
        		return 
        		FirstOf( array(recvalueMetaGenerator), 
        		collection(recvalueMetaGenerator), 
        		map(recvalueMetaGenerator))
                . WithPreRequirement(P -> P.ty.isConcrete()) ;
        //} ;
    }

}
