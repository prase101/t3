/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Value;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.T3Random;
import Sequenic.T3.utils.Maybe;

import java.util.Random;

public class DoubleMG {

    public Random rnd = T3Random.getRnd() ;

    static public boolean isdoubleOrDouble(JType ty) {
        Class C = JTypeUtils.getTopClass(ty) ;
        if (C==null) return false ;
        return (C == Double.TYPE || C == Double.class) ;
    }

    /**
     * Construct a float generator that will randomly generates floats between
     * the specified lowerbound (inclusive) and upperbound (inclusive). The
     * floats are not really uniformly generated. Instead, a number between 0 and 1.0
     * is generated, then multiplied by the specified range.
     */
    public Generator<PARAM,STEP> roundedUniform(double lowerbound, double upperbound) {
        assert lowerbound < upperbound ;
        return
                P -> { if (isdoubleOrDouble(P.ty)) {
                    Double x = new Double(rnd.nextDouble()*(upperbound - lowerbound) + lowerbound)  ;
                    return new Maybe(new CONST(x, P.ty)) ;
                }
                else return null ;
                } ;
    }

}
