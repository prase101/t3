/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Value;

import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.T3Random;
import Sequenic.T3.utils.* ;

import java.util.Random;

public class IntMG {

    public Random rnd = T3Random.getRnd() ;

    static public boolean isIntOrInteger(JType ty) {
        Class C = JTypeUtils.getTopClass(ty) ;
        if (C==null) return false ;
        return (C == Integer.TYPE || C == Integer.class) ;
    }

    /**
     * Construct an int generator that will randomly and uniformly generates ints between
     * the specified lowerbound (inclusive) and upperbound (exclusive).
     */
    public Generator<PARAM,STEP> uniform(int lowerbound, int upperbound) {
        return
                P -> { if (isIntOrInteger(P.ty)) {
                    Integer x = new Integer(rnd.nextInt(upperbound - lowerbound) + lowerbound)  ;
                    return new Maybe(new CONST(x, P.ty)) ;
                }
                else return null ;
                } ;
    }

    /**
     * Construct a int generator, that will randomly and uniformly generates ints between
     * (and inclusive) -k and k. The distribution is uniform.
     */
    public Generator<PARAM,STEP> uniform(int k) {
        return uniform(-k,k+1) ;
    }

}
