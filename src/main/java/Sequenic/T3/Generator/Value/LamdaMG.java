package Sequenic.T3.Generator.Value;


import java.lang.reflect.Method;
import java.util.Map;
import java.util.function.*;

import Sequenic.T3.ImplementationMap;
import Sequenic.T3.Generator.*;
import Sequenic.T3.JavaType.JTfun;
import Sequenic.T3.JavaType.JType;
import Sequenic.T3.JavaType.JTypeUtils;
import Sequenic.T3.JavaType.Solver;
import Sequenic.T3.Sequence.Datatype.METHOD;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.Sequence.Datatype.STEP;
import Sequenic.T3.utils.FUN;
import Sequenic.T3.utils.Maybe;

/**
 * For generating Functions, Predicates, and BiFunctions.
 */
public class LamdaMG {
	
	ImplementationMap ImpsMap ;
	
	Method funcGen ;
	Method predGen ;
	Method bifuncGen ;
	
	public LamdaMG(ImplementationMap imap) {
		ImpsMap = imap ;
		Method[] ms = SomeObjGenerators.class.getMethods() ;
		for (Method m : ms) {
			String mname = m.getName();
			if (mname.equals("genConstantFunction")) funcGen = m ;
			else if (mname.equals("genConstantPredicate")) predGen = m ;
			else if (mname.equals("genConstantBiFunction")) bifuncGen = m ;
		}
	}
	
	public  Generator<PARAM,STEP> constlambdaMG(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
		return P -> {
			if (!(P.ty instanceof JTfun)) return null ;
			JTfun ty_ = (JTfun) P.ty ;
			Class F = ty_.fun ;
			Map<String, JType> myTySubsts = Solver.solveClassTyvars(ImpsMap, ty_) ;
			
			if (Predicate.class.isAssignableFrom(F)) {
				JType Aty = new JTfun(Object.class) ;
				JType Bty = new JTfun(Boolean.class) ;
				if (ty_.args.length>=1) Aty = ty_.args[0] ;
				Maybe<STEP> A = recvalueMetaGenerator.fun.generate(new PARAM("ignored",Aty)) ;
				if (A==null) return null ;
				Maybe<STEP> B = recvalueMetaGenerator.fun.generate(new PARAM("y",Bty)) ;
				if (B==null) return null ;
				STEP[] args = {A.val,B.val} ;
				return new Maybe(new METHOD(predGen, null , args))   ;	
			}
			
			if (BiFunction.class.isAssignableFrom(F)) {
				JType Aty = new JTfun(Object.class) ;
				JType Bty = new JTfun(Object.class) ;
				JType Cty = new JTfun(Object.class) ;
				if (ty_.args.length>=1) Aty = ty_.args[0] ;
				if (ty_.args.length>=2) Bty = ty_.args[1] ;
				if (ty_.args.length>=3) Cty = ty_.args[1] ;
				Maybe<STEP> A = recvalueMetaGenerator.fun.generate(new PARAM("ignored1",Aty)) ;
				if (A==null) return null ;
				Maybe<STEP> B = recvalueMetaGenerator.fun.generate(new PARAM("ignored2",Bty)) ;
				if (B==null) return null ;
				Maybe<STEP> C = recvalueMetaGenerator.fun.generate(new PARAM("y",Cty)) ;
				if (B==null) return null ;
				STEP[] args = {A.val,B.val, C.val} ;
				return new Maybe(new METHOD(bifuncGen, null , args))   ;	
			}
			
			if (Function.class.isAssignableFrom(F)) {
				JType Aty = new JTfun(Object.class) ;
				JType Bty = new JTfun(Object.class) ;
				if (ty_.args.length>=1) Aty = ty_.args[0] ;
				if (ty_.args.length>=2) Bty = ty_.args[1] ;
				Maybe<STEP> A = recvalueMetaGenerator.fun.generate(new PARAM("ignored",Aty)) ;
				if (A==null) return null ;
				Maybe<STEP> B = recvalueMetaGenerator.fun.generate(new PARAM("y",Bty)) ;
				if (B==null) return null ;
				STEP[] args = {A.val,B.val} ;
				return new Maybe(new METHOD(funcGen, null , args))   ;	
			}
			
			return null ;
		} ;
	}

}
