/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Value;

import Sequenic.T3.*;
import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.*;

import java.lang.reflect.*;
import java.util.*;
import java.util.logging.Logger;


public class ObjectMG {

    public Random rnd = T3Random.getRnd() ;

    public ImplementationMap ImpsMap ;

    /**
     * Indicates what the current level in the tree of nested object creation.
     */
    public int currentLevel = 0 ;

    /**
     * Indicate the maximum depth of object nesting that is allowed.
     */
    public int maxTreeDepth = 5 ;

    public ObjectMG(int maxLevelOfObjectNesting,
                     ImplementationMap imap)
    {
        assert maxLevelOfObjectNesting > 0 ;
        maxTreeDepth =  maxLevelOfObjectNesting ;
        ImpsMap = imap ;
    }

    private void increaseLevel() { currentLevel++ ; }
    private void decreaseLevel() { currentLevel-- ; }

    /**
     * Produces a generator that generates a call to a public constructor.
     * The valueMetaGenerator is the used top-level value meta-generator.
     */
    public Generator<PARAM,STEP> useConstructor(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
        return P -> {

        	// P.ty is assumed to have been solved, so it must be concrete:
        	// (solving it at the caller side leads to a problem)
        	assert P.ty.isConcrete() ;        	
        	assert P.ty instanceof JTfun ;
            JTfun ty_ = (JTfun) P.ty ;
            Class C =  ty_.fun ;
            assert C!=null ;

            if (currentLevel >= maxTreeDepth)  {
                return new Maybe<STEP>(new CONST(null,P.ty)) ;
            }

            
            // invariant: if before the invocation of a call to this generator the
            // level = L, then after the invocation the level has been restored to L.
            increaseLevel();
            try {
            
               Constructor Constr = ImpsMap.getRndConstructor(C) ;
               if (Constr==null) return null ;

               Class implClass = Constr.getDeclaringClass() ;

               // calculate new type-substitutions to be passed deeper to the arguments
               // of the constructor:
               Map<String, JType> myTySubsts = Solver.solveClassTyvars(ImpsMap, ty_) ;

               Parameter[] params = Constr.getParameters() ;
               STEP[] argSteps = new STEP[params.length] ;
               Type[] paramsTypes = Constr.getGenericParameterTypes() ;

               // HACK #paramTypes should be equal to #params, but some cases seem to violate this;
               // bug in Java8?? Try to put some fix...
               // Anyway, when they mismatch we will return a fail:
              if (paramsTypes.length != params.length) {
            	  Type[] fix = Constr.getParameterTypes() ;
            	  if (fix.length == params.length) {
            		  Logger.getLogger(CONSTANTS.T3loggerName).warning("** Strange mismatch in #args in constructor " 
                              + Constr + ", using getParameterTypes instead of getGenericParameterTypes.") ;
            		  paramsTypes = fix ;
            	  }
            	  else
            	  if (paramsTypes.length < params.length) {
                	  Logger.getLogger(CONSTANTS.T3loggerName).warning("** Strange mismatch in #args in constructor " 
                              + Constr + ", dropping it.") ;
            		  return null ;
            	  }
            	  else {
            		  Logger.getLogger(CONSTANTS.T3loggerName).warning("** Strange mismatch in #args in constructor " 	  
                               + Constr + ", dropping overflowing type-parameters (at the end side).") ;
            	      Type[] paramsTypes_ = new Type[params.length] ;
            	      for (int i=0; i<params.length; i++) paramsTypes_[i] = paramsTypes[i] ;
            	      paramsTypes = paramsTypes_ ;
            	  }
              }

               for (int i=0; i<argSteps.length; i++) {
                  // solve the type here! :
            	  JType paramTy =   Solver.solve(ImpsMap,myTySubsts,JTypeUtils.convert(paramsTypes[i])) ;
            	  // if no solution can be found, the we can't instantiate this constructor:
            	  if (paramTy==null) return null ;
                  PARAM p = new PARAM(params[i].getName(),paramTy) ;
                  Maybe<STEP> step_ = recvalueMetaGenerator.fun.generate(p) ;
                  // recGenerator should actually not return null
                  if (step_ == null) return null;
                  argSteps[i] = step_.val ;
               }

               // specifying the constructor to have tyimpl is safe, but leads to
               // less re-use of the objects in the pool :
               //return new Maybe(new CONSTRUCTOR(tyimpl,Constr,argSteps))   ;

               return new Maybe(new CONSTRUCTOR(P.ty,Constr,argSteps))   ;
            }
            finally {
                // make sure that the level is decreased again before leaving
                decreaseLevel();
            }
        }  ;
    }


    /**
     * Produces a generator that generates a call to a random creator-like
     * method that can produce an instance of P.
     */
    public Generator<PARAM,STEP> useCreationyMethod(FUN<Generator<PARAM,STEP>> recvalueMetaGenerator) {
        return P -> {

        	// P.ty is assumed to have been solved, so it must be concrete:
            assert P.ty.isConcrete() ;
            assert P.ty instanceof JTfun ;
            JTfun ty_ = (JTfun) P.ty ;
            Class C =  ty_.fun ;
            assert C!=null ;

            if (currentLevel >= maxTreeDepth)  {
                return new Maybe<STEP>(new CONST(null,P.ty)) ;
            }

            // invariant: if before the invocation of a call to this generator the
            // level = L, then after the invocation the level has been restored to L.
            increaseLevel();
            try {
            	//else System.out.println(">>> " + C.getName());
                Method CM = ImpsMap.getRndCreationyMethod(C) ;
                if (CM==null) return null ;
                Class implClass = CM.getDeclaringClass() ;

                // calculate new type-substitutions to be passed deeper to the arguments
                // of the constructor:
                Map<String, JType> myTySubsts = Solver.solveClassTyvars(ImpsMap, ty_) ;
                
                Parameter[] params = CM.getParameters() ;
                STEP[] argSteps = new STEP[params.length] ;
                Type[] paramsTypes = CM.getGenericParameterTypes() ;

                //S ystem.err.println(">>>useCreatorMethod #params=" + params.length + ", #paramTypes="+paramsTypes.length);
                // assert paramsTypes.length == params.length : "useCreatorMethod" ;

                // HACK #paramTypes should be equal to #params, but some cases seem to violate this;
                // bug in Java8??
                // Anyway, when they mismatch we will return a fail:
                if (paramsTypes.length != params.length) return null ;

                for (int i=0; i<argSteps.length; i++) {
                	// solve the type here! :
              	    JType paramTy =   Solver.solve(ImpsMap,myTySubsts,JTypeUtils.convert(paramsTypes[i])) ;
              	    // if no solution can be found, the we can't instantiate this constructor:
              	    if (paramTy==null) return null ;
                    PARAM p = new PARAM(params[i].getName(),paramTy) ;
                    Maybe<STEP> step_ = recvalueMetaGenerator.fun.generate(p) ;
                    // recGenerator should actually not return null
                    if (step_ == null) return null;
                    argSteps[i] = step_.val ;
                }

                return new Maybe(new METHOD(CM,null,argSteps))   ;
            }
            finally {
                // make sure that the level is decreased again before leaving
                decreaseLevel();
            }
        }  ;
    }

    public static void main(String[] args) {

    }
}
