/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Generator.Value;


import Sequenic.T3.Generator.Generator;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.JavaType.JTfun;
import Sequenic.T3.JavaType.JType;
import Sequenic.T3.JavaType.JTypeUtils ;
import Sequenic.T3.Pool;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.T3Random;
import Sequenic.T3.utils.Maybe;

import java.util.Random;

public class REFMG {

    public Random rnd = T3Random.getRnd() ;
    public Pool pool ;

    public REFMG(Pool pool) { this.pool = pool ;}

    public Generator<PARAM,STEP> random() {
        return P -> {
            Integer k = pool.rndGetIndex(P.ty) ;
            if (k!=null) {
            	//System.out.println("===> REUSING obj: " + pool.get(k)) ;
                return new Maybe(new REF(k)) ;
            }
            else {
                // System.out.println(">> Asking for " + P.cty + " from the pool, but found none!") ;
                return null ;
            }
        }  ;
    }

    public Generator<PARAM,STEP> objectUnderTest() {
        return P -> {
            Integer k = pool.getIndexOfObjectUnderTest() ;
            if (k==null) return null ;
            if (P.ty.equals(pool.getTypeOfObjectUnderTest())) {
            	//System.out.println("#### YES") ;
                return new Maybe(new REF(k)) ;
            }
            else return null ;
        }  ;
    }

    public Generator<PARAM,STEP> objectUnderTestIfSubclass() {
        return P -> {
            Integer k = pool.getIndexOfObjectUnderTest() ;
            if (k==null) return null ;
            Class Ctobj = pool.getObjectUnderTest().getClass() ;
            //System.out.println("#### P.ty = " + JTypeUtils.getTopClass(P.ty) + " vs " + Ctobj.getName()) ;
            if (JTypeUtils.getTopClass(P.ty).isAssignableFrom(Ctobj)) {
            	//System.out.println("#### ok") ;
                return new Maybe(new REF(k)) ;
            }
            else return null ;
        }  ;
    }

    public static void main(String[] args) {
        Pool pool = new Pool() ;
        JType intTy = new JTfun(Integer.class) ;
        PARAM i = new PARAM("i",intTy) ;
        pool.put(intTy, new Integer(100)) ;
        Generator<PARAM,STEP> gen = new REFMG(pool).random() ;
        System.out.println("** " + gen.generate(i).val) ;
        pool.put(intTy,new Integer(99)) ;
        System.out.println("** " + gen.generate(i).val) ;
    }
}
