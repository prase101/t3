/*
 * Copyright 2014 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

import java.io.* ;
import java.lang.reflect.*;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.* ;
import java.util.Map.Entry;
import java.util.concurrent.* ;
import java.util.logging.*;
import java.util.stream.Collectors;

import Sequenic.T3.Examples.Friends.Person;
import Sequenic.T3.Reflection.ClassesScanner;
import Sequenic.T3.Sequence.Datatype.*;
import Sequenic.T3.utils.MyEnum;
import Sequenic.T3.utils.SomeObject;


/**
 * This class essentially implements a mapping between classes, and how their instance
 * can be created. These are used for creating values needed as parameters in our
 * test-sequences. These values are in themselves part of the testing-scope, so we will
 * just limit ourselves to public-way of creating thoses instances.
 *
 * A class is called publicly instantiable if it is not an interface,
 * and it has either a public constructor, or a public creation-method. A creation-method
 * of a class C is a static method of C that returns an instance of C, and takes no parameter
 * of type C. Note that an abstract class can be publicly instantiable if it provides
 * a concrete creation method.
 *
 * We will here maintain a map between classes that are not publicly self-instantiable (in 
 * particular abstract classes and interfaces), and their publicly self-instantiable 
 * implementations.
 *
 * NOTE, TO DO: the class maintains two data structures for two purposes: (1) infomation on
 * how to instantiate a set of classes, and (2) a certain kind of subclassing information
 * -- see above. The logic of (1) is fully isolated here. (1) is used by value generators.
 * (2) is used by the the class TestingScope, which then adds its own logic to infer how
 * in instantiate CUT. The whole logic around (2) is used by target-obj generator to instantiate CUT.
 * 
 * Having two logics for doing almost the same thing is ERROR PRONE and should be
 *  (TO DO) consolidated to a single logic.
 *
 */
public class ImplementationMap {


    public static Class[] standardKnownInstantiableClasses() {
        Class[] classes = {
                LinkedList.class,
                HashSet.class,
                HashMap.class,
                SomeObject.class,
                Integer.class,  Long.class, Byte.class, Short.class, Float.class, Double.class, Boolean.class,
                MyEnum.class,
                FileWriter.class,
                FileReader.class,
                StringBuilder.class,
                StringWriter.class,
                org.xml.sax.helpers.AttributesImpl.class
        }  ;
        return classes ;
    }

    static  public class ConcurrentSet<T> {
    	ConcurrentHashMap<T,Integer> s = new ConcurrentHashMap<T,Integer>() ; 	
    	public void register(T x) { s.put(x,1) ; }
    	
    	public T getRnd() {
    		int N = s.size() ;
    		if (N<=0) return null ;
    		int k = T3Random.getRnd().nextInt(N) ;
    		int i=0 ;
    		for (Entry<T,Integer> e : s.entrySet()) {
    			if (i==k) return e.getKey() ;
    			i++ ;
    		}
    		return null ;
    	}
    	
    	public int size() { return s.size() ; }
    	
    	public List<T> allMembers() {
    		return s.keySet().stream().collect(Collectors.toList()) ;
    	}
    	
    }
    
    /**
     * This contains lists of public constructors and methods that can be used to
     * instantiate C from any location (so, publicly).
     */
    public static class Instantiation {
    	Class C ;
    	// public construtors declared in C
    	ConcurrentSet<Constructor> directConstructors = new ConcurrentSet<Constructor>() ;
    	// public constuctors of subclasses of C
    	ConcurrentSet<Constructor> indirectConstructors = new ConcurrentSet<Constructor>() ;
    	// public creator methods declared in C
    	ConcurrentSet<Method> creationMethods = new ConcurrentSet<Method>() ;
    	// public (and static) creator-like methods from any other class, that can instantiate C
    	ConcurrentSet<Method> creationLikeMethods = new ConcurrentSet<Method>() ;
    	
    	Instantiation(Class C) { this.C = C ; }
    	
    	public boolean hasKnownPublicInstantiationMethod() {
    		/*
    		System.err.println(">>>>> " + C.getName() 
    		   + ", #dconstr=" + directConstructors.size()
    		   + ", #indirConstr=" + indirectConstructors.size()
    		   + ", #cretm=" + creationMethods.size()
    		   + ", #cretlikem=" + creationLikeMethods.size()
    		   ) ;
    		*/
    		return directConstructors.size() > 0 || indirectConstructors.size() > 0 
    				|| creationMethods.size() > 0 || creationLikeMethods.size() > 0 ;
    	}
    	
    	// true if C is not abstract nor interface, and has at least one public way
    	// to instantiate itself.
    	public boolean hasKnownPublicSelfInstantiationMethod() {
    		if (Modifier.isAbstract(C.getModifiers()) || C.isInterface()) return false ;
    		return directConstructors.size() > 0 || creationMethods.size() > 0 ;
    	}
    	
    	void register(Constructor co) {
    		Class D = co.getDeclaringClass() ;
    		if (C == D) directConstructors.register(co) ;
    		else if (C.isAssignableFrom(D)) indirectConstructors.register(co) ;
    	}
    	
    	void register(Method m) {
    		Class D = m.getDeclaringClass() ;
    		if (C == D) creationMethods.register(m) ;
    		else creationLikeMethods.register(m) ;
    	}
    	
    	Constructor getRndConstructor() {
    		Constructor co = directConstructors.getRnd() ;
    		if (co != null ) return co ;
    		return indirectConstructors.getRnd() ;
    	}
    	
    	Method getRndCreationyMethod() {
    		Method m = creationMethods.getRnd() ;
    		if (m != null) return m ;
    		return creationLikeMethods.getRnd() ;
    	}   	
    }
    
    /**
     * The set of all classes known to this map, along with information on how to
     * publicly instantiate them, as far as this is known.
     */
    public ConcurrentHashMap<Class,Instantiation> knownClasses ;
    
 
    /**
     * This maps interfaces and abstract classes C to a set of concrete and instantiable
     * implementation classes D1,D2,... 
     */
    public ConcurrentHashMap<Class, ConcurrentSet<Class>> impsMap  ;

    /**
     * Constructor. It will pre-populate the map the given classes, and by
     * scanning the given directory for classes.
     */
    public ImplementationMap(String[] classesDirs, Class ... someKnownClasses) {

    	knownClasses = new ConcurrentHashMap<Class,Instantiation>(200)   ;
    	impsMap = new ConcurrentHashMap<Class, ConcurrentSet<Class>>(50) ;

        // populate with the given classes first:
        for (Class C : someKnownClasses) registerClass(C) ;        
        for (Class C : standardKnownInstantiableClasses()) registerClass(C) ;

        // further populate by scanning:
        ClassesScanner scanner = new ClassesScanner(classesDirs) ;
        scanner.scan();
        for (Class C : scanner.getScanned()) registerClass(C) ;
    }
    
    public void registerClass(Class C) {
    	if (knownClasses.containsKey(C)) return ;
    	Instantiation instantiation = new Instantiation(C) ;
    	
    	if (! Modifier.isAbstract(C.getModifiers())) {
    		for (Constructor co : C.getDeclaredConstructors()) {
        		int m = co.getModifiers() ;
        		if (Modifier.isPublic(m) && ! Modifier.isAbstract(m)) instantiation.register(co) ;
        		//if (!Modifier.isPrivate(m) && ! Modifier.isAbstract(m)) instantiation.register(co) ;
        	}
    	}
    	for (Method m : getPublicCreatorMethods(C)) {
    		instantiation.register(m);
    	}
    	
    	for (Entry<Class,Instantiation> e : knownClasses.entrySet()) {
    		Class D = e.getKey() ;
    		if (C == D) continue ;
    		Instantiation Di = e.getValue() ;
    		if (C.isAssignableFrom(D)) {
    			for (Constructor co : Di.directConstructors.allMembers()) 
    				instantiation.register(co) ;
    		}
    		if (D.isAssignableFrom(C)) {			
    			for (Constructor co : instantiation.directConstructors.allMembers()) 
    				Di.register(co); 
    		}
    		for (Method m : getPublicCreatorLikeMethods(C,D)) Di.register(m);
    		for (Method m : getPublicCreatorLikeMethods(D,C)) instantiation.register(m);
    	}
    	
    	knownClasses.put(C, instantiation) ;
    	
    	// if C is an interface or abstract, add it to impsMap:
    	//if (C.isInterface() || Modifier.isAbstract(C.getModifiers())) {
    	// If C is not publicly self-instantiable:
    	if(!instantiation.hasKnownPublicSelfInstantiationMethod()) {
    		ConcurrentSet<Class> impsOfC = new ConcurrentSet<Class>() ;
    		for (Entry<Class,Instantiation> e : knownClasses.entrySet()) {
    			Class D = e.getKey() ;
    			Instantiation Di = e.getValue() ;
    			// This is wrong logic:
    			//if (C != D && C.isAssignableFrom(D) && Di.hasKnownPublicInstantiationMethod()) impsOfC.register(D) ;
    			// Only add D if it is publicly self-instantiable:
    			if (C != D && C.isAssignableFrom(D) && Di.hasKnownPublicSelfInstantiationMethod()) impsOfC.register(D) ;
    		}
    		impsMap.put(C, impsOfC) ;
    	}
    	else {
    		// if C is concrete and instantiable, check if it can be added as an implementation to
    		// classes already in the impsMap:
    		//if (instantiation.hasKnownPublicInstantiationMethod()) { <-- wrong!
    		if (instantiation.hasKnownPublicSelfInstantiationMethod()) {
    	    	for (Entry<Class,ConcurrentSet<Class>> e : impsMap.entrySet()) {
    				Class D = e.getKey() ;
    				if (C != D && D.isAssignableFrom(C)) e.getValue().register(C); 
    	    	}
    		}
    	}
    }
    
    
    /**
     * Return the creator-methods of the class C.
     */
    public static List<Method> getPublicCreatorMethods(Class C) {
        return getPublicCreatorLikeMethods(C,C) ;
    }

    /**
     * Return a list of public static methods declared in Chost, that can construct
     * an object of class C, and does not take C itself as parameter.    
     */
    public static List<Method> getPublicCreatorLikeMethods(Class Chost, Class C) {
    	List<Method> creatorMethods =  new LinkedList<Method>() ;
    	// if (Chost.isInterface() ||  Modifier.isAbstract(Chost.getModifiers())) return null ;
        if (Chost.isInterface()) return creatorMethods ;
        
        Method[] declaredmethods = null ;
        try {
            // this can fail :(
            declaredmethods = Chost.getDeclaredMethods() ;
        }
        catch (Throwable e) { return creatorMethods ; }
        for (Method m : declaredmethods) {
            int mod = m.getModifiers() ;
            if(! Modifier.isStatic(mod) ||  ! Modifier.isPublic(mod) || Modifier.isAbstract(mod)) continue ;
            //if(! Modifier.isStatic(mod) ||  Modifier.isPrivate(mod) || Modifier.isAbstract(mod)) continue ;
            if(! C.isAssignableFrom(m.getReturnType())) continue ;
            boolean takeC = false ;
            for (Class D : m.getParameterTypes())
                if (D==C) { takeC = true ; break ; }
            if (! takeC) {
                creatorMethods.add(m) ;
            }
        }
        return creatorMethods ;
    }

    public Constructor getRndConstructor(Class C) {
    	if (!knownClasses.containsKey(C)) registerClass(C) ;
    	return knownClasses.get(C).getRndConstructor() ;
    }
    
    public Method getRndCreationyMethod(Class C) {
    	if (!knownClasses.containsKey(C)) registerClass(C) ;
    	return knownClasses.get(C).getRndCreationyMethod() ;
    }
    
    public boolean hasKnownInstantiationMethod(Class C) {
    	if (!knownClasses.containsKey(C)) registerClass(C) ;
    	return knownClasses.get(C).hasKnownPublicInstantiationMethod() ;
    }
    
    public int numberOfInstantiable() {
    	int k = 0 ;
    	for (Entry<Class,Instantiation> e : knownClasses.entrySet()) {
    		if (e.getValue().hasKnownPublicInstantiationMethod()) k++ ;
    	}
    	return k ;
    }
    
    /**
     * Get an implementation class of C. If C is already publicly self-instantiable, return C itself. Else
     * we look in the impsMap. If no implementation map can be found, return null.
     */
    public Class getRndImp(Class C) {
    	if (!knownClasses.containsKey(C)) registerClass(C) ;
    	Instantiation I = knownClasses.get(C) ;
    	//if (I.hasKnownPublicInstantiationMethod()) return C ; <-- wrong!
    	if (I.hasKnownPublicSelfInstantiationMethod()) return C ; 
    	if (!impsMap.containsKey(C)) return null ;
    	//Logger.getLogger(CONSTANTS.T3loggerName).info("########  " + C.getName() + " has no known instantiation method!") ;
    	return impsMap.get(C).getRnd() ;
    }
    
    public void print() {
    	StringBuilder sb = new StringBuilder() ;
    	sb.append("** Known classes:") ;
    	int k = 0 ;
    	for (Entry<Class,Instantiation> e : knownClasses.entrySet()) {		
    		
    		Class D = e.getKey() ;
    		Instantiation I = e.getValue() ;
    		sb.append("\n     " + D.getName()) ;
    		
    		if (I.hasKnownPublicInstantiationMethod()) 
    			sb.append(", instantiable dc/cm/ic/clm:"
    					+ I.directConstructors.size() + "/"	
    					+ I.creationMethods.size() + "/"
    					+ I.indirectConstructors.size() + "/"
    					+ I.creationLikeMethods.size() + "."
    					) ;
    		else sb.append(", not instantiable.") ;
    		if (D.isInterface() || Modifier.isAbstract(D.getModifiers())) {
    			sb.append(" Imps: ") ;
    			List<Class> imps = impsMap.get(D).allMembers() ;
    			if (imps.isEmpty()) sb.append(" none.") ; 
    			else {
    			    int j = 0 ;
    				for (Class E : imps) { 
    					if (j>0) sb.append(",") ;
    					sb.append(" " + E.getName()) ;
    					j++ ;
    			    }
    			}
    		}
    		k++ ;	
    	}
    	sb.append("\n** Total classes = " + k) ;
    	System.out.print(sb.toString());
    }
    
    /**
     * Read entries from an imap.txt file. Such a file contains lines of the form:
     * 
     *      C:C1:C2:C3...
     *      
     * Where C and Ci are class names, and Ck's are all subclasses of C. This function
     * add all these classes into this Implementation-map.
     */
    public void addFromImapFile(ClassLoader classloader, String dir) throws IOException {
    	Path path =FileSystems.getDefault().getPath(dir,"imap.txt") ;
		String[] lines = Files.readAllLines(path).toArray(new String[0]) ;
		int parsed = 0 ;
		int added = 0 ;
		for (String line : lines) {
			String[] cnames = line.split(":") ;
			for (int i=1; i<cnames.length; i++) {
				parsed++ ;
				try {
					Class C =   classloader.loadClass((cnames[i])) ;
					registerClass(C) ;
					added++ ;
				}
				catch(Throwable t) { }
			}
		}
		Logger.getLogger(CONSTANTS.T3loggerName).info("Parsed "
				+ parsed + " class names from imap.txt, adding "
				+ added + " to an internal imap.");
    }
    
    public static void main(String[] args) throws IOException {
    	ImplementationMap IM = new ImplementationMap(new String[0]) ;
    	IM.registerClass(Sequenic.T3.Examples.Item.class);
    	IM.registerClass(Sequenic.T3.Examples.InterfaceAndAbstract.Car.class);
    	IM.registerClass(Sequenic.T3.Examples.InterfaceAndAbstract.Vehicle.class);
    	
    	IM.print();
    	
        String[] dirs = {"/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin"} ;
        IM = new ImplementationMap(dirs) ;
        IM.registerClass(Collection.class);
        IM.registerClass(File.class);
        //IM.print();
        
        List<Class> imps = IM.impsMap.get(STEP.class).allMembers() ;
        for (Class I : imps) System.out.println("STEP's imp: " + I) ;
        imps = IM.impsMap.get(Collection.class).allMembers() ;
        for (Class I : imps) System.out.println("Collection's imp: " + I) ;
        System.out.println("Constructor for Colelction:" + IM.getRndConstructor(Collection.class)) ;
        System.out.println("Constructor for Set:" + IM.getRndConstructor(Set.class)) ;
        System.out.println("Constructor for List:" + IM.getRndConstructor(List.class)) ;
    }


}
