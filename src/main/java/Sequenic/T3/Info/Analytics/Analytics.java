package Sequenic.T3.Info.Analytics;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class Analytics {

	static Map<String,DataTable> mydata = new HashMap<String,DataTable>() ;
	
	public static void registerDataTable(String name, String... columnNames) {
		DataTable dt = new DataTable(columnNames.length) ;
		dt.addHeaderRow(columnNames) ;
		mydata.put(name,dt) ;
	}
	
	public static void removeDataTable(String name) {
		mydata.remove(name) ;
	}
	
	public static DataTable getDataTable(String name) {
		return mydata.get(name) ;
	}
	
	/**
	 * The added values should be either an integer, a double, or a string.
	 */
	public static void addRow(String tableName, Object... values) {
		DataTable dt = mydata.get(tableName) ;
		if (dt != null) dt.addRow(values) ;
	}
	
	
	public static void clear() {
		mydata.clear(); 
	}
	
	public static void save(String dirpath, String prefix, String suffix) throws IOException {
		for (Entry<String,DataTable> E : mydata.entrySet()) {
			String prefix_ = "" ;
			if (prefix != null) prefix_ = prefix + "_" ;
			String suffix_ = "" ;
			if (suffix != null) suffix_ = "_" + suffix  ;
			String fname = dirpath 
					+ "/" + prefix_
					+ E.getKey() 
					+ suffix_ + ".txt" ;
			E.getValue().save(fname);
		}
	}
	
	
	public static void main(String[] args) throws IOException {
		Analytics.clear(); 
		Analytics.registerDataTable("temperature","temp","day");
		Analytics.registerDataTable("fun","happy","day");
		Analytics.addRow("temperature",30d,1) ;
		Analytics.addRow("temperature",31d,2) ;
		Analytics.addRow("fun",99d,1) ;
		Analytics.addRow("fun",0d,2) ;
		Analytics.save("/Users/iswbprasetya/tmp/t3","mydata","xxx") ;
	}

}
