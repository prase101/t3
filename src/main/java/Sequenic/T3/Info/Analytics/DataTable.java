package Sequenic.T3.Info.Analytics;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.* ;

/**
 * 
 * @author iswbprasetya
 *
 */
public class DataTable {
	
	int dimension ;
	public String separator = ":" ;
	boolean isInteger = false ;
	String[] headerrow = null ;
	List<Object[]> rows = new LinkedList<Object[]>() ; // elements should be either a string, integer, or double
	
	public DataTable(int dimension) {
		this.dimension = dimension ;
	}
	
	public DataTable addHeaderRow(String... columnNames) {
		if (columnNames.length != dimension) throw new IllegalArgumentException() ;
		headerrow = columnNames ;
		return this ;
	}
	
	public DataTable addRow(Object... values) {
		if (values.length != dimension) throw new IllegalArgumentException() ;
		rows.add(values) ;
		return this ;
	}
		
	public String toString() {
		StringBuffer s = new StringBuffer() ;
		if(headerrow != null) {
			for(int k=0; k<dimension; k++) {
				if (k>0) s.append(separator) ;
				s.append(headerrow[k]) ;
			}
			s.append("\n") ;
		}
		for (Object[] R : rows) {
			for(int k=0; k<dimension; k++) {
				if (k>0) s.append(separator) ;
				s.append(R[k].toString()) ;
			}
			s.append("\n") ;
		}
		return s.toString() ;				
	}
	
	/**
	 * Save the data to a text file.
	 */
    public void save(String filename) throws IOException  {
    	Path path = Paths.get(filename);
        Files.write(path, toString().getBytes());    	
    }
	
	
	public static void main(String[] args) throws IOException {
		DataTable dt = new DataTable(2) ;
		dt.addHeaderRow("x","y") ;
		dt.addRow(23,40d) ;
		dt.addRow(0.01d,9.000000001d) ;
		System.out.println(dt.toString()) ;
		dt.save("/Users/iswbprasetya/tmp/t3/somedata.txt") ;
	}
	
	

}
