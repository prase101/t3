package Sequenic.T3.Info;

import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.CoverageBuilder;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.IMethodCoverage;
import org.jacoco.core.analysis.ICounter;
import org.jacoco.core.data.ExecutionData;
import org.jacoco.core.data.ExecutionDataStore;
import org.jacoco.core.data.IExecutionDataVisitor;
import org.jacoco.core.data.SessionInfoStore;
import org.jacoco.core.instr.Instrumenter;
import org.jacoco.core.runtime.IRuntime;
import org.jacoco.core.runtime.LoggerRuntime;
import org.jacoco.core.runtime.RuntimeData;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.utils.Pair;

import java.util.* ;
import java.util.logging.Logger;
import java.io.* ;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * A class that can provide on-line code coverage information on the CUT.
 * It requires an instance of JacocoInstrumenter that was used to instrument
 * the CUT. This instrumenter holds a pointer to the instrumented CUT.
 */
public class CodeCoverage {
	
	private Class iCUT ;
	private byte[] originalCodeOfCUT ;
	
	// administration data
	private RuntimeData rtdata ; 
	private CumulativeExecDataStore cumulativeExecDataStore = new CumulativeExecDataStore(new ExecutionDataStore()) ; 
	private SessionInfoStore sessionInfos = new SessionInfoStore();
	private CodeCoverageInfo covInfo ;
	private Analyzer analyzer ;
	
	public JacocoInstrumenter instrumenter ;
	

	/**
	 * Create an instance of code-coverage runtime tracking. Pass on the instance of
	 * JacoccoInstrumenter used to instrument the CUT. It holds a pointer to the
	 * instrumented version of the CUT, as well as its original code.
	 * @throws Exception 
	 */
	public CodeCoverage(JacocoInstrumenter instrumenter) throws Exception {	
	  this.instrumenter = instrumenter ;
      rtdata = new RuntimeData() ;
      instrumenter.getJacocoLogger().startup(rtdata);
	  // preparing the analyzer:
	  covInfo = new CodeCoverageInfo(new CoverageBuilder()) ;
	  analyzer = new Analyzer(cumulativeExecDataStore.stdExecDataStore,covInfo.coverageBuilder);
	  iCUT = instrumenter.getInstrumentedCUT() ;
	  originalCodeOfCUT = instrumenter.getOriginalCUTcode() ;
	}
	
	/**
	 * Clear the store raw runtime data, as well as the upper level coverage data stores.
	 */
	public void clear()  { 
		rtdata.reset() ;
		cumulativeExecDataStore.reset();
	}
	
	/**
	 * Clear the stored raw runtime data.
	 */
	public void clearRawRTdata()  { 
		rtdata.reset() ;
	}
	
	/**
	 * This will merge/add raw runtime data of CUT's executions since
	 * the last call to clearRawRTdata() (or since the start, if there
	 * has been no such call), into the existing coverage data managed
	 * by cumulativeExecDataStore. 
	 */
	public void collectRTdata()  { 
		rtdata.collect(cumulativeExecDataStore, sessionInfos, false);
	}

	/**
	 * Analyze the collected runtime data,and calculate coverage information.
	 * @throws IOException 
	 */
	public CodeCoverageInfo analyze() throws IOException  {
		analyzer.analyzeClass(originalCodeOfCUT,iCUT.getName());
		return covInfo ;
	}
	
	public CodeCoverageInfo getCoverageInfo() { return covInfo ; }
	
	/**
	 * A variation of Jacoco's standard ExecutionDataStore, which is cumulative.
	 * (The original ExecutionDataStore seems to maintain its information by
	 *  keeping a pointer to the raw execution data collected by instrumented
	 *  CUT; as a result it does not behave cumulatively. That is, if we reset/clear
	 *  the collected raw data, the information in ExecutionDataStore will also 
	 *  reflect a cleared coverage.)
	 */
	public static class CumulativeExecDataStore implements  IExecutionDataVisitor {
		
		//private boolean[] probes = null ;
		ExecutionDataStore stdExecDataStore ;
		ExecutionData myexecData = null ;
		
		private CumulativeExecDataStore() { } 
		public CumulativeExecDataStore(ExecutionDataStore stdExecDataStore) { 
			this.stdExecDataStore = stdExecDataStore ;
		} 
		
		/**
		 * Clear the execution information.
		 */
		public void reset() { 
			if (myexecData != null) myexecData.reset();
			stdExecDataStore.reset();
		}
		
		@Override
		public void visitClassExecution(ExecutionData execdata) {
			
			boolean[] probes_ = execdata.getProbes() ;
			/*
			if (probes_ == null) return ;
			if (probes == null) {
				probes = new boolean[probes_.length] ;
				for (int i=0; i<probes.length; i++) probes[i] = probes_[i] ;
			}
			else {
                int N = probes.length ;
                if (probes.length != probes_.length) N = Math.min(probes.length,probes_.length) ;
				for (int i=0; i<N; i++) probes[i] = probes[i] || probes_[i] ;
				if (probes.length < probes_.length) {
					N = probes_.length ;
					boolean[] x = new boolean[N] ;
					for (int i=0; i<probes.length ;i++) x[i] = probes[i] ;
					for (int i=probes.length; i<N; i++) x[i] = probes_[i] ;
					probes = x ;
				}
			}
			*/
			if (myexecData == null) {
				boolean [] copyProbes = new boolean[probes_.length] ;
				for (int i=0; i<copyProbes.length; i++) copyProbes[i] = probes_[i] ;
				myexecData = new ExecutionData(execdata.getId(),execdata.getName(),copyProbes) ;
			}
			else {
				myexecData.merge(execdata);
			}
			stdExecDataStore.put(myexecData);
		}
		
		/*
		public int covered() {
			if (probes == null) return 0 ; 
			int k = 0 ;
			for (int i=0; i<probes.length; i++) if (probes[i]) k++ ;
			return k ;
		}
		
		public int total() { 
			if (probes == null) return 0 ;
			return probes.length ;
		}
		
		public int uncovered() { return total() - covered() ; }
		public boolean fullyCovered() { return covered() == total() ; }
		public double ratio() { return ((double) covered()) / ((double) total()) ; }
		
		public void printCov() {
			System.out.println("==> " + covered() + "/" + total());
		}
		*/
	}
	
	/**
	 * An instance of this class is produced by analyze(), and can be queried
	 * for various coverage information at the moment analyze() is invoked.
	 */
	public static class CodeCoverageInfo {
		CoverageBuilder coverageBuilder ;
		
		CodeCoverageInfo(CoverageBuilder cb) { coverageBuilder = cb ; }
		
		
		public boolean isFullyCovered(String classname) {
		   return getClassCov(classname,"maxed") > 0f ;
		}
		
		/**
		 * To get coverage count over the whole CUT.
		 */
		public double getClassCov(String classname, String type) {
			for (IClassCoverage cc : coverageBuilder.getClasses()) {
				String cname = cc.getName().replace("/",".") ;
				if (cname.equals(classname)) {
					if (type.equals("total")) return cc.getBranchCounter().getTotalCount() ;
					if (type.equals("count")) return cc.getBranchCounter().getCoveredCount() ;
					if (type.equals("ratio")) {
						if (cc.getBranchCounter().getTotalCount() == 0) return 1 ;
						return cc.getBranchCounter().getCoveredRatio() ;
					}
					int cnt = cc.getBranchCounter().getCoveredCount() ;
					int tot = cc.getBranchCounter().getTotalCount()  ;
					if (cnt==tot) return 1 ; else return 0 ;
				}
			}
			return 0 ;
		}
		
		public boolean isFullyCovered(String classname, Method m) {
			return  getCov(classname,m,"maxed") > 0f ;
		}
		
		public boolean isFullyCovered(String classname, Constructor c) {
			return  getCov(classname,c,"maxed") > 0f ;
		}
		
		public double getCov(String classname, Method  m, String infoType) {
			return getCov(classname, m.getName(), tySignature(m.getParameterTypes()), infoType) ;
		}
		
		public double getCov(String classname, Constructor  c, String infoType) {
			return getCov(classname, "<init>", tySignature(c.getParameterTypes()), infoType) ;
		}
		
		/**
		 * To get the coverage count of methods with the given name and type
		 * signature.If the type signature is left null, then all methods with
		 * the same name will be counted along.
		 */
		public double getCov(String classname, String meth, 
				String mTySignature,
				String infoType) {
			for (IClassCoverage cc : coverageBuilder.getClasses()) {
				String cname = cc.getName().replace("/",".") ;
				if (cname.equals(classname)) {
					double count = 0 ;
					double total = 0 ;
					for (IMethodCoverage mc : cc.getMethods()) {
					   String sig_ = normalizeJacocoTySignature(mc.getDesc()) ;
					   //System.out.println("<< " + mc.getName() + ":" + sig_ 
					   //   + " vs " + mTySignature + " >>");
					   if (mc.getName().equals(meth)) {	
						  if (mTySignature != null && !sig_.equals(mTySignature)) continue ;
						  total += mc.getBranchCounter().getTotalCount() ;
						  count +=  mc.getBranchCounter().getCoveredCount() ;
						  if (mTySignature != null) break ;
					   }
					}
					if (infoType.equals("total")) return total ;
					if (infoType.equals("count")) return count ;
					if (infoType.equals("ratio")) {
					   if (total == 0) return 1 ;
					   else return count / total ;
					}
					//System.out.print("<< " + count + ">>") ;
					//System.out.print("<< " + total + ">>") ;
					   
					if (count == total) return 1 ; else return 0 ;
				}
			}
			return 0 ;
		}
		
		public void print() {
			for (final IClassCoverage cc : coverageBuilder.getClasses()) {
				System.out.println("Branch coverage of class " + cc.getName() + " : " 
				 + cc.getBranchCounter().getCoveredCount() + "/"
				 + cc.getBranchCounter().getTotalCount() 
			     + ", in %: "+ cc.getBranchCounter().getCoveredRatio());
			}
		}
		
		private static String normalizeJacocoTySignature(String ty) {
			int k = ty.indexOf(')') ;
			return ty.substring(1,k) ;
		}
		
		private static String tySignature(Class[] cs) {
			if (cs == null || cs.length == 0) return "" ;
			StringBuilder s = new StringBuilder() ;
			for (int i=0; i<cs.length ; i++) s.append(tySignature(cs[i])) ;
			return s.toString() ;
		}
		
		private static String tySignature(Class C) {
			if (C == Integer.TYPE) return "I" ;
			if (C == Boolean.TYPE) return "Z" ;
			if (C == Byte.TYPE) return "B" ;
			if (C == Short.TYPE) return "S" ;
			if (C == Long.TYPE) return "J" ;
			if (C == Character.TYPE) return "C" ;
			if (C == Float.TYPE) return "F" ;
			if (C == Double.TYPE) return "D" ;
			if (C.isArray()) {
				return "[" + tySignature(C.getComponentType()) ;
			}
			return "L" + C.getName().replace(".", "/") + ";" ;
		}
	}
	
	static public void main(String[] args) throws Exception {
		//String rootDir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3_externalExamples/bin" ;
		//JacocoInstrumenter ji = new JacocoInstrumenter(rootDir,"SomeExamples.Item") ;
		String rootDir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3/bin" ;
		JacocoInstrumenter ji = new JacocoInstrumenter(rootDir,"Sequenic.T3.Examples.Item") ;
		Class iCUT = ji.getInstrumentedCUT() ;
		
		System.out.println(iCUT.getName());
		
		CodeCoverage CC = new CodeCoverage(ji) ;
		CC.clear() ;
		CC.analyze().print() ;
		
		Constructor ItemCo = iCUT.getDeclaredConstructor(String.class) ;
		Method getPrice = iCUT.getDeclaredMethod("getPrice") ;
		Object o = ItemCo.newInstance("book") ;
		getPrice.invoke(o) ;
		CC.collectRTdata() ;		
		CC.analyze().print() ;
		
		//JacocoInstrumenter.shutDownJacocoLogger();
		
		//rootDir = "/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3_externalExamples/bin" ;
		//ji = new JacocoInstrumenter(rootDir,"SomeExamples.IncomeTax") ;
		ji = new JacocoInstrumenter(rootDir,"Sequenic.T3.Examples.IncomeTax") ;
		iCUT = ji.getInstrumentedCUT() ;
		System.out.println(iCUT.getName()) ;
		CC = new CodeCoverage(ji) ;
		CC.clear() ;
		CC.analyze().print() ;
		
		Constructor TaxCo = iCUT.getDeclaredConstructor(Integer.TYPE, String.class) ;
		Method getTax = iCUT.getDeclaredMethod("getTax") ;
		Method equals = iCUT.getDeclaredMethod("equals",Object.class) ;
		
		
		Object t = TaxCo.newInstance(1,"INSIDE") ;
		CC.collectRTdata() ;		
		CC.analyze().print() ;
		
		getTax.invoke(t) ;
		CC.collectRTdata() ;
		CC.analyze().print() ;
		
		equals.invoke(t,new Object()) ;
		CC.clearRawRTdata();
		CC.collectRTdata() ;
		CC.analyze().print() ;
		
		equals.invoke(t,t) ;
		CC.collectRTdata() ;
		CC.analyze().print() ;

		System.out.println("Class: " 
		    + CC.getCoverageInfo().getClassCov(iCUT.getName(),"count")
		    + "/"
		    + CC.getCoverageInfo().getClassCov(iCUT.getName(),"total")
		    + ", "
		    + CC.getCoverageInfo().getClassCov(iCUT.getName(),"ratio")
		    + ", fully covered: " 
		    + CC.getCoverageInfo().isFullyCovered(iCUT.getName()));
		
		System.out.println("getTax: " 
		    + CC.getCoverageInfo().getCov(iCUT.getName(),getTax,"count")
		    + "/"
		    + CC.getCoverageInfo().getCov(iCUT.getName(),getTax,"total")
		    + ", "
		    + CC.getCoverageInfo().getCov(iCUT.getName(),getTax,"ratio")
		    + ", fully covered: " 
		    + CC.getCoverageInfo().isFullyCovered(iCUT.getName(),getTax));
		
		System.out.println("equals: " 
		    + CC.getCoverageInfo().getCov(iCUT.getName(),equals,"count")
		    + "/"
		    + CC.getCoverageInfo().getCov(iCUT.getName(),equals,"total")
		    + ", "
		    + CC.getCoverageInfo().getCov(iCUT.getName(),equals,"ratio")
		    + ", fully covered: " 
		    + CC.getCoverageInfo().isFullyCovered(iCUT.getName(),equals));
		
		System.out.println("Constructor: " 
			    + CC.getCoverageInfo().getCov(iCUT.getName(),TaxCo,"count")
			    + "/"
			    + CC.getCoverageInfo().getCov(iCUT.getName(),TaxCo,"total")
			    + ", fully covered: " 
			    + CC.getCoverageInfo().isFullyCovered(iCUT.getName(),TaxCo));

	}
	
}
