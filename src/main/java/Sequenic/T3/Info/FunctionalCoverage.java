/*
 * Copyright 2014 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Info;

import java.io.OutputStream;
import java.lang.reflect.*;
import java.util.*;
import java.util.Map.Entry;

import Sequenic.T3.TestingScope;
import Sequenic.T3.Sequence.Datatype.*;


/**
 * Provide info about coverage over members of the target class, e.g.
 * methods coverage, mutators coverage, etc.
 *
 */
public class FunctionalCoverage {
	
	TestingScope scope ;
	public OutputStream out ;
	
	public Map<Object,Set> allMethodpairs = new HashMap<Object,Set>() ;
	public Map<Object,Set> allPairs = new HashMap<Object,Set>() ;
	
	public Set coveredMethods = new HashSet() ;
	public Set coveredCreatorMethods = new HashSet() ;
	public Set coveredConstructors = new HashSet() ;
	public Set coveredFields = new HashSet() ;
	
	public Map<Object,Set> coveredMethodpairs = new HashMap<Object,Set>() ;
	public Map<Object,Set> coveredPairs = new HashMap<Object,Set>() ;
	
	public FunctionalCoverage(TestingScope scope,OutputStream out) {
		this.scope = scope ;
		this.out = out ;
		
		List allNonCreators = new LinkedList() ;
    	List allCreators = new LinkedList() ;
    	
    	allNonCreators.addAll(scope.methods) ;
    	allNonCreators.addAll(scope.fields) ;
    	
    	allCreators.addAll(scope.constructors) ;
    	allCreators.addAll(scope.creatorMethods) ;
    	    	 	
    	for (Object m1 : scope.methods) {
    		for (Object m2 : scope.methods) {
    			if (!allMethodpairs.containsKey(m1)) {
    				allMethodpairs.put(m1,new HashSet()) ;
    			}
    			allMethodpairs.get(m1).add(m2);
    		}
    	}
    	
    	for (Object c : allCreators) {
    		for (Object m : scope.methods) {
    			if (!allMethodpairs.containsKey(c)) {
    				allMethodpairs.put(c,new HashSet()) ;
    			}
    			allMethodpairs.get(c).add(m);
    		}
    	}
    	  	
    	for (Object m1 : allNonCreators) {
    		for (Object m2 : allNonCreators) {
    			// if the second element is a field update, not sensical as a target pair:
    			if (m2 instanceof Field) continue ;
    			if (!allPairs.containsKey(m1)) {
                    allPairs.put(m1,new HashSet()) ;
    			}
				allPairs.get(m1).add(m2);
    		}
    	}  
    	
    	for (Object c : allCreators) {
    		for (Object m : allNonCreators) {
    			// if the second element is a field update, not sensical as a target pair:
    			if (m instanceof Field) continue ;
    			if (!allPairs.containsKey(c)) {
                    allPairs.put(c,new HashSet()) ;
    			}
				allPairs.get(c).add(m);
    		}
    	}
    	
	}
	
    private String coverage(int covered, int tobeCovered) {
    	String s = "" + covered + "/" + tobeCovered ;
    	if (tobeCovered==0) s += " (100%)" ;
    	else {
    		double p = Math.round(10000 * covered/tobeCovered)/100.0 ;
    		s += " (" + p + "%)" ;
    	}
    	return s ;	
    }
    
    
    private String coverage(Set covered, List tobeCovered) {
    	return coverage(covered.size(),tobeCovered.size()) ;	
    }
    
    
    private String coverage(Map<Object,Set> covered, Map<Object,Set> tobeCovered) {
    	int Ncovered = 0 ;
    	int NtobeCovered = 0 ;
    	for (Entry k : covered.entrySet()) {
    		Set val = (Set) k.getValue() ;
    		Ncovered += val.size() ;
    	}
    	for (Entry k : tobeCovered.entrySet()) {
    		Set val = (Set) k.getValue() ;
    		NtobeCovered += val.size() ;
    	}
    	return coverage(Ncovered,NtobeCovered) ;
    }
    
    
    private void write(String s) throws Exception {
    	out.write(s.getBytes());
    }
    
    private void writeln(String s) throws Exception {
    	write(s + "\n") ;
    }
    
    
    /**
     * Estimate the number of paths that a program has, based on its signature.
     */
    static public int estimatedNumberOfPaths(Method m) {
    	int treeShape_count  = 1  ;
    	int graphShare_count = 1 ;
    	Class[] argTys = m.getParameterTypes() ;
    	for (Class C : argTys) {
    	    if (Collection.class.isAssignableFrom(C) || C.isArray()) {
    	    	treeShape_count += 2 ;
    	    	graphShare_count = graphShare_count * 3 ;
    	    }
    	    else {
    	    	treeShape_count += 1 ;
    	    	graphShare_count = graphShare_count * 2 ;
    	    } 
    	}
    	if (Modifier.isStatic(m.getModifiers())) {
	    	treeShape_count += 1 ;
	    	graphShare_count = graphShare_count * 2 ;
    	}
    	
    	return (int) (Math.round(0.65 * (double) treeShape_count + 0.35 * (double) graphShare_count)) ; 
    }
    
    static public int estimatedNumberOfPaths(Constructor c) {
    	int treeShape_count  = 1  ;
    	int graphShare_count = 1 ;
    	Class[] argTys = c.getParameterTypes() ;
    	for (Class C : argTys) {
    	    if (Collection.class.isAssignableFrom(C) || C.isArray()) {
    	    	treeShape_count += 2 ;
    	    	graphShare_count = graphShare_count * 3 ;
    	    }
    	    else {
    	    	treeShape_count += 1 ;
    	    	graphShare_count = graphShare_count * 2 ;
    	    }  
    	}
    	return (int) (Math.round(0.65 * (double) treeShape_count + 0.35 * (double) graphShare_count)) ; 
    }
    
    
    public void printAllUncoveredConstructors()  throws Exception {
    	write("** Uncovered constructors:") ;
        if (scope.constructors.size() <= coveredConstructors.size()) {
        	writeln(" -") ;
        	return ;
        }
        writeln("") ;
        for (Constructor co : scope.constructors) {
        	if (coveredConstructors.contains(co)) continue ;
        	writeln("   " + co.toString() 
        			+ ", estimated complexity=" + estimatedNumberOfPaths(co)) ;   	
        }
    }
    

    public void printAllUncoveredCreatorMethods()  throws Exception {
    	write("** Uncovered creator methods:") ;
        if (scope.creatorMethods.size() <= coveredCreatorMethods.size()) {
        	writeln(" -") ;
        	return ;
        }
        writeln("") ;
        for (Method cm : scope.creatorMethods) {
        	if (coveredCreatorMethods.contains(cm)) continue ;
        	writeln("   " + cm.toString()
        			+ ", estimated complexity=" + estimatedNumberOfPaths(cm)) ;       	
        }
    }
    
      
    public void printAllUncoveredMethods()  throws Exception {
    	write("** Uncovered methods:") ;
        if (scope.methods.size() <= coveredMethods.size()) {
        	writeln(" -") ;
        	return ;
        }
        writeln("") ;
        for (Method m : scope.methods) {
        	if (coveredMethods.contains(m)) continue ;
        	writeln("   " +  m.toString()
        			+ ", estimated complexity=" + estimatedNumberOfPaths(m)) ;   	
        }
        //for (Object m : coveredMethods) {
        //	System.out.println("**>> covered method " + (Method) m) ;
        //}
    }
    
    public void printAllUncoveredMethodPairs()  throws Exception {
    	write("** Uncovered methods pairs:") ;
    	
    	int k=0 ;
    	for (Entry<Object,Set> e : allMethodpairs.entrySet()) {
    		Object m1 = e.getKey() ;
    		for (Object m2 : e.getValue()) {
    			if (coveredMethodpairs.containsKey(m1)) {
    				if (coveredMethodpairs.get(m1).contains(m2)) continue ;
    			}
    			write("\n   " +  m1.toString() + "  x  " + m2.toString()) ;
    			k++ ;
    		}
    	}
        if (k==0) write(" -") ;
        writeln("") ;
    }
    
    public void printAllUncoveredFields()  throws Exception {
    	write("** Uncovered fields:") ;
        if (scope.fields.size() <= coveredFields.size()) {
        	writeln(" -") ;
        	return ;
        }
        writeln("") ;
        write("   ") ;
        int k = 0 ;
        for (Field f : scope.fields) {
        	if (coveredFields.contains(f)) continue ;
        	if (k>0) write("; ") ;
        	write(f.toString()) ;    	
        	k++ ;
        }
        writeln("") ;
    }
	
    /**
     * Calculating fields, constructors, creator methods, and methods
     * covered by the given test suite.
     */
    private void calculateSingletonCoverage(SUITE suite){
    	
    	for (SEQ seq : suite.suite) {
    		for (STEP step : seq.steps) {
    			//System.out.println("**>> step: " + step);
    			if (step instanceof UPDATE_FIELD) {
    				if (coveredFields.contains(getActualMetaStep(step))) continue ;
    				Field f = getMatch((UPDATE_FIELD) step) ;
    				if (f != null) coveredFields.add(f) ;
    			}
    			if (step instanceof METHOD) {
    				METHOD step_ = (METHOD) step ;
    				if (step_.isObjectUnderTestCreationStep) {
    					if (coveredCreatorMethods.contains(getActualMetaStep(step))) continue ;
    					Method cm = getMatch(scope.creatorMethods,step_) ;
    					if (cm!=null) coveredCreatorMethods.add(cm) ;
    				}
    				else {
    					if (coveredMethods.contains(getActualMetaStep(step))) continue ;
    					Method m = getMatch(scope.methods,step_) ;  					
    					if (m!=null) coveredMethods.add(m) ;
    				}
    			}
    			if(step instanceof CONSTRUCTOR) {
    				if (coveredConstructors.contains(getActualMetaStep(step))) continue ;
    				Constructor co = getMatch((CONSTRUCTOR) step) ;	
    				if (co!=null) coveredConstructors.add(co) ;
    			}
    		}
    	}  	
    }
    
    /**
     * Return a field from the list of to-be covered fields, that matches
     * the given field, if any. Else it returns null.
     */
    private Field getMatch(UPDATE_FIELD step) {
    	Field f2 = (Field) getActualMetaStep(step) ;
    	for (Field f1 : scope.fields) {
    		if (isMatchedBy(f1,f2)) return f1 ;
    	}
    	return null ;
    }
    
    private boolean isMatchedBy(Field f1, Field f2) { return f1.equals(f2) ; }
    
    /**
     * Return a method from the list methods, that matches
     * the given method, if any. Else it returns null. "Match" here means "is covered by".
     * Note that it is not always literal equality.
     */
    private Method getMatch(List<Method> methods, METHOD step) {
    	Method m2 = (Method) getActualMetaStep(step) ;
    	for (Method m1 : methods) {
    		if (isMatchedBy(m1,m2)) return m1 ;
    	}
    	return null ;
    }   
    
    private boolean isMatchedBy(Method m1, Method m2) {
    	if (m1.equals(m2)) return true ;
		Class C = m1.getDeclaringClass() ;
		if (C.isInterface() || Modifier.isAbstract(m1.getModifiers())) {
			if (hasSameSignature(m1,m2)) return true ;
		}
		return false ;
    }
    
    /**
     * Return a constructor from the list of to-be covered constructors, that matches
     * the given constructor, if any. Else it returns null.
     */
    private Constructor getMatch(CONSTRUCTOR step) {
    	Constructor co2 = (Constructor) getActualMetaStep(step) ;
    	for (Constructor co1 : scope.constructors) {
    		if (isMatchedBy(co1,co2)) return co1 ;
    	}
    	return null ;
    } 
    
    private boolean isMatchedBy(Constructor co1, Constructor co2) {
    	if (co1.equals(co2)) return true ;
		if (Modifier.isAbstract(co1.getModifiers())) {
			if (hasSameSignature(co1,co2)) return true ;
		}
		return false;
    }
    
    /**
     * Checks if two methods have the same signature.
     */
    private boolean hasSameSignature(Method m1, Method m2) {
        if (m1.getName() != m2.getName()) return false ;
        int mod1 = m1.getModifiers() ;
        int mod2 = m2.getModifiers() ;
        if (Modifier.isStatic(mod1) && ! Modifier.isStatic(mod2)) return false ;
        if (Modifier.isStatic(mod2) && ! Modifier.isStatic(mod1)) return false ;
        Class[] argTys1 = m1.getParameterTypes() ;
        Class[] argTys2 = m2.getParameterTypes() ;
        if (argTys1.length != argTys2.length) return false ;
        for (int k=0; k<argTys1.length; k++) 
        	if (argTys1[k] != argTys2[k]) return false ;
        return true ;
    }
    
    private boolean hasSameSignature(Constructor co1, Constructor co2) {
        Class[] argTys1 = co1.getParameterTypes() ;
        Class[] argTys2 = co2.getParameterTypes() ;
        if (argTys1.length != argTys2.length) return false ;
        for (int k=0; k<argTys1.length; k++) 
        	if (argTys1[k] != argTys2[k]) return false ;
        return true ;
    }
    
    /**
     * To calculate which pairs of method-method and pairs of method-member
     * are covered by the given suite.
     */
    private void calculatePairCoverage(SUITE suite){
    	for (SEQ seq : suite.suite) {
    		int N = 0 ;
    		// calculate the number of non-instrumentation steps:
    		for (STEP s : seq.steps) {
    			if (isCUMstep(s)) N++ ;
    		}
    		// get all non-instrumentation steps:
    		STEP[] steps = new STEP[N] ;
    		int k=0 ;
    		for (STEP s : seq.steps) {
    			if (isCUMstep(s)) {
    				steps[k] =s ;
    				k++ ;
    			}
    		}
    		// now calculate pair-coverage:
    		for (k=0; k<N-1; k++) {
    			STEP step = steps[k] ;
    			
    	    	//System.out.println("### step [" + k + "] " + step) ;
    	      			
    			STEP next = steps[k+1] ;
    			
    			Object step_ = getActualMetaStep(step) ;
    			Object next_ = getActualMetaStep(next) ;
    			
    			// if next is a field-update, it is not set as a goal:
    			if (next instanceof UPDATE_FIELD) continue ;
    			
    			Object[] coveredPair = getMatch(allPairs,step_,next_) ;
    			if (coveredPair!=null) {
    				Object p1 = coveredPair[0] ;
    				Object p2 = coveredPair[1] ;
    				if (!coveredPairs.containsKey(p1)) {
            			coveredPairs.put(p1, new HashSet()) ;
            		} 
    				coveredPairs.get(p1).add(p2) ;
    			}
    			   	
    			if (step instanceof UPDATE_FIELD) continue ;
    			
    			coveredPair = getMatch(allMethodpairs,step_,next_) ;
    			if (coveredPair!=null) {
    				Object p1 = coveredPair[0] ;
    				Object p2 = coveredPair[1] ;
    				if (!coveredMethodpairs.containsKey(p1)) {
    					coveredMethodpairs.put(p1, new HashSet()) ;
            		} 
    				coveredMethodpairs.get(p1).add(p2) ;
    			}
    		}
    	}  	
    }
    
    private Object[] getMatch(Map<Object,Set> targetPairs, Object o1, Object o2) {
    		
    	for (Entry<Object,Set> e : targetPairs.entrySet()) {
    		Object p1 = e.getKey() ;
    		// literal match:
    		boolean match1 = p1==o1 ;
    		if (!match1) {
    			if (p1 instanceof Method && o1 instanceof Method) {
    				match1 = isMatchedBy((Method) p1, (Method) o1) ;
    				//System.out.println("### (mm) " + p1 + " x " + o1 + ", match = " + match1) ;
    			}
    			else if (p1 instanceof Constructor && o1 instanceof Constructor) {
    				match1 = isMatchedBy((Constructor) p1, (Constructor) o1) ;
    				//System.out.println("### (cc) " + p1 + " x " + o1 + ", match = " + match1) ;
    			}
    			// for fields, they are matched if they are literally equal, so when
    			// we are here, they are not matched anyway:
    		}
    		if (!match1) continue ;
    		
    		for (Object p2 : e.getValue()) {
    			boolean match2 = p2 == o2 ;
    			if (!match2) {
    				if (p2 instanceof Method && o2 instanceof Method) {
        				match2 = isMatchedBy((Method) p2, (Method) o2) ;
        			}
        			else if (p2 instanceof Constructor && o2 instanceof Constructor) {
        				match2 = isMatchedBy((Constructor) p2, (Constructor) o2) ;
        			}
        			// for fields, they are matched if they are literally equal, so when
        			// we are here, they are not matched anyway:
    			}
    			if (match2) {
    				// then we have a matching pair!
    				Object[] result = new Object[2] ;
    				result[0] = p1 ;
    				result[1] = p2 ;
    				return result ;
    			}
    		}
    	}
    	return null ;
    }
    
    public void calculate(SUITE S) {
    	calculateSingletonCoverage(S) ;
    	calculatePairCoverage(S) ;
    }
    
    private Object getActualMetaStep(STEP step) {
		if (step instanceof UPDATE_FIELD) {
			return ((UPDATE_FIELD) step).field ;
		}
		if (step instanceof METHOD) {
			return ((METHOD) step).method ;
		}
		if (step instanceof CONSTRUCTOR) {
			return ((CONSTRUCTOR) step).con ;
		}
		return null ;
    }
    
    private boolean isCUMstep(STEP step) {
    	return step instanceof UPDATE_FIELD
    			|| step instanceof CONSTRUCTOR
    			|| step instanceof METHOD ;
    }
    
    public void printReport(int verbosity) throws Exception {
    	writeln("** Constructor coverage    : " + coverage(coveredConstructors,scope.constructors)) ;
    	writeln("** Creator-method coverage : " + coverage(coveredCreatorMethods,scope.creatorMethods)) ;
       	writeln("** Method coverage         : " + coverage(coveredMethods,scope.methods)) ;
        writeln("** Field coverage          : " + coverage(coveredFields,scope.fields)) ;
        writeln("** Method-pair coverage    : " + coverage(coveredMethodpairs,allMethodpairs)) ;
        writeln("** Member-pair coverage    : " + coverage(coveredPairs,allPairs)) ;
        if (verbosity>0) {
        	printAllUncoveredConstructors() ;
        	printAllUncoveredCreatorMethods() ;
        	printAllUncoveredMethods() ;
        	printAllUncoveredFields() ;
        }
        if (verbosity>1) {
        	printAllUncoveredMethodPairs() ;
        }
    }
    
    

    

    

}
