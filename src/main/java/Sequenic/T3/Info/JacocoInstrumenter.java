package Sequenic.T3.Info;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.jacoco.core.instr.Instrumenter;
import org.jacoco.core.runtime.IRuntime;
import org.jacoco.core.runtime.LoggerRuntime;
import org.jacoco.core.runtime.RuntimeData;

import Sequenic.T3.CONSTANTS;

/**
 * Utility to instrument CUT with Jacoco to obtain coverage information.
 * The instrumenter is implemented as a custom class loader. 
 * This class is only responsible with applying the instrumentation, and
 * hooking in a runtime logger to collect the data. It does not do the
 * coverage calculation itself.
 * 
 * IMPORTANT NOTE:
 * When using this instrumenter, it is highly recommended that the CUT 
 * and any other classes D that the CUT ever needs during its executions, 
 * which link back to CUT are also loaded by this instrumenter. Otherwise,
 * D will refer to a different incarnation of the CUT, and the executions
 * will crash due to class conflict.
 * 
 * The easiest thing to achieve the above is by putting the CUT and D's in
 * a root-dir which is OUTSIDE the JVM's classpaths. This root-dir is to be
 * specified to this instrumenter. JVM's default loader cannot see them, 
 * so can also not load them. This instrumenter is the only loader that can
 * load them.
 * 
 * If the testing environment is as such that the above separation of root-dir
 * is not possible or highly inconvenient, then CARE must be taken, that JVM's
 * default loader DOES NOT load the CUT nor any of the D's before this 
 * instrumenter does.
 * 
 * USAGE CONVENTION:
 * 
 * Before start testing CUT, do this:
 * 
 * (1) ji = new JacocoInstrumenter(CUTrootDir, CUTname)  ... see the note above.    
 * (2) Class iCUT = ji.getInstrumentedCUT() ;
 * (3) run T3/G2 on this iCUT. If Scope or Imap have to created prior to invoking T3, pass on
 *      iCUT !
 * (4) After we are done, call  shutDownJacocoLogger()
 *  
 */
public class JacocoInstrumenter extends ClassLoader {
	
	/**
	 * The logger to which instrumented code will send its raw data.
	 */
	private IRuntime jacocoLogger = new LoggerRuntime()  ;
	public IRuntime getJacocoLogger() { return jacocoLogger ; }
	
	/*
	 * This will clear the log kept by the logger. Call this before running
	 * a new CUT for measurement.
     * --> dropped.
    static public void shutDownJacocoLogger() { 
    	if (jacocoLogger != null) {
    		jacocoLogger.shutdown() ; 
    		System.out.println(">>> shutdown...") ;
    	}
    }
    */
	
	
	/**
	 * Turn "A.B.C" to "A/B/C.class"
	 */
	static public String getClassfilePath(String className) {
		String className_ = className.replace('.', File.separatorChar) + ".class" ;
		return className_ ;
	}
	
	static private String getFullPackageName(String className) {
		String pckgName = "" ;
		int k = className.lastIndexOf('.') ;
		if (k>=0) pckgName = className.substring(0,k) ;
		return pckgName ;
	}
	
	/** 
	 * Get the binary content of a class with the given name, it should return a non-empty
	 * byte[].
	 */
	public static byte[] getClassfileContent(String classfileRootDir, String className) throws IOException {	
		Path path = Paths.get(classfileRootDir,getClassfilePath(className)) ;
		byte[] code = Files.readAllBytes(path) ;
		if (code == null) throw new IOException("The corresponding class-file is empty!") ;
		return code ;
	}
	
	
	/*
	static private void createDirectoryStructureForPackage(String root, String pckgFullName) throws IOException {
		if (pckgFullName.length() == 0) return ;
		int k = pckgFullName.indexOf('.') ;
		String firstdir = pckgFullName ;
		String rest = "" ;
		if (k>=0) {
			firstdir = pckgFullName.substring(0,k) ;
			rest = pckgFullName.substring(k+1,pckgFullName.length()) ;
		}
		Path dir = Paths.get(root,firstdir) ;
		if (! dir.toFile().exists()) Files.createDirectories(dir) ;
		createDirectoryStructureForPackage(dir.toString(),rest) ;
	}
	*/
	

    private String binroot ;
	private String CUTname = null ;
	private byte[] originalCUTcode = null ;
	private Class<?> iCUT = null ;	
	private Map<String,Class<?>> alreadyLoaded = new HashMap<String,Class<?>>() ;
        
    public JacocoInstrumenter(String rootdir, String CUTname) throws ClassNotFoundException {
        super() ;
        if (CUTname == null) throw new ClassNotFoundException("Null is given as the CUT-name.") ;
        binroot = rootdir ;
        this.CUTname = CUTname;
        iCUT = loadClass(CUTname,true) ;
    }
        
    public String getCUTname() { return CUTname ; }
    public byte[] getOriginalCUTcode() { return originalCUTcode ; }
    public Class<?> getInstrumentedCUT() { return iCUT ; }
            
       
	@Override
	protected Class<?> loadClass(final String name, boolean resolve) throws ClassNotFoundException {
		// CUTname should not be null
		Class<?> C = null ; 
		// we will have non-standard loading sequence
		// (1) first check if C is cached by this loader
		C = alreadyLoaded.get(name);
		if (C!= null) { return C ; }
		// (2) else, if C is the CUT, explicitly load it from binroot, then instrument it
		if (name.equals(CUTname)) {
			   byte[] code ;
			   try {
			      code = getClassfileContent(binroot,name) ;
			   }
			   catch(Exception e) { throw new ClassNotFoundException(e.getMessage()) ; }
			   originalCUTcode = code ;
			   //shutDownJacocoLogger() ;
			   Instrumenter instrumenter = new Instrumenter(getJacocoLogger()) ;
			   byte[] instrumented ;
			   try {
				   instrumented = instrumenter.instrument(code,name) ;
			   }
			   catch(Exception e) { throw new ClassNotFoundException(e.getMessage()) ; }
			   iCUT = defineClass(name, instrumented, 0, instrumented.length) ;
			   Logger.getLogger(CONSTANTS.T3loggerName).info(CUTname + " is instrumented with Jacoco.");
			   alreadyLoaded.put(name, iCUT) ;
			   if (resolve) resolveClass(iCUT) ;
			   return iCUT ;
		   }
		   // (3) if it cannot be found, we will look for it first in binroot:		   
		   try { 
			   byte[] code = getClassfileContent(binroot,name) ; 
			   C = defineClass(name,code, 0, code.length) ;
			   alreadyLoaded.put(name, C) ;
			   if (resolve) resolveClass(C) ;
			   return C ;
		   }
		   // (4) if that fails, load C with the default class loader:
		   catch(Exception e) { 
			   return super.loadClass(name,resolve) ;
		    }		   
	}

	
	public static void main(String[] args) throws Exception {
		JacocoInstrumenter ji = new JacocoInstrumenter("/Users/iswbprasetya/eclipseWorkspace/t3Workspace/t3_externalExamples/bin","SomeExamples.Triangle1") ;
		Class iC = ji.getInstrumentedCUT() ;
		System.out.println(iC.getName());
		
	}

}
