/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.JavaType;

import java.io.Serializable;
import java.util.Map;

/**
 * Representing a Java type-expression. 
 */
public interface JType extends Serializable {

    /**
     * True if the type represents an unparameterized class in Java, such as Integer.
     * LinkedList (without parameter) is treated as unparameterized.
     */
    public boolean isUnparameterizedType() ;

    /**
     * True if the type contains no type variables nor wildcards.
     */
    public boolean isConcrete() ;

    /**
     * True if the type is concrete, and is an array.
     */
    public boolean isConcreteArray() ;
    public boolean isConcreteCollection() ;
    public boolean isConcreteMap() ;
   
    /**
     * Apply the given subsitutions. Side effecting. The substitutions are assumed to be
     * valid substituions of the corresponding type variables.
     */
    public JType subst(Map<String,JType> substitutions) ;
    
}
