/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.JavaType;

import java.lang.reflect.*;
import java.util.*;

public class JTypeUtils {

    private static boolean hasNoActualUpperbound(Type[] upperbounds) {
        if (upperbounds.length != 1) return false ;
        return upperbounds[0] == Object.class ;
    }
    
	/**
	 * Use this to convert a class-declaration to a JType. If the class has
	 * type parameters, these will be correctly included in
	 * the translation.
	 */
	public static JType classdecl2JType(Class C) {
		JTfun jty = (JTfun) JTypeUtils.convert(C) ;
		if (C.isArray()) return jty ;
		TypeVariable[] targs = C.getTypeParameters() ;
		jty.args = new JType[targs.length] ;
		for (int i=0; i<jty.args.length; i++)
		   jty.args[i] = convert(targs[i]) ;
		return jty ;
	}

	/**
	 * Convert a class declaration to a JType expression, but this ignores the class's type
	 * parameters, if there are any.
	 */
	public static JTfun naiveClassdecl2JType(Class C) {
	   return new JTfun(C) ;
	}
	   
    /**
     * For converting Java's Type-expression to JType. No type instantiation is done.
     * Don't use this to convert a class-declaration; this is done by classdecl2JType above.
     */
    public static JType convert(Type ty) {
        //System.out.println("####### ty = " + ty) ;
        if (ty instanceof Class) {
            Class C = (Class) ty ;
            if (C.isArray()) {
                return new JTfun(Array.class, convert(C.getComponentType())) ;
            }
            else return new JTfun(C) ;
        }

        if (ty instanceof ParameterizedType) {
            ParameterizedType ty2 = (ParameterizedType) ty ;
            Type[] args   = ty2.getActualTypeArguments() ;
            JType[] args_ = new JType[args.length] ;
            // HACK! for Enum, which has a circular signature Enum<E extends Enum<E>>; this
            // does not look to be solvable; without this hack the conversion will runin
            // cycle. Need to check what Java's type semantic saya on this matter.
            Class tyclass =  (Class) ty2.getRawType() ;
            if (Enum.class.isAssignableFrom(tyclass)) {
                TypeVariable ty3 = (TypeVariable) args[0] ;
                args_[0] =  new JTvar(ty3.getName(), new JType[0]) ;
                return new JTfun(tyclass, args_) ;
            }
            //System.err.println((">>> ty " + ty2));
            for (int i=0; i<args.length; i++) {
            	// System.err.println((">>> arg " + i + ": " + args[i]));
            	// HACK some argument is type variable, whose upperbound is ty2 itself :(
            	// this leads to cycle, prevent!
            	boolean cyclic = false ;
            	Type aty = args[i] ;
            	if (aty instanceof TypeVariable) {
            		TypeVariable aty_ = (TypeVariable) aty ;
            		Type[] upperbounds = aty_.getBounds() ;
            		for (Type cty : upperbounds) {
            			if (cty.toString().equals(ty2.toString())) {
            				// cycle!!
            				args_[i] = new JTvar(aty_.getName()) ;
            				cyclic = true ;
            				break ;
            			}
            		}
            	}
            	if (!cyclic) args_[i] = convert(aty) ;
            }
            return new JTfun(tyclass, args_) ;
        }

        if (ty instanceof TypeVariable) {
            TypeVariable ty3 = (TypeVariable) ty ;
            Type[] upperbounds = ty3.getBounds() ;
            if (hasNoActualUpperbound(upperbounds)) {
                return new JTvar(ty3.getName()) ;
            }
            JType[] args = new JType[upperbounds.length] ;
            for (int i=0; i< args.length; i++) {
            	//System.err.println((">>> upperbound " + i + ": " + upperbounds[i]));
                args[i] = convert(upperbounds[i]) ;
            }
            return new JTvar(ty3.getName(),args) ;
        }

        if (ty instanceof WildcardType) {
            Jwildcard r = null ;
            WildcardType ty4 = (WildcardType) ty ;
            Type[] upperbounds = ty4.getUpperBounds() ;
            if (hasNoActualUpperbound(upperbounds)) {
                r = new Jwildcard() ;
            }
            else {
                JType[] ubs = new JType[upperbounds.length] ;
                for (int i=0; i<ubs.length; i++) ubs[i] = convert(upperbounds[i]) ;
                r = new Jwildcard(ubs) ;
                return r ;
            }
            Type[] lowerbounds = ty4.getLowerBounds() ;
            JType[] lbs = new JType[lowerbounds.length] ;
            for (int i=0; i<lbs.length; i++) lbs[i] = convert(lowerbounds[i])  ;
            r.setLowerBounds(lbs);
            return r;
        }

        if (ty instanceof GenericArrayType) {
            GenericArrayType ty5 = (GenericArrayType) ty ;
            return new JTfun(Array.class, convert(ty5.getGenericComponentType()))  ;
        }
        //System.out.println(">>>OUCH!");
        assert false ; // should not come tot this point!
        return null ;
    }


    /**
     * Return the top class of a the type. If it has none, null is returned.
     */
    public static Class getTopClass(JType ty) {
        if (!(ty instanceof JTfun)) return null ;
        JTfun ty_ = (JTfun) ty ;
        return ty_.fun ;
    }


    public static Map<String,JType> cloneTypeSubsts(Map<String,JType> substs) {
    	Map<String,JType> copy = new HashMap<String,JType>() ;
    	copy.putAll(substs);
    	return copy ;
    }
    
    public static int getArrayDimension(JType ty) {
    	if (!ty.isConcreteArray()) return 0 ;
    	JType elemType = ((JTfun) ty).args[0] ;
    	return getArrayDimension(elemType) + 1 ;
    }
    
    public static JType getDeepestArrayElementTy(JType ty) {
    	if (!ty.isConcreteArray()) return ty ;
    	JType elemType = ((JTfun) ty).args[0] ;
    	return getDeepestArrayElementTy(elemType) ;
    }
    
    public static void main(String[] args) throws Exception {

        String[] bs = new String[0] ;
        List<Integer> x = new LinkedList<Integer>() ; x.add(99);
        List<Integer>[] cs = new LinkedList[1] ;
        System.out.println("" + convert(Integer.class)) ;
        System.out.println("" + convert(Integer.TYPE)) ;
        System.out.println("" + convert(bs.getClass())) ;
        System.out.println("" + convert(x.getClass())) ;
        System.out.println("" + convert(cs.getClass())) ;

        Method test_ = JTypeUtils.class.getDeclaredMethod("test") ;
        System.out.println("" + convert(test_.getGenericReturnType())) ;

    }




}
