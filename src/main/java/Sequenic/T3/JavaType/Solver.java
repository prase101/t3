package Sequenic.T3.JavaType;

import java.lang.reflect.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.logging.Logger;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.ImplementationMap;
import Sequenic.T3.Reflection.Reflection;
import Sequenic.T3.utils.Pair;
import Sequenic.T3.utils.SomeObject;

/**
 * Provide a function to solve a type expression T. A solution is another type
 * expression T0 which is supposed to be instantiable by T3, but is a subtype
 * of T. Unfortunately, the solver is unsound because Java's reflection is
 * limited in determining subtyping relation of over parameterized type 
 * expressions.
 * 
 *
 */
public class Solver {

	
	/**
	 * True if texpr represents int or Integer, etc
	 */
	static boolean isPrimitiveLike(JType texpr){
		if (texpr instanceof JTfun) {
			Class C = ((JTfun) texpr).fun ;
			return Reflection.isPrimitiveLike(C) ;
		}
		return false ;
	}
	
	static boolean isString(JType texpr){
		if (texpr instanceof JTfun) {
			return ((JTfun) texpr).fun == String.class ;
		}
		return false ;
	}
	
	static boolean isEnum(JType texpr){
		if (texpr instanceof JTfun) {
			return ((JTfun) texpr).fun.isEnum() ;
		}
		return false ;
	}
			
	
   /**
    * The main solver function. Given a type expression, this will try to find a solution.
    * It may return null (unable to find a solution). It may also return an incorrect
    * solution, due to inaccuracy of Java's reflection.
    */
	/*
	static public JType solve(ImplementationMap imap, Map<String,JType> substitutions, JType texpr) {
	    System.out.println("###>>> ty = " + texpr) ;
		JType sol = solve___(imap,substitutions,texpr) ;
		System.out.println("###>>> solution = " + sol) ;
		return sol ;
	}
	*/
   static public JType solve(ImplementationMap imap, Map<String,JType> substitutions, JType texpr) {    
	    //System.err.println(">>> solving: " + texpr.toString());
		if (isPrimitiveLike(texpr) || isString(texpr) || isEnum(texpr))
			return texpr ;
		
		if (texpr instanceof JTfun) {
			JTfun texpr_ = (JTfun) texpr ;
			Class Imp = texpr_.fun ;
			imap.registerClass(Imp) ;
			JTfun solution = new JTfun(Imp) ;
			// the case when texpr is not a generic / has no type argument
			if (texpr_.args == null || texpr_.args.length == 0) {
			   return solution ;	
		   }
		
		   // case when texpr has type arguments... this case is in principle unsound			
			solution.args = new JType[texpr_.args.length] ;
			for (int k=0; k<texpr_.args.length; k++) {
			   JType targ = solve(imap,substitutions,texpr_.args[k]) ;
			   if (targ==null) return null ;
			   solution.args[k] = targ ;
			}
			// note that one of the args may give no solution; we will let this
			// to be dealt with solveClassTyvars.
			return solution ;	
		}
		
		if (texpr instanceof JTvar) {
			return solveJTvar(imap,substitutions,(JTvar) texpr) ;
		}
			
		// the last case, is when texpr is a wildcard
		return solveJwildcard(imap,substitutions,(Jwildcard) texpr) ;
	}
	
	/**
	 * Solving a type expression of the form of a type-variable. Note that a type variable
	 * may specify upperbound(s), which are again type expressions. This method may
	 * extend the given substitutions.
	 */
	static public JType solveJTvar(ImplementationMap imap, Map<String,JType> substitutions, JTvar tvar) {
		JType ty = substitutions.get(tvar.name) ;
		JType solution = null ;
		if (ty != null) return ty ;
		if (tvar.upperbounds == null || tvar.upperbounds.length==0) {
			solution = new JTfun(SomeObject.class) ; // SomeObject is already in imap, no need to register it
			// extend the substitutions too:
			substitutions.put(tvar.name,solution) ;
			return  solution ;
		}
		// case with just one upper bound:
		if (tvar.upperbounds.length == 1) {
			solution = solve(imap,substitutions,tvar.upperbounds[0]) ;
			// extend the substitutions too:
			substitutions.put(tvar.name,solution) ;
			return solution ;
		}
		// case with more than one upperbounds; this is potentially unsound
		// the first bound can be a tvar, class, or interface; 
		// the other bounds are all, says JLS, interface.
		List<JTfun> ubounds = new LinkedList<JTfun>() ;
		for (JType bound : tvar.upperbounds) {
			JType bound_ = solve(imap,substitutions,bound) ;
			// this should not be the case:
			if (! (bound_ instanceof JTfun)) continue ;
			ubounds.add((JTfun) bound_) ;
		}
		// find an instantiable class in the imap that satisfies all the bounds
		// TODO: type parameters in the bounds are currently NOT soundly handled
		JTfun solution2 = null ;
		for (Entry<Class,ImplementationMap.Instantiation> e : imap.knownClasses.entrySet()) {
			Class C = e.getKey() ;
			if (! e.getValue().hasKnownPublicInstantiationMethod()) continue ;
			solution2 = new JTfun(C) ;
			boolean found = true ;
			for (JTfun B : ubounds) {
				if (! B.fun.isAssignableFrom(C)) {
					found = false ; break ;
				}
				// hackish: simply taking over the parameters of the first upperbound that have them:
				if (solution2.args!= null && solution2.args.length==0 && B.args!=null && B.args.length>0)
					solution2.args = B.args ;
			}
			if (found) {
				// C is a solution; if it has type parameters, we ignore it :( don't know what to
				// do with them
				Logger.getLogger(CONSTANTS.T3loggerName).warning("Potentially unsound solution of this type-var: " 
						+ tvar
						+ "\n offered solution: " + solution2
						);
				return solution2 ;
			}
		}
		// no solution can be found!
		return null ;			
	}
	
	/**
	 * Solving a type expression of the form of a type-variable. Note that a wildcard
	 * may specify a lowerbound or an upperbound, which are again type expressions.
	 * According to the Java Language Specification (7 and 8), at most one bound can
	 * be specified (e.g. we can't have a wildcard with two upperbounds, or with one
	 * lower and also one upperbound.
	 */
	static public JType solveJwildcard(ImplementationMap imap, Map<String,JType> substitutions, Jwildcard wcard) {
       // case when we have no upper nor lower bound:
		if ((wcard.upperbounds == null || wcard.upperbounds.length==0)
			&&
			(wcard.lowerbounds == null || wcard.lowerbounds.length==0)) 
		{
			return new JTfun(SomeObject.class) ;
		}
		// case with one upper bound:
		if (wcard.upperbounds.length > 0) {
			return solve(imap,substitutions,wcard.upperbounds[0]) ;
		}
		// case with one lower bound, this may be unsoundly handled:
		JType solution = solve(imap,substitutions,wcard.lowerbounds[0]) ;
		Logger.getLogger(CONSTANTS.T3loggerName).warning("Potentially unsound solution of this wildcard: " 
					+ wcard
					+ "\n offered solution: " + solution
					);
		return solution ;
	}
	
	/*
	 * Check if a type expression can be directly instantiated.
	 *
	static boolean isDirectlyInstantiablexxx(JType texpr) {
		if (texpr instanceof JTvar || texpr instanceof Jwildcard) return false ;
	    JTfun T = (JTfun) texpr;
	    if (! ImplementationMap.isPubliclyInstantiable(T.fun)) return false ;
	    if (T.args == null) return true ;
	    for (JType arg : T.args) {
	    	if (! isDirectlyInstantiable(arg)) return false ;
	    }
	    return true ;
	}
	*/
	
	/**
	 * Collecting all type variables in a type expression.
	 */
	static void getTyVars(List<String> accumulator, JType texpr) {
		if (texpr instanceof JTvar) {
			JTvar tvar = (JTvar) texpr ;
			accumulator.add(tvar.name) ;
			if (tvar.upperbounds == null) return ;
			for (JType bound : tvar.upperbounds) getTyVars(accumulator,bound) ;
			return ;
		}
		if (texpr instanceof Jwildcard) {
			Jwildcard wc = (Jwildcard) texpr ;
			if (wc.upperbounds != null) {
				for (JType bound : wc.upperbounds) getTyVars(accumulator,bound) ;
			}
			if (wc.lowerbounds != null) {
				for (JType bound : wc.lowerbounds) getTyVars(accumulator,bound) ;
			}
			return ;
		}
		if (texpr instanceof JTfun) {
			JTfun X = (JTfun) texpr ;
			if (X.args == null) return ;
			for (JType arg : X.args) getTyVars(accumulator,arg) ;			
		}
	} 
	
	static List<String> getVarsInBounds(JTvar tvar) {
		 List<String> vars = new LinkedList<String>() ;
		 if (tvar.upperbounds == null) return vars ;
		 for (JType bound : tvar.upperbounds) getTyVars(vars,bound) ;
		 return vars ;
	}
	
	
	static List<Pair<TypeVariable,Integer>> reorderTyVars(TypeVariable[] tvars) {
		LinkedList<Pair<TypeVariable,Integer>> result = new LinkedList<Pair<TypeVariable,Integer>>() ;
		LinkedList<Pair<TypeVariable,Integer>> tvars_ = new LinkedList<Pair<TypeVariable,Integer>>() ;
		for (int k=0; k < tvars.length ; k++) {
			tvars_.add( new Pair(tvars[k],k)) ;
		}
		while (! tvars_.isEmpty()) {
			Pair<TypeVariable,Integer> selected = null ;
			for (Pair<TypeVariable,Integer> x : tvars_) {
				selected = x ;
				for(Pair<TypeVariable,Integer> y : tvars_) {
					if (x==y) continue ; 
					JTvar y_ = (JTvar) JTypeUtils.convert(y.fst) ;
					if (getVarsInBounds(y_).contains(x.fst.getName())) {
						selected = null ;
						break ;
					}
				}
				if (selected != null) break ;
			}
			if (selected != null) {
				result.addFirst(selected) ;
				tvars_.remove(selected) ;
			}
			else { // this should not occur! it implies that we have a cycle
 				result.addFirst(tvars_.get(0)) ;
 				tvars_.remove(0) ;
			}			
		}
		return result ;
	}
	
	
	/**
	 * Let JTFun C args be T0. C is a class that is defined to take parameters,
	 * e.g. tv0,tv1,...  This function tries to find correct type-instances for
	 * tv0,...  If it is specified in args0, this will be used. Else we invoke
	 * solve to solve it. 
	 * 
	 * If for variable tv, no instantiation can be found, it will be mapped to null.
	 */
	static public Map<String,JType> solveClassTyvars(ImplementationMap imap, JTfun T0) {
		Map<String,JType> substitutions = new HashMap<String,JType>() ;
		if (isPrimitiveLike(T0) || isString(T0) || isEnum(T0)) return substitutions ;
		TypeVariable[] tvars = T0.fun.getTypeParameters() ;
		List<Pair<TypeVariable,Integer>> tvars_ = reorderTyVars(tvars) ;
		
		for (Pair<TypeVariable,Integer> x : tvars_) {
			TypeVariable x_ = x.fst  ;
			//System.out.println(">> " + x_) ;
			int k = x.snd ;
			
			if (T0.args != null && k<T0.args.length && (T0.args[k]!=null)) {
				JType y = T0.args[k] ;
				substitutions.put(x_.getName(),y) ;
			}
			else {
				JType x__ =  JTypeUtils.convert(x_) ;
				JType y_ = solve(imap, new HashMap<String,JType>(), x__.subst(substitutions)) ;
				substitutions.put(x_.getName(),y_) ;
			}
			
		}
		return substitutions;
	}
	
	
	// running some tests
	public static void main(String[] args){
		System.out.println("" + Integer.TYPE + " " + Integer.TYPE.isPrimitive()) ;
		System.out.println("" + Integer.class + " " + Integer.class.isPrimitive()) ;
		System.out.println("" + String.class + " " + Integer.class.isPrimitive()) ;
		
		String[] dirs = { "./bin" } ;
		ImplementationMap imap = new ImplementationMap(dirs) ;
		
		System.out.println("Solving int --> " 
				+ solve(imap,new HashMap<String,JType>(),JTypeUtils.convert(Integer.TYPE))) ;
		System.out.println("Solving Integer --> " 
				+ solve(imap,new HashMap<String,JType>(),JTypeUtils.convert(Integer.class))) ;

		Function<Class,Integer> testSolveClassTyvars = C -> {
			System.out.println("Instantiating " + JTypeUtils.naiveClassdecl2JType(C)
					+ " --> " 
					+ solveClassTyvars(imap,JTypeUtils.naiveClassdecl2JType(C))) ;
			return null ;
		} ;
		
		testSolveClassTyvars.apply(Integer.class) ;
		testSolveClassTyvars.apply(Map.class) ;
			
		testSolveClassTyvars.apply(Sequenic.T3.Examples.ParameterizedTypes.MyMap.class) ;
		testSolveClassTyvars.apply(Sequenic.T3.Examples.ParameterizedTypes.MyGenericClass1.class) ;
		
		// this gives no solution:
		testSolveClassTyvars.apply(Sequenic.T3.Examples.ParameterizedTypes.MyGenericClass1b.class) ;
		
		testSolveClassTyvars.apply(Sequenic.T3.Examples.ParameterizedTypes.MyGenericClass1c.class) ;
		
		// the one below actually have parameters that are unsolvable, if run
		// java's reflection is not smart enough; and will cause stack overflow
		// test.apply(Sequenic.T3.Examples.ParameterizedTypes.MyGenericClass2.class) ;
		
		// this one cannot be precisely solved:
		testSolveClassTyvars.apply(Sequenic.T3.Examples.ParameterizedTypes.MyGenericClass3.class) ;
		
		
		Function<Class,Integer> test2SolveClassTyvars = C -> {
			JTfun C__ = (JTfun) JTypeUtils.naiveClassdecl2JType(C) ;
			C__.args = new JType[1] ;
			C__.args[0] = JTypeUtils.naiveClassdecl2JType(Long.class) ;
			System.out.println("XInstantiating " + C__
					+ " --> " 
					+ solveClassTyvars(imap,C__)) ;
			return null ;
		} ;
		test2SolveClassTyvars.apply(Integer.class) ;
		test2SolveClassTyvars.apply(Map.class) ;
		test2SolveClassTyvars.apply(Sequenic.T3.Examples.ParameterizedTypes.MyMap.class) ;
		test2SolveClassTyvars.apply(Sequenic.T3.Examples.ParameterizedTypes.MyGenericClass1.class) ;		
		test2SolveClassTyvars.apply(Sequenic.T3.Examples.ParameterizedTypes.MyGenericClass1c.class) ;
		test2SolveClassTyvars.apply(Sequenic.T3.Examples.ParameterizedTypes.MyGenericClass3.class) ;
		
		Map<String,JType> substitutions = new HashMap<String,JType>() ;
		
		
		Method[] methods = Sequenic.T3.Examples.ParameterizedTypes.MyGenericMethods.class.getDeclaredMethods() ;
		for (Method foo : methods) {
			String o = "" + foo.getName()  + " : " ;
			substitutions.clear();
			substitutions.put("T", JTypeUtils.naiveClassdecl2JType(Boolean.class)) ;
			Type[] tyargs = foo.getGenericParameterTypes() ;
			int k=0 ;
			for (Type ty : tyargs) {
				if (k>0) o += ", " ;
				JType ty_ = JTypeUtils.convert(ty) ;
				JType solution = solve(imap,substitutions,ty_) ;
				o += ty_ + "--> " + solution ;
				k++ ;
			}
			System.out.println("Instantiating " + o) ;
		}
		

	}
}
