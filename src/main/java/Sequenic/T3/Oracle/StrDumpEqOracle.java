/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Oracle;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Reflection.Reflection;
import Sequenic.T3.utils.PP;
import Sequenic.T3.utils.Show;
import Sequenic.T3.utils.Maybe;
//import Sequenic.T3.utils.Show;
import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Logger;

import static Sequenic.T3.Reflection.Reflection.*;

/**
 * Representing an oracle of the form target = expectation. They are considered
 * equal if their string-dump is the same. Because making a string dump can be
 * expensive (if the objects are comples), only the object's own fields will be
 * dumped. So, subfields will not be dumped.
 * 
 * @author Wishnu Prasetya
 * 
 */
public class StrDumpEqOracle extends Oracle {

	private static final long serialVersionUID = 1L;

	public Maybe<String> expectedReceiverObj = null;
	public Maybe<String> expectedReturnedObj = null;
	public Maybe<String> expectedException = null;

	public StrDumpEqOracle(Maybe<Object> receiverObj, 
			Maybe<Object> returnObj,
			Maybe<Throwable> thrownException) 
	{
		if (receiverObj != null)
			try {			
  				expectedReceiverObj = new Maybe(StrDump(receiverObj.val));
  				//System.out.println(">>> injecting rec-obj oracle") ;
			} catch (Exception e) {
				// cannot str-dump! So, no expectation too.
			} ;
			
		if (returnObj != null)
			try {
				expectedReturnedObj = new Maybe(StrDump(returnObj.val));
				//System.out.println(">>> injecting RET-obj oracle") ;
			} catch (Exception e) {
				// cannot str-dump! So, no expectation too.
			} ;
		if (thrownException != null) {
			expectedException = new Maybe(ExcStrDump(thrownException.val)) ;
		    //System.out.println(">>> injecting EXC-obj oracle") ;
		}
	}

	/**
	 * Convert an object to a string to be used as an oracle. Note that some abstraction
	 * is applied. E.g. numeric values may be truncated and rounded, and likewise, large
	 * collections may be truncated. (see the corresponding code below),
	 */
	public static String StrDump(Object o) {
		//return Show.show(o, 3, 2);
		return stringtify(o, 2) ;
	}
	
	static public List<Class> dontShow ;
	// Class initialization:
    static 
    {
    	dontShow = new LinkedList<Class>();
    	dontShow.add(Random.class);
    	dontShow.add(Reader.class);
    	dontShow.add(Writer.class);
    	dontShow.add(InputStream.class);
    	dontShow.add(OutputStream.class);
    	dontShow.add(RandomAccessFile.class);
    	dontShow.add(File.class) ;
    	dontShow.add(Class.class) ;
    }
    
    static boolean dontShow(Object o) {
    	Class C = o.getClass() ;
    	for (Class D : dontShow) if (D.isAssignableFrom(C)) return true ;
    	return false ;
    }
	
	static public String stringtify(Object o, int maxObjectDepth) {
		return stringtifyWorker(new StringBuilder(), o, true, maxObjectDepth).toString() ;
	}
	
	static public String stringtify(
			Object o, 
			boolean abstractPrimitives,
			int maxObjectDepth) {
		return stringtifyWorker(new StringBuilder(), o, abstractPrimitives, maxObjectDepth).toString() ;
	}
	
	static private StringBuilder stringtifyWorker(
			StringBuilder s, 
			Object o, 
			boolean abstractPrimitives,
			int maxObjectDepth) {
		if (o==null) { s.append("NULL") ; return s ; }
		Class C = o.getClass() ;
		if (maxObjectDepth <= 0) { s.append(C.getName()) ; return s ; }
		
		// strings:
		if (C == String.class) {
			String o_ = (String) o ;
			int N = o_.length() ;
			if (N>8) s.append(o_.substring(0,8) + "%" + N) ;
			else s.append(o_) ;
			return s ;
		}
		
		// if the flag is turned on, convert double, float, long, int abstractly
		if (abstractPrimitives) {
		   String v = abstractlyStringtifyNumericDFLI(o) ;
		   if (v != null) {
			   s.append(v) ; return s ;
		   }
		}
		// the rest of primitives and enum:
		if (Reflection.isPrimitiveLike(C) || C.isEnum()) {
			s.append(o.toString()) ;
			return s ;
		}
		
		// some of the dontShow classes, which can be shown a bit more refined:
		if (o instanceof File) {
			File f = (File) o ;
			s.append(f.toString()) ; return s ;
		}
		if (o instanceof OutputStream) {
			OutputStream o_ = (OutputStream) o ;
			return stringtifyWorker(s,o_.toString(),abstractPrimitives,maxObjectDepth) ;
		}
        // the rest of dont show:
		if (dontShow(o)) { s.append(C.getName()) ; return s ; }
		
		// arrays, lists ... sets and maps
		s.append("{") ;
		if (C.isArray()) {
			int N = Array.getLength(o) ;
			int N_ = Math.min(N,8) ;
			for (int i=0; i<N_; i++) {
				s.append(";X" + i + ":") ;
				stringtifyWorker(s,Array.get(o, i),abstractPrimitives,maxObjectDepth-1) ;
				if (i>=7) break ;
			}
			s.append("}") ; 
			if (N>8) s.append("%" + N) ; 
			return s ;
		}
		if (o instanceof Collection) {
			if (o instanceof Set) {
				Set o_ = (Set) o ;
				s.append(C.getName() + "%" + o_.size()) ;
				return s ;
			}
			Collection V = (Collection) o ;
			int i = 0 ;
			for (Object x : V) {
				s.append(";X" + i + ":") ;
				stringtifyWorker(s,x,abstractPrimitives,maxObjectDepth-1) ;
				i++ ;
				if (i>=8) break ;
			}
			s.append("}") ; 
			if (V.size()>8) s.append("%" + V.size()) ; 
			return s ;
		}
		if (o instanceof Map) {
			Map M = (Map) o ;
			s.append(C.getName() + "%" + M.size()) ;
			/*
			int i = 0 ;
			for (Object e : M.entrySet()) {
				Entry e_ = (Entry) e ;
				s.append(";key" + i + ":") ;
				stringtifyWorker(s,e_.getKey(),level-1) ;
				s.append(";val" + i + ":") ;
				stringtifyWorker(s,e_.getValue(),level-1) ;
				i++ ;
				if (i>=8) break ;
			}
			s.append(">") ; 
			if (M.size()>8) s.append("%" + M.size()) ; 
			*/
			return s ;
		}
		// last case is if it is an object
		Field[] fields = Show.getAllNonStaticFields(C).toArray(new Field[0]) ;
		for (int i=0; i<fields.length; i++) {
			Field f = fields[i] ;
			String fname = f.getName() ;
			String fname_ = fname.toLowerCase() ;
			if (fname.equals("$assertionsDisabled") ||
					fname_.contains("time") ||
					fname_.contains("second") ||
					fname_.contains("minute")
					) continue ;
			f.setAccessible(true);
			s.append(";" + f.getName() + ":") ;
			try {
				stringtifyWorker(s,f.get(o),abstractPrimitives,maxObjectDepth-1) ;
			}
			catch(Exception e) { s.append("??") ; }
		}
		s.append("}") ; 
		return s ;
	}

	// this applies trunctation and rounding
	static private String abstractlyStringtifyNumericDFLI(Object o) {
		Class C = o.getClass() ;
		if (C == Float.class || C == Double.class) {
			double d = 0 ;
			if (C == Double.class) d = (double) o ;
			if (C == Float.class) d = (double) ((float) o) ;
			String s = "" + d ;
			try {
				// this should not fail... but just to be sure let's put it in a try-catch
				Double d0 = Math.floor(d) ;
				Double delta = d - d0 ;
				d = d0 + Math.round(delta*10000d) / 10000d ;
				s = "" + d ; 
			}
			catch(Exception e) { }
			return s ;
		}
		if (C == Long.class || C == Integer.class) {
			long x = 0  ;
			if (C == Long.class)    x = (long) o ;
			if (C == Integer.class) x = (long)((int) o) ;
			String s = "" + x ;
			if (x < Short.MIN_VALUE || x > Short.MAX_VALUE) {
				String z = "" + Math.abs(x/100) ;
				int start = Math.max(0,z.length() - 3) ;
				s = "100%" + z.length() + "%" + z.substring(start) ;
				if (x<0) s = "-" + s ;	
			}
			return s ;
		}
		return null ;
	}
	
	private static String ExcStrDump(Throwable e) {
		if (e==null) return "null" ;
		return e.getClass().getName() ;
	}
	
	public String check(
			Maybe<Object> receiverObj, 
			Maybe<Object> returnObj,
			Maybe<Throwable> thrownException) 
	{
		//System.out.println(">>> checking an oracle..., ") ;
	    //System.out.println("       RET: " + returnObj + " vs expected (encoded): " + expectedReturnedObj) ;
		//System.out.println("       tobj:" + receiverObj + " vs expected (encoded): " + expectedReceiverObj) ;
		//System.out.println("       exc: " + thrownException + " vs expected (encoded): " + expectedException) ;
		
		if (thrownException!=null && expectedException != null) {
			String s = ExcStrDump(thrownException.val) ;
			if (! s.equals(expectedException.val)) {
				//System.out.println(">>> violating exception oracle!") ;
				String msg ;
				if (expectedException.val=="null") 
				   msg = "Exc-violation: expecting no exception, but got " + s ;
				else
				   msg = "Exc-violation: expecting: " + expectedException.val + " to be thrown, but getting: " + s ;	
				//System.out.println(">>> violating oracle: " + msg) ;
				return msg;
			}
		}
		
		if (receiverObj!=null && expectedReceiverObj != null) {
			try {
				String s = StrDump(receiverObj.val);
				if (! s.equals(expectedReceiverObj.val)) {
				  String msg = "Expecting: " + expectedReceiverObj.val + "\nbut getting: " + s;
				  // Logger.getLogger(CONSTANTS.T3loggerName).info(">>> An StrDumbEqOracle detects a VIOLATION!")
				  // ;
				  //System.out.println(">>> violating oracle: " + msg) ;
				  return msg;
				}
			} catch (Exception e) {
				// StrDump unfortunately throws an exception, so we cannot really
				// check if it would have violated
				// the expectation --> ignore
			}
		}
		
		if (returnObj!=null && expectedReturnedObj != null) {
			try {
				String s = StrDump(returnObj.val);
				if (! s.equals(expectedReturnedObj.val)) {
				  String msg = "Expecting: " + expectedReturnedObj.val + "\nbut getting: " + s;
				  //System.out.println(">>> RET-oracle violation! " + msg) ;
				  return msg;
				}
			} catch (Exception e) {
				// ignore
			}
		}
		// none of the oracles is violated:
        return null ;	
	}
	
	
	static public void main(String args[]) {
		System.out.println(stringtify("hello world",5)) ;
		System.out.println(stringtify("hi",5)) ;
		
		System.out.println(stringtify(null,5)) ;
		System.out.println(stringtify(new Object(),5)) ;
		System.out.println(stringtify(new Sequenic.T3.Examples.Item("Foo"),5)) ;
		System.out.println(stringtify(new Sequenic.T3.Examples.Triangle1(),5)) ;
		int[] a = {99,0} ;
		System.out.println(stringtify(a,5)) ;
		List s = new LinkedList() ;
		System.out.println(stringtify(s,5)) ;
		s.add(new Integer(99)) ;
		System.out.println(stringtify(s,3)) ;
		s.add(s) ;
		System.out.println(stringtify(s,5)) ;
		System.out.println(stringtify(900.00007,5)) ;
	}

}
