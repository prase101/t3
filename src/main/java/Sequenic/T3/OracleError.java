/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

/**
 * A test sequence may be decorated with some oracles. When such an oracle is
 * violated, we will represent this by an instance of this error class. 
 */
public class OracleError extends Error {
	
	private static final long serialVersionUID = 1L;

	public OracleError(String info) {
        super(info) ;
    }
	
	public OracleError(String info, Throwable cause) {
        super(info,cause) ;
    }
	
    public OracleError(Throwable cause) {
        super(cause) ;
    }
}
