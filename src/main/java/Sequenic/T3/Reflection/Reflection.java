/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Reflection;
import Sequenic.T3.CONSTANTS;

import java.io.IOException;
import java.util.*;
import java.lang.reflect.*;

/**
 * Utility class providing reflection functions.
 */
public class Reflection {

    private static Class[] boxingTypes = {
            Byte.class,
            Short.class,
            Integer.class,
            Long.class,
            Float.class,
            Double.class,
            Boolean.class,
            Character.class
    };

    public static boolean isBoxingType(Class C) {
        for (Class D : boxingTypes) {
            if (C==D) return true ;
        }
        return false ;
    }

    public static boolean isPrimitiveLike(Class C) {
        return C.isPrimitive() || isBoxingType(C) ;
    }

    public static boolean isConcreteClass(Class C) {
        return !C.isInterface() && !Modifier.isAbstract(C.getModifiers())    ;
    }

    /**
     * To obtain all non-interface and non-abstract superclasses of a given
     * class. The class "Object" is not included in the returned list.
     */
    public static List<Class> getAllConcreteSuperClasses(Class C) {
        List<Class> collected = new LinkedList<Class>();
        Class D = C.getSuperclass() ;
        while (D != null && D != Object.class) {
            if (isConcreteClass(D)) collected.add(D);
            D = D.getSuperclass() ;
        }
        return collected;
    }

    /**
     * To obtain all superclasses of a given class. The class "Object"
     * is not included in the returned list.
     */
    public static List<Class> getALLSuperClasses(Class C) {
        LinkedList<Class> collected = new LinkedList<Class>();
        Class D = C.getSuperclass() ;
        while (D != null && D != Object.class) {
            collected.addLast(D);
            D = D.getSuperclass() ;
        }
        return collected;
    }


    public static boolean isAccessibleFrom(Method M, Package P) {
        return isAccessibleFrom(M.getDeclaringClass(),M.getModifiers(),P) ;
    }

    public static boolean isAccessibleFrom(Field f, Package P) {
        return isAccessibleFrom(f.getDeclaringClass(),f.getModifiers(),P) ;
    }

    public static boolean isAccessibleFrom(Constructor C, Package P) {
        return isAccessibleFrom(C.getDeclaringClass(),C.getModifiers(),P) ;
    }

    private static boolean isAccessibleFrom(Class C, int modifier, Package P) {
        if (Modifier.isPublic(modifier)) return true ;
        if (C.getPackage() == P) {
            return ! Modifier.isPrivate(modifier) ;
        }
        return false ;
    }

    public static Class getClassFromName(String name)
            throws
            IOException,
            ClassNotFoundException,
            SecurityException{
        if (name.equals("boolean")) return Boolean.TYPE ;
        if (name.equals("char")) return Character.TYPE ;
        if (name.equals("byte")) return Byte.TYPE ;
        if (name.equals("short")) return Short.TYPE ;
        if (name.equals("int")) return Integer.TYPE ;
        if (name.equals("long")) return Long.TYPE ;
        if (name.equals("float")) return Float.TYPE ;
        if (name.equals("double")) return Double.TYPE ;
        return Class.forName(name) ;
    }


    private static <T> void addTo(List<T> U, T[] V) {
        for (T x : V) U.add(x) ;
    }

    public static void main(String[] args) {
        System.out.println("" + isBoxingType(Integer.TYPE)) ;
        System.out.println("" + isBoxingType(Integer.class)) ;
        System.out.println("" + isPrimitiveLike(Integer.class)) ;
    }
}
