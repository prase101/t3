/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

import org.apache.commons.cli.*;

import Sequenic.T3.Sequence.Datatype.SUITE;
import Sequenic.T3.Sequence.Datatype.SUITE_RT_info;

import java.io.File;
import java.io.FileOutputStream;
import java.util.logging.*;
import java.util.* ;


/**
 * A command-line tool to replay suite-files.  The sequences will be replayed one at a time
 * (sequential).
 */
public class ReplayCmd {

    Option o_help = new Option("help", "help",false,"Print this message.") ;
    Option o_replayd = new Option("b", "bulk", true ,"If specified the target is a directory; all suite-files there whose name match this regex pattern are replayed. See doc of java.util.regex.Pattern.") ;
    Option o_runAll = new Option("ra","runall",false,"If set, all sequences will be replayed; exception will not be rethrown.") ;
    Option o_regressionMode = new Option("rm","regressionmode",false,"If set, and runall is false, only thrown OracleError will be rethrown.") ;
    Option o_showExcExecution = new Option("sx","showXexec",false,"If set, then execution that throws an exception will be printed.") ;  
    Option o_logging = new Option("lf","logfile",true,"To echo messages to a log-file.") ;
    Option o_reportOut = new Option("rs","reportStream",true,"To send report to a file.") ;
    Option o_seqOut    = new Option("tps","tracePrintStream",true,"To send sequence-prints to a file.") ;
    Option o_showLength = new Option("sl","showlength",true,"If set will print each step in the suffix of each replayed sequence, up to this length. Default=10.") ;
    Option o_showDepth = new Option("sd","showdepth",true,"If set, when on object is printed, it is printed up to this depth. Defailt=3.") ;
    
    Options options = new Options() ;
    
    private FileHandler loghandler = null ;
    long runtime;
    
    CommandLine cmd ;
    public SuiteAPI suiteAPI ;
    
    Config config ;

    ReplayCmd() {
        runtime = System.currentTimeMillis() ;
        options.addOption(o_help) ;
        o_replayd.setArgName("\"regexpr\""); options.addOption(o_replayd)  ;
        options.addOption(o_runAll) ;
        options.addOption(o_regressionMode) ;
        options.addOption(o_showExcExecution) ;
        o_logging.setArgName("file"); options.addOption( o_logging) ;
        o_reportOut.setArgName("file"); options.addOption(o_reportOut) ;
        o_seqOut.setArgName("file"); options.addOption(o_seqOut) ;
        o_showLength.setArgName("int"); options.addOption(o_showLength) ;
        o_showDepth.setArgName("int"); options.addOption(o_showDepth) ;
        config = new Config() ;
    }

 
    private String getTarget(CommandLine cmd) {
        String[] residual = cmd.getArgs() ;
        for (int k=0; k<residual.length; k++)
            if (residual[k] != null && !residual[k].isEmpty() && !residual[k].equals(" "))
                return residual[k] ;
        return null ;
    }

    void printHelpInfo() {
        HelpFormatter formatter = new HelpFormatter();
        String header = "T3 test-suites replay tool. v0" ;
        String footer = "(c) author: Wishnu Prasetya" ;
        formatter.printHelp(100,"java -cp <T3jar><:otherpath>* Sequenic.T3.ReplayCmd <options>* <file or dir>", header, options, footer) ;
    }


    void configure() throws Exception {
    	 
        if (cmd.hasOption(o_logging.getLongOpt())) {
            String logname = cmd.getOptionValue(o_logging.getLongOpt())  ;
            loghandler = new FileHandler("" + logname + "_" + System.currentTimeMillis() + ".txt") ;
            // get the root logger
            Logger logger =  Logger.getLogger(CONSTANTS.T3loggerName) ;
            if (logger.getParent() != null && logger.getParent()!=logger) logger = logger.getParent()  ;
            logger.addHandler(loghandler);
            loghandler.setLevel(Level.FINE);
            loghandler.setFormatter(new SimpleFormatter());
        }

        config.replayRunAll   = cmd.hasOption(o_runAll.getLongOpt()) ;
        config.regressionMode = cmd.hasOption(o_regressionMode.getLongOpt()) ;
        config.replayShowExcExecution = cmd.hasOption(o_showExcExecution.getLongOpt()) ;
        if (cmd.hasOption(o_showLength.getLongOpt()))
            config.replayShowLength = Integer.parseInt(cmd.getOptionValue(o_showLength.getLongOpt())) ;
        if (cmd.hasOption(o_showDepth.getLongOpt()))
            config.replayShowDepth = Integer.parseInt(cmd.getOptionValue(o_showDepth.getLongOpt())) ;

        if (cmd.hasOption(o_reportOut.getLongOpt())) {
        	String streamName = cmd.getOptionValue(o_reportOut.getLongOpt()) ;
        	if (streamName.equals("System.err")) {
        		//System.out.println("**>> streamName = " + streamName) ;
        		config.reportOut = System.err ;
        	}
        	else config.reportOut = new FileOutputStream(streamName) ;
        }
        
        if (cmd.hasOption(o_seqOut.getLongOpt())) {
        	String streamName = cmd.getOptionValue(o_seqOut.getLongOpt()) ;
        	if (streamName.equals("System.err")) config.sequencePrintOut = System.err ;
        	else config.sequencePrintOut = new FileOutputStream(streamName) ;
        }
        
        suiteAPI = new SuiteAPI(config) ;
        
    }
    
    /**
     * to run a suite, to be overriden by a subclass that wants to run the suite in a different way
     */
    SUITE_RT_info runSuite(SUITE S) throws Exception {
    	return suiteAPI.replay(ClassLoader.getSystemClassLoader(),S) ;
    }
    
    private void runReplay(String[] args) throws Throwable {
    	CommandLineParser parser = new GnuParser();
        cmd = parser.parse(options, args);
    	String target = getTarget(cmd) ;
    	
        if (target == null || cmd.hasOption(o_help.getLongOpt())) {
            printHelpInfo();
            return ;
        }
        
        configure() ;
        
        List<SUITE> suites ;
        if (cmd.hasOption(o_replayd.getLongOpt())) {
        	String regexpr  = cmd.getOptionValue(o_replayd.getLongOpt()) ;
        	suites = suiteAPI.load(regexpr, target) ;
        	//System.out.println(">>> received bulk option = " + regexpr + ", loading " + suites.size() + " sequences.") ;    
        }
        else {
        	suites = new LinkedList<SUITE>() ;
        	suites.add(suiteAPI.load(target)) ;        	
        }
        
        if (suites.isEmpty()) return ;
        suiteAPI.config.CUT = Class.forName(suites.get(0).CUTname) ;
        SUITE S = new SUITE(suiteAPI.config.CUT.getName()) ;
        for (SUITE T : suites) S = SUITE.union(S,T) ;
        S.suitename = "dummy name, a merged suite" ;
        SUITE_RT_info info = null ;
        try {
        	info = runSuite(S) ;
        	info.printReport(suiteAPI.config.reportOut);
        }
        finally {
            runtime = System.currentTimeMillis() - runtime ;
            Logger.getLogger(CONSTANTS.T3loggerName).info(">> Replay runtime = " + runtime + "ms");
            if (loghandler != null) {
            	loghandler.flush();
            }
            suiteAPI.config.reportOut.flush();
            suiteAPI.config.sequencePrintOut.flush();
            if (!config.replayRunAll && info.firstViolation != null) throw info.firstViolation ;
        }
    }
    

    public static void main(String optionsString) throws Throwable {
    	String[] args = optionsString.split("( )+") ;
    	main(args) ;
    }
    
    public static void main(String[] args) throws Throwable {
    	ReplayCmd replay = new ReplayCmd() ;
    	replay.runReplay(args);
        // exec("--out=report.txt --showlength=0 --norethrow --bulk=Sequenic.T3.Examples.Friends.Person d:/tmp") ;
        // exec("-mc --showlength=0 --norethrow --bulk=Sequenic.T3.Examples.Friends.Person d:/tmp") ;
        // exec("-mc --silent --norethrow --bulk=Sequenic.T3.Examples.Abstractx.Car d:/tmp") ;
    }

}
