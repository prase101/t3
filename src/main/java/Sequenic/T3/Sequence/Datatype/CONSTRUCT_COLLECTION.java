/*
 * Copyright 2010 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Sequence.Datatype;

import Sequenic.T3.JavaType.*;
import Sequenic.T3.Sequence.Datatype.STEP.PreConditionCheckPlaceHolder;
import Sequenic.T3.*;
import Sequenic.T3.utils.*;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;

/**
 * A STEP to construct an array, collection, or map like object. 
 */
public class CONSTRUCT_COLLECTION extends STEP implements Serializable {
	
	private static final long serialVersionUID = 1L;

    /**
     * The concrete type of the array/collection to generate.
     */
    public JType cty  ;

    public STEP[] elements = null ;
    // the keys, only for Map:
    public STEP[] keys = null ;

    /**
     * 
     * @param concreteType  The type of the structure to construct.
     * @param elems  The STEPs to construct the elements.
     * @param keys   The STEPs to construct the keys. Only needed for maps, and should be
     *               of equal length as elems. For other types, this should be null.
     */
    public CONSTRUCT_COLLECTION(JType concreteType,
                                STEP[] keys,
                                STEP[] elems)
    {
        cty = concreteType ;
        this.keys = keys ;
        elements = elems;   
    }

    public CONSTRUCT_COLLECTION(JType concreteType, STEP[] elems) {
        this(concreteType,null,elems) ;
    }

    public CONSTRUCT_COLLECTION() { } 
    
    @Override
    public boolean isCreationStep() { return false ; } ;
    
    /**
     * Note: collection-like will not be put in the Pool. Its elements can be
     * cached in the Pool.
     */
    @Override 
    public STEP_RT_info exec(Class CUT, Pool pool, PreConditionCheckPlaceHolder precondPlaceHolder) 
    		throws 
    		Exception
    {
        //System.out.println(">>>" + this);

    	STEP_RT_info r = new STEP_RT_info(this,CUT,pool.getObjectUnderTest()) ;
    	
    	STEP_RT_info[] argsResults = STEP.execMany(CUT,pool, elements) ;
        //System.out.println(">>>" + elements.length + ", #argsResults=" + argsResults.length);
    	int N = argsResults.length ;
    	r.args = new Object[N] ;
    	// if one of the argument fails or throws an exception, then this MKVAL cannot be executed
        // so we return a null :
    	for (STEP_RT_info i : argsResults) {
    	   if (i==null || i.exc != null) {
    		   return null ;
    	   }
    	}
        for (int i=0; i<N; i++) r.args[i] = argsResults[i].returnedObj ;
        
        if (precondPlaceHolder!=null) precondPlaceHolder.test(null, this, r.args);

        // if the creation of elements all succeeds:
    	JTfun cty_ = (JTfun) cty ;
    	if (cty_.isConcreteArray()) {
            // Below we'll create a n array of just the top-type of arg[0].
            // This is an over approximation, but at least would not crash at the run-time.
            // Generators should make sure that the arguments created are of consistent
            // type.
            // Something analogous is done with collections and maps.
    		int dimension = JTypeUtils.getArrayDimension(cty) ;
    		Class elemTy = JTypeUtils.getTopClass(JTypeUtils.getDeepestArrayElementTy(cty)) ;
    		if (dimension>1) {
    			int[] dummyDims = new int[dimension-1] ;
    			Object dummy = Array.newInstance(elemTy,dummyDims) ; 
    			elemTy = dummy.getClass() ;
    		}
            //System.out.println(">>> type of array's element: " + elemTy.getName()) ;
    		r.returnedObj = Array.newInstance(elemTy, N) ;
    		for (int i=0; i<N; i++)  {
                // when generically adding alements to an array as we do below, Java does not
                // do auto-unboxing of primitives; it will actually crash. So we have to do
                // this ourselves:
                if (elemTy == Boolean.TYPE)
                    ((boolean[]) r.returnedObj)[i] = (boolean) (r.args[i]) ;
                else if (elemTy == Byte.TYPE)
                    ((byte[]) r.returnedObj)[i] = (byte) (r.args[i]) ;
                else if (elemTy == Short.TYPE)
                    ((short[]) r.returnedObj)[i] = (short) (r.args[i]) ;
                else if (elemTy == Integer.TYPE)
                    ((int[]) r.returnedObj)[i] = (int) (r.args[i]) ;
                else if (elemTy == Long.TYPE)
                    ((long[]) r.returnedObj)[i] = (long) (r.args[i]) ;
                else if (elemTy == Character.TYPE)
                    ((char[]) r.returnedObj)[i] = (char) (r.args[i]) ;
                else if (elemTy == Float.TYPE)
                    ((float[]) r.returnedObj)[i] = (float) (r.args[i]) ;
                else if (elemTy == Double.TYPE)
                    ((double[]) r.returnedObj)[i] = (double) (r.args[i]) ;
    			else {
    				//System.out.print("<<< " + r.args[i]) ;
    				try { Array.set(r.returnedObj,i,r.args[i]); }
    				catch(Throwable e) { 
    					//System.out.println("THROWING " + e) ; 
    					//System.out.println("array type: " + cty);
    					//System.out.println("elem T3-type: " + elemTy);
    					//System.out.println("elem type: " + r.args[i].getClass());
    					throw e ; 
    				}
    				//System.out.println(">>> " + r.args[i]) ;
    			}
            }
            pool.put(cty,r.returnedObj)  ;
    		return r ;
    	}
    	
    	if (cty_.isConcreteCollection()) {
    		r.returnedObj = cty_.fun.newInstance() ;
    		for (int i=0; i<N; i++)
    			((Collection) r.returnedObj).add(r.args[i]) ;
            pool.put(cty,r.returnedObj)  ;
    		return r ;
    	}
    	
    	// else then it should be a Map
    	if (! cty_.isConcreteMap())
    		throw new T3Exception("Expecting a Map, but get " + cty) ;
    	
    	assert keys.length == elements.length ;
    	
    	STEP_RT_info[] keysResults = STEP.execMany(CUT, pool, keys) ;
    	// if one of the keys fails or throws an exception, then this MKVAL also fails:
    	for (STEP_RT_info i : keysResults) {
    	   if (i==null || i.exc != null) {
    		   return null ;
    	   }
    	}
        for (int i=0; i<N; i++) r.args[i] = new Pair(keysResults[i].returnedObj, argsResults[i].returnedObj) ;


        r.returnedObj = cty_.fun.newInstance() ;
    	for (int i=0; i<N; i++) {
    		Pair x = (Pair) r.args[i] ;
    		((Map) r.returnedObj).put(x.fst,x.snd) ;
    	}
        pool.put(cty,r.returnedObj)  ;
    	return r ;
    }

 
    /**
     * For serialization.
     */
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        stream.writeObject(cty);
        stream.writeObject(keys);
        stream.writeObject(elements);
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream) 
    		throws 
    		IOException, 
    		ClassNotFoundException 
    {
         cty      = (JType) stream.readObject() ;
         keys     = (STEP[]) stream.readObject();
         elements = (STEP[]) stream.readObject();
    }

    @Override
    public String toString() {
        String result = "COL-like " + cty + ": ";

        String elemsTxt = "";
        for (int i = 0; i < elements.length; i++) {
        	if (cty.isConcreteMap()) {
        		elemsTxt = elemsTxt + "<" + keys[i]
        				            + "," + elements[i] + ">" ;
        	}
        	else elemsTxt = elemsTxt + elements[i];
            if (i < elements.length - 1) {
                elemsTxt = elemsTxt + ",\n";
            }
        }
        result = result + "[ " + StringFormater.indentButFirst(elemsTxt,3) + " ]";

        return result;
    }
    
    @Override
    public STEP clone_withoutOracles() {
    	CONSTRUCT_COLLECTION col = new CONSTRUCT_COLLECTION() ;
    	col.cty = cty ;
    	if (elements != null) {
    		col.elements = new STEP[elements.length] ;
    		for (int i=0; i<elements.length ; i++) col.elements[i] = elements[i].clone_withoutOracles() ;
    	}
    	if (keys != null) {
    		col.keys = new STEP[keys.length] ;
    		for (int i=0; i<keys.length ; i++) col.keys[i] = keys[i].clone_withoutOracles() ;
    	}
    	return col ;
    }

}
