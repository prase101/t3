/*
 * Copyright 2014 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Sequence.Datatype;

import java.io.Serializable;

import Sequenic.T3.Pool;
import Sequenic.T3.Sequence.Datatype.STEP.PreConditionCheckPlaceHolder;

/**
 * Representing a bogus-step; inserted to insert instrumentation points along
 * a test-sequence.
 */
public class INSTRUMENT extends STEP implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Should never be executed.
	 */
	@Override
	public STEP_RT_info exec(Class CUT, Pool pool, PreConditionCheckPlaceHolder precondPlaceHolder) throws Exception {
		assert false ;
		return null;
	}

    @Override
    public boolean isCreationStep() { return false ; } ;
    
    @Override
    public STEP clone_withoutOracles() { return new INSTRUMENT() ; }
    
}
