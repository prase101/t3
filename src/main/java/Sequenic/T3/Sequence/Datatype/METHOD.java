/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Sequence.Datatype;

import Sequenic.T3.JavaType.JType;
import Sequenic.T3.JavaType.JTypeUtils;
import Sequenic.T3.Pool;
import Sequenic.T3.ExceptionOracleError;
import Sequenic.T3.OracleError;
import Sequenic.T3.Oracle.*;
import Sequenic.T3.Reflection.Reflection;
import Sequenic.T3.Sequence.Datatype.STEP.PreConditionCheckPlaceHolder;
import Sequenic.T3.utils.Maybe;
import Sequenic.T3.utils.Show;
import Sequenic.T3.utils.StringFormater;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;

/**
 * This represents a STEP to call a method.
 */
public class METHOD extends STEP implements Serializable {
	
	private static final long serialVersionUID = 1L;

    public Method method = null ;
    public STEP   target = null ;
    public STEP[] params = null ;

    /**
     * Flag to indicate whether the step is a step that creates the object under test.
     */
    public boolean isObjectUnderTestCreationStep  = false ;

    public Oracle oracle = Oracle.TT ;
    
   /**
     * @param m        The method to call in this step.
     * @param target   The target/receiver of the call. If there is none, set this to null.
     * @param params       The STEPs needed to construct the parameters for m.
     */
    public METHOD(Method m, STEP target, STEP[] params) {
        method = m;
        this.target = target ; 
        this.params = params;
    }

    public METHOD(){ }
    
    @Override
    public boolean isCreationStep() { return isObjectUnderTestCreationStep ; } ;

    @Override 
    public STEP_RT_info exec(Class CUT, Pool pool, PreConditionCheckPlaceHolder precondPlaceHolder)
    		throws 
    		Exception
    {
    	STEP_RT_info r = new STEP_RT_info(this,CUT,pool.getObjectUnderTest()) ;
    	
    	// construct the parameters:
    	STEP_RT_info[] argsResults = STEP.execMany(CUT,pool, params) ;
    	int N = argsResults.length ;
    	r.args = new Object[N] ;
    	// if one of the arguments fails or throws an exception, then this STEP fails:
    	for (STEP_RT_info i : argsResults) {
    	   if (i==null || i.exc != null) {
    		   return null ;
    	   }
    	}
        for (int i=0; i<N; i++) r.args[i] = argsResults[i].returnedObj ;
        
        // construct the target object:
    	STEP_RT_info targetResult = null ;
    	if (target != null) {
    		targetResult = target.exec(CUT,pool,null) ;
    		if (targetResult==null || targetResult.exc != null) {
    			return null ;
    		}
    		r.receiverObj = targetResult.returnedObj ;
    	}
    	else r.receiverObj = null ;
    	
    	if (precondPlaceHolder!=null) precondPlaceHolder.test(r.receiverObj, this, r.args);

    	
    	// now call the method
        method.setAccessible(true); // force it to be accessible
        try {
        	
           // various debugs:
        	
           //System.out.println("**>> METHOD.java: method  = " + method);
           //System.out.println("**>> METHOD.java: rec-Meta   = " + target);
           //System.out.println("**>> METHOD.java: index of OUT = " + pool.getIndexOfObjectUnderTest());
           //System.out.println("**>> METHOD.java: rec-obj = " + targetResult.returnedObj);
           //System.out.println("**>> METHOD.java: rec-obj = " + r.receiverObj);
           //System.out.print("**>> METHOD.java meta-args " + params.length + ":");
           //for (Object y : params) System.out.print(" /" + y) ;
           //System.out.println("") ;
           //System.out.print("**>> METHOD.java args " + r.args.length + ":");
           //for (Object y : r.args) System.out.print(" /" + y) ;
           //System.out.println("") ;
           //System.out.print("**>> METHOD.java argtypes: ") ;
           //for (Type argty : method.getGenericParameterTypes()) System.out.print(" /" + argty) ;
           //System.out.println("") ;
                     
           r.returnedObj = method.invoke(r.receiverObj, r.args) ;
        } 
        /*
        catch (IllegalAccessException e) { throw e ; }
        catch (IllegalArgumentException e) { 
        	   // this is the case e.g. when the number of supplied parameters are incorrect
        	   throw e ; } 
        */
        catch (InvocationTargetException e) { 
        	   // the invoked method has thrown an exception
        	   r.exc = e.getCause() ;
        	   assert r.exc != null ;
        }
        catch (Exception e) {
        	//System.out.println(">>> threw exception: " + e) ;
        	//e.printStackTrace(System.out);
        	//System.out.println(">>> " + r.receiverObj);
        	//System.out.println(">>> " + (r.receiverObj.getClass() == Sequenic.T3.Examples.IncomeTax.class));
        	throw e ;
        }
        //System.out.println(">>> " + ! Reflection.isPrimitiveLike(r.returnedObj.getClass())) ;
        if (r.exc == null && r.returnedObj != null && ! Reflection.isPrimitiveLike(r.returnedObj.getClass())) {
        	JType rty = JTypeUtils.convert(r.returnedObj.getClass()) ;
            pool.put(rty,r.returnedObj) ;
            //System.out.println(">>> put : " + r.returnedObj + " in the pool") ;
        }

        checkClassinv(CUT,pool,pool.getObjectUnderTest(),r)  ;
        
        // don't lift the returned object and thrown exception; wrap explicitly to also represent when a null is 
        // returned / null exception is thrown
        String check1 = oracle.check(Maybe.lift(r.receiverObj), new Maybe(r.returnedObj), new Maybe(r.exc)) ;
        if (check1 != null) {
        	if (check1.startsWith("Exc-violation:"))
        		 r.exc = new ExceptionOracleError("T3: violating an oracle. Info: " + check1) ;
        	else r.exc = new OracleError("T3: violating an oracle. Info: " + check1) ;
        }     
        return r ;
       
    }

    @Override
    public void clearOracle() {
        oracle = Oracle.TT ;
    }

    /**
     * For serialization.
     */
    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
       stream.writeObject(method.getDeclaringClass().getName());
       stream.writeObject(method.getName()) ;
       Class[] paramTypes   = method.getParameterTypes();
       String[] paramTypes_ = new String[paramTypes.length];
       for (int i = 0; i < paramTypes.length; i++) {
            paramTypes_[i] = paramTypes[i].getName();
       }
       stream.writeObject(paramTypes_);
       stream.writeObject(target) ;
       stream.writeObject(params);
       stream.writeObject(new Boolean(isObjectUnderTestCreationStep));
       stream.writeObject(oracle) ;
    }

    /**
     * For serialization.
     */
    private void readObject(java.io.ObjectInputStream stream) 
    		throws 
    		IOException, 
    		ClassNotFoundException,
    		NoSuchMethodException, 
    		SecurityException
    {
        //System.out.println("$$") ;
      	Class C = Class.forName((String) stream.readObject()) ;
      	String methodname = (String) stream.readObject() ;
        String[] paramTypes_ = (String[]) stream.readObject();
        Class[] paramTypes   = new Class[paramTypes_.length];
        for (int i = 0; i < paramTypes_.length; i++) {
             paramTypes[i] = Reflection.getClassFromName(paramTypes_[i]);
        }
        method = C.getDeclaredMethod(methodname, paramTypes) ;
        target = (STEP)   stream.readObject();
        params = (STEP[]) stream.readObject();
        isObjectUnderTestCreationStep = (Boolean) stream.readObject() ;
        oracle = (Oracle) stream.readObject() ;
    }

    
    public String toString() {
        String result = "CALL " ;
        if (target == null) result += "static " ;
        result += method.getName() ;
        
        if (target != null) {
        	result += " on " + target + "\nwith\n" ;
        }
        
        result += "(" ;
        for (int i=0; i<params.length; i++) {
            result += params[i] ;
            if (i<params.length-1) result += ",\n" ;
        }
        result += ")" ;
        return StringFormater.indentButFirst(result,3) ;
    }
    
    @Override
    public STEP clone_withoutOracles() {
    	METHOD m = new METHOD() ;
    	m.method = method ;
    	if (target!=null) m.target = target.clone_withoutOracles() ;
    	if (params != null) {
    		m.params = new STEP[params.length] ;
    		for (int i=0; i<params.length ; i++) m.params[i] = params[i].clone_withoutOracles() ;
    	}
    	m.isObjectUnderTestCreationStep = isObjectUnderTestCreationStep ;
    	return m ;    	
    }

}
