package Sequenic.T3.Sequence.Datatype;

import java.util.*;

import Sequenic.T3.Pool;
import Sequenic.T3.T3Random;
import Sequenic.T3.JavaType.JType;

/**
 * Utility to Mutate and cross over sequences. Uhm.. for now only cross-over.
 */
public class Mutation {
	
	static private STEP fix(Map<Integer,JType> typemapHost, Map<Integer,JType> typemapOrigin, STEP step) 
	{
		if (step instanceof CONST || step instanceof INSTRUMENT) {
			return step ;
		}
		if (step instanceof REF) {
			int hostN = typemapHost.size() ;
			if (hostN==0) return null;
			REF ref = (REF) step ;	
			JType hostty = typemapHost.get(ref.index) ;
			JType orgty  = typemapOrigin.get(ref.index) ;
			if (orgty.equals(hostty)) return ref ;
			
			int k = Math.min(ref.index-1, hostN-1) ; //note: at index we wont have a match
			while (0<=k) {
				// search fix at position lower than k
				JType ty = typemapHost.get(k) ;
				if (orgty.equals(ty)) {
					// fix is found
					ref.index = k ;
					return ref ;
				}
				k-- ;
			}
			// no match... search above k then
			k = ref.index+1 ;
			while (k < hostN) {
				JType ty = typemapHost.get(k) ;
				if (orgty.equals(ty)) {
					// fix is found
					ref.index = k ;
					return ref ;
				}
				k++ ;
			}
			// no match!
			return null ;
		}
		if (step instanceof UPDATE_FIELD) {
			UPDATE_FIELD f = (UPDATE_FIELD) step ;
			STEP patch = fix(typemapHost,typemapOrigin,f.val) ;
			if (patch==null) return null ;
			f.val = patch ;
			return f ;
		}
		if (step instanceof METHOD) {
			METHOD m = (METHOD) step ;
			if (m.target != null) {
			  STEP patch = fix(typemapHost,typemapOrigin,m.target) ;
			  if (patch==null) return null ;
			  m.target = patch ;
			}
			for (int i=0; i<m.params.length; i++) {
				STEP patch = fix(typemapHost,typemapOrigin,m.params[i]) ;
				if (patch==null) return null ;
				m.params[i] = patch ;
			}
			return m ;
		}
		if (step instanceof CONSTRUCTOR) {
			CONSTRUCTOR c = (CONSTRUCTOR) step ;
			for (int i=0; i<c.params.length; i++) {
				STEP patch = fix(typemapHost,typemapOrigin,c.params[i]) ;
				if (patch==null) return null ;
				c.params[i] = patch ;
			}
			return c ;
		}
		if (step instanceof CONSTRUCT_COLLECTION) {
			CONSTRUCT_COLLECTION c = (CONSTRUCT_COLLECTION) step ;
			if (c.elements != null) {
			   for (int i=0; i<c.elements.length; i++) {
				  STEP patch = fix(typemapHost,typemapOrigin,c.elements[i]) ;
				  if (patch==null) return null ;
				  c.elements[i] = patch ;
			   }
			}
			if (c.keys != null) {
				for (int i=0; i<c.keys.length; i++) {
					  STEP patch = fix(typemapHost,typemapOrigin,c.keys[i]) ;
					  if (patch==null) return null ;
					  c.keys[i] = patch ;
				   }
			}
			return c ;
		}
		return null ;
	}

	static private SEQ fix(Map<Integer,JType> typemapOrigin, Class CUT, SEQ seq, int lengthOfSafePrefix) throws Exception {
		// first create a clone of seq, with cleared oracles 
		seq = seq.clone_withoutOracles() ;
		Pool pool = new Pool() ;
		SEQ patched = new SEQ() ;
		for (int k=0; k<lengthOfSafePrefix; k++) {
			patched.steps.add(seq.steps.get(k)) ;
		}
		seq.exec(CUT,pool) ;
		Map<Integer,JType> typemapHost = pool.getTypeMap() ;
		int N = seq.length() ;
		for (int k=lengthOfSafePrefix; k<N; k++) {
			STEP newStep = fix(typemapHost,typemapOrigin,seq.steps.get(k)) ;
			if (newStep==null) return null ;
			newStep.exec(CUT, pool, null) ;
			patched.steps.add(newStep) ;
		}
		return patched ;
	}
	
	/**
	 *  let the indices of seq1 = [0..a1) ++ [a1..b1) ++ [b1 .. N1)
	 *      the indices of seq2 = [0..a2] ++ [a2..b2) ++ [b2 .. N2)
	 *      
	 *  If successful, the crossover will produce:
	 *  
	 *       seq1' = seq1[0..a1) ++ seq2[a2..b2) ++ seq1[b1 .. N1)
	 *       seq2' = seq2[0..a2] ++ seq1[a1..b1) ++ seq2[b2 .. N2)
	 *       
	 *  we require: that 1<=a1<b1<=N1 and similarly 1<=a2<b1<=N2
	 *       
	 */
	static private List<SEQ> crossover(Class CUT, SEQ seq1, SEQ seq2, 
			int a1, int b1, int a2, int b2) throws Exception {
		int N1 = seq1.length() ;
		int N2 = seq2.length() ;
		if (! (1<=a1 && a1<b1 && b1<=N1 && 1<=a2 && a2<b2 && b2<=N2)) throw new IllegalArgumentException() ;
		Pool pool1 = new Pool() ;
		Pool pool2 = new Pool() ;
		seq1.exec(CUT, pool1) ;
		Map<Integer,JType> tymap1 = pool1.getTypeMap() ;
		Map<Integer,JType> tymap2 = pool2.getTypeMap() ;
		SEQ seq1new = new SEQ() ;
		SEQ seq2new = new SEQ() ;
		for (int k=0; k<a1; k++) {
			seq1new.steps.add(seq1.steps.get(k)) ;
		}
		for (int k=a2; k<b2; k++) {
			seq1new.steps.add(seq2.steps.get(k)) ;
		}
		for (int k=b1; k<N1; k++) {
			seq1new.steps.add(seq1.steps.get(k)) ;
		}
		for (int k=0; k<=a2; k++) {
			seq2new.steps.add(seq2.steps.get(k)) ;
		}
		for (int k=a1; k<b1; k++) {
			seq2new.steps.add(seq1.steps.get(k)) ;
		}
		for (int k=b2; k<=N2; k++) {
			seq2new.steps.add(seq2.steps.get(k)) ;
		}
		seq1new = fix(tymap1,CUT,seq1new,a1) ;
		seq2new = fix(tymap2,CUT,seq1new,a2) ;
		List<SEQ> result = new LinkedList<SEQ>() ;
		if (seq1new != null) result.add(seq1new) ;
		if (seq2new != null) result.add(seq2new) ;
		return result ;
	}
	
	static private int findINSTRUMENT(SEQ seq) {
		int k = 0 ;
		for (STEP st : seq.steps) {
			if (st instanceof INSTRUMENT) return k ;
			k++ ;
		}
		return -1 ;
	}
	
	/**
	 * Peform a crossover of two sequences. The crossed middle-section will be
	 * selected to be a random segment around the first INSTRUMENT step in each
	 * sequence. If no such step exists, then a random middle point is chosen.
	 */
	static private List<SEQ> crossover(Class CUT, SEQ seq1, SEQ seq2) throws Exception {
		int N1 = seq1.length() ;
		int N2 = seq2.length() ;
		if (N1<=1 || N2<=1) return new LinkedList<SEQ>() ;
		
		int m1 = findINSTRUMENT(seq1) ;
		if (m1<0) m1 = 1 + T3Random.getRnd().nextInt(N1-1) ;
		int m2 = findINSTRUMENT(seq2) ;
		if (m2<0) m2 = 1 + T3Random.getRnd().nextInt(N2-1) ;
		int a1 = Math.max(1,m1 - T3Random.getRnd().nextInt(m1)) ;
		int b1 = Math.min(N1, m1 + 1 + T3Random.getRnd().nextInt(N1-m1)) ;
		int a2 = Math.max(1,m2 - T3Random.getRnd().nextInt(m2)) ;
		int b2 = Math.min(N1, m2 + 1 + T3Random.getRnd().nextInt(N2-m2)) ;
		
		return crossover(CUT,seq1,seq2,a1,b1,a2,b2) ;
	}
	
	/**
	 * Perform a single round of cross-over over the sequences in the given suite. 
	 * Each time, two random members of the suite is selected, and crossed over. 
	 * The results (at most 2 new sequences) are added to the list. This is 
	 * repeated N times. Note that cross-over results are not added back to the 
	 * given suite for further cross over.
	 * 
	 * Note that the resulting new suite will have no oracles. 
	 */
	static public SUITE crossover(Class CUT, SUITE suite, int N) {
		SUITE newsuite = new SUITE(suite.CUTname) ;
		int basePopulationSize = suite.suite.size() ;
		while (N>0) {
			List<SEQ> children = null ;
			try {
				SEQ seq1 = suite.suite.get(T3Random.getRnd().nextInt(basePopulationSize)) ;
				SEQ seq2 = suite.suite.get(T3Random.getRnd().nextInt(basePopulationSize)) ;
				children = crossover(CUT,seq1,seq2) ;
				newsuite.suite.addAll(children) ;
			}
			catch(Exception e){ }
			N-- ;
		}
		return newsuite ;
	}
	
	
}
