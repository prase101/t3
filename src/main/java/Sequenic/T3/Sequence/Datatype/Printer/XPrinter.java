package Sequenic.T3.Sequence.Datatype.Printer;

import java.util.List;
import java.util.logging.Logger ;
import Sequenic.T3.CONSTANTS ;
import Sequenic.T3.Pool ;
import Sequenic.T3.Sequence.Datatype.* ;

/**
 * Utility to print the execution of a test sequence and a test suite.
 */
public class XPrinter {

	public XPrinter() { }
	
	STEP.PreConditionCheckPlaceHolder mkEntryFormatter(StringBuffer z, Integer[] stepNr) {
		return new STEP.PreConditionCheckPlaceHolder() {	
			public void test(Object targetObj, STEP step, Object[] stepArgs) {
				result = true   ;
				if (step instanceof INSTRUMENT) {
					z.append("** [" + stepNr[0] + "] INSTRUMENT \n") ; return ;
				}
				z.append("** << [" + stepNr[0] + "] ") ;
				if  (step instanceof METHOD) {
					formatMethodCallEntry(z,(METHOD) step,targetObj,stepArgs) ; return ;
				}
				if (step instanceof CONSTRUCTOR) {
					formatConstructorCallEntry(z,(CONSTRUCTOR) step,stepArgs) ; return ; 
				}	
				if (step instanceof UPDATE_FIELD) {
					formatUpdateFieldEntry(z,(UPDATE_FIELD) step,targetObj,stepArgs[0]) ; return ;
				}			
			}
	     } ;
	}
	
	// will be called at the end of a test-step:
	void exitFormatter(StringBuffer z, STEP_RT_info info, int stepNr) {
		STEP step = (STEP) info.step;
		if (step instanceof INSTRUMENT) return ;
		z.append("\n** on EXIT\n") ;
		if (step instanceof METHOD)
			formatMethodCallExit(z, (METHOD) step,info.receiverObj,info.args,info.returnedObj,info.exc) ;
			else if (step instanceof CONSTRUCTOR)
			formatConstructorCallExit(z, (CONSTRUCTOR) step,info.args,info.returnedObj,info.exc) ;
			else if (step instanceof UPDATE_FIELD)
			formatUpdateFieldExit(z, (UPDATE_FIELD) step,info.receiverObj) ;
		z.append("** [" + stepNr + "] >>\n") ;
	}

	// override this	
	public void print(StringBuffer z, SUITE S) throws Exception {
		Class CUT = Class.forName(S.CUTname) ;
		Pool pool = new Pool() ;
		int k = 0 ;
		z.append("** SUITE name=" + S.suitename + " , CUT=" + S.CUTname + " "
				+ ", #seqs=" + S.suite.size()
				+ ", timestamp=" + S.timeStamp ) ;
		
		for (SEQ seq : S.suite) {
			z.append("\n******** SEQUENCE " + k + "\n") ;
			print(z,CUT,pool,seq) ;
			k++ ;
		}
	}
	
	public String print(SUITE S) throws Exception {
		StringBuffer z = new StringBuffer() ;
		print(z,S) ;
		return z.toString() ;
	}
	
	public String print(Class CUT, SEQ sequence) {
		StringBuffer z = new StringBuffer() ;
		print(z,CUT, new Pool(), sequence) ;
		return z.toString() ;
	}
	
	public void print(StringBuffer z, Class CUT, Pool pool,SEQ sequence) {
		pool.reset() ;
		Integer[] k = {0} ;
		STEP.PreConditionCheckPlaceHolder entryFormatter = mkEntryFormatter(z,k) ;
		for (STEP step : sequence.steps) {
			STEP_RT_info info = Xutils.executeSTEP(CUT,pool,entryFormatter,step,k[0]) ;
		    if (info==null) break ;
			exitFormatter(z,info,k[0]) ;
			k[0]++ ;
		}
	}
	
	// override this
	void write(String s) { System.out.print(s) ; }
	void writeln(String s) { write(s + "\n") ; }
		
	// override this
	void formatUpdateFieldEntry(StringBuffer z, UPDATE_FIELD field, Object receiver, Object arg) {
		z.append("** UPDATING FIELD ") ;
		z.append(field.field.getName() + " of ") ; 
		formatObject(z,receiver) ;
		z.append(", WITH ") ;
		formatObject(z,arg) ;
	}
		
	// override this
	void formatUpdateFieldExit(StringBuffer z, UPDATE_FIELD field, Object receiver) {
		z.append("** ") ;
		formatObject(z,receiver) ;
	}
		
	// override this
	void formatMethodCallEntry(StringBuffer z, METHOD m, Object receiver, Object[] args) {
		z.append("** CALLING METHOD ") ;
		z.append(m.method.getName() + "(rec=") ;
		formatObject(z,receiver) ;
		int i=0 ;
		for (Object o : args) {
			z.append(", x" + i + "=") ; formatObject(z,o) ;
			i++ ;
		}
		z.append(")") ;
	}
		
	// override this
	void formatMethodCallExit(StringBuffer z, METHOD m, Object receiver, Object[] args, Object retval, Object exc) {
		z.append("** of ") ;
		z.append(m.method.getName() + ": rec=") ; 
		formatObject(z,receiver) ;
		int i=0 ;
		for (Object o : args) {
			z.append(", x" + i + "=") ; formatObject(z,o) ;
		}
		z.append(", RETURN ") ; formatObject(z,retval) ;
		z.append(", THROW ")  ; formatObject(z,exc) ; 
	}
		
	// override this
	void formatConstructorCallEntry(StringBuffer z, CONSTRUCTOR c, Object[] args) {
		z.append("** calling CONSTRUCTOR ") ;
		z.append(c.con.getName() + "(") ;
		int k = 0 ;
		for (Object o : args) {
			if (k>0) z.append(", ") ;
			z.append("x" + k + "=") ;
			formatObject(z,o) ;
			k++ ;
		}
		z.append(")") ;
	}

	// override this
	void formatConstructorCallExit(StringBuffer z, CONSTRUCTOR c, Object[] args, Object retval, Object exc) {
		z.append("** of constructor ") ;
		z.append(c.con.getName() + ": ") ;
		int k = 0 ;
		for (Object o : args) {
			if (k>0) z.append(", ") ;
			z.append("x" + k + "=") ;
			formatObject(z,o) ;
			k++ ;
		}
		z.append(", RETURN ") ; formatObject(z,retval) ;
		z.append(", THROW ")  ; formatObject(z,exc) ;
	}
		
	// override this
	void formatObject(StringBuffer z, Object o) { 
		if (o==null) z.append("null") ;
		else z.append(o.toString()) ; 
	}	
}
