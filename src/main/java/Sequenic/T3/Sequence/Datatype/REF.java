/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T3.Sequence.Datatype;
import java.io.* ;

import Sequenic.T3.Pool;
import Sequenic.T3.Sequence.Datatype.STEP.PreConditionCheckPlaceHolder;

/**
 * This STEP is used to construct an object by by picking it from
 * an {@link Sequenic.T3.Pool object pool}. The Pool-ID of the picked
 * object will be remembered in this step.
 */
public class REF extends STEP implements Serializable {

	private static final long serialVersionUID = 1L;
	
    public int index = -1 ;

    /**
     * @param i The Pool-ID of the object supplied by this step.
     */
    public REF(int i) {
        index = i;
    }
    
    public REF() { }
    
	@Override
	public STEP_RT_info exec(Class CUT, Pool pool, PreConditionCheckPlaceHolder precondPlaceHolder)
			throws Exception {
		
    	STEP_RT_info r = new STEP_RT_info(this,CUT,pool.getObjectUnderTest()) ;
    	if (precondPlaceHolder!=null) precondPlaceHolder.test(null, this, new Object[0]);
    	r.returnedObj = pool.get(index) ;
    	return r ;
	}

    public String toString() { return "REF " + index ; }
    
    @Override
    public boolean isCreationStep() { return false ; } ;
    
    @Override
    public STEP clone_withoutOracles() { return new REF(index) ; }
    

}
