/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T3.Sequence.Datatype;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.ClassInvariantError;
import Sequenic.T3.Violation;
import Sequenic.T3.Pool;

import java.lang.reflect.*;


/**
 * An object of this class is a meta representation of a unit execution that
 * ultimately constitutes a test-sequence. 
 * When executed, it creates a value or an object. To execute, sufficient
 * context must be given.
 * STEPs are serializable, so we can save them to be re-executed later. 
 */
public abstract class STEP {
	
	private static final long serialVersionUID = 1L;
	
	abstract public boolean isCreationStep() ;
	
	/**
	 * Added to facilitate suite query. This is latter used to hold a pre-condition that should be 
	 * checked just before the step is executed. The result is stored in a field so that it can
	 * be accessed later after the step has been executed. Ideally we want to just pass a closure
	 * to do this; but since this class is on the CommonSrc side, we use this place holder class
	 * instead. In its usage, the place holder can be subclassed, with a mechanism to pass on
	 * closure.
	 *
	 */
	public static class PreConditionCheckPlaceHolder {
		public Boolean result = null ;
		public void test(Object targetObj, STEP step, Object[] stepArgs) { }
	}

    /**
     * To execute a STEP.
     */
    abstract public STEP_RT_info exec(Class CUT, Pool pool, PreConditionCheckPlaceHolder precondCheckPlaceHolder)
    		throws 
    		Exception ;

    /**
     * Execute an aray of STEPs. This is typically used to construct
     * the arguments of a method call.
     */
    public static STEP_RT_info[] execMany(Class CUT, Pool pool, STEP[] steps)
        throws
        Exception  {

    	assert steps != null ;
    	STEP_RT_info[] result = new STEP_RT_info[steps.length];
    	for (int i = 0; i < steps.length; i++) {
    		STEP_RT_info r = steps[i].exec(CUT, pool, null);
    		result[i] = r ;
    		if (r.exc != null) break ;
            }
        return result;
    }

    /**
     * check CUT's invariant, if any. Class invs of superclasses are not checked. We assume
     * that it is CUT's own responsibility to invoke them in its own class inv, if
     * they are indeed to be imposed.
     */
    protected void checkClassinv(Class CUT, Pool pool, Object targetObj, STEP_RT_info info) {
        // if the step is already trowing an exception, we will not bother to check the classinv
        if (info.exc != null) return ;

        if (targetObj == null) return ;

        try {
            Method classinv = CUT.getDeclaredMethod(CONSTANTS.classinv_name)  ;
            classinv.setAccessible(true); // force it to be accessible
            try {
                Boolean ok = (Boolean) classinv.invoke(targetObj) ;
                if (!ok) info.exc = new ClassInvariantError("Against invariant formulated in " 
                                                 + classinv.getName()
                                                 + " in " + CUT.getName()) ;
            }
            catch (IllegalAccessException e) { throw e ; }
            catch (IllegalArgumentException e) {
                // this is the case e.g. when the number of supplied parameters are incorrect
                throw e ; }
            catch (InvocationTargetException e) {
                // the invoked method has thrown an exception
                info.exc = e.getCause() ;
                assert info.exc != null ;
                return ;
            }
        }
        catch (Exception e) { }
    }

    /**
     * For clearing out all oracles attached to this step, if any.
     */
    public void clearOracle() { }
    
    /**
     * This creates a clone of this STEP. However if this STEP is a CONST, its val will not be deeply cloned.
     * Instead, the new copy of the CONST will be a pointer to the same val.
     * Furthermore, oracles stored in the step will be cleared.
     */
    abstract public STEP clone_withoutOracles() ;
}
