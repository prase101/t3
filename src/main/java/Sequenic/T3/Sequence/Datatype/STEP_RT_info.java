/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T3.Sequence.Datatype;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.utils.Show;

/**
 * An object of this class is a description used to hold runtime information about 
 * a test step (what the step is, what the actual concrete parameters passed to
 * it) and the result of executing it. By 'result' we mean the object
 * returned by the step, along with information about exception or violation
 * thrown by the step.
 * 
 */
public class STEP_RT_info {

    public Class CUT ;

	/**
	 * The step that produces this result.
	 */
	public STEP step = null;

	/**
	 * The object under the test associated to the step producing this result.
	 */
	public Object objectUnderTest = null;

	/**
	 * Actual parameters passed to the step producing this result.
	 */
	public Object[] args = null;

	/**
	 * Actual receiver object of the step producing this result. Null if such
	 * the step has no receiver object.
	 */
	public Object receiverObj = null;

	public Object returnedObj = null;

	/**
	 * Exception thrown by the step.
	 */
	public Throwable exc = null;
	
	public STEP_RT_info() { } 
	
	public STEP_RT_info(STEP s, Class CUT, Object objectUnderTest) {
		step = s ;
        this.CUT = CUT ;
		this.objectUnderTest = objectUnderTest ;
	}

    private Class getThrowingClass(Throwable t) {
        StackTraceElement[] stack = t.getStackTrace() ;
        if (stack == null || stack.length==0) return null ;
        try {
            String className = stack[0].getClassName() ;
            return Class.forName(className) ;
        }
        catch (Exception e) {
            return null ;
        }
    }
	 
	/**
	 * True if the thrown exception is either an IllegalArgumentException, or
	 * an PRE-assertion violation, thrown by CUT or its superclass.
	 */
	public boolean inputWasIncorrect() {
		if (exc==null) return false ;
		// need to take into account that CUT is loaded with a different class loader, so this
		// does not always work:
		//if (exc instanceof IllegalArgumentException
        //        && getThrowingClass(exc).isAssignableFrom(CUT) ) return true ;
		String excName = exc.getClass().getSimpleName() ;
		//System.out.println(">>> " + excName);
		if (excName.equals("IllegalArgumentException")
            && getThrowingClass(exc).isAssignableFrom(CUT)) return true ;
		if (excName.equals("AssertionError")) {
			String flag = exc.getMessage() ;
			if (flag != null 
					&& flag.startsWith("PRE")
                    && getThrowingClass(exc).isAssignableFrom(CUT)
                    )
                return true ;
		}
		return false ;
	}

	/**
	 * True if the thrown exception is does not belong to the category inputWasIncorrect.
	 */
	public boolean raisesViolation() {
		if (exc==null) return false ;
		return !inputWasIncorrect() ;
	}


    /**
     * For printing the step to some output-stream.
     */
    public void print(int showDepth, OutputStream out) {
        try {
           out.write((step.toString() + "\n").getBytes());
           if (raisesViolation()) out.write("   >> VIOLATION!\n".getBytes());
           if (inputWasIncorrect())  out.write("   >> an assumption is breached...\n".getBytes());
           if (receiverObj != null) {
               out.write(("   -- state of the receiver object:\n"  +  Show.show(receiverObj,3,showDepth)).getBytes()) ;
           }
           if (returnedObj != null) {
                out.write(("   -- returned value:\n"  +  Show.show(returnedObj,3,showDepth)).getBytes()) ;
           }
            if (exc != null) {
                out.write(("   -- thrown exception:\n").getBytes()) ;
                if (out instanceof PrintStream) exc.printStackTrace((PrintStream) out);
                else out.write(exc.toString().getBytes());
           }
           //out.write("\n---------------------\n".getBytes());
           out.flush();
        }
        catch (IOException e) {
            Logger.getLogger(CONSTANTS.T3loggerName).log(Level.WARNING,"fail to print to output-stream: " + e);
        }
    }
    

}
