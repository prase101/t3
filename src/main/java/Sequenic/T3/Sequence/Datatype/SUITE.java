/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */

package Sequenic.T3.Sequence.Datatype;

import java.io.*;
import java.lang.reflect.* ;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import Sequenic.T3.CONSTANTS;
import Sequenic.T3.ExceptionOracleError;
import Sequenic.T3.Info.FieldPairsCoverage;
import Sequenic.T3.Oracle.*;
import Sequenic.T3.Oracle.Oracle.* ;
import Sequenic.T3.Pool;
import Sequenic.T3.Violation;
import Sequenic.T3.OracleError;
import Sequenic.T3.utils.Maybe;
import Sequenic.T3.utils.Pair;


/**
 * A class representing a test-suite, which is a set of test-sequences. This variant
 * only support sequential suite-execution.
 */
public class SUITE implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    public String CUTname ;
    public String timeStamp ;
    public String suitename = null ;
	public List<SEQ> suite = new LinkedList<SEQ>() ;

    public SUITE() {}

    public SUITE(String CUTname) {
        this.CUTname = CUTname ;
    }

    /**
     * Destructive Union of two suites. However, an empty-suite is never side-effected;
     * thus alloweing them to be used as shared zero-element of the operation.
     * T1 cannot be null, T2 can be null.
     */
    static public SUITE union(SUITE T1, SUITE T2) {
    	if (T2 == null || T2.suite.isEmpty()) return T1 ;
    	if (T1.suite.isEmpty()) return T2 ;
        T1.suite.addAll(T2.suite) ;
        return T1 ;
    }
    
    public SUITE plus(SUITE T2) {
        if (T2 != null) suite.addAll(T2.suite) ;
        return this ;
    }
    
    /**
     * Randomly split the suite into k parts, of size N/k.
     */
    public List<SUITE> rsplit(int k) {
    	List<SUITE> z = new LinkedList<SUITE>() ;
    	List<SEQ> seqs = new LinkedList<SEQ>(suite) ;
        Random rnd = new Random() ;
    	int n = suite.size() / k ;
    	if (n*k < suite.size()) n++ ;
    	for (int k_ = 0 ; k_ < k ; k_ ++) {
    		if (seqs.isEmpty()) break ;
    		SUITE S = new SUITE(CUTname) ;
    		S.timeStamp = timeStamp ;
    		z.add(S) ;
    		for (int i=0; i<n; i++) {
    			if (seqs.isEmpty()) break ;
    			int selected = rnd.nextInt(seqs.size()) ;
    			S.suite.add(seqs.get(selected)) ;
    			seqs.remove(selected) ;
    		}
    	}
    	return z ;
    }
    
    public SUITE filter(Predicate<SEQ> p) {
    	SUITE T = new SUITE(CUTname) ;
    	T.timeStamp = timeStamp ;
    	T.suite = suite.stream().filter(p).collect(Collectors.toList()) ;
    	return T ;
    }
    
    public SUITE filter(Method m) {
    	Predicate<STEP> p = step -> {
    		if (step instanceof METHOD) return ((METHOD) step).method.equals(m) ;
    		return false ;
    	} ;
    	return filter(seq -> seq.some(p)) ;
    }
    
    public SUITE filterWithMethodName(String mname) {
    	Predicate<STEP> p = step -> {
    		if (step instanceof METHOD) return ((METHOD) step).method.getName().equals(mname) ;
    		return false ;
    	} ;
    	return filter(seq -> seq.some(p)) ;
    }
    
    public boolean all(Predicate<SEQ> p) {
    	for (SEQ s : suite) {
    		if (! p.test(s)) return false ;
    	}
    	return true ;
    }
    
    public <R> List<R> map(Function<SEQ,R> f) {
    	return suite.stream().map(f).collect(Collectors.toList()) ;
    }
    
    public <R> List<R> stepMap(Function<STEP,R> f) {
    	List<R> result = new LinkedList<R>() ;
    	for (SEQ s : suite) {
    		for (STEP step : s.steps) result.add(f.apply(step)) ;
    	}
    	return result ;
    }
    
    public Set<Constructor> getConstructors() {
    	Set<Constructor> result = new HashSet<Constructor>() ;
    	for (SEQ s : suite) {
    		for (STEP step : s.steps) {
    			if (step instanceof CONSTRUCTOR){
    				result.add(((CONSTRUCTOR) step).con) ;
    			}
    		}
    	}
    	return result ;
    }
    
    public Set<Method> getMethod() {
    	Set<Method> result = new HashSet<Method>() ;
    	for (SEQ s : suite) {
    		for (STEP step : s.steps) {
    			if (step instanceof METHOD){
    				result.add(((METHOD) step).method) ;
    			}
    		}
    	}
    	return result ;
    }
    
    /*
     * Get coverage info.
     * 
    public <C> Coverage<C> getCovered(Function<SEQ,List<C>> fcover) {
    	Coverage<C> info = new Coverage<C>() ;
    	for (SEQ s : suite) info.register(fcover.apply(s));
    	return info ;
    }
    
    public <C> Coverage<C> getStepCovered(Function<STEP,List<C>> fcover) {
    	Coverage<C> info = new Coverage<C>() ;
    	for (SEQ s : suite) {
    		for (STEP step : s.steps) info.register(fcover.apply(step));
    	}
    	return info ;
    }
    */
    
    private int SEQhashcode(SEQ seq) {
    	StringWriter str = new StringWriter() ;
    	for (STEP step : seq.steps) {
    		if (step instanceof INSTRUMENT) continue ;
    		str.append(step.toString()) ;
    	}
    	return str.toString().hashCode() ;
    }
    
    /**
     * This drop all sequences that are "duplicates". For efficiency, duplicates are not
     * literally checked; we check it based on the hash-code of the string representation
     * of the sequences. The string representation of a sequence is the concatenation of
     * the toString of its every step, except INSTRUMENTATION. 
     */
    public void dropDuplicates() {
    	Set seen = new HashSet<Integer>() ;
    	List<SEQ> tobeDeleted = new LinkedList<SEQ>() ;
    	for (SEQ s : suite) {
    		int hash = SEQhashcode(s) ;
    		if (seen.contains(hash)) tobeDeleted.add(s) ;
    		else seen.add(hash) ;
    	}
    	suite.removeAll(tobeDeleted) ;
    }
    
    
    
    private void write(OutputStream out, boolean showExecution, String s) throws IOException {
    	if (showExecution && out!=null)
    	   out.write(s.getBytes())  ;
    }
    
    private void write(OutputStream out, String s) throws IOException {
    	if (out!=null)
    	   out.write(s.getBytes())  ;
    }
                     

    /**
     * Sequentially execute a suite. If runAll is set to true, all sequences in the suite
     * will be run; else the execution will stop at the first step, in the first
     * execution that throws an exception. This exception will be wrapped in a Violation,
     * and then (re-)thrown.
     * 
     * <p>If the regressionMode is set to true, then only thrown OracleError will be counted
     * as a violation. If runAll is set to true, then this error will not be retrhown; else
     * it will be wrapped in a Violation, and re-thrown.
     * 
     * <p> If showExcExecution is set to true, then executions that throw exception (or Oracle
     * Error in the regressionMode) will be shown/reported into the given output stream.
     */
    public SUITE_RT_info exec(Pool pool,
    		         ClassLoader classloader,
    		         FieldPairsCoverage ocCollector,
                     int showLength,
                     int showDepth,
                     boolean showExcExecution,
                     boolean runAll,
                     boolean regressionMode,
                     OutputStream out) throws Exception
    {
        long runtime = System.currentTimeMillis() ;
        Class CUT = classloader.loadClass(CUTname) ;
        
        SUITE_RT_info suiteinfo = new SUITE_RT_info(this,ocCollector) ;

        //int numOfViolatingSequences = 0 ;
        //int numOfAsmBreachingSequences = 0 ;
        //int numOfFailingSequences = 0 ;

        int n = 0 ;
        SEQ_RT_info r = null ;

		for (SEQ sigma : suite) {
			//System.out.println("**>> n = " + n + ", runall = " + runAll) ;
			suiteinfo.executed++ ;
			try {
				
                r = sigma.exec(CUT,pool,ocCollector,0,0,null) ;
                
                if (regressionMode) {
                	if (r.lastInfo.exc != null) {
                		if (r.lastInfo.exc instanceof OracleError) {
                		   showSequence(sigma,CUT,pool,showLength,showDepth,showExcExecution,out,n) ;
                		   write(out,showExcExecution,"\n** The above sequence is VIOLATING an oracle!")  ;
                		   if (suiteinfo.firstViolation == null ) suiteinfo.firstViolation = r.lastInfo.exc ;
                		   suiteinfo.violating.add(sigma) ;
                		   if (r.lastInfo.exc instanceof ExceptionOracleError)
                			   suiteinfo.inconsistentExc.add(sigma) ;
                		}
                		else {
                			Object culprit = r.lastInfo.step ;
                			Oracle o = null ;
                			boolean hasOracle = false ;
                			if (culprit instanceof METHOD) {
                				METHOD m = (METHOD) culprit ; o = m.oracle ;
                			}
                			else if (culprit instanceof CONSTRUCTOR) {
                				CONSTRUCTOR c = (CONSTRUCTOR) culprit ; o = c.oracle ;		
                			}
                			else if (culprit instanceof UPDATE_FIELD) {
                				UPDATE_FIELD f = (UPDATE_FIELD) culprit ; o = f.oracle ;
                			}
                			hasOracle = o != null && ! Oracle.TT.getClass().isInstance(o) ;	
                			if(hasOracle) write(out,showExcExecution,"\n** A sequence throws an exception, but is allowed by its oracle." )  ;
                			else { 
                			  showSequence(sigma,CUT,pool,showLength,showDepth,showExcExecution,out,n) ;
                			  write(out,showExcExecution,"\n** The above sequence throw an exception; the corresponding step sets no oracle though.")  ;
                			  // cant qualify this as violation! :
                			  //if (suiteinfo.firstViolation == null ) suiteinfo.firstViolation = r.lastInfo.exc ;
                   			  //suiteinfo.violating.add(sigma) ;
                			}
                		}
                		
                	}
                }
                else {
                   if (r.lastInfo.inputWasIncorrect())  {
                      showSequence(sigma,CUT,pool,showLength,showDepth,showExcExecution,out,n) ;
                      write(out,showExcExecution,"\n** The above sequence is INVALID!")  ;   
                      suiteinfo.invalid.add(sigma) ;
                   }
                   else if (r.lastInfo.raisesViolation())  {
                      showSequence(sigma,CUT,pool,showLength,showDepth,showExcExecution,out,n) ;
                      write(out,showExcExecution,"\n** The above sequence is VIOLATING!")  ;    
                      if (suiteinfo.firstViolation == null ) suiteinfo.firstViolation = r.lastInfo.exc ;
                      suiteinfo.violating.add(sigma) ;
                      //System.out.println("**>> #violating = " + suiteinfo.violating.size()) ;
                   }
                }
            }
			catch(Exception e) {
                write(out,showExcExecution,"\n** The above sequence FAILs!")  ;
                
                //System.out.println("**>> a sequence has failed: " + e) ;
                //e.printStackTrace(); 
                /*
                int k = 0 ;
                for (STEP step : sigma.steps) {
                   System.out.println("**> " + k + ": " + step) ;
                   k++ ;
                }
                */
                suiteinfo.failing.add(sigma) ;
                r = null ;
            }
            if (!suiteinfo.violating.isEmpty() && !runAll) break ;
            n++  ;
		}
		
		suiteinfo.runtime = System.currentTimeMillis() - runtime ;
        return suiteinfo ;
	}
    
    private SEQ_RT_info showSequence(SEQ sigma,
    		Class CUT,
    		Pool pool,
    		int showLength,
            int showDepth,
            boolean showExcExecution,
            OutputStream out,
            int sigmaNumber) 
    {
    	if (!showExcExecution) return null ;
    	if (out == null) return null ;
    	int N = sigma.steps.size() ;       
    	try {
    	  out.write(("\n**--- [" + CUTname + " seq " + sigmaNumber + "] , length=" + N + " throws an exception.").getBytes());
     	  int showFrom = 0 ;
          if (showLength >= 0) showFrom = sigma.steps.size() - showLength ;
          SEQ_RT_info r = sigma.exec(CUT,pool,null,showFrom,showDepth,out) ;
          return r ;
    	}
    	catch(Exception e){
    		return null ;
    	}
    }


    /**
     * Enhance a sequence with injected oracles. Oracles are only injected along the
     * last steps (suffix), which are either a method call, a constructor call,
     * or a field update, of the specified length.
     * 
     * If the sequence fails, no oracle will be injected.
     *
     * The method returns true if it manages to insert an oracle; else false.
     */
    protected boolean injectOracles(Class CUT, SEQ sigma, int injectionSuffixLength) {
    	if (sigma.steps.isEmpty()) return false ;
    	// check if the sequence can be executed without failing:
    	try { sigma.exec(CUT, new Pool()) ;} catch(Exception e) { return false ; }
    	
    	// collect all potential steps that can be injected with oracles:
    	List<STEP> injectibles = new LinkedList<STEP>() ;
    	for (STEP s : sigma.steps) {
    		if (s instanceof CONSTRUCTOR || s instanceof METHOD)
    			injectibles.add(s) ;
    	}
    	//System.err.println(">>> " + injectibles.size());
    	// drop the prefix that are not to be injected:
    	int ignoredPrefix = injectibles.size() - 1 ; // at least one step to be enhanced
    	if (injectionSuffixLength>0)
    	    ignoredPrefix = injectibles.size() - injectionSuffixLength ;
    	for (int k=0; k<ignoredPrefix; k++) injectibles.remove(0) ;
    	
    	boolean injected = false ; 
        SEQ sigma_ = new SEQ() ;
        for (STEP s : sigma.steps) {
        	sigma_.steps.add(s) ;
        	if (injectibles.contains(s)) {
        		boolean i = injectOracleOnLastStep(CUT,sigma_) ;
        		if (i) injected = true ;
        	}
        }
        
        // check if the oracles are stable enough 
        try { 
        	int checkagain = 2 ; // check twice
        	while (checkagain>0) {
        	   checkagain -- ;
        	   SEQ_RT_info info = sigma.exec(CUT, new Pool()) ;
        	   if (info.isFail() && info.lastInfo.exc instanceof OracleError) {
        		  sigma.clearOracle();
        		  return false ;
        	    }
        	}
        } 
        catch(Exception e) { 
        	sigma.clearOracle() ;
        	return false ; 
        }
        
        /*
        int N = injectibles.size() ;
    	for (int k=N-1; 0<=k ; k--) {
    		STEP tobeInjected = injectibles.get(k) ;
    		SEQ sigma_ = new SEQ() ;
    		for (STEP s : sigma.steps) {
    			sigma_.steps.add(s) ;
    			if (s == tobeInjected) {
    				injected = injected || injectOracleOnLastStep(CUT,sigma_) ;
    				break ;
    			}
    		}
    	}
    	*/
        
        //System.err.println(">>> injecting oracles on seq done");
    	return injected ;
    }
    
    
    private boolean injectOracleOnLastStep(Class CUT, SEQ sigma) {
        try {
            SEQ_RT_info r = sigma.exec(CUT, new Pool()) ;
            
            boolean injected = false ;
            
            // now inject the oracles ;
            STEP laststep = sigma.steps.getLast() ;
            if (laststep instanceof CONSTRUCTOR) {
                //System.out.println(">>> injecting constructor oracle...");
            	CONSTRUCTOR step_ = (CONSTRUCTOR) laststep ;
            	Maybe<Object> returned = Maybe.lift(r.lastInfo.returnedObj) ;
            	// don't lift the thrown exception, wrap explicitly to also represent 
            	// when null/no exception is thrown:
            	Maybe<Throwable> exc = new Maybe(r.lastInfo.exc) ;
            	step_.oracle = new StrDumpEqOracle(null,returned,exc) ;
            	injected = true ;
            }
            else if (laststep instanceof METHOD) {
            	//System.out.println(">>> injecting method oracle...");
            	METHOD step_ = (METHOD) laststep ;
            	
            	Maybe<Object> receiver = Maybe.lift(r.lastInfo.receiverObj) ;
            	// don't lift the returned object, wrap explicitly to also represent 
            	// when a null is returned
            	Maybe<Object> returned = new Maybe(r.lastInfo.returnedObj) ;
        		Maybe<Throwable> exc   = new Maybe(r.lastInfo.exc) ;
        		if (step_.method.getName().equals("hashCode")) {
        			// don't put oracle on the return-val of hashCode!
        			step_.oracle = new StrDumpEqOracle(receiver,null,exc) ;
        		}
        		else step_.oracle = new StrDumpEqOracle(receiver,returned,exc) ;
            	injected = true ;
            }
            /*
            else if (laststep instanceof UPDATE_FIELD) {
                //System.out.println(">>> injecting field oracle...");
            	UPDATE_FIELD step_ = (UPDATE_FIELD) laststep ;
            	Maybe<Object> receiver = Maybe.lift(r.lastInfo.objectUnderTest) ;
            	Maybe<Throwable> exc = new Maybe(r.lastInfo.exc) ;
        		if (receiver!=null) {
            		step_.oracle = new StrDumpEqOracle(receiver,null,exc) ;
            		injected = true ;
            	}
            }
            */
            
            if (!injected) return false ;
            
            // now, re-execute the enhanced sequence to check that it does not fail or
            // violates the newly added oracles (e.g. because of non-determinism) ; 
            // if it fails or violates the oracles, clear up the oracles.
            try {
                r = sigma.exec(CUT, new Pool()) ;
                if (r.isFail() && r.lastInfo.exc instanceof OracleError) {
                    sigma.steps.getLast().clearOracle() ;
                    //System.out.println(">>> clearing oracle on step " + sigma.steps.getLast()) ;
                    return false ;
                }
                else return true ;
            }
            catch(Exception e) {
                sigma.steps.getLast().clearOracle();
                //System.out.println(">>> clearing oracle on step " + sigma.steps.getLast()) ;
                return false ;
            }

        }
        catch(Exception e) { return false ; }
    }

    /**
     * Enhance the non-failing sequences in the suite with injected oracles.
     */
	public void injectOracles(Class CUT, boolean isSingleCore, int injectionSuffixLength) {
        int k = 0 ;
        for (SEQ seq : suite) {
            boolean injected = injectOracles(CUT,seq,injectionSuffixLength) ;
            if (injected) k++ ;
        }
        Logger.getLogger(CONSTANTS.T3loggerName).info("** Injected oracles on " + k + " sequences (out of " 
        		+ suite.size() + ") of a suite of " + CUTname + " ...") ;
    }

    /**
     * To split a suite into up to N smaller suites, who size is at least minsize.
     * The timestamps are left unset.
     *
     */
    public List<SUITE> split(int N, int minsize) {
       int k = Math.max(minsize, suite.size() / N) ;
       //System.out.println(">>> splittng, k = " + k) ;
       List<SUITE> subsuites = new LinkedList<SUITE>() ;
       SUITE S = new SUITE(CUTname) ;
       subsuites.add(S) ;
       int k_ = 0 ;
       int left = suite.size() ;
       for (SEQ sigma : suite) {
           S.suite.add(sigma) ;
           left-- ;
           if (k_ < k) k_ ++ ;
           else {       
        	  //System.out.println("*** about to split a suite, left = " + left) ;
              k_ = 0 ;
              if (left>0) {
            	//System.out.println(">>> splitting a suite, split " + k_) ;
                S = new SUITE(CUTname) ;
                subsuites.add(S) ;
              }
          }
       }
       // System.out.println(">>> suites size: " + subsuites.size()) ;
       return subsuites ;
    }

    /**
     * Save the suite in the given directory. The suite is saved in a file named:
     * name__timestamp.tr. If suitename is not null, that is used as the name;
     * else we use the CUTname. 
     * Return the full path to the trace file.
     */
    public String save(String dir) throws Exception {
    	return save(dir,true) ;
    }
    
    /**
     * Save this suite in the given directory, with the given file name. The boolean flag
     * specifies whether a time-stamp will be added to the name.
     * Return the full path to the trace file.
     */
    public String save(String dir, String name, boolean withTimeStamp) throws Exception {
         SUITE T = new SUITE() ;
         T.CUTname = CUTname ;
         if (name.endsWith(".tr")) name = name.substring(0,name.length() - 3) ;
         T.suitename = name ;
         T.timeStamp = this.timeStamp ;
         T.suite = this.suite ;
         return T.save(dir,withTimeStamp) ;
    }
    
    /**
     * Save this suite into the given directory. The tracefile will be named as this suite name.
     * The name is extended with a time-stamp depending on the time-stamp flag. 
     * If the suite has no name, then the suite's CUT's name is used.
     * The method returns the full path to the trace-file.
     */
    public String save(String dir, boolean withTimeStamp) throws Exception {
        String filename = "" ;
        if (suitename != null) filename += suitename ; else filename += CUTname ;
        if (withTimeStamp) filename += "__" + timeStamp + ".tr" ;
        else filename += ".tr" ;
        Path fp = FileSystems.getDefault().getPath(dir,filename) ;
        File f  = fp.toFile()  ;
        OutputStream fout = new FileOutputStream(f);
        OutputStream buffer = new BufferedOutputStream(fout);
        ObjectOutputStream oos = new ObjectOutputStream(buffer);
        try { 
        	oos.writeObject(this) ;
        	oos.flush();
        	Logger.getLogger(CONSTANTS.T3loggerName).info("** Saving a test-suite to " + fp + ", " + f.length()/1000 + " KB") ;
        	return fp.toString() ;
        }
        catch (Exception e) {
        	Logger.getLogger(CONSTANTS.T3loggerName).warning("** Failed to save a test suite to " + filename);
        	throw e ;
        }
        finally {
            oos.close() ;
        }
    }

    /**
     * Load a test suite from a trace file. The parameter should be a full path to this file.
     */
    public static SUITE load(String filename) throws Exception {
    	if (!filename.endsWith(".tr")) filename += ".tr" ;
        File f = new File(filename) ;
        InputStream file = new FileInputStream(filename);
        InputStream buffer = new BufferedInputStream(file);
        ObjectInputStream ois = new ObjectInputStream ( buffer );
        try {
            SUITE S = (SUITE) ois.readObject() ;
            return S ;
        }
        finally {
            ois.close();
            Logger.getLogger(CONSTANTS.T3loggerName).info("** Suite " + filename + " is loaded, " + f.length()/1000 + " KB") ;
        }
    }
    
    /**
     * Generate a JUnit test file, that will load the given trace-file and replay it. The file
     * will be put in the given directory, with the given test-class name.
     * Some parameters for the replay-mode can be specified here.
     * If a sequence throws a violation, this will be printed on System.err.
     */
    public static void mkJunitTestFile(
    		String dir, 
    		String testclassName, 
    		String traceFile,
    		int showLength,
            int showDepth,
            boolean showExcExecution,
            boolean regressionMode 		
    		) throws Exception {
    	File f = new File(dir + File.separator + testclassName + ".java") ;
    	PrintWriter pw = new PrintWriter(f);
    	pw.write("import org.junit.* ;\n") ;
    	pw.write("import static org.junit.Assert.* ;\n") ;
    	pw.write("import java.util.logging.Logger;\n") ;
    	pw.write("import Sequenic.T3.* ;\n") ;
    	pw.write("import Sequenic.T3.Sequence.Datatype.* ;\n") ;
    	pw.write("public class " + testclassName + " {\n\n") ;
    	pw.write("   public " + testclassName + "() { }\n\n") ;
    	pw.write("   static SUITE S = null ;\n") ;
    	pw.write("   static SUITE_RT_info info = null ;\n") ;
    	pw.write("   static Logger log ;\n\n") ;
    	// loading and executing the trace file; only need to do this 1x for all test cases in the class:
    	pw.write("   @BeforeClass\n") ;
    	pw.write("   public static void loadtrace() throws Exception {\n") ;
    	pw.write("      log = Logger.getLogger(CONSTANTS.T3loggerName) ;\n");
    	pw.write("      S = SUITE.load(\"" + traceFile + "\") ;\n");
    	pw.write("      Pool pool = new Pool() ;\n") ;
    	pw.write("      Thread mainthread = Thread.currentThread() ;\n") ;
    	pw.write("      Thread suiterunner = new Thread() {\n") ;
    	pw.write("         public void run() {\n") ;
    	pw.write("            try { info = S.exec(pool," + testclassName + ".class.getClassLoader(),"
    			                                   + "null,"
						    			           + showLength + ","
						    			           + showDepth + ","
						    			           + showExcExecution + ","
						    			           + "true ,"  // runAll is set to true
						    			           + regressionMode + ","
						    			           + "null) ;\n"   // suppress violation reporting, else use System.err 
    			 ) ;
    	pw.write("                  mainthread.interrupt(); }\n") ;
    	pw.write("            catch (Exception e) {  } }\n") ;
    	pw.write("      } ;\n") ;
    	pw.write("      suiterunner.start() ;\n") ;
    	// wait 10s -- hard wired here:
    	pw.write("      try { mainthread.sleep(10000); log.warning(\"** JUnit's test suite execution has TIMED OUT.\"); }\n") ;
    	pw.write("      catch (Exception e) { log.warning(\"Test suite execution is done.\"); }") ;
    	pw.write("   }\n\n") ;
    	// test-1 
    	pw.write("   @Test\n") ;
    	pw.write("   public void test1() throws Exception {\n") ;
    	pw.write("      if (info==null) { log.info(\"NO VERDICT.\"); return ; } \n");
    	pw.write("      boolean verdict = !info.seenCrash() ;\n");
    	pw.write("      if (verdict) log.info(\"PASS\") ; else log.info(\"FAIL xxxxxxxxxxxx \") ;\n") ;
    	pw.write("      assertTrue(verdict) ;\n");
    	pw.write("   }\n\n") ;
    	// test-2 
    	pw.write("   @Test\n") ;
    	pw.write("   public void testExceptionTypeOnly() throws Exception {\n") ;
    	pw.write("      if (info==null) { log.info(\"NO VERDICT.\"); return ; } \n");
    	pw.write("      boolean verdict = info.inconsistentExc.isEmpty() ;\n");
    	pw.write("      if (verdict) log.info(\"PASS\") ; else log.info(\"FAIL xxxxxxxxxxxx \") ;\n") ;
    	pw.write("      assertTrue(verdict) ;\n");
    	pw.write("   }\n\n") ;
    	// test-3
    	pw.write("   @Test\n") ;
    	pw.write("   public void testAllOracles() throws Exception {\n") ;
    	pw.write("      if (info==null) { log.info(\"NO VERDICT.\"); return ; } \n");
    	pw.write("      boolean verdict = info.violating.isEmpty() ;\n");
    	pw.write("      if (verdict) log.info(\"PASS\") ; else log.info(\"FAIL xxxxxxxxxxxx \") ;\n") ;
    	pw.write("      assertTrue(verdict) ;\n");
    	pw.write("   }\n") ;
    	pw.write("}") ;
    	pw.flush(); pw.close();
    }

    /**
     * Load all suites from the given target directory, that match the given
     * regular expression.
     */
    public static List<SUITE> loadMany(String regexpr, String dir) throws Exception {
    	
    	Pattern pattern = Pattern.compile(regexpr);
    	
        List<SUITE> SS = new LinkedList<SUITE>() ;
        Path dir_ = FileSystems.getDefault().getPath(dir) ;
        if (!Files.isDirectory(dir_)) return SS ;
        DirectoryStream<Path> stream = Files.newDirectoryStream(dir_) ;
        for (Path entry: stream) {
            String entry_ =  entry.getFileName().toString() ;
            if (Files.isDirectory(entry)) continue ;
            if (entry_.startsWith(".")) continue ;
            Matcher m = pattern.matcher(entry_) ;
            if (! m.matches()) continue ;
            if (entry_.endsWith(".tr"))     {
                try {
                  SUITE T = load(entry.toString()) ;
                  SS.add(T) ;
                }
                catch (Exception e) {
                    Logger.getLogger(CONSTANTS.T3loggerName).warning("xx Failing to load a trace-file: "
                            + entry.toString()
                            + ", exception thrown: " + e.toString());
                    e.printStackTrace();
                }
            }
        }
        return SS ;
    }
    

    
    public String showSuiteStatistics() {
        String s = "" ;
        int N = suite.size() ;
        s +=   "** Suite name        : " + suitename ;
        s += "\n** CUT               : " + CUTname ;
        s += "\n** Suite size        : " + N ;

        if (N==0) return s ;
        int len = 0 ;
        for (SEQ sigma : suite) {
            len += sigma.length()  ;
        }
        float avrg = (float) len / (float) N ;
        s += "\n** Avrg. seq. length : " + avrg ;
        
        return s ;
    }
    
    /**
     * Create a clone of this suite. However any CONST-step in the suite will not be
     * completely cloned; namely, its val will not be cloned. Instead, the new copy of the CONST 
     * will be a pointer to the same val.
     * Furthermore, oracles stored in the steps will be cleared.
     */
    public SUITE clone_withoutOracles() {
    	SUITE S = new SUITE() ;
    	S.CUTname = CUTname ;
    	S.suitename = suitename ;
    	for (SEQ seq : suite) S.suite.add(seq.clone_withoutOracles()) ;
    	return S ;
    }


    public static void main(String[] args) throws Exception{
        List<SUITE> suites = loadMany("", "d:/tmp")  ;
        int i = 1 ;
        for (SUITE S : suites)    {
            System.out.println("\n ************ suite " + i);
            S.exec(new Pool(),ClassLoader.getSystemClassLoader(),null,3,3,true,false,true,System.out) ;
            i++ ;
        }


    }

}
