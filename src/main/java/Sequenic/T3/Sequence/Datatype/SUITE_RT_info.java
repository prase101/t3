/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Sequence.Datatype;

import java.io.OutputStream;
import java.util.*;

import Sequenic.T3.Info.FieldPairsCoverage;

public class SUITE_RT_info {
	
	public SUITE suite ;
	public int executed = 0 ;
	
	/**
	 * Executed sequences that throws non-assumptive exceptions.
	 */
	public List<SEQ> violating = new LinkedList<SEQ>() ;
	
	public List<SEQ> inconsistentExc = new LinkedList<SEQ>() ;
	
	/**
	 * Executed sequences that throws assumptive exceptions.
	 */
	public List<SEQ> invalid = new LinkedList<SEQ>() ;
	
	/**
	 * Executed sequences that for some reason fail to complete.
	 */
	public List<SEQ> failing = new LinkedList<SEQ>() ;
	
	/**
	 * Will contain the first violation found.
	 */
	public Throwable firstViolation = null ;
	
    public long runtime = 0 ;
    
    public FieldPairsCoverage objectCoverageCollector ;
    
    public SUITE_RT_info(SUITE suite, FieldPairsCoverage ocCollector) {
    	this.suite = suite ;
    	objectCoverageCollector = ocCollector ;
    }
    
    public void printReport() throws Exception {
    	printReport(System.out) ;
    }
    
    public void printReport(OutputStream out) throws Exception {
    	if (out==null) return ;

        writeln(out,suite.showSuiteStatistics()) ;
        
        if (suite.suite.isEmpty()) return ;
        writeln(out,"** Executed   : " + executed) ;
        writeln(out,"** Violating  : " + violating.size()) ;
        writeln(out,"** Invalid    : " + invalid.size()) ;
        writeln(out,"** Failing    : " + failing.size()) ;
        writeln(out,"** Runtime    : " + runtime + " ms") ;
        if (objectCoverageCollector!=null) objectCoverageCollector.printReport(out) ;
    }
    
    private void write(OutputStream out,String s) throws Exception {
    	out.write(s.getBytes());
    }
    
    private void writeln(OutputStream out,String s) throws Exception {
    	write(out,s + "\n") ;
    }
    
    public boolean seenCrash() {
    	// TODO
    	return suite == null ;
    }

    
}
