/*
 * Copyright 2015 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.Sequence.Datatype ;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Pool ;
import Sequenic.T3.Sequence.Datatype.* ;

public class Xutils {
	
	private static boolean isCreationStep(STEP step) {
		if (step instanceof CONSTRUCTOR) {
			return ((CONSTRUCTOR) step).isObjectUnderTestCreationStep ;
		}
		if (step instanceof METHOD) {
			return ((METHOD) step).isObjectUnderTestCreationStep ;
		}
		return false ;
	}
		
	
	// remove INSTRUMENT
	public static LinkedList<STEP> removeInstrumentation(List<STEP> seq) {
		LinkedList<STEP> seq2 = new LinkedList<STEP>() ;
		for (STEP s : seq) {
			if (! (s instanceof INSTRUMENT)) seq2.add(s) ;
		}
		return seq2 ;
	}
	
	public static SEQ removeInstrumentation(SEQ seq) {
		SEQ seq2 = new SEQ() ;
		seq2.steps = removeInstrumentation(seq.steps) ;
		return seq2 ;
	}
	
	static public SUITE removeInstrumentation(SUITE suite) {
		SUITE S = new SUITE(suite.CUTname) ;
		for (SEQ seq : suite.suite) S.suite.add(Xutils.removeInstrumentation(seq)) ;
		return S ;	
	}
	
	
	/**
	 * Execute the k-th STEP. Returns the resulting execution-info if it succeeds,
	 * else null.
	 */
	public static STEP_RT_info executeSTEP(Class CUT, 
		Pool pool, 
		STEP.PreConditionCheckPlaceHolder precondHolder, 
		STEP step,
		int k) 
	{
		try {
			STEP_RT_info info = step.exec(CUT,pool,precondHolder) ;
			if (k==0 && isCreationStep(step) && info.returnedObj != null) {
				pool.markAsObjectUnderTest(info.returnedObj) ;
				info.objectUnderTest = info.returnedObj ;
			}
			return info ;
		}
		catch(Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			
			Logger.getLogger(CONSTANTS.T3loggerName).warning("*** A test step failed to execute." + step
					+ ", throwing " + sw.toString()
					) ;
			return null ;
		}
	}

	/**
	 * Execute a sequence up to (and including) the k-th step. We need the pool to
	 * be passed on, so that we can keep the side effects in it.
	 */
	public static STEP_RT_info executeSeqUpTo(Class CUT, 
			Pool pool, 
			STEP.PreConditionCheckPlaceHolder precondHolder, 
			SEQ seq,
			int k) 
	{
		int i = 0 ;
		pool.reset();
		STEP_RT_info info = null ;
		for (STEP step : seq.steps) {
			info = executeSTEP(CUT,pool,precondHolder,step,i) ;
			if (info == null) {
				System.err.println(">>> execute STEP gives null! " + i + " " + step) ;
				return null ;
			}
			if (i>=k) return info ;
			i++ ;
		}
		// the sequence is shorter than k ...
		return info ;
	}
	
	/**
	 * Excute the whole test sequence.
	 */
	public static STEP_RT_info executeSeq(Class CUT, 
			STEP.PreConditionCheckPlaceHolder precondHolder, 
			SEQ seq) 
	{
		return executeSeqUpTo(CUT,new Pool(), precondHolder, seq, seq.steps.size()-1) ;
	}
	
	public static String getSTEPName(STEP step) {
		if (step instanceof METHOD) {
			METHOD m = (METHOD) step ;
			return m.method.getName() ;
		}
		if (step instanceof CONSTRUCTOR) {
			CONSTRUCTOR c = (CONSTRUCTOR) step ;
			return c.con.getDeclaringClass().getSimpleName() ;
		}
		if (step instanceof UPDATE_FIELD) {
			UPDATE_FIELD fu = (UPDATE_FIELD) step ;
			return fu.field.getName() ;
		}
		return null ;
	}
	
	public static STEP[] getSTEPParams(STEP step) {
		if (step instanceof METHOD) {
			METHOD m = (METHOD) step ;
			return m.params ;
		}
		if (step instanceof CONSTRUCTOR) {
			CONSTRUCTOR c = (CONSTRUCTOR) step ;
			return c.params ;
		}
		if (step instanceof UPDATE_FIELD) {
			UPDATE_FIELD fu = (UPDATE_FIELD) step ;
			STEP[] params = { fu.val } ;
			return params ;
		}
		return null ;
	}
	
	private static STEP migrateTestStep(Class CUT0, Class CUT1, STEP step) throws Exception {
		if (step instanceof CONST) {
			CONST c = (CONST) step ;
			CONST newc = new CONST() ;
			newc.cty = c.cty ;
			newc.val = c.val ;
			return newc ;
		}
		if (step instanceof REF) {
			REF ref = (REF) step ;
			return new REF(ref.index) ;
		}
		if (step instanceof INSTRUMENT) return new INSTRUMENT() ;
		if (step instanceof UPDATE_FIELD) {
			UPDATE_FIELD f = (UPDATE_FIELD) step  ;
			UPDATE_FIELD newf = new UPDATE_FIELD() ;
			newf.field = f.field ;
			if (f.field.getDeclaringClass().equals(CUT0)) {
				Field fz = null ;
				try { fz = CUT1.getDeclaredField(f.getName()) ; }
				catch(Exception e) { }
				if (fz != null) newf.field = fz ;
			}
			newf.oracle = f.oracle ;
			newf.val = migrateTestStep(CUT0,CUT1,f.val) ;
			return newf ;
		}
		if (step instanceof CONSTRUCT_COLLECTION) {
			CONSTRUCT_COLLECTION col = (CONSTRUCT_COLLECTION) step ;
			CONSTRUCT_COLLECTION newcol = new CONSTRUCT_COLLECTION() ;
			newcol.cty = col.cty ;
			newcol.elements = new STEP[col.elements.length] ;
			for (int k=0; k<newcol.elements.length; k++) newcol.elements[k] = migrateTestStep(CUT0,CUT1,col.elements[k]) ;
			if (col.keys == null) newcol.keys = null ;
			else {
				newcol.keys = new STEP[col.keys.length] ;
				for (int k=0; k<newcol.keys.length; k++) newcol.keys[k] = migrateTestStep(CUT0,CUT1,col.keys[k]) ;
			}
			return newcol ;
		}
		if (step instanceof METHOD) {
			METHOD m = (METHOD) step ;
			METHOD newm = new METHOD() ;
		    
			newm.isObjectUnderTestCreationStep = m.isObjectUnderTestCreationStep ;
			newm.oracle = m.oracle ;
			    
			newm.method = m.method ;
			if (m.method.getDeclaringClass().equals(CUT0)) {
				Method mz = null ;
				try { mz = CUT1.getDeclaredMethod(m.method.getName(), m.method.getParameterTypes()) ; }
				catch(Exception e) { }	
			    if (mz != null) newm.method = mz ;
			}
			
		    newm.target = migrateTestStep(CUT0, CUT1, m.target) ;
		    newm.params = new STEP[m.params.length] ;
		    for (int k=0; k<newm.params.length; k++) newm.params[k] = migrateTestStep(CUT0, CUT1, m.params[k]) ;
			return newm ;
		}
		if (step instanceof CONSTRUCTOR) {
			CONSTRUCTOR co = (CONSTRUCTOR) step ;
			CONSTRUCTOR newco = new CONSTRUCTOR() ;
			
			newco.con = co.con ;
			if (co.con.getDeclaringClass().equals(CUT0)) {
				Constructor cz = null ;
				try { cz = CUT1.getDeclaredConstructor(co.con.getParameterTypes()) ; }
				catch(Exception e) { }
				if (cz != null) newco.con = cz ;
			}

			newco.cty = co.cty ;
			newco.oracle = co.oracle ;
			newco.isObjectUnderTestCreationStep = co.isObjectUnderTestCreationStep ;
			newco.params = new STEP[co.params.length] ;
			for (int k=0; k<newco.params.length; k++) newco.params[k] = migrateTestStep(CUT0, CUT1, co.params[k]) ;
			return step ;
		}
	   // else
	   return null ;
	}
	
	/**
	 * Clone the test sequence, such all steps (constructor call, method call, or field update) that
	 * are declared in CUT0 new become steps based on CUT1. CUT0 and CUT1 must share the same
	 * ancestors. Moreover, CUT1 should be either a sibling of CUT0 with the same signature,
	 * or a subclass of CUT0.
	 */
	static public SEQ migrateTestSeq(Class CUT0, Class CUT1, SEQ seq) throws Exception {
		SEQ z = new SEQ() ;
		for (STEP step : seq.steps) {
			STEP newstep = migrateTestStep(CUT0,CUT1,step) ;
			if (newstep == null) return null ;
			z.steps.add(newstep) ;
		}
		return z ;
	}
	
	/**
	 * Clone the test suite, such all steps (constructor call, method call, or field update) that
	 * are declared in CUT0 new become steps based on CUT1. CUT0 and CUT1 must share the same
	 * ancestors. Moreover, CUT1 should be either a sibling of CUT0 with the same signature,
	 * or a subclass of CUT0.
	 */
	static public SUITE migrateTestSuite(Class CUT0, Class CUT1, SUITE S) throws Exception {
		SUITE Z = new SUITE() ;
		Z.CUTname = S.CUTname ;
		Z.suitename = S.suitename ;
		for (SEQ seq : S.suite) {
			SEQ newseq = migrateTestSeq(CUT0,CUT1,seq) ;
			if (newseq==null) return null ;
			Z.suite.add(newseq) ;
		}
		return Z ;
	}
}
