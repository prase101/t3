package Sequenic.T3;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.jacoco.core.instr.Instrumenter;

import Sequenic.T3.Info.JacocoInstrumenter;

/**
 * A simple class loader to load classes from a given root-dir, which may fall
 * outside JVM's classpaths.
 */
public class SimpleClassLoader extends ClassLoader {

	
	private String binroot ;
	private Map<String,Class<?>> alreadyLoaded = new HashMap<String,Class<?>>() ;
	
	public SimpleClassLoader(String binroot) { 
		super() ; 
		this.binroot = binroot ;
	}
	
	@Override
	protected Class<?> loadClass(final String name, boolean resolve) throws ClassNotFoundException {
		// CUTname should not be null
		Class<?> C = null ; 
		// we will have non-standard loading sequence
		// (1) first check if C is cached by this loader
		C = alreadyLoaded.get(name);
		if (C!= null) { return C ; }
		// (2) else, load it from binroot
		byte[] code ;
		try {
			code = JacocoInstrumenter.getClassfileContent(binroot,name) ;
			C = defineClass(name, code, 0, code.length) ;
			alreadyLoaded.put(name,C) ;
			if (resolve) resolveClass(C) ;
			return C ;
		}
		// (3) if that fails, load C with the default class loader:
		catch(Exception e) { 
		    return super.loadClass(name,resolve) ;
		}		   
	}
	
}
