/*
 * Copyright 2014 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

import Sequenic.T3.Generator.*;
import static Sequenic.T3.Generator.GenCombinators.* ;
import Sequenic.T3.Generator.SeqAndSuite.*;
import Sequenic.T3.Generator.Step.*;
import Sequenic.T3.Generator.Value.*;
import Sequenic.T3.Sequence.Datatype.* ;
import Sequenic.T3.utils.*;

import java.io.OutputStream;
import java.util.function.Function;
import java.util.logging.*;
import java.util.* ;

import Sequenic.T3.Examples.Friends.Person;

/**
 * This class provides top-level APIs to test a class. It uses T3's default suite
 * generator.
 */
public class T3SuiteGenAPI extends SuiteAPI2 {

    protected Function<Pool,Generator<PARAM,STEP>> valueMetaGenerator ;
    
    public T3SuiteG T3defaultSuiteGenerator ;
   
    public T3SuiteGenAPI() {} 
    
    public T3SuiteGenAPI(Config config) {
    	super(config) ;
    	config.print() ; 
    	configure(null) ;
    } 
    
    public T3SuiteGenAPI(
    		Config config, 
    		Function<ImplementationMap,Function<Pool,Generator<PARAM,STEP>>> fValueMetaGenerator,
    		Class...classesTobeScanned)
    {	
    	super(config) ;
    	config.print() ; 
    	configure(fValueMetaGenerator,classesTobeScanned) ;
    }
    
    private void configure(
    		Function<ImplementationMap,Function<Pool,Generator<PARAM,STEP>>> fValueMetaGenerator,
    		Class...classesTobeScanned) 
    {
    	// setting up implementation-map and testing scope:
    	impMap = new ImplementationMap(config.dirsToClasses,classesTobeScanned) ;
    	scope = new TestingScope(impMap,config.CUT) ;
    	scope.testingFromTheSamePackagePespective = config.assumeClientInTheSamePackage ;
    	scope.includePrivateAndDefaultMembers = config.includePrivateAndDefaultMembers ; 
    	
    	config.reportWriteln("-----");
    	config.reportWriteln("** Scanned : " + impMap.knownClasses.size() + " classes, "
    			+ impMap.numberOfInstantiable()
    			+ " are instantiable."
    			) ;
    	
    	// setting up generators:
    	if (fValueMetaGenerator == null) {
    		// use T3's default value generator:
    		getT3logger().info("Using T3's default value generator.");
    		ValueMG valueMG = new ValueMG(config.maxLevelOfObjectsNesting,config.maxCollectionSize,impMap) ;
    		this.valueMetaGenerator = pool -> valueMG.gen1closed(pool) ;
    	}
    	else {
    		getT3logger().info("Using a custom value generator.");
    		this.valueMetaGenerator = fValueMetaGenerator.apply(impMap) ;
    	}
    	T3defaultSuiteGenerator = new T3SuiteG(scope,this.valueMetaGenerator) ;
    	T3defaultSuiteGenerator.maxPrefixLength = config.maxPrefixLength ;
    	T3defaultSuiteGenerator.maxSuffixLength = config.maxSuffixLength ;
    	T3defaultSuiteGenerator.maxNumberOfStepRetry = config.maxNumberOfStepRetry ;
    	T3defaultSuiteGenerator.maxNumberOfSeqRetry = config.maxNumberOfSeqRetry ;
    	T3defaultSuiteGenerator.maximizePrefix = config.maximizePrefix ;
    	T3defaultSuiteGenerator.fieldUpdateProbability = config.fieldUpdateProbability ;
    	T3defaultSuiteGenerator.multiplier = config.suiteSizeMultiplierPerGoal ;
    	T3defaultSuiteGenerator.maxNumberOfPairs = config.maxNumberOfPairs ;
    	if (config.surpressPairwiseTesting) T3defaultSuiteGenerator.maxNumberOfPairs = 0 ;
    	T3defaultSuiteGenerator.includeInheritedMembers = config.includeInheritedMembers ;
    	 			
    	flushReportStream() ;
    }

    /**
     * A simplified constructor, where we can pass a custom-generator that does not use
     * the implementation map nor the pool. When the custom generator fails, we will make
     * T3 to fall back to its own default generator.
     */
    public T3SuiteGenAPI(
    		Generator<PARAM,STEP> valueMetaGenerator,
    		Config config, 
    		Class...classesTobeScanned)
    {
    	this(config) ;
    	if (valueMetaGenerator == null) configure(null,classesTobeScanned) ;
    	else {
    	   Function<ImplementationMap,Function<Pool,Generator<PARAM,STEP>>> valMetaGen 
    	      = 
    	      impmap -> {
        	     ValueMG defaultValMetaGen = new ValueMG(config.maxLevelOfObjectsNesting, 
        		                                       config.maxCollectionSize, 
        			                                   impmap) ;  
        		 return pool -> FUN.recbind((FUN<Generator<PARAM,STEP>> g) -> FirstOf(valueMetaGenerator, defaultValMetaGen.gen1open(pool, g))) ;
        	   } ;
           configure(valMetaGen,classesTobeScanned) ;
    	}
    }
    
    /**
     * Factory method for compatibility with older code. 
     * Has the same functionality of the constructor of the same signature.
     */
    static public T3SuiteGenAPI mkT3SuiteGenAPI(
    		Generator<PARAM,STEP> valueMetaGenerator,
    		Config config, 
    		Class...classesTobeScanned)
    {
       return new T3SuiteGenAPI(valueMetaGenerator,config,classesTobeScanned) ;
    }
    
    
    
    /**
    * The worker of test; to produce a single suite.
    */
    public SUITE suite(boolean adt) {
    	
    	Class CUT = scope.CUT ;
    	SUITE S ;
        if (config.regressionMode) {  
        	config.reportWriteln("-----") ;
        	if (adt) {
        		S = T3defaultSuiteGenerator.adt_grey(config.numberOfCores) ;
        		config.reportWriteln("** Performed ADT testing") ;
        	}
        	else {
        		S = T3defaultSuiteGenerator.nonadt_grey(config.numberOfCores) ;
        		config.reportWriteln("** Performed non-ADT testing") ;
        	}
        	config.reportWriteln(scope.toString()) ;     	
        	if (config.injectOracles) {
        		getT3logger().info("** Injecting oracles"); 
        		injectOracles(S) ;
        	}
        	else getT3logger().info("** Skipping oracles-injection"); 
        }
        else {
        	config.reportWriteln("-----") ;
        	if (adt) {
        		S =  T3defaultSuiteGenerator.adt_no_asmviol(config.numberOfCores) ;
        		config.reportWriteln("** Performed ADT testing") ;
        	}
        	else {
        		S =  T3defaultSuiteGenerator.nonadt_no_asmviol(config.numberOfCores) ;
        		config.reportWriteln("** Performed non-ADT testing") ;
        	}
        	
        	config.reportWriteln(scope.toString()) ;
        	if (config.dropInvalidTraces) dropAsmViolatingSequences(S) ;
        	if (config.keepOnlyRedTraces) dropNonViolatingSequences(S) ;
        }
        dropBrokenSequences(S) ;
        
        if (config.dropDuplicates) S.dropDuplicates(); 
        
        if (adt) S.suitename = "ADT_" + S.CUTname ;
        else S.suitename     = "nonADT_" + S.CUTname ;
        S.timeStamp = "" + System.currentTimeMillis()  ;

        config.reportWriteln("-----") ;
        config.reportWriteln("** Suite generated.") ;
        config.reportWriteln(S.showSuiteStatistics()) ;
        flushReportStream() ;
        return S ;
    }
    
    public SUITE ADT()    { return suite(true) ; }
    public SUITE nonADT() { return suite(false) ; }

    
    /**
     * Generate a test suite for the CUT, and split into sub-suites.
     */
    private List<SUITE> suites(boolean adt) {
        List<SUITE> suites = new LinkedList<SUITE>() ;
        SUITE S = suite(adt) ;
        if (config.splitSuite<=1) suites.add(S) ;
        else  {
        	config.reportWriteln("** Spliting the Suite into (at most) " + config.splitSuite + " subsuites.") ;
            // minimum size for each subsuite is set to 10
            suites = S.split(config.splitSuite,10) ;
            int k = 0 ;
            for (SUITE T : suites) {
                T.timeStamp = S.timeStamp ;
                T.suitename = S.suitename + "_S" + k  ;
                k++ ;
            }
        }
        return suites ;
     }


    /**
     * Generate test suites for the CUT, and saving the resulting suites.
     */
    public List<SUITE> suites(boolean adt, String dir) throws Exception {
    	long t0 = System.currentTimeMillis();
    	List<SUITE> SS = suites(adt) ;
    	if (dir!=null) {
    	   for (SUITE S : SS) {
        	  S.save(dir);
    	   }
    	}
        flushReportStream() ;
        config.reportWriteln("** Runtime = " + (System.currentTimeMillis() - t0)) ;
        return SS ;
    }
    
    

    /**
     * Just for testing.
     */
     public static void main(String[] args) throws Exception {
    	 test1() ;
     }
     
     private static void test1() throws Exception {
    	 Config config = new Config() ;
    	 config.CUT = Person.class ;
         config.setDirsToClasses("./bin") ;
         config.regressionMode = false ;
         T3SuiteGenAPI tester = new T3SuiteGenAPI(config) ;
         List<SUITE> SS = tester.suites(true,".") ;
         tester.info(SS.get(0));
         
         //tester.replay(SS.get(0));
     }


}
