/*
 * Copyright 2014 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Examples.Cycles.PersonCycle;
import Sequenic.T3.Examples.Friends.Person;
import Sequenic.T3.ImplementationMap;
import Sequenic.T3.JavaType.*;
import Sequenic.T3.Reflection.Reflection;

import java.lang.reflect.*;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Logger;

/**
 * Representing the constructors, methods, and fields that are to be included in testing.
 */
public class TestingScope {

    /**
     * The class targeted for testing. This should not be an abstract nor interface.
     */
    public Class CUT ;

    /**
     * The proposed concrete type of CUT. If the CUT has type-variables; these will be
     * instantiated to concrete types specified by this JType.
     */
    //public JType CUTconcreteType ;  --> no longer needed.

    public ImplementationMap impsMap ;
    


    /**
     *  When true (default), will include all non-private members in the testing scope;
     *  else only public members are included.
     */
    public boolean testingFromTheSamePackagePespective = true ;
    public boolean includePrivateAndDefaultMembers = false ;
    
    public List<Constructor> constructors   = new LinkedList<Constructor>() ;
    public List<Method>      creatorMethods = new LinkedList<Method>() ;
    public List<Method> methods = new LinkedList<Method>();
    public List<Field> fields = new LinkedList<Field>() ;
    
    public List<Method> mutators = new LinkedList<Method>();
    public List<Method> nonMutators = new LinkedList<Method>();


    public TestingScope(ImplementationMap impsMap, Class CUT) {
       assert ! CUT.isInterface() ;
       this.impsMap = impsMap ;
       this.CUT = CUT ;
       // tySubstitution = Solver.solveClassTyvars(imap, JTypeUtils.naiveClassdecl2JType(CUT)) ;
       // CUTconcreteType = JTypeUtils.T3instantiateTypeParams(CUT) ;
    }
    
    /**
     * Clearing the scope.
     */
    public void clear() {
    	methods.clear();
    	constructors.clear();
    	fields.clear();
    	creatorMethods.clear();
    	mutators.clear();
    	nonMutators.clear();
    }
      
    private static boolean isMutator(Method M) {
        String name = M.getName().toLowerCase() ;
        if (name == "size" 
        		|| name == "length" 
        		|| name == "tostring"
                || name == "toarray"
                || name == "toint"
        		|| name == "equals"
        		|| name == "clone"
        		|| name == "hashcode"       		
        		|| name == "indexof"      		

           ) return false ;
        return !(name.startsWith("get") || name.startsWith("is") || name.startsWith("contains")) ;
    }
    
    private void calculateMutators() {
    	for (Method M : methods) {
    		if (isMutator(M)) mutators.add(M) ;
    		else nonMutators.add(M) ;
    	}
    }
    
    /**
     * Configure the scope for ADT-based testing.
     */
    public void configureForADTtesting() {
    	clear() ;
        // get the constructors in scope
        constructors =  getAccessibleConstructors()  ;

        // get the methods in scope
        List<Method> allMethods = getAccessibleMethods() ;
        for (Method M : allMethods) {
            if (Modifier.isStatic(M.getModifiers())) {
                // only include a static method if it actually takes a parameter of type CUT
                if (hasParameterOfExactlyThisType(M,CUT))  methods.add(M) ;
            }
            else methods.add(M) ;
        }
    	//System.out.println("### Included in the scope:") ;
        //for (Method M : methods) {
        //	System.out.println("### " + M.getName()) ;
        //}
        
        // get the fields in scope (static fields are already excluded)
        for (Field F : getAccessibleFields()) {
            fields.add(F) ;
        }
        // get the creation methods
        List<Method> candidates = new LinkedList<Method>() ;
        candidates.addAll(allMethods) ;
        try {
            candidates.addAll(getStaticMethodsOfEnclosingClasses(CUT)) ;
        }
        catch(Throwable t){ }
        for (Method M : candidates) {
            if (M.getDeclaringClass() == CUT) { // this is a bit wierd.. but maybe if the actual target is an inner class and the CUT is configure to be the enclosing class...
                if (! Modifier.isStatic(M.getModifiers())) continue ;
                if (M.getReturnType() != CUT) continue ;
                //if (hasParameterOfThisType(M,CUT)) continue ;
                if (hasParameterOfExactlyThisType(M,CUT)) continue ;
                creatorMethods.add(M) ;
            }
        }
        // split mutators and non-mutators:
        calculateMutators() ;
    }

    private List<Method> getStaticMethodsOfEnclosingClasses(Class C) {
    	List<Method> ms = new LinkedList<Method>() ;
    	Class D = C.getDeclaringClass() ;
    	if (D==null || D==C) return ms ;
    	for (Method m : D.getDeclaredMethods()) {
    		if (Modifier.isStatic(m.getModifiers())) ms.add(m) ;
    	}
    	ms.addAll(getStaticMethodsOfEnclosingClasses(D)) ;
    	return ms ;
    }
    

    /**
     * Configure the scope for non-ADT-based testing. 
     * NOTE: static fields will be excluded.
     */
    public void configureForNonADTTesting() {
    	clear();
        for (Method M : getAccessibleMethods()) {
            int mod = M.getModifiers() ;
            if (Modifier.isStatic(mod)) {
                methods.add(M) ;
            }
        }        
        // static fields are to be excluded!!
        //for (Field F : getAccessibleFields()) {
        //    if (Modifier.isStatic(F.getModifiers()))
        //       fields.add(F) ;
        //}
        // split mutators and non-mutators:
        calculateMutators() ;
    }

    /**
     * Get all constructors of the specified signature. If no signature is given, then 
     * all members of constructors will be returned. If just one class is specified, any
     * constructor that takes a parameter of that class is considered a match. 
     * If c contains two or more classes, the a constructor that takes parameters of the
     * specified types-prefix is considered a match.
     */
    public List<Constructor> constructors(Class ... c) {
    	List<Constructor> candidates = new LinkedList<Constructor>() ;
    	if (constructors.isEmpty()) return candidates ;
    	if(c.length==0) {
    		return constructors ;
    	}
    	if(c.length==1) {
    	   for (Constructor C : constructors) {
    		   for(Class argty : C.getParameterTypes()) {
    			   if (argty==c[0]) {
    				   candidates.add(C) ;
    				   break ;
    			   }
    		   }
    	   }
    	}
    	else {// c.length > 1
    	  for (Constructor C : constructors) {
    	    Class[] paramTypes = C.getParameterTypes() ;
    	    if (c.length > paramTypes.length) continue ;
    	    boolean match = true ;
 		    for(int i=0; i<c.length; i++) {
 			   if (paramTypes[i]!=c[i]) {
 				   match = false ;
 				   break ;
 			   }
 		    }
 		    if (match) candidates.add(C) ;
  	      }
    	}
       return candidates ;
    }
    
    /**
     * Return all methods matching the signature. If name is null, return all methods.
     */
    static private List<Method> findMethod(List<Method> methods, String name, Class ... c) {
    	if (name==null) return methods ;
    	List<Method> candidates0 = new LinkedList<Method>() ;
    	if (methods.isEmpty()) return candidates0 ;
    	
    	for (Method M : methods) {
    		if (M.getName() == name) candidates0.add(M) ;
    	} 
    	
    	if(candidates0.isEmpty()) return candidates0 ;
    	
    	List<Method> candidates = new LinkedList<Method>() ;
    	if(c.length==1) {
    	   for (Method M : candidates0) {
    		   for(Class argty : M.getParameterTypes()) {
    			   if (argty==c[0]) {
    				   candidates.add(M) ;
    				   break ;
    			   }
    		   }
    	   }
    	}
    	else {// c.length > 1
    	  for (Method M : candidates0) {
    	    Class[] paramTypes = M.getParameterTypes() ;
    	    if (c.length > paramTypes.length) continue ;
    	    boolean match = true ;
 		    for(int i=0; i<c.length; i++) {
 			   if (paramTypes[i]!=c[i]) {
 				   match = false ;
 				   break ;
 			   }
 		    }
 		    if (match) candidates.add(M) ;
  	      }
    	}
   	   return candidates ;
    }
    
    public List<Method> methods(String name, Class... c) {
    	return findMethod(methods,name,c) ;
    }
    
    public List<Method> mutators(String name, Class... c) {
    	return findMethod(mutators,name,c) ;
    }
    
    public List<Method> creatorMethods(String name, Class... c) {
    	return findMethod(creatorMethods,name,c) ;
    }

    public List<Method> methods() {
    	return methods ;
    }
    
    public List<Method> mutators() {
    	return mutators ;
    }
    
    public List<Method> creatorMethods() {
    	return creatorMethods ;
    }
    /**
     * Filter the members in the scope using the given predicates.
     */
    public void filter(Predicate<Object> selector)
    {
        List<Constructor> selectedConstructors = new LinkedList<Constructor>() ;
        for (Constructor co : constructors)
            if (selector.test(co)) selectedConstructors.add(co) ;
        constructors = selectedConstructors ;

        List<Method> selectedMethods = new LinkedList<Method>() ;
        for (Method M : methods)
            if (selector.test(M)) selectedMethods.add(M) ;
        methods = selectedMethods ;
        calculateMutators() ;

        selectedMethods = new LinkedList<Method>() ;
        for (Method M : creatorMethods)
            if (selector.test(M)) selectedMethods.add(M) ;
        creatorMethods = selectedMethods ;

        List<Field> selectedFields = new LinkedList<Field>() ;
        for (Field F : fields)
            if (selector.test(F)) selectedFields.add(F) ;
        fields = selectedFields ;
    }


    /**
     * Check if the method m has a parameter of type D, to which we can pass
     * an instance of C (so, if D is an ancestor of C).
     */
    static private boolean hasParameterOfThisType(Method m, Class C) {
         for (Class param : m.getParameterTypes()) {
             if (param.isAssignableFrom(C)) return true ;
         }
         return false ;
    }

    /**
     * Check if the method m has a parameter of type C.
     */
    static private boolean hasParameterOfExactlyThisType(Method m, Class C) {
        for (Class param : m.getParameterTypes()) {
            if (param == C) return true ;
        }
        return false ;
    }


    /**
     * Return all methods of C that are visible and accessible.
     * NOTE: abstract methods are excluded.
     */
    public List<Method> getAccessibleMethods() {

        List<Method> methods = new LinkedList<Method>();
        List<Method> candidates = new LinkedList<Method>();

        for (Method M : CUT.getDeclaredMethods()) candidates.add(M) ;
        for (Class D : Reflection.getALLSuperClasses(CUT)) {
            for (Method M : D.getDeclaredMethods()) {
            	// we won't include static method defined by superclass:
            	if (Modifier.isStatic(M.getModifiers())) continue ;
            	// if a method with the same name and type is already in the candidates, then M
            	// is overriden; so, not visible from the client:
            	boolean overriden = false ;
            	String mname = M.getName() ;
            	Class[] paramtypes = M.getParameterTypes() ;
            	for (Method K : candidates) {
            		if (! mname.equals(K.getName())) continue ;
            		if (K.getParameterTypes().length != paramtypes.length) continue ;
            		overriden = true ;
            		for(int i=0; i<paramtypes.length ; i++) {
            			if (paramtypes[i] != K.getParameterTypes()[i]) {
            				overriden = false ;
            				break ;
            			}
            		}
            		if (overriden) break ;
            	}
                if (!overriden) candidates.add(M) ;
            }
        }

        for (Method M : candidates) {
            int mod = M.getModifiers() ;
            if (Modifier.isAbstract(mod)) continue ; // don't put abstract methods in the scope
            String name = M.getName() ;
            if (name.equals(CONSTANTS.classinv_name)
                    || name.startsWith("<")  // exclude <init> and <clinit>
                    || name.startsWith("$jacoco")
               )
               continue;
            if (isVisible(M.getDeclaringClass(),M.getModifiers())) methods.add(M) ;
        }
        
        // if m_spec() is present, remove m()
        List<Method> specs = new LinkedList<Method>();
        for (Method M : methods) {
        	if (M.getName().endsWith(CONSTANTS.specmethod_suffix)) {
        		//System.out.println("### adding spec " + M.getName()) ;
        		specs.add(M) ;
        	}
        }
        List<Method> methods_ = new LinkedList<Method>();
        for (Method M : methods) {
        	String name = M.getName() ;
        	boolean hasSpec = false ;
        	for (Method S : specs) {
        		String specname = S.getName() ;
        		String name2 = specname.substring(0,specname.length() - CONSTANTS.specmethod_suffix.length()) ;
        		// System.out.println("### comparing " + name + " vs " + name2) ;
        		if (name.equals(name2)) { hasSpec = true ; break ; }
        	}
        	if (!hasSpec) methods_.add(M) ;
        }
        return methods_ ;
    }
    
    private boolean isVisible(Class declaringClass, int modifier) {
    	if (includePrivateAndDefaultMembers) return true ;
    	if (testingFromTheSamePackagePespective) {
    		if (declaringClass.getPackage() == CUT.getPackage())    		
    		    return (! Modifier.isPrivate(modifier)) ;
    		else 
    			return (Modifier.isPublic(modifier)) ;
    	}
    	else return (Modifier.isPublic(modifier)) ;
    }
    
    /**
     * Returns the set of accessible constructors on the CUT. If CUT is abstract, no
     * constructor is returned, since an abstract class cannot be instantiated through
     * its constructor (even if it declares one).
     */

    public List<Constructor> getAccessibleConstructors() {

        List<Constructor> constructors = new LinkedList<Constructor>();

        // an abstract class cannot be instantiated through its constructors,
        // even if it has one, so don't bother to add its constructors:
        if (Modifier.isAbstract(CUT.getModifiers())) {
        	return constructors ;
        }
        if (CUT.isInterface()) return constructors ;

        for (Constructor con : CUT.getDeclaredConstructors()) {
            //int mod = con.getModifiers() ;
            //if (Modifier.isAbstract(mod)) continue ;
            if (isVisible(con.getDeclaringClass(),con.getModifiers()))
                constructors.add(con) ;
        }
        return constructors ;
    }


    /**
     * Get all fields that are visible and accessible from the client package. 
     * 
     * NOTE: Static fields will be EXCLUDED, because updating them may cause a test sequence
     * to influence the next ones. hence making test sequences not independent of
     * each other. Note that this cannot be fully prevented, since invoking a method
     * can still indirectly change a static field. But at least the risk is mitigated.
     */
    public List<Field> getAccessibleFields() {

        List<Field> fields = new LinkedList<Field>();
        List<Field> candidates = new LinkedList<Field>();

        for (Field F : CUT.getDeclaredFields()) {
        	// exclude static fields:
        	if (Modifier.isStatic(F.getModifiers())) continue ;
        	candidates.add(F) ;
        }
        for (Class D : Reflection.getALLSuperClasses(CUT)) {
            for (Field F : D.getDeclaredFields()) {
            	// exclude static fields:
            	if (Modifier.isStatic(F.getModifiers())) continue ;
            	// if field of the same name is already in candidates, then F is shadowed; 
            	// and so not visible from the client:
            	boolean shadowed = false ;
            	String fname = F.getName() ;
            	for (Field G : candidates) {
            		
            		if (fname.equals(G.getName())) {
            			shadowed = true ;
            			break ;
            		}
            	}
            	if (!shadowed) candidates.add(F) ;
            }
        }

        for (Field F : candidates) {
            int mod = F.getModifiers() ;
            if (Modifier.isAbstract(mod)) continue ;
            if (Modifier.isFinal(mod)) continue ;
            String fname = F.getName() ;
            if (fname.equals("serialVersionUID")) continue ;
            if (fname.startsWith(CONSTANTS.auxField_prefix)) continue ;
            if (isVisible(F.getDeclaringClass(),F.getModifiers())) fields.add(F) ;
        }
        return fields ;
    }

    private String printParamTypes(Class[] params) {
    	String s = "(" ;
    	for (int k=0; k< params.length ; k++) {
    		if (k>0) s += ", " ;
    		s += params[k].getName() ;
    	}
    	s += ")" ;
    	return s ;
    }

    public String toString() {
        String s = "Testing scope:" ;
        String accessScope = "\nIncluding only public members" ;
        if (includePrivateAndDefaultMembers) accessScope = "\nAlso including default and protected members" ;
        else if (testingFromTheSamePackagePespective) accessScope = "\nAlso including default and protected members" ;
        s += "CUT = " + CUT.getName() ;
        // s += "\nConcrete type = " + CUTconcreteType ;
        s += accessScope ;
        s += "\nConstructors: " + constructors.size()  ;
        for (Constructor C : constructors) {
            s += "\n  " + Modifier.toString(C.getModifiers()) + " " + C.getName() ;
            s += " " + printParamTypes(C.getParameterTypes()) ;
        }
        s += "\nCreator methods: " + creatorMethods.size() ;
        for (Method M : creatorMethods) {
            s += "\n   " + Modifier.toString(M.getModifiers()) + " " +  M.getName()  ;
            s += " " + printParamTypes(M.getParameterTypes()) ;
        }
        s += "\nMutators: "  + mutators.size() ;
        for (Method M : mutators) {
            s += "\n   " + Modifier.toString(M.getModifiers()) + " " +  M.getName()  ;
            s += " " + printParamTypes(M.getParameterTypes()) ;
        }
        s += "\nNon-mutators: "  + nonMutators.size() ;
        for (Method M : nonMutators) {
            s += "\n   " + Modifier.toString(M.getModifiers()) + " " +  M.getName()  ;
            s += " " + printParamTypes(M.getParameterTypes()) ;
        }
        s += "\nFields: " + fields.size() ;
        for (Field F : fields)
            s += "\n   " + Modifier.toString(F.getModifiers()) + " " + F.getName() ;
        // s += "\n---------------" ;
        return s ;
    }
    

    public static void main(String[] args) {
    	String[] dirs = {"./bin"} ; 
    	ImplementationMap imap = new ImplementationMap(dirs) ;
        TestingScope scope = new TestingScope(imap,LinkedList.class) ;
        scope.configureForADTtesting();
        System.out.println(scope.toString());

    }

}
