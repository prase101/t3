package Sequenic.T3;

/**
 * When a test sequence throws an exception, when T3 is configured to re-throw exceptions,
 * it will be wrapped inside this Violation class, so that we can distinguish it from
 * exception thrown by T3 itself.
 */
public class Violation extends Error {

	private static final long serialVersionUID = 1L;

	public Violation(String info) {
        super(info) ;
    }
	
	public Violation(String info, Throwable cause) {
        super(info,cause) ;
    }
	
    public Violation(Throwable cause) {
        super(cause) ;
    }
}
