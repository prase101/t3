package Sequenic.T3.utils;

import java.util.Random;

public class GaussRandom extends Random {

	public GaussRandom() { super() ; }
	public GaussRandom(long seed) { super(seed) ; }
	
	/**
	 * Return the next random-int in the range of 0..(range-1),
	 * with Gaussian distribution.
	 */
	public int nextGaussInt(int range){
		double range_ = (double) range ;
		double width = range_/2.0 ;
		// careful.. this is a bit tricky
		double y = nextGaussian()*width + width ;
		y = Math.max(0,y) ;
		y = Math.min(range_ - 1, y) ;
		return (int) Math.round(y) ;
	}

}
