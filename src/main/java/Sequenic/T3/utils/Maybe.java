package Sequenic.T3.utils;

import java.io.Serializable;

public class Maybe<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;

    public T val ;

    public Maybe(T val) { this.val = val ; }

    public String toString() {
        if (val==null) return "nothing"  ;
        return ("val = "  + val.toString()) ;
    }
    
    /**
     * Wrap a value in a Maybe-object. However, if the value is null,
     * then null is retured as well.
     */
    public static <U> Maybe<U> lift(U x) {
    	if (x==null) return null ;
    	else return new Maybe(x) ;
    			
    }

}
