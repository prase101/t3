package Sequenic.T3.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.nio.file.Path;

/**
 * Utilities to clone objects, to serialize objects to a file, and to load it back
 * from the file.
 */

public class SerializationUtils {
	
	/**
	 * Return a deep-clone of an object. Currently implemented via
	 * serialization; thus assuming that the cloned object is
	 * serializable.
	 */
	static public <T> T clone(T o) 
		throws IOException, 
			   ClassNotFoundException 
	{
		ByteArrayOutputStream outstream = new ByteArrayOutputStream() ;
		ObjectOutputStream cout = new ObjectOutputStream(outstream) ;
		cout.writeObject(o) ; 
		ByteArrayInputStream instream = new ByteArrayInputStream(outstream.toByteArray()) ;
		ObjectInputStream cin = new ObjectInputStream(instream) ;
		T copy = (T) cin.readObject() ;
		return copy ;
	
	}
	
	static public void save(String fname, Serializable o) throws Exception {
	    Path fp = FileSystems.getDefault().getPath(fname) ;
	    File f  = fp.toFile()  ;
	    OutputStream fout = new FileOutputStream(f);
	    OutputStream buffer = new BufferedOutputStream(fout);
	    ObjectOutputStream oos = new ObjectOutputStream(buffer);
	    try { oos.writeObject(o); }
	    finally { oos.close(); }
	 }

	 public static Object load(String fname) throws Exception {
	    File f = new File(fname) ;
	    InputStream file = new FileInputStream(fname);
	    InputStream buffer = new BufferedInputStream(file);
	    ObjectInputStream ois = new ObjectInputStream ( buffer );
	    try {
	    	Object S = ois.readObject() ;
	        return S ;
	    }
	    finally { ois.close(); }
	 }



}
