/*
 * Copyright 2013 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 *
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3.utils;

/**
 *
 * <p>The show recursively traverses the structure of the
 * object. Circular pointer will be marked. A maximum depth (of the
 * traversal) can also be specified.
 *
 * <p>Numbers will be associated with the shown object and all its
 * subobject, so that we can indicate if a (sub-) object points to an
 * object that has been shown. It is possible to reset the numbering
 * at each call to {@link Sequenic.T2.Obj#Show show} or to keep the
 * numbering {@link Sequenic.T2.Obj#showWithContNum accross multiple calls}.
 *
 * <p>Note: showing primitive typed and boxing typed values is a
 * problem. We use reflection to obtain the types of the fields, but
 * typically reflection will say that a primitive typed field to have
 * the corresponding boxing class. So we can't see the difference
 * between the two. Currently we'll just omit the numbering
 * information on these kind of values, since they will falsely signal
 * for aliasing.
 */
import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Reflection.Reflection;

import java.lang.reflect.*;
import java.util.*;
import java.io.*;
import static Sequenic.T3.Reflection.Reflection.* ;
//import Sequenic.T2.Msg.*;
//import Sequenic.T2.Engines.Util ;

// import Sequenic.T2.examples.* ;

/**
 * A utility to show/print the content of an object to a string.

 * @author Wishnu Prasetya
 *
 */
public class Show {

    /**
     * Mapping of seen objects to unique numbers. The numbers can be seen as their IDs.
     */
    public IdentityHashMap<Object, Integer> objIDs = new IdentityHashMap<Object, Integer>();
    
    /**
     * Show will only go down up to this maximum depth of object
     * structure. Sub-objects at the deeper depth will not be
     * shown. The default is 5.
     */
    public int maxDepth = 5;
    
    /**
     * The initial indentation. Default is 3.
     */
    public int InitialIndentation = 3 ;
    

    /**
     * Let C be a class. If (C,fns) is in this map, then when showing
     * instances of C, only fields named in fns will be showed.
     * 
     * <p>Add entries to this variable to have a class with lots of fields
     * shown more compactly.
     */
    static public Map<Class, List<String>> showFilter = new HashMap<Class, List<String>>();
    
    /**
     * When a class C is listed here, then when showing an instance of 
     * C we will only show its name (its internal state will not be 
     * shown at all). 
     * 
     * <p>Add classes to this variable to surpress showing their state.
     */
    static public List<Class> veryCompactShow;
    
    
    // Class initialization:
    static 
    {
        veryCompactShow = new LinkedList<Class>();
        veryCompactShow.add(Random.class);
        veryCompactShow.add(Reader.class);
        veryCompactShow.add(Writer.class);
        veryCompactShow.add(InputStream.class);
        veryCompactShow.add(OutputStream.class);
        veryCompactShow.add(RandomAccessFile.class);
        veryCompactShow.add(File.class) ;
        veryCompactShow.add(Class.class) ;
    }

    public Show() { }

    /**
     * Create a new Shower object, with the specified maximum show
     * depth and initial indentation.
     */
    public Show(int initialIndent, int maxdepth) {
        maxDepth = maxdepth;
        InitialIndentation = initialIndent;
    }

    /**
     * Show the object o. The object numbering is maintained across
     * multiple calls to this method.
     */
    public String showWithContNum(Object o) {
        LinkedList visited = new LinkedList();
        return showWorker(o, visited, new PP(), maxDepth).render(InitialIndentation);
    }


    /**
     * Reset the continous numbering.
     */
    public void resetContinousNumbering() {
        objIDs.clear();
    }

    /**
     * Show the object o. The object numbering is reset at each call.
     *
     * @param indent Initial indentiation.
     */
    public static String show(Object o, int indent, int maxDepth) {
        Show s = new Show(indent, maxDepth);
        return s.showWithContNum(o);
    }

    /**
     * Show an object, with default depth and initial indentation. Object numbering 
     * is not continued over multiple calls.
     */
    static public String show(Object o) {
    	Show s = new Show();
        return s.showWithContNum(o);
    }

    /**
     * Get ALL fields of a class. This includes private fields, and fields
     * of the superclasses. Fields from class Objects will be excluded.  These
     * will be the fields that will be shown.
     *
     * Static, abstract, and final fields are excluded.
     * Auxiliary fields are excluded.
     */
    public static List<Field> getAllNonStaticFields(Class C) {
        List<Field> fields = new LinkedList<Field>();
        List<Class> ancestors = new LinkedList<Class>() ;
        ancestors.add(C) ;
        ancestors.addAll(getALLSuperClasses(C))  ;
        for (Class D : ancestors) {
            Field[] fieldsOfD = D.getDeclaredFields() ;
            for (Field f : fieldsOfD)   {
                int mf = f.getModifiers() ;
                // unsurpressing "final", since a reference field can itself be final, but
                // the data it points to may be changing
                if (Modifier.isStatic(mf) || Modifier.isAbstract(mf) /*|| Modifier.isFinal(mf)*/) continue ;
                if (f.getName().startsWith(CONSTANTS.auxField_prefix)) continue ;
                fields.add(f) ;
            }
        }
        return fields ;
    }


    // the worker function for show:
    private PP showWorker(Object o,
            Collection visited,
            PP previousPP,
            int depth) {
        if (depth <= 0) {
            return previousPP.aside_(PP.text("..."));
        }
        if (o == null) {
            return previousPP.aside_(PP.text("null"));
        }
        Class C = o.getClass();

        // primitive type:
        //if (C.isPrimitive() || C.isEnum() || C.getName().equals("java.lang.String")) {
        if (Reflection.isPrimitiveLike(C) || C.isEnum() || C.getName().equals("java.lang.String")) {
            return previousPP.aside_(PP.text("(" + C.getSimpleName() + ") : " + o));
        }
        
        
        // Handle class with compacted show:
        for(Class D : veryCompactShow) {
            if (D.isAssignableFrom(C)) {
                return previousPP.aside_(PP.text("(" + C.getSimpleName() + ") : ..."));
            }
        }
        // else: 
        Integer indexOf_o = objIDs.get(o) ;
        if (indexOf_o==null) {
            // o has not been taken in the pool
        	indexOf_o = objIDs.size();
            objIDs.put(o, indexOf_o);
        }
   
        // o has been visited before :
        if (visited.contains(o)) {
            return previousPP.aside_(PP.text("(" + C.getSimpleName() +
                    ") ---> @" + indexOf_o));
        }
        // else o has not been visited:
        visited.add(o) ;
        
        // Array : 
        int i = 0;
        if (C.isArray()) {
            PP arrayPP = PP.text("(Array)  @ " + indexOf_o);
            for (i = 0; i < Array.getLength(o); i++) {
                // System.out.println("@" + i) ;
            	if (i>=10) {
            		int N = Array.getLength(o) - 10 ;
            		arrayPP.ontop(PP.text("... " + N + " more elements, but not shown")) ;
            		break ;
            	}
            	PP elemPP = showWorker(Array.get(o, i),
                        visited,
                        PP.text("[" + i + "]"),
                        depth - 1) ;
            	if (i==0) elemPP.indent(InitialIndentation) ;
            	arrayPP.ontop(elemPP) ;
            }
            return previousPP.aside_(arrayPP);
        //Message.console(Message.DEVEL_WARNING,"Cannot show an array.",new Show()) ;
        //return PP.aside(previousPP, PP.text("...some array")) ;			
        }

        // Collection:
        if (Collection.class.isAssignableFrom(C)) {
            PP colPP = PP.text("(" + C.getSimpleName() +
                    ")  @  " + indexOf_o);
            i = 0;
            for (Object element : (Collection) o) {
            	if (i>=10) {
            		int N = ((Collection) o).size() - 10 ;
            		colPP.ontop(PP.text("... " + N + " more elements, but not shown")) ;
            		break ;
            	}
            	PP elemPP = showWorker(element,
                        visited,
                        PP.text("[" + i + "]"),
                        depth - 1);
            	if (i==0) elemPP.indent(InitialIndentation) ;
            	colPP.ontop(elemPP) ;
                i++;
            }
            return previousPP.aside_(colPP);
        }
        
        // Map
        if (Map.class.isAssignableFrom(C)) {
            PP colPP = PP.text("(" + C.getSimpleName() +
                    ")  @  " + indexOf_o);
            i = 0;
            for (Object element : ((Map) o).entrySet()) {
            	if (i>=10) {
            		int N = ((Map) o).size() - 10 ;
            		colPP.ontop(PP.text("... " + N + " more elements, but not shown")) ;
            		break ;
            	}
            	Map.Entry e = (Map.Entry) element ;
            	PP keyPP = showWorker(e.getKey(),visited,PP.text("["), depth-1) ;
            	keyPP.aside(PP.text("]")) ;
            	PP elemPP = showWorker(e.getValue(),
                        visited,
                        keyPP ,
                        depth - 1);
            	if (i==0) elemPP.indent(InitialIndentation) ;
            	colPP.ontop(elemPP) ;
                i++;
            }
            return previousPP.aside_(colPP);
        }


        // if the object is not array nor collection nor map:
        // Box types : 
        if (isBoxingType(C)) {
            return previousPP.aside_(PP.text("(" + C.getSimpleName() +
                    ")" // ") @ " 
                    // + indexOf_o
                    + " : " + o));
        }
       
        // getting all C's fields, including those declared by superclasses,
        // and we first remove those fields which have been specified not
        // to be shown:
        List<Field> allfields = getAllNonStaticFields(C);
        List<Field> tobeRemoved = new LinkedList<Field>();
        List<String> onlyShowTheseFields = showFilter.get(C) ;
        for (Field f : allfields) {
            f.setAccessible(true);
            if (f.getName().equals("$assertionsDisabled")) {
                tobeRemoved.add(f);
            }
            if (onlyShowTheseFields == null) continue ;
            boolean found = false ;
            for (String fn : onlyShowTheseFields) {
                if (f.getName().equals(fn)) { found = true ; break ; }
            } 
            if (!found) tobeRemoved.add(f);  
        }
        for (Field f : tobeRemoved) {
            allfields.remove(f);
        }

        PP titleLine = PP.text("(" + C.getName() + ") @ " + indexOf_o);

        if (allfields.isEmpty()) {
            return previousPP.aside_(titleLine);
        }
        String fname;
        // PP pp_fields = previousPP.aside_(titleLine) ;
        PP pp_fields = titleLine;
        PP entry = null;
        Object fieldval = null;
        i = 0;

        for (Field field : allfields) {

            try {
                fieldval = field.get(o);
                entry = PP.text("" + field.getName());
                if (field.getDeclaringClass() != C) {
                    entry = PP.text("" +
                            field.getDeclaringClass().getSimpleName() + "." + field.getName());
                }
                pp_fields.ontop(showWorker(fieldval, visited, entry, depth - 1));
            } catch (IllegalAccessException ex) {
                ex.printStackTrace(System.out);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace(System.out);
            }

        }

        return previousPP.aside_(pp_fields);

    }
    
    // test:
    static public void main(String[] args) {
    
    System.out.println(show(new Integer(100))) ;
    System.out.println(show("Hello ET!")) ;
    System.out.println(show(new int[2])) ;
        
    List<String> xs = new LinkedList<String>() ;
    xs.add("holla") ; xs.add(null) ;
    System.out.println(show(xs)) ;
    
    List ys = new LinkedList() ;
    ys.add(new Object()) ;
    ys.add(ys) ;
    System.out.println("circular ys: " + show(ys)) ;
    
    HashMap<String,Integer> ms = new HashMap<String,Integer>() ;
    ms.put("holla", 999) ;
    ms.put("ouch", -1) ;
    
    System.out.println(show(ms)) ;
    System.out.println(show(ms,3,2)) ;
    
    System.out.println("\n+++" + show(new java.math.BigDecimal("111111111111111111111111111.1"))) ;
    
    
    }
}