package Sequenic.T3.Generator;

import static org.junit.Assert.*;
import org.junit.Test;


import java.lang.reflect.Array;
import java.util.logging.Level;
import java.util.logging.Logger;

import Sequenic.T3.ImplementationMap;
import Sequenic.T3.Pool;
import Sequenic.T3.Generator.Value.CollectionLikeMG;
import Sequenic.T3.Generator.Value.IntMG;
import Sequenic.T3.Generator.Value.NullMG;
import Sequenic.T3.Generator.Value.ValueMGCombinators;
import static Sequenic.T3.Generator.GenCombinators.* ;
import Sequenic.T3.JavaType.JTfun;
import Sequenic.T3.JavaType.JType;
import Sequenic.T3.JavaType.JTypeUtils;
import Sequenic.T3.Sequence.Datatype.CONST;
import Sequenic.T3.Sequence.Datatype.PARAM;
import Sequenic.T3.Sequence.Datatype.STEP;
import Sequenic.T3.Sequence.Datatype.STEP.PreConditionCheckPlaceHolder;
import Sequenic.T3.Sequence.Datatype.STEP_RT_info;
import Sequenic.T3.utils.FUN;
import Sequenic.T3.utils.Maybe; 

/**
 * This shows examples of constructing a meta-representation of a "value".
 * A value is either a primitive value e.g. an integer or a string, or an
 * array, or a collection. Null is also a value.
 * 
 * To turn a meta-value to a value, we invoke its "exec"-method. It takes
 * three parameters:
 * 
 *     metavalue.exec(CUT,pool,pre-condition-check-placeholder)
 *     
 * CUT stands for the "class-under-test"; since T3 is a testing tool, it 
 * assumes that you use it to target a CUT. This is less relevant when
 * constructing a meta-val, but nevertheless the API wants you to specify
 * a CUT.   
 * 
 * "pool" : object pool; see the original T2-paper.
 * 
 * pre-condition-check-placeholder: we can ignore this for now.
 */
public class Test_ValueGenerator {
	
	@Test
	public void test_IntMG1() throws Exception {
		// in this example, we will create a meta-value that represents
		// an integer. 
		
		// we will create the meta-val via a meta-val generator.
		// let's use this generator, that will create a random int
		// in [0..10), with uniform distribution.
		Generator<PARAM,STEP> gen = new IntMG().uniform(0,10) ;
			
		// gen can be thought as a function PARAM -> STEP. We don't
		// actually need a PARAM here, but in T3 generating a value is
		// usually done because we need the value as an instance of
		// a parameter of some method-under-test.
		
		// So, imagine you want to create an instance of some parameter named "x",
		// whose type is int:
		
		// Specify the parameter to instantiate:
		PARAM param = new PARAM("x",JTypeUtils.convert(Integer.TYPE)) ;
		
		// Now, invoke the generator to create a meta-value:
		Maybe<STEP> metaValue_ = gen.generate(param) ;
		STEP metaValue = metaValue_.val ;
		
		// Execute the meta-val:
		Class ClassUnderTest = null ; // don't need this for this example
		Pool pool = new Pool() ; 
		PreConditionCheckPlaceHolder precondR = null ; // don't need this for this example
		STEP_RT_info result = metaValue.exec(ClassUnderTest, pool, precondR) ;
		
		// now, get the int:
		int val = (int) result.returnedObj ;
		System.out.println(">> value = " + val) ;
		assertTrue(0<=val && val<10) ;
	}
	
	@Test
	public void test_IntMG2() throws Exception {
		// as test_IntMG1, but using a different int-gen:
		
		Generator<PARAM,STEP> gen = ValueMGCombinators
				.Integer(ValueMGCombinators.OneOf(1,99,999)) ;
		
		
		PARAM param = new PARAM("x",JTypeUtils.convert(Integer.TYPE)) ;
		Maybe<STEP> metaValue_ = gen.generate(param) ;
		STEP metaValue = metaValue_.val ;
		
		Class ClassUnderTest = null ; // don't need this for this example
		Pool pool = new Pool() ; 
		PreConditionCheckPlaceHolder precondR = null ; // don't need this for this example
		STEP_RT_info result = metaValue.exec(ClassUnderTest, pool, precondR) ;
		
		int val = (int) result.returnedObj ;
		System.out.println(">> value = " + val) ;
		assertTrue(val==1 || val==99 || val==999) ;
	}
	
	@Test
	public void test_nullMG() throws Exception {
		// constructing a meta-value representing null
		
		Generator<PARAM,STEP> gen = new NullMG().constant() ;
		
		PARAM param = new PARAM("y",JTypeUtils.convert(Object.class)) ;	
		Maybe<STEP> metaValue_ = gen.generate(param) ;
		STEP metaValue = metaValue_.val ;
		
		Class ClassUnderTest = null ; // don't need this for this example
		Pool pool = new Pool() ; 
		PreConditionCheckPlaceHolder precondR = null ; // don't need this for this example
		STEP_RT_info result = metaValue.exec(ClassUnderTest, pool, precondR) ;
		
		Object val = result.returnedObj ;
		System.out.println(">> value = " + val) ;
		
		assertTrue(val==null) ;
	}
	
	@Test
	public void test_arrayMG() throws Exception {
		// constructing a meta-value representing an array of Integer:
		
		// First, specify your generator for the elements of the array:
		Generator<PARAM,STEP> elementMG = OneOf(
				ValueMGCombinators.Integer(ValueMGCombinators.OneOf(-1,0,1)),
				new NullMG().constant()
			) ;
							
		// to build the meta-val generator for collection-like, we will also need
		// a so-called interface-map; see T2 original paper:
		String[] dirsToScan = {
				"~/eclipse-workspace/myT3/bin"
		} ;
		ImplementationMap imap = new ImplementationMap(dirsToScan) ;

		// need to pack the element meta-gen inside a FUN-structure:
		FUN wrapper = new FUN() ;
		wrapper.fun = elementMG ;
		
		int sizeArrayToGenerate = 10 ;

		// now, the collection-like meta-gen:
		Generator<PARAM,STEP> gen = new CollectionLikeMG(sizeArrayToGenerate,imap) . collectionlike(wrapper) ;
		
		// now invoke the gen to generate a meta-value representing an array of the
		// requested type:
		JType elemTy = JTypeUtils.convert(Integer.class) ;
		JTfun arrayTy = new JTfun(Array.class, elemTy) ;		
		PARAM param = new PARAM("a",arrayTy) ;
		Maybe<STEP> metaValue_ = gen.generate(param) ;
		STEP metaValue = metaValue_.val ;
		
		// now, execute the meta-val to obtain the array:
		Class ClassUnderTest = null ; // don't need this for this example
		Pool pool = new Pool() ; 
		PreConditionCheckPlaceHolder precondR = null ; // don't need this for this example
		STEP_RT_info result = metaValue.exec(ClassUnderTest, pool, precondR) ;

		Integer[] array = (Integer[]) result.returnedObj ;
		Object val = result.returnedObj ;
		System.out.print(">> array, size=" + array.length + ", elems:" ) ;
		for (int k=0; k<array.length; k++) {
			System.out.print(" " + array[k]) ;
		}
		System.out.println("") ;
		assertTrue(array.length<=sizeArrayToGenerate) ;
	}
	
}
