package TestT3SuiteGen.TestClasses;

import TestT3SuiteGen.TestLogger;

abstract public class AbstractClass_withCreationMethod {
	
	static class ConcreteClass extends AbstractClass_withCreationMethod {
		@Override
		public void fasbtract() { TestLogger.log("abstract")  ; }
	}
	
	
	abstract public void fasbtract() ;
	public void fconcrete()  { TestLogger.log("concrete") ; }
	
	public static AbstractClass_withCreationMethod xxxCreationMethod() { 
		TestLogger.log("creation method") ;
		return new ConcreteClass() ; 
	}

}
