package TestT3SuiteGen.TestClasses;

import TestT3SuiteGen.TestLogger;

public class Creation {
	
	public Creation() { TestLogger.log("public constructor") ; }
	protected Creation(int x) { TestLogger.log("protected constructor") ; }
	Creation(int x, int y) { TestLogger.log("default constructor") ; }
	private Creation(int x, int y, int z) { TestLogger.log("private constructor") ; }
	
	public static Creation xxxCreationMethod() { 
		TestLogger.log("creation method") ;
		return new Creation() ; 
	}
	
	public void foo() { TestLogger.log("foo") ; }
	
}
