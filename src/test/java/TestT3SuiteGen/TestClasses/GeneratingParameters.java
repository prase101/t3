package TestT3SuiteGen.TestClasses;

import static TestT3SuiteGen.TestLogger.* ;

import java.util.*;

public class GeneratingParameters {
	
	public void fbool(boolean x) { log("boolean") ; }
	public void fBoolean(Boolean x) { if (x!=null) log("Boolean") ; }

	public void fchar(char x) { log("char") ; }
	public void fCharacter(Character x) { if (x!=null) log("Character") ; }

	public void fbyte(byte x) { log("byte") ; }
	public void fByte(Byte x) { if (x!=null) log("Byte") ; }

	public void fshort(short x) { log("short") ; }
	public void fShort(Short x) { if (x!=null) log("Short") ; }

	public void fint(int x) { log("int") ; }
	public void fInteger(Integer x) { if (x!=null) log("Integer") ; }

	public void flong(long x) { log("long") ; }
	public void fLong(Long x) { if (x!=null) log("Long") ; }

	public void ffloat(float x) { log("float") ; }
	public void fFloat(Float x) { if (x!=null) log("Float") ; }

	public void fdouble(double x) { log("double") ; }
	public void fDouble(Double x) { if (x!=null) log("Double") ; }

	public void fstring(String x) { log("String") ; }
	
	public void farray(String[] x) { if (x!=null) log("array") ; }
	public void fcollection(Collection<Character> x) { if (x!=null) log("collection") ; }
	public void fset(Set<Character> x) { 
		if (x!=null) log("set") ; 
	}
	public void flist(List<Character> x) { 
		System.out.println(">>>> flist " + x);
		if (x!=null) log("list") ; 
	}
	public void fmap(Map<String,Character> x) { if (x!=null) log("map") ; }
	
	public void fcompatable(Comparable x) { if (x!=null) log("Comparable") ; }
	
	public void fAppendable(Appendable x) { if (x!=null) log("Appendable") ; }
}
