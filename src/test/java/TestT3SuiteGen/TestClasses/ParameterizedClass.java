package TestT3SuiteGen.TestClasses;

import TestT3SuiteGen.TestLogger;

public class ParameterizedClass<T ,  U extends ClassWithGenericMethod.B> {
	
    static private void trace(String m, Object o) {
		TestLogger.log("** " + m + " FALSE: " + o.getClass()) ;
		System.out.println("** " + m + " gets " + o.getClass());
    }
	

    static private void trace(String m, Object o1, Object o2) {
		TestLogger.log("** " + m + " FALSE: " + o1.getClass() + ", " + o2.getClass()) ;
		System.out.println("** " + m + " gets " + o1.getClass() + ", " + o2.getClass());
    }
	
	T t ;
	U u ;
	
    public ParameterizedClass(T x, U y)	{
    	if (x==null || y==null) throw new IllegalArgumentException() ;
    	t = x ;
    	u = y ;
    }
    
    public void m1(T x) {
    	if (x==null) throw new IllegalArgumentException() ;
    	String mname = "m1" ;
    	System.out.println(">> " + mname + ", t: " + t.getClass() + ", x: " + x.getClass()) ;
    	if (! t.getClass().equals(x.getClass())) trace(mname,t,x) ;
    }
    
    public void m2(U y) {
    	if (y==null) throw new IllegalArgumentException() ;
    	String mname = "m2" ;
    	System.out.println(">> " + mname + ", u: " + u.getClass() + ", y: " + y.getClass()) ;
    	if (! u.getClass().equals(y.getClass())) trace(mname,u,y) ;
    }

}
