package TestT3SuiteGen;

import java.util.*;

public class TestLogger {

	public static List<String> log = new LinkedList<String>() ;
	
	public static void log(String s) { log.add(s) ; }
	
	public static void resetLog() { log.clear(); }
	
	public static boolean contain(String s) {
		return log.contains(s) ;
	}
	
	public static boolean empty() { return log.isEmpty() ; }
	
	public static boolean containSegment(String s) {
		for (String t : log) {
			if (t.contains(s)) return true ;
		}
		return false ;
	}
	
}
