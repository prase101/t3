package TestT3SuiteGen.Tests;

import static org.junit.Assert.*;

import org.junit.*;

import TestT3SuiteGen.TestClasses.* ;
import TestT3SuiteGen.* ;
import Sequenic.T3.* ;

public class Test_Creation {
	
	@BeforeClass 
	public static void runT3() {
		TestLogger.resetLog();
		Config config = new Config() ;
		config.CUT = Creation.class ;
		config.maxPrefixLength = 1 ;
		config.suiteSizeMultiplierPerGoal = 50 ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(config) ;
	    t3.suite(true) ;
	}

	// test that constructors of different visibility are accounted correctly
	@Test
	public void test0() {
	    assertTrue(TestLogger.contain("foo")) ;
	    assertTrue(TestLogger.containSegment("public")) ;
	    assertTrue(TestLogger.containSegment("protected")) ;
	    assertTrue(TestLogger.containSegment("default")) ;
	    assertFalse(TestLogger.containSegment("private")) ;
  
	}
	
	@Test
	public void test1() {    
	    // creation method can be used too
	    assertTrue(TestLogger.contain("creation method")) ;  
	}
	
}
