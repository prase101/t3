package TestT3SuiteGen.Tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Test;

import Sequenic.T3.CONSTANTS;
import Sequenic.T3.Config;
import Sequenic.T3.T3SuiteGenAPI;
import TestT3SuiteGen.TestLogger;
import TestT3SuiteGen.TestClasses.SimpleSubclass;

public class Test_Enum {

	static enum Color { Red, Green, Blue }
	static enum Day { Monday, Tuesday, OtherDay }
	
	public static class UsingEnum {
		public void foo(Color c, Day d, int x) {
			int dummy ;
			if (c == Color.Red) dummy=1 ;
			if (d == Day.Monday) dummy=2 ;
			System.out.println(">>>foo(" + c + "," + d + "," + x + ")") ;
	        TestLogger.log(">>>foo(" + c + "," + d + "," + x + ")") ;
		}
	}

	@Test
	public void test0() {
		TestLogger.resetLog();
		Config config = new Config() ;
		config.CUT = UsingEnum.class ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(config) ;
	    t3.suite(true) ;
	    assertTrue(TestLogger.containSegment(">>>foo(Red")) ;
	}
}
