package TestT3SuiteGen.Tests;

import static org.junit.Assert.*;

import org.junit.*;

import TestT3SuiteGen.TestClasses.* ;
import TestT3SuiteGen.* ;
import Sequenic.T3.* ;
import static TestT3SuiteGen.TestLogger.* ;

public class Test_GeneratingParameters {
	
	@BeforeClass 
	public static void runT3() {
		TestLogger.resetLog();
		Config config = new Config() ;
		config.CUT = GeneratingParameters.class ;
		config.suiteSizeMultiplierPerGoal = 1 ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(config) ;
	    t3.suite(true) ;
	}
	
	@Test
	public void testPrimtives() {
		assertTrue(contain("boolean")) ;
		assertTrue(contain("Boolean")) ;
		assertTrue(contain("char")) ;
		assertTrue(contain("Character")) ;
		assertTrue(contain("byte")) ;
		assertTrue(contain("Byte")) ;
		assertTrue(contain("short")) ;
		assertTrue(contain("Short")) ;
		assertTrue(contain("int")) ;
		assertTrue(contain("Integer")) ;
		assertTrue(contain("long")) ;
		assertTrue(contain("Long")) ;
		assertTrue(contain("float")) ;
		assertTrue(contain("Float")) ;
		assertTrue(contain("double")) ;
		assertTrue(contain("Double")) ;
		assertTrue(contain("String")) ;

	}

	@Test
	public void testCollectionLine() {
		assertTrue(contain("array")) ;
		assertTrue(contain("collection")) ;
		assertTrue(contain("set")) ;
		assertTrue(contain("list")) ;
		assertTrue(contain("map")) ;
	}
	
	@Test
	public void testIfCanReplaceInterfaceWithImpl() {
		assertTrue(contain("Comparable")) ;
	}
	
	@Test
	public void testmore() {
		assertTrue(contain("Appendable")) ;
	}
}
