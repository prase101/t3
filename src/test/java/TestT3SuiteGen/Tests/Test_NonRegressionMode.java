package TestT3SuiteGen.Tests;

import static org.junit.Assert.*;

import org.junit.*;

import TestT3SuiteGen.TestClasses.* ;
import TestT3SuiteGen.* ;
import Sequenic.T3.* ;
import Sequenic.T3.Sequence.Datatype.*;

public class Test_NonRegressionMode {
	
	public static class A {	
		public void wrong() { throw new Error() ; }
	}
	
	@Test
	public void test_find_error() throws Exception {
		Config config = new Config() ;
		config.CUT = A.class ;
		config.maxPrefixLength = 1 ;
		config.regressionMode = false ;
		config.replayRunAll = false ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(config) ;
	    SUITE S = t3.suite(true) ;
	    Throwable e = t3.replay(S).firstViolation ;
	    System.out.println(">> test_find_error: " + e) ;
	    assertTrue (e instanceof Error) ;
	}
	
	public static class B1 {
		public void f(int x) {
			System.out.println(">> f: " + x) ;
			if (x <= 9) throw new IllegalArgumentException() ;
			assert x > 9 ;
		}
		
		public void g(int x) {
			System.out.println(">> g: " + x) ;
			assert x > 9 : "PRE" ;
			assert x > 9 ;
		}
	}
	
	@Test
	public void test_precond_filtering() throws Exception {
		Config config = new Config() ;
		config.CUT = B1.class ;
		config.maxPrefixLength = 3 ;
		config.regressionMode = false ;
		config.replayRunAll = true ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(config) ;
	    SUITE S = t3.suite(true) ;
	    SUITE_RT_info r =  t3.replay(S) ;
	    r.printReport(System.out) ;
	    assertTrue(r.violating.size() == 0) ;   
	}
	
	public static class B2 {

		public void h(int x) {
			System.out.println(">> h: " + x) ;
			assert x > 9 : "PRE" ;
			assert x > 50 ;
		}
	}
	
	@Test
	public void test_precond_filtering2() throws Exception {
		Config config = new Config() ;
		config.CUT = B2.class ;
		config.maxPrefixLength = 3 ;
		config.regressionMode = false ;
		config.replayRunAll = true ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(config) ;
	    SUITE S = t3.suite(true) ;
	    SUITE_RT_info r =  t3.replay(S) ;
	    r.printReport(System.out) ;
	    assertTrue(r.violating.size() > 0) ;
	    assertTrue(r.invalid.size() == 0) ;    
	}
	
	
	public static class I {
		private int x=0 ;
		public void inc() { x++ ; }
		private boolean classinv__() { return x<=10 ; }
	}
	
	@Test
	public void test_precond_classinv() throws Exception {
		Config config = new Config() ;
		config.CUT = I.class ;
		config.maxPrefixLength = 15 ;
		config.regressionMode = false ;
		config.replayRunAll = true ;
		T3SuiteGenAPI t3 = new T3SuiteGenAPI(config) ;
	    SUITE S = t3.suite(true) ;
	    SUITE_RT_info r =  t3.replay(ClassLoader.getSystemClassLoader(),S) ;
	    System.out.println(">> test_precond_classinv: " + r.firstViolation) ;
	    assertTrue(r.firstViolation instanceof ClassInvariantError) ;
	    assertTrue(r.violating.size() > 0) ;   
	}

}
