package Sequenic.T3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.*;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

/**
 * Utility to scan JVM classpaths to collect all class names under a given package, along with 
 * their inheritence relations, then save the info to a ":" separated text file.
 */
public class cpscannerCmd {
	
	static private void save(String fname, String content) throws Exception {
    	File file = new File(fname);   	 
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(content);
		bw.close();
    }
	
	/**
	 * List all classes under (recursive) the given package. For each, we also list all its subclasses
	 * found under the package. Each class is also listed with its modifier. Then, the whole information
	 * is saved to a file.
	 */
	public static String MkInheritenceMap(String pckgName, String savefile) throws Exception {
		
		Reflections reflections = new Reflections(pckgName, new SubTypesScanner(false));

		Set<Class<? extends Object>> allclasses = reflections.getSubTypesOf(Object.class) ;
		
		Map inheritence = new HashMap<Class,List<Class>>() ;
		String z = "" ;
		int k = 0 ;
		for (Class C : allclasses) {
			List<Class> subclasses = new LinkedList<Class>() ;
			if (k>0) z += "\n" ;
			// System.out.print("" + C + ":" + C.getModifiers()) ;
			z += C.getName()  ;
			inheritence.put(C, subclasses) ;
			for (Class D : allclasses) {
				if (C.isAssignableFrom(D)) { 
					//System.out.print(":" + D ) ;
					z += ":" + D.getName() ;
					subclasses.add(D) ;
				}
			}
			//System.out.println("") ;
			k++ ;
		}
		save(savefile,z) ;
		System.err.println("** Saving imap: " + savefile);
		return z ;
	}

	static private String getPakageName(String classname) {
		int k = classname.indexOf('$') ;
		if (k>=0) classname = classname.substring(0,k) ;
		k = classname.lastIndexOf('.') ;
		if (k<0) return "" ;
		return classname.substring(0,k) ;
	}
	
	static private String getAncestorPckg(String pckgName, int distance) {
		if (distance <= 0) return pckgName ;
		int k = pckgName.lastIndexOf('.') ;
		if (k<0) return pckgName ;
		return getAncestorPckg(pckgName.substring(0,k),distance-1) ;
	}
	
	
	/**
	 * arg0: package name, or CUT name if arg3 exists
	 * arg1: name of the text-file to save the inheritence-info
	 * arg3: optional, is an integer k. Class will be scanned from the ancestor
	 *       package of arg0 (as CUT), with the distance k from the CUT. 
	 */
	private static void mainWorker(String[] args) throws Exception {
		if (args.length < 2) throw new IllegalArgumentException();
		if (args.length == 2) {
			MkInheritenceMap(args[0],args[1]) ;
			return ;
		}
		
		String CUT = args[0] ;
		int k = Integer.parseInt(args[2]) ;
		while (k>=0) {
			try {
			  String pckgroot = getAncestorPckg(getPakageName(CUT),k) ;
			  MkInheritenceMap(pckgroot,args[1]) ;	
			  System.err.println("** Extracting imap.txt was successful, with root " + pckgroot);
			  return ;
			}
			catch(Throwable t) { 
				t.printStackTrace(System.err);
				k-- ; 
			}
		}
		throw new IOException("Somehow fails...") ;
	}
	
	public static void main(String[] args) throws Exception {
		mainWorker(args) ;
		//MkInheritenceMap("com.google.common.base","/Users/iswbprasetya/tmp/t3/imap_.txt") ;		
	}

}
