package Sequenic.T3.Daikon;

import java.util.*;

/** 
 * A representation of a sample, of a program point.
 */
public class Sample {
    String programPointName ;
    int nonce ;
    String pptype ;  // currently just entry or exit
    List<Value> values ;
    
    public Sample(String ppname, boolean entry) {
    	programPointName = ppname ;
    	if (entry) pptype = "enter" ; else pptype = "exit" ;
    	values = new LinkedList<Value>() ;
    }
    
    public Value getVar(String varname) {
		for (Value x : values) {
			if (x.varname.equals(varname)) return x ;
		}
		return null ;
	}
    
    public String toString(){
    	StringBuffer buf = new StringBuffer() ;
    	buf.append(programPointName) ;
    	buf.append("\nthis_invocation_nonce") ;
    	buf.append("\n" + nonce) ;
    	for (Value x : values) {
    		buf.append("\n" + x) ;
    	}
    	return buf.toString() ;
    }
}
