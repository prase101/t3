package Sequenic.T3.Daikon;

import java.io.PrintStream;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A list of samples
 */
public class SampleList {
	
	List<Sample> samples ;
	
	public SampleList(List<Sample> samples) { this.samples = samples ; }
	
	private List<String> getAllPPnames() {
		return samples.stream().map(s -> s.programPointName).distinct().collect(Collectors.toList()) ;
	}
	
	// return the all samples that belong to the same program points
	private List<Sample> getSamplesOfPP(String ppname) {
		return samples.stream().filter(s -> s.programPointName.equals(ppname)).collect(Collectors.toList()) ;
	}
	
	private List<Value> getSamplesOfVar(String ppname, String varname) {
		List<Sample> mysamples = getSamplesOfPP(ppname) ;
		return mysamples.stream().map(s -> s.getVar(varname)).filter(v -> v != null).collect(Collectors.toList()) ;
	}
	
	// Get all var-names that occur from given samples of the same program point.
	// Careful with the order, as this will be reflected in the order they will later be
	// declared in dtrace file.
	static private List<String> getVarNames(List<Sample> samples) {
		List<String> names = new LinkedList<String>() ;
		for (Sample s : samples) {
			for (Value x : s.values) {
				if (names.contains(x.varname)) continue ;
				names.add(x.varname) ;
			}
		}
		return names ;
	}
	
	// check if C2 is a more general type of C1
	static private Class refineType(Class old, Class newlyseen) {
		if (typeIsUnknown(old)) return newlyseen ;
		if (typeIsUnknown(newlyseen)) return old ;
		if (old.equals(newlyseen)) return old ;
		if (isNumeric(old) && isNumeric(newlyseen)) {
			String old_ = Value.getDaikonType(old) ;
			String newlyseen_ = Value.getDaikonType(newlyseen) ;
			if (old_.equals(Value.DOUBLE) || newlyseen_.equals(Value.DOUBLE)) return Double.class ;
			//System.err.println(">>> old: " + old.getName() + ", newly seen: " + newlyseen.getName()) ;
			return Integer.class ;
		}
		if (isStringlike(old) && isStringlike(newlyseen)) return String.class ;
		if (old.isAssignableFrom(newlyseen)) return old ;
		if (newlyseen.isAssignableFrom(old)) return newlyseen;
		return Object.class ;
	}
	
	static private boolean typeIsUnknown(Class ty) { return ty.equals(Value.UNKNOWNTYPE) ; }
	static private boolean isNumeric(Class ty) {
		String ty_ = Value.getDaikonType(ty) ;
		return ty_.equals(Value.INT) || ty_.equals(Value.DOUBLE) ;
	}
	
	static private boolean isStringlike(Class ty) {
		String ty_ = Value.getDaikonType(ty) ;
		return ty_.equals(Value.STRING) ;
	}
	
	// decide the types of each variable in a set of samples 
	static private HashMap<String,Class> getVarTypes(List<Sample> samples) {
		//List<String> names = getVarNames(samples) ;
		HashMap<String,Class> tymap = new HashMap<String,Class>() ;
		for (Sample s : samples) {
			for (Value x : s.values) {
				Class C = tymap.get(x.varname) ;
				if (C==null) tymap.put(x.varname, x.type) ;
				else tymap.put(x.varname, refineType(C,x.type)) ;
			}
		}
		//for (Map.Entry<String,Class> e : tymap.entrySet()) {
		//	System.err.println("## " + e.getKey() + " : " + e.getValue()) ;
		//}
		return tymap ;
	}
	
	static private <T> T find(Collection<T> S, Predicate<T> p) {
		for (T x : S) {
			if (p.test(x)) return x ;
		}
		return null ;
	}
	
	/**
	 * Fix the samples:
	 *    (1)  so that for every program point, the variables are listed in the same order.
	 *         Artificial non-sensical-entries are introduced for variables (of a program point) 
	 *         that do not occur in all samples (of the program point).
	 *    (2) make the types of samples of the same variable the same
	 *    (3)  array-like variables, get suffix "[]" in their names
	 *    (4) because some value-types cannot for example be null, we need to normalize those: 
	 *           - array-content cannot be null; for not null-array will be turned to an empty array
	 *           - string-content cannot be null; so this will be turned to ""
	 *           - primitive values cannot be null; this is converted to nonsensical
	 */
	public void fix() {
		List<String> pps = getAllPPnames() ;
		for (String pp : pps) {
			List<Sample> z = getSamplesOfPP(pp) ;
			List<String> names = getVarNames(z) ;
			HashMap<String,Class> tymap = getVarTypes(z) ;
			for (Sample s :z) {
				List<Value> fixedvalues = new LinkedList<Value>() ;
				for (String xname : names) {
					Value y = find(s.values, v -> v.varname.equals(xname)) ;
					if (y == null) {
						y = Value.mk_nonsensical(xname) ;
						y.type = tymap.get(xname) ;
					}
					fixedvalues.add(y) ;
				} 
				// fix types, names, and null array:
				for (Value y : fixedvalues) {
					y.type = tymap.get(y.varname)  ;
					String ty_ = Value.getDaikonType(y.type) ;
					if (Value.isArrayLike(y.type) && !y.varname.endsWith("[]")) y.varname += "[]" ;				
					if ((y.value== null || y.value.equals(Value.NONSENSICAL))) {
						if (Value.isArrayLike(y.type)) y.value = "[]" ;
						else if (ty_.equals(Value.STRING)) y.value = Value.daikonizeString("") ;
						else if (ty_.equals(Value.BOOL)|| ty_.equals(Value.INT) || ty_.equals(Value.DOUBLE)) y.value = "0" ;
					}
					if (ty_.equals(Value.HASH)) {
						try { Integer.parseInt(y.value) ; }
						catch (Throwable t) { y.value = "" + (new Object()).hashCode() ; }
					}
					    
				}
				s.values = fixedvalues ;
			}
		}
	}
	
	public List<ProgramPointDecl> getPPdecls() {
		List<ProgramPointDecl> ppdecls = new LinkedList<ProgramPointDecl>() ;
		List<String> pps = getAllPPnames() ;
		for (String ppname : pps) {
			Sample pp1 = find(samples, s -> s.programPointName.equals(ppname)) ;
			ProgramPointDecl ppdecl = new ProgramPointDecl(ppname,pp1.pptype) ;
			ppdecls.add(ppdecl) ;
			List<Sample> z = getSamplesOfPP(ppname) ;
			List<String> names = getVarNames(z) ;
			HashMap<String,Class> tymap = getVarTypes(z) ;
			for (String xname : names) {
				ProgramPointDecl.VariableDecl x = new ProgramPointDecl.VariableDecl() ;
				x.name = xname ;
				Class C = tymap.get(xname) ;
				// for now we just map all variables to "kind" variable, except array-like
				// which are mapped to "array"-kind.
				if (Value.isArrayLike(C)) x.kind = "array" ; else x.kind = "variable" ;  
				x.declType = C.getName() ;
				x.daikonType = Value.getDaikonType(C) ;
				x.arraydim = null ;
				if (Value.isArrayLike(C)) x.arraydim = 1 ;
				x.comparability = Value.getDaikonComparability(C) ;
				ppdecl.vardecls.add(x) ;
			}
		}
		return ppdecls ;
	}
	
	public boolean checkConsistency() {
		List<ProgramPointDecl> ppdecls =  getPPdecls() ;
		for (Sample s : samples) {
			ProgramPointDecl pp = find(ppdecls, p -> p.name.equals(s.programPointName)) ;
			for (Value x : s.values) {
				ProgramPointDecl.VariableDecl xdecl = find(pp.vardecls, vd -> vd.name.equals(x.varname)) ;
				if (!Value.getDaikonType(x.type).equals(xdecl.daikonType)) {
					printInconsistency(pp,s,x) ;
					return false ;
				}
				try {
				  if (xdecl.daikonType.equals(Value.INT)) Integer.parseInt(x.value) ;
				  if (xdecl.daikonType.equals(Value.DOUBLE)) Double.parseDouble(x.value) ;
				}
				catch(Throwable t) { 
					printInconsistency(pp,s,x) ;
					return false ;
				}
			}
		}
		return true ;
	}
	
	static void printInconsistency(ProgramPointDecl pp, Sample s , Value v) {
		System.err.println("** Inconsistency in the samples detected. Declaration: ") ;
		System.err.println(pp.toString()) ;
		System.err.println("But found this value: " + v.varname 
				+ "(" + v.type + "/" + v.getDaikonType() + ") = " 
				+ v.value) ;
		System.err.println("in this sample:") ;
		System.err.println(s.toString()) ;
	}
	
	public void writeAsDtrace(PrintStream out) {
		fix() ;
		if (!checkConsistency()) throw new Error("** The gathered samples are unconsistent!") ;
		List<ProgramPointDecl> ppdecls =  getPPdecls() ;
		int k = 0 ;
		out.print("decl-version 2.0\n\n");
		for (ProgramPointDecl pp : ppdecls) {
			if (k>0) out.print("\n\n");
			out.print(pp.toString());
			k++ ;
		}
		out.print("\n\n// \n") ;
		out.print("// ==================== start of samples \n");
		out.print("//") ;
		for (Sample s : samples) out.print("\n\n" + s);			
	}
	
	static public void main(String[] args) {
		Integer.parseInt("-532.948884803782") ;
	}

}
