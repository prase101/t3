package Sequenic.T3.Daikon;

import java.io.*;

public class StringSystemOut {
	
	PrintStream origSystemOut ;
	ByteArrayOutputStream bp ;
	PrintStream p ;
	
	public StringSystemOut() {
		origSystemOut = System.out ;
		bp = new ByteArrayOutputStream() ;
		p = new PrintStream(bp) ;
	}
	
	public void reroute() { System.setOut(p); }
	
	public void restore() { 
		p.close();
		System.setOut(origSystemOut); 
	}
	
	public String toString() {
		p.flush(); 
		return bp.toString() ;
	}

}
