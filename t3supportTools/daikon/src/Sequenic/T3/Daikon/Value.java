package Sequenic.T3.Daikon;

import java.lang.reflect.Array;
import java.util.*;

import Sequenic.T3.Reflection.*;

/**
 * A representation of (variable-name, value) pair.
 */
public class Value {
	
	String varname ;
	Class  type ;
	String value ;
	
	public String toString() {
		String s = varname ;
		s +=    "\n" + value 
		        // + "\n" + type  + " -> " + getDaikonType() // comment this out for actual use!
		        + "\n0"      // setting modified bit to always 0 (unmodified), which is safe
		        ;
		return s; 
	}
	
	static String NONSENSICAL = "nonsensical" ;
	static String NULL = "null" ;
	static String BOOL = "boolean" ;
	static String INT = "int" ;
	static String DOUBLE = "double" ;
	static String HASH = "hashcode" ;
	static String STRING = "java.lang.String" ;
	
	static public class UnknownType { }
	
	static Class UNKNOWNTYPE = UnknownType.class ;

	private Value(String varname) {	
		this.varname = varname ;
		type = UNKNOWNTYPE ;
		value = NONSENSICAL ;
	}
	
	public Value(String varname, Object val) {
		this.varname = varname ;
		if (val == null) {
			this.type = UNKNOWNTYPE ; // type is unknown
			value = null ; 
			return ;
		}
		
		type = val.getClass() ;
		if (Reflection.isPrimitiveLike(type) || type == String.class) {
			value = singleObjToString(type,val) ;
			/* debug:
			 if (type == Integer.class) {		 
				try { Integer.parseInt(value) ; }
				catch (Throwable t) { throw new Error() ; }
			}
			*/
			return ;
		}
		if (type.isArray()) {
			value = arrayToString(val) ;
			return ;
		}
		if (Collection.class.isAssignableFrom(type)) {
			value =  arrayToString(((Collection) val).toArray()) ;
			return ;
		}
		// else... its an object of other types, we'll just record information if it is null or not
		value = singleObjToString(type,val) ;
	}
	
	public static Value mk_nonsensical(String varname) {
		return new Value(varname) ;
	}
	
	static public String daikonizeString(String s) {
		s = s.replace("\\", "\\\\") ;
		s = s.replace("\"", "\\\"") ;
		s = s.replace("\n","\\n") ;	
		return "\"" +  s + "\"" ;
	}
	
	static private String singleObjToString(Class C, Object x) {
		if (Reflection.isPrimitiveLike(C)) {
			if (x==null) return NONSENSICAL ;
			else return x.toString() ;
		}
		if (C == String.class) {
			if (x==null) return NONSENSICAL ;
			else return daikonizeString(x.toString()) ;
		}
		if (x == null) return NULL ;
		return Integer.toString(x.hashCode()) ;
	}
	
	static private String arrayToString(Object a) {
		Class elemtype = a.getClass().getComponentType() ;
		StringBuffer buf = new StringBuffer() ;
		buf.append("[") ;
		int N = Array.getLength(a) ;
		for (int k = 0 ; k<N ; k++) {
			Object x = Array.get(a,k) ;
			if (k>0) buf.append(" ") ;
			buf.append(singleObjToString(elemtype,x)) ;
		}
		buf.append("]") ;
		return buf.toString() ;
	}
		
	static public String getDaikonType(Class type) {
		if (type==null) return null ;
		if (type.isArray()) {
			return getDaikonType_(type.getComponentType()) + "[]" ;
		}
		if (Collection.class.isAssignableFrom(type)) {
			return getDaikonType_(Object.class) + "[]" ;
		}
		return getDaikonType_(type) ;
	}
	
	static public boolean isArrayLike(Class type) {
		String daikonty = getDaikonType(type);
		return daikonty.endsWith("[]") ;
	}
	
	static public boolean isPrimitiveLine(Class type) {
		return Reflection.isPrimitiveLike(type) ;
	}
	
	static public String getDaikonComparability(Class type) {
		if (type == null) return null ;
		String daikonty = getDaikonType(type) ;
		if (daikonty.equals(BOOL)) return "0" ;
		if (daikonty.equals(INT)) return "1" ;
		if (daikonty.equals(DOUBLE)) return "2" ;
		if (daikonty.equals(STRING)) return "3" ;
		if (daikonty.equals(HASH)) return "4" ;
		if (daikonty.endsWith("[]")) {
			daikonty = daikonty.substring(0, daikonty.length() - 2) ;
			if (daikonty.equals(BOOL)) return "10[0]" ;
			if (daikonty.equals(INT)) return "11[1]" ;
			if (daikonty.equals(DOUBLE)) return "12[2]" ;
			if (daikonty.equals(STRING)) return "13[3]" ;
			if (daikonty.equals(HASH)) return "14[4]" ;
		}
		return null ;	
	}
	
	public String getDaikonType() { return getDaikonType(type) ; }
 	
	private static String getDaikonType_ (Class C) {
		if (C == Character.class || C == Character.TYPE) return STRING ;
		if (C == Boolean.class || C == Boolean.TYPE) return BOOL ;
		if (C == Byte.class || C == Byte.TYPE) return INT ;
		if (C == Short.class || C == Short.TYPE) return INT ;
		if (C == Integer.class || C == Integer.TYPE) return INT ;
		if (C == Long.class || C == Long.TYPE) return INT ;
		if (C == Float.class || C == Float.TYPE) return DOUBLE ;
		if (C == Double.class || C == Double.TYPE) return DOUBLE ;
		if (C == String.class) return STRING ;
		return HASH ;
	}
	
	
	public static void main(String[] args) {
		// tests
		System.out.println(new Value("x",(int) 99) .toString() ) ;
		System.out.println(new Value("x",(Integer) 99) .toString() ) ;
		System.out.println(new Value("x","a\\b\nc") .toString() ) ;
		System.out.println(new Value("x",new Object()) .toString() ) ;
		int[] a = {0,1,2} ;
		Integer[] b = {0,null,2} ;
		System.out.println(new Value("a",a) .toString() ) ;
		System.out.println(new Value("b",b) .toString() ) ;
		List<Integer> s = new LinkedList<Integer>() ;
		s.add(99) ; s.add(3) ; s.add(null) ;
		System.out.println(new Value("s",s) .toString() ) ;
		System.out.println(new Value("x",null) .toString() ) ;
	}

}
