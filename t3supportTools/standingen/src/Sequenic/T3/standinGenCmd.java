/*
 * Copyright 2015 Wishnu Prasetya.
 *
 * This file is part of T3.
 * T3 is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License (GPL) as published by the
 * Free Software Foundation; either version 3 of the License, or any
 * later version.
 * 
 * T3 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * A copy of the GNU General Public License can be found in T3 distribution.
 * If it is missing, see http://www.gnu.org/licenses.
 */
package Sequenic.T3;

import java.io.*;
import java.lang.reflect.* ;
import java.nio.file.*;

/**
 * Used to generate a stand-in class, which is used to test an abstract class.
 * An abstract class cannot be instantiated through its constructor, even if a
 * concrete constructor is provided. T3 can still instantiate it if it has a
 * creation method, else there is no way T3 can create an instance.
 * 
 * A stand-in is a concrete class that simply extends an abstract class; so
 * instances of the abstract class can be created through its stand-in.
 * 
 * There are some TODOs left... see the code.
 */
public class standinGenCmd {
	
	
	static boolean debug = false ;
	
	static boolean isDefault(int modifier) {
		if (Modifier.isPublic(modifier) || Modifier.isPrivate(modifier) || Modifier.isProtected(modifier))
			return false ;
		else 
			return true ;
	}
	static String mkStandInCode(String standinName, Class abstractClass) throws Exception {
		
		// we will for not not generate the stand-in in the same package:
		String s = "// package " + abstractClass.getPackage().getName() + "\n" ;
		
		
		TypeVariable[] tyvars = abstractClass.getTypeParameters() ;
		String tyvars_ = "" ;
		if (tyvars != null && tyvars.length > 0) {
			tyvars_ += "<" ;
			for (int k=0; k<tyvars.length ; k++) {
				if(k>0) tyvars_ += "," ;
				tyvars_ += tyvars[k].toString() ;
			}
			tyvars_ += ">" ;
		}
		
		s += "// a stand-in class for " +  abstractClass + "\n";
		s += "public class " + standinName + tyvars_ 
				+ " extends " + abstractClass.getName() + tyvars_
				+ " {\n" ;
		
		// generate code for constructors
		Constructor[] cons = abstractClass.getDeclaredConstructors() ;
		if (cons.length > 0) s += "    // constructors:\n" ; 
		for (Constructor co : cons) {
			int flag = co.getModifiers() ;
			// for now ignoring abstract constructors:
			if (isDefault(flag)) continue ;
			
			if (Modifier.isPublic(flag) || Modifier.isProtected(flag)) {
				
				String modifier = "public" ;
				if ( Modifier.isProtected(flag)) modifier = "protected" ;
				
				// generating the header
				Type[] tys = co.getGenericParameterTypes() ;
				s += "    " + modifier + " " + standinName + "(" ;
				for (int k=0; k<tys.length; k++) {
					if (k>0) s += ", " ;
					s += tys[k].getTypeName() + " x" + k ;
				}
				s += ")" ;
				// exception list:
				Class[] excs = co.getExceptionTypes() ;	
				if (excs!=null && excs.length>0) {
					s += " throws " ;
					int k=0 ;
					for (Class e : excs) {
						if (k>0) s += ", " ;
						s += e.getName() ;
						k++;
					}
				}	
				s += " {" ;
				// the body:
				if (Modifier.isAbstract(flag)) {
					s += " throw new UnsupportedOperationException() ; " ;
				}
				else {
					s += " super(" ;
					for (int k=0; k<tys.length;k++) {
						if (k>0) s += ", " ;
						s += "x" + k ;
					}
					s += ") ;" ;
				}

				s += "}\n" ;
			}
		}
		
		
		// generate code for the methods...
		// still not complete if the class has an abstract inherited method; TODO!
		Method[] methods = abstractClass.getDeclaredMethods() ;
		if (methods.length > 0) s += "    // methods:\n" ; 
		for (Method m : methods) {
			int flag = m.getModifiers() ;
			// for now ignoring abstract constructors:
			if (isDefault(flag)) continue ;
						
			// we won't create a wrapper for static methods; G2 will target them directly anyway
			if (Modifier.isStatic(flag)) continue ;
			
			// currently ignoring default method, which require this class to be in the same package; TODO!
			if (Modifier.isPublic(flag) || Modifier.isProtected(flag)) {
			
				// generate the header:
				String modifier = "public" ;
				if ( Modifier.isProtected(flag)) modifier = "protected" ;				
				s += "    " + modifier ;
				Type[] tys   = m.getGenericParameterTypes() ;
				Type retty   = m.getGenericReturnType() ;
				s += " " + retty.getTypeName() + " " + m.getName() + "(" ;
				for (int k=0; k<tys.length; k++) {
					if (k>0) s += ", " ;
					s += tys[k].getTypeName() + " x" + k ;
				}
				s += ")" ;
				// exception list:
				Class[] excs = m.getExceptionTypes() ;	
				if (excs!=null && excs.length>0) {
					s += " throws " ;
					int k=0 ;
					for (Class e : excs) {
						if (k>0) s += ", " ;
						s += e.getName() ;
						k++;
					}
				}
				s += " { " ;
				if (Modifier.isAbstract(flag)) {
					s += " throw new UnsupportedOperationException() ; " ;
				}
				else {
					s += " return super." + m.getName() + "(" ;
					for (int k=0; k<tys.length;k++) {
						if (k>0) s += ", " ;
						s += "x" + k ;
					}
					s += ") ;" ;
				}
				s += " }\n" ;
			}
		}		
		s += "}" ;
		return s ;
	}
	
	
	/**
	 * Save the content in the file.
	 */
    static private void save(String filename, String content) throws IOException  {
    	Path path = Paths.get(filename);
        Files.write(path, content.getBytes());    	
    }
	

	public static void generateStandIn(String absclassName, String srcpath, String compileCmd) throws Exception {
		//String absclassName = "com.puppycrawl.tools.checkstyle.api.AbstractLoader" ;
		Class absclass = Class.forName(absclassName) ;
		
		if (! Modifier.isAbstract(absclass.getModifiers())) {
			// if it is not abstract, there is no need to create a stand-in
			return ;
		}
	 	
		String standInName =  absclass.getSimpleName() + "_StandIn" ;
		//String srcpath = "./src" ;
		String filename  = srcpath + "/" + standInName + ".java" ;
    	
    	String code = mkStandInCode(standInName,absclass) ;    	
    	if (debug) System.err.println(code);
    	
    	// if (file.exists()) return ; well just overwrite 
    	
		save(filename,code) ;
		System.err.println("** a stand-in " + filename + " is created.") ;
		
		if(compileCmd == null) return ;
		
		compileCmd += " " + filename ;
		System.err.println("** about to execute: " + compileCmd) ;
		Process p = Runtime.getRuntime().exec(compileCmd);
		p.waitFor();

		// echo msg we get from the javac:
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		String line = "";			
		while ((line = reader.readLine())!= null) {
			System.err.println(">> " + line) ; 
		}
		
		System.err.println("** a stand-in " + filename + " is compiled.") ;
				
	}
	
	/**
	 * arg[0] : the full name of the abstract class whose stand-in must be created
	 * arg[1] : the path to the src directory to put the stand-in
	 * arg[2] : javac command to compile, e.g. javac -cp bla
	 */
	public static void main(String[] args) throws Exception {
		if (args.length == 2) 
			generateStandIn(args[0],args[1],null);
		else
			generateStandIn(args[0],args[1],args[2]) ;	
		/*
		generateStandIn(
				"Sequenic.T3.Examples.InterfaceAndAbstract.Vehicle",
				"/Users/iswbprasetya/tmp/t3/standins",
				"javac -cp ./bin:../t3/bin") ;
		*/
	}

}
