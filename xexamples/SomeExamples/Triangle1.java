package SomeExamples;


public class Triangle1 {
	
	private float x = 0 ;
	private float y = 0 ;
	private float z = 0 ;

	public Triangle1(){ 
		//System.out.println("    reating a triangle");
	}
	public void setX(float x) {
		if (x<=0) throw new IllegalArgumentException() ;
		this.x = x ;
		//System.out.println("    SetX");
	}
	public void setY(float y) {
		if (y<=0) throw new IllegalArgumentException() ;
		this.y = y ;
		//System.out.println("    SetY");
	}
	public void setZ(float z) {
		if (z<=0) throw new IllegalArgumentException() ;
		this.z = z ;
		//System.out.println("    SetZ");
	}
	public boolean isTriangle() {
		if (x >= y+z) return false ;
		if (y >= x+z) return false ;
		return z < x+y ;
	}
	public boolean isIsoleces() {
		if (x==y) return true ;
		if (y==z) return true ;
		if (x==z) return true ;
		return false ;
	}
	public boolean isEquilateral() {
		return (x==y) && (y==z) ;
	}
	public boolean isScalene() {
		return ! isIsoleces() ;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Triangle1) {
			//if (o != this) System.out.println(">>> o is NOT this!") ;
			//else System.out.println(">>> o is this!") ;
			Triangle1 t = (Triangle1) o ;
			if (x == t.x && y == t.y && z == t.z) 
				 return true ;
			else return false ;
		}
		return false ;
	}
}
